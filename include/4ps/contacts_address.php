<span class="contacts__subtitle">Юридический адрес:</span> <span class="contacts__text">105082, Россия, Москва, переулок Спартаковский д.2, строение 1, помещение III, комната 2А</span>
<div class="contacts__block">
 <span class="contacts__subtitle">Фактический адрес:</span> <span class="contacts__text">127247, Россия, г. Москва, Дмитровское ш.</span> <span class="contacts__text">100/2 БЦ Норд Хаус, оф. 2112</span>
</div>
<div class="contacts__block">
 <span class="contacts__subtitle">Телефоны:</span> <span class="contacts__text contacts__text--link"> <a href="tel:+74996494984" class="contacts__text-link">
	+7 499 649 49 84 </a> </span>
	<!--
	<span class="contacts__text contacts__text--link">
		<a href="#" class="contacts__text-link">+7-925-411-50-47</a>
	</span>
	-->
</div>
<div class="contacts__block">
 <span class="contacts__subtitle">E-mail:</span> <span class="contacts__text"> <a href="mailto:info@4prosport.ru" class="contacts__text-link">
	info@4prosport.ru </a> </span>
</div>
<br>
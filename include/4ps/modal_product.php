<style type="text/css">
	/* The modal-product (background) */
.modal-product {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 30; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;

    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* modal-product Content */
.modal-product-content-product {
    background-color: #fefefe!important;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    max-width: 320px;
}


/* The close-product Button */
.close-product {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
    position: absolute;
    margin-top: -23px;
}

.close-product:hover,
.close-product:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
body .hide_header {
	z-index: 0;
}
#myFormProd .contacts-form__label {
	text-align: left;
}
textarea.input--medium {
	height: auto;
	padding-top: 10px;
    padding-bottom: 10px;
    margin-top: 8px;
}
#content_prod_modal {
    text-align: center;
    margin: 10px 0;
    font-size: 1.1em;
    font-weight: 600;
}
.prod_id_hide {
	display: none;
}
</style>
<!-- The modal-product -->
<div id="mymodal-product" class="modal-product">
	<span class="prod_id_hide"></span>
	<!-- modal-product content -->
	<div class="modal-product-content-product">
		<span class="close-product">&times;</span>
		<form id="myFormProd">
			<div class="label contacts-form__label">
				<label for="contacts_form_price">Имя:</label>
			</div>
			<input id="name_prod" name="name" type="text" class="input input--medium" placeholder="Иванов Иван" size="20" required><br/>

			<div class="label contacts-form__label">
				<label for="contacts_form_price">Email:</label>
			</div>
			<input id="email_prod" name="email" type="text" class="input input--medium" placeholder="____@__.__" size="20" required><br/>

			<div class="label contacts-form__label">
				<label for="contacts_form_price">Телефон:</label>
			</div>
			<input id="phone_prod" name="phone" type="text" class="input input--medium" placeholder="8(___)-___-__-__" size="20" required><br/>

			<textarea id="comment_prod" name="comment" class="input input--medium" placeholder="Комментарий к запросу" rows="6"></textarea><br/>

			<input class="btn btn-red btn--bubble-left contacts-for__btn" type="submit" value="Отправить">
		</form>
		<div id="content_prod_modal"></div>
	</div>
</div>
<script>
// Get the modal-product
var modalproduct = document.getElementById('mymodal-product');

// Get the <span> element that close-products the modal-product
var span = document.getElementsByClassName("close-product")[0];
// When the user clicks the button, open the modal-product


// When the user clicks on <span> (x), close-product the modal-product
span.onclick = function() {
    modalproduct.style.display = "none";
    $(".header").removeClass("hide_header");
    $(".bx-filter .bx-filter-parameters-box").removeClass("hide_header");

}
// When the user clicks anywhere outside of the modal-product, close-product it
window.onclick = function(event) {
    if (event.target == modalproduct) {
        modalproduct.style.display = "none";
        $(".header").removeClass("hide_header");
        $(".bx-filter .bx-filter-parameters-box").removeClass("hide_header");
    }
}
$(document).ready(function(){
	$('.myBtn_product').on('click', function(){
		$("#content_prod_modal").html("");
		modalproduct.style.display = "block";
		$(".header").addClass("hide_header");
		$(".bx-filter .bx-filter-parameters-box").addClass("hide_header");
		$(".prod_id_hide").text($(this).attr("prod"));
	});

	$('#myFormProd').submit(function(){
		var prod_id = $('#mymodal-product .prod_id_hide').text();
		$.ajax({
			type: "POST",
			url: "/ajax/product.php",
			data: "username="+$("#name_prod").val()+"&phone="+$("#phone_prod").val()+"&email="+$("#email_prod").val()+"&comment="+$("#comment_prod").val() +"&prod_id="+prod_id,
			success: function(html){
				$("#content_prod_modal").html(html);
		   }
		});
		return false;
	});

});
</script>
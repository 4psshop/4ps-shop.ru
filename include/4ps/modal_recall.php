<style type="text/css">
	/* The modal-recall (background) */
.modal-recall {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 30; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;

    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
/* modal-recall Content */
.modal-recall-content-recall {
    background-color: #fefefe!important;
    margin: auto;
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    max-width: 320px;
}


/* The close-recall Button */
.close-recall {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close-recall:hover,
.close-recall:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>		            	            
<!-- The modal-recall -->
<div id="mymodal-recall" class="modal-recall">
	<!-- modal-recall content -->
	<div class="modal-recall-content-recall">
		<span class="close-recall">&times;</span>
		<form id="myForm">
			<div class="label contacts-form__label">
				<label for="contacts_form_price">Имя:</label>
			</div>
			<input id="username" type="text" class="input input--medium" placeholder="Иванов Иван" size="20" required><br/><br/>
			<div class="label contacts-form__label">
				<label for="contacts_form_price">Телефон:</label>
			</div>
			<input id="username1" type="text" class="input input--medium" placeholder="8(___)-___-__-__" size="20" required><br/><br/>	
			<input class="btn btn-red btn--bubble-left contacts-for__btn" type="submit" value="Отправить">
		</form>
		<div id="content"></div>
	</div>
</div>
<script>
// Get the modal-recall
var modalrecall = document.getElementById('mymodal-recall');
// Get the button that opens the modal-recall
var btn = document.getElementById("myBtn");
// Get the <span> element that close-recalls the modal-recall
var span = document.getElementsByClassName("close-recall")[0];
// When the user clicks the button, open the modal-recall 
btn.onclick = function() {
    modalrecall.style.display = "block";
}
// When the user clicks on <span> (x), close-recall the modal-recall
span.onclick = function() {
    modalrecall.style.display = "none";
}
// When the user clicks anywhere outside of the modal-recall, close-recall it
window.onclick = function(event) {
    if (event.target == modalrecall) {
        modalrecall.style.display = "none";
    }
}
$(document).ready(function(){
	$('#myForm').submit(function(){
		$.ajax({
			type: "POST",
			url: "/ajax/recall.php",
			data: "username="+$("#username").val()+"&phone="+$("#username1").val(),
			success: function(html){
				$("#content").html(html);
		   }
		});
		return false;
	});
	
});
</script>
<? if (preg_match('/^prochee\//', $arParams['WBL_SECTION_CODE_PATH']) === 1): ?>
    <!-- side certificate -->
    <div class="side-certificate">
        <div class="side-certificate__inner">
            <img width="205" alt="дилер согл.jpg" src="/local/templates/4sport/pic/side-certif/img01.jpg" height="204" title="дилер согл.jpg" align="top" id="myImg" style="width: 241px; height: 187px;">
        </div>
    </div>
    <!-- side certificate end -->
<? endif; ?>

<!-- side usefull information --> <a class="side-usefull-info" href="/poleznaya-informatsiya/">
    <div class="side-usefull-info__inner">
        <div class="side-usefull-info__holder">
 <span class="side-usefull-info__title">Полезная<br>
		 информация</span>
        </div>
    </div>
</a>
<!-- The Modal -->
<div id="myModal" class="modal">
    <span class="close">×</span> <img src="null" class="modal-content" id="img01">
    <div id="caption">
    </div>
</div>
<style>
    #myImg {
        border-radius: 5px;
        cursor: pointer;
        transition: 0.3s;
    }

    #myImg:hover {
        opacity: 0.7;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9); /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
    }

    /* Caption of Modal Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation */
    .modal-content, #caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
        from {
            -webkit-transform: scale(0)
        }
        to {
            -webkit-transform: scale(1)
        }
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }
        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }
</style>
<script>
    // Get the modal
    // var modal = document.getElementById('myModal');

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    // var img = document.getElementById('myImg');
    // var modalImg = document.getElementById("img01");
    // var captionText = document.getElementById("caption");
    // img.onclick = function(){
    //     modal.style.display = "block";
    //     modalImg.src = this.src;
    //     captionText.innerHTML = this.alt;
    // }

    if (document.querySelectorAll('#myImg').length > 0 && document.querySelectorAll('#myModal').length > 0) {
        document.querySelector('#myImg').addEventListener('click', function(e) {
            if (document.querySelectorAll('#myModal').length > 0) {
                document.querySelector('#myModal').style.display = 'block';
            }
            if (document.querySelectorAll('#img01').length > 0) {
                document.querySelector('#img01').src = e.target.getAttribute('src');
            }
            if (document.querySelectorAll('#caption').length > 0) {
                document.querySelector('#caption').innerHTML = e.target.getAttribute('alt');
            }
        });
    }

    // Get the <span> element that closes the modal
    // var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    if (document.querySelectorAll('.close') > 0 && document.querySelectorAll('#myModal').length > 0) {
        document.querySelector('.close').addEventListener('click', function() {
            document.querySelector('#myModal').style.display = 'none';
        });
    }
    // span.onclick = function() {
    //     modal.style.display = "none";
    // }
</script>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, купить");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек");
$APPLICATION->SetPageProperty("title", "Контакты 4 ПРО СПОРТ, способы проезда и обратная связь");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт");
$APPLICATION->SetPageProperty("description", "Контакты, способы проезда и обратная связь с 4 ПРО СПОРТ");
$APPLICATION->SetTitle("Контакты");
?><!-- contacts block -->

<div class="contacts">
	<div class="contacts__holder">
		<div class="contacts__left">
			<div class="contacts__block">
			<!-- Address -->
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/include/4ps/contacts_address.php"
				)
			);?>
		</div>
		<div class="contacts__right">
			<h2 class="contacts__title">Обратная связь</h2>
			 <!-- contacts form -->
			<form action="" method='POST' id="contactForm">
                <?=bitrix_sessid_post();?>
				<div class="contacts-form">
					<div class="row contacts-form__row">
						<div class="col-sm-100 col-md-50 contacts-form__col">
							<div class="form-row contacts-form__row-indent">
								<div class="label contacts-form__label">
									<label for="contacts_form_price">Имя:</label>
								</div>
								<input type="text" name="contacts_form_price" required id="contacts_form_name" class="input input--medium" placeholder="Иванов Иван">
							</div>
						</div>
						<div class="col-sm-100 col-md-50 contacts-form__col">
							<div class="form-row contacts-form__row-indent">
								<div class="label contacts-form__label">
									<label for="contacts_form_email">E-mail:</label>
								</div>
								<input type="email" name="contacts_form_email" required id="contacts_form_email" class="input input--medium" placeholder="hello@4prosport.ru" value="">
							</div>
						</div>
					</div>
					<div class="form-row contacts-form__row-indent">
						<div class="label contacts-form__label">
							<label for="contacts_form_question">Ваш вопрос:</label>
						</div>
						<textarea name="contacts_form_question" id="contacts_form_question" class="input input--medium input--textarea" placeholder="Ваш вопрос"></textarea>
					</div>
					<button type="submit" class="btn btn-red btn--bubble-left contacts-for__btn"> 
						<span class="btn-black__inner"> 
						<span class="btn__icon"> </span> 
						<span class="btn__text btn__text--middle">Задать вопрос</span> 
						</span> 
					</button>
				</div>
				<div id="result"></div>
			</form>
			<?$APPLICATION->IncludeComponent(
			"bitrix:main.include",
				"",
				Array(
				"AREA_FILE_SHOW" => "file",
				"AREA_FILE_SUFFIX" => "inc",
				"COMPOSITE_FRAME_MODE" => "A",
				"COMPOSITE_FRAME_TYPE" => "AUTO",
				"EDIT_TEMPLATE" => "",
				"PATH" => "/include/4ps/contacts_modal.php"
				)
			);?>			
			<script>
			$(document).ready(function(){
				$('#contactForm').submit(function(){

				    $.ajax({
				        url:'/ajax/contacts.php',
				        data:({
                            username: $("#contacts_form_name").val(),
                            email: $("#contacts_form_email").val(),
                            question: $("#contacts_form_question").val(),
                            sessid: $("#contactForm #sessid").val()
				        }),
				        async:false,
				        type:'POST',
				        dataType: 'json',
				        success: function(ResultAjax){
				            console.log(ResultAjax);
                            modal.style.display = "block";

                            if( ResultAjax.STATUS == 'SUCCESS' )
                            {
                                for( i=0; i < document.querySelectorAll('#contactForm input, #contactForm textarea').length; i++ )
                                {
                                    document.querySelectorAll('#contactForm input, #contactForm textarea')[i].value=''
                                }
                            }

				        }
				    });
					return false;
				});

			});
			</script>
			 <!-- contacts form end -->
		</div>
	</div>
	 <!-- Карта яндекса --> 
	 <?$APPLICATION->IncludeComponent(
	"bitrix:map.yandex.view", 
	".default", 
	array(
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"CONTROLS" => array(
			0 => "ZOOM",
			1 => "MINIMAP",
			2 => "TYPECONTROL",
			3 => "SCALELINE",
		),
		"INIT_MAP_TYPE" => "MAP",
		"MAP_DATA" => "a:4:{s:10:\"yandex_lat\";d:55.87530837546294;s:10:\"yandex_lon\";d:37.52259095105803;s:12:\"yandex_scale\";i:13;s:10:\"PLACEMARKS\";a:1:{i:0;a:3:{s:3:\"LON\";d:37.548042904969;s:3:\"LAT\";d:55.881981031962;s:4:\"TEXT\";s:96:\"Дмитровское шоссе, д.100, стр.2, бизнес-центр \"НордХаус\" \";}}}",
		"MAP_HEIGHT" => "400",
		"MAP_ID" => "",
		"MAP_WIDTH" => "100%",
		"OPTIONS" => array(
			0 => "ENABLE_SCROLL_ZOOM",
			1 => "ENABLE_DBLCLICK_ZOOM",
			2 => "ENABLE_DRAGGING",
		),
		"COMPONENT_TEMPLATE" => ".default"
	),
	false
);?>
	<div class="contacts__holder">
		<div class="contacts__left">
 			<!-- Реквизиты -->
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/include/4ps/contacts_req.php"
				)
			);?>
		</div>
		<div class="contacts__right">
		 	<!-- about -->
			<?$APPLICATION->IncludeComponent(
				"bitrix:main.include",
				"",
				Array(
					"AREA_FILE_SHOW" => "file",
					"AREA_FILE_SUFFIX" => "inc",
					"COMPOSITE_FRAME_MODE" => "A",
					"COMPOSITE_FRAME_TYPE" => "AUTO",
					"EDIT_TEMPLATE" => "",
					"PATH" => "/include/4ps/contacts_about.php"
				)
			);?>
		</div>
	</div>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
$MESS["SECTION_NOTIFICATION"] = "Текст уведомления";
// Params
$MESS["PARAMS_JQUERY_EN"] = "Подключать jQuery Битрикс";
$MESS["PARAMS_JQUERY_NO"] = "Нет, jQuery уже подключен";
$MESS["PARAMS_JQUERY_EN"] = "Подключать jQuery";
$MESS["PARAMS_JQUERY2_EN"] = "Подключать jQuery 2";
$MESS["PARAMS_SCRIPT_EN"] = "Подключать плагин jQuery Cookie";
$MESS["PARAMS_SET_COOKIE_EXPIRES"] = "Время, в течение которого будет жить cookie";
$MESS["PARAMS_SET_COOKIE_PATH"] = "Путь, по которому создается cookie";
$MESS["PARAMS_SET_COOKIE_SECURE"] = "Передавать cookie по защищенному протоколу HTTPS";
$MESS["PARAMS_SET_COOKIE_SECURE_YES"] = "Да";
$MESS["PARAMS_SET_COOKIE_SECURE_NO"] = "Нет";
$MESS["PARAMS_USE_STANDARD_NOTIFICATION"] = "Использовать стандартное уведомление";
$MESS["PARAMS_SET_STANDARD_NOTIFICATION_TEXT"] = "да";
$MESS["PARAMS_SET_CUSTOM_NOTIFICATION_TEXT"] = "нет";
$MESS["PARAMS_SET_TEXT"] = "Свой текст уведомления";
$MESS["PARAMS_SET_LINK"] = "Ссылка на политику обработки персональных данных";
$MESS["PARAMS_SET_COLOR_SCHEME"] = "Цветовая схема";
$MESS["PARAMS_SET_COLOR_SCHEME_LIGHT"] = "светлая";
$MESS["PARAMS_SET_COLOR_SCHEME_DARK"] = "темная";
$MESS["PARAMS_SHOW_ACCEPT_BUTTON"] = "Показывать кнопку";
$MESS["PARAMS_ACCEPT_BUTTON_NAME"] = "Надпись на кнопке";
$MESS["PARAMS_ACCEPT_BUTTON_NAME_DEFAULT"] = "Принять";
$MESS["PARAMS_ACCEPT_BUTTON_BG"] = "Цвет кнопки";
$MESS["PARAMS_ACCEPT_BUTTON_COLOR"] = "Цвет текста кнопки";
// Tips
$MESS["SET_COOKIE_EXPIRES_TIP"] = "Указывается значение в днях";
$MESS["SET_LINK_TIP"] = "Обязательное поле для заполнения";
$MESS["SET_TEXT_TIP"] = "Для указания в тексте ссылки на Политику конфиденциальности используйте конструкцию <i>&lt;a href=\"/ссылка/\"&gt;текст ссылки&lt;/a&gt;</i>";
?>
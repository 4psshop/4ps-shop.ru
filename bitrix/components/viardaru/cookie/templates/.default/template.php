<?
if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
/**
 * Bitrix vars
 *
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 * @global CMain $APPLICATION
 * @global CUser $USER
 */
?>
<?if ($arParams['SCRIPT_EN'] == 'Y'):?>
	<script src="/bitrix/components/viardaru/cookie/templates/.default/jquery.cookie.js"></script>
<?endif;?>

<?if ($arParams['SET_COLOR_SCHEME'] == 'light') {
  $this->addExternalCss($this->GetFolder() . "/themes/theme_light.css");
}
elseif ($arParams['SET_COLOR_SCHEME'] == 'dark') {
   $this->addExternalCss($this->GetFolder() . "/themes/theme_dark.css");
}
?>

<div class="cookie-block">
    <div class="cookie-block-wrap">
    	<?if(empty($arParams['SET_TEXT'])):?>
        <div class="cookie-block-info"><?=GetMessage('NOTIFICATION_TEXT')?> <a href="<?=$arParams['SET_LINK']?>" target="_blank"><?=GetMessage('NOTIFICATION_LINK')?></a>.</div>
      <?else:?>
        <div class="cookie-block-info"><span><? echo htmlspecialchars_decode($arParams['SET_TEXT']); ?></span></div>
      <?endif;?>

      <?if ($arParams['SHOW_ACCEPT_BUTTON'] == 'Y' && !empty($arParams['SET_ACCEPT_BUTTON_NAME'])):?>
        <button class="btn btn-lg cookie-block-icon" style="color: <?=$arParams['SET_ACCEPT_BUTTON_COLOR']?>; background: <?=$arParams['SET_ACCEPT_BUTTON_BG']?>"><?=$arParams['SET_ACCEPT_BUTTON_NAME']?></button>
      <?else:?>
        <span class="cookie-block-icon"></span>
      <?endif;?>
  </div>
</div>

<script>
	BX.message({
  		COOKIE_EXPIRES: '<? echo $arParams["SET_COOKIE_EXPIRES"]; ?>',
  		COOKIE_PATH: '<? echo $arParams["SET_COOKIE_PATH"]; ?>',
  		COOKIE_SECURE: '<? echo $arParams["SET_COOKIE_SECURE"]; ?>',
	});
</script>
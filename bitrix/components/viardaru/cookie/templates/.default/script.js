$(document).ready(function() {
	if(!$.cookie('cookie_agree')) $('.cookie-block').addClass('cookie-block-visible');
	
	$('.cookie-block-icon').click(function() {
		$.cookie('cookie_agree', 'true', {expires: BX.message("COOKIE_EXPIRES"), path: BX.message("COOKIE_PATH"), secure: BX.message("COOKIE_SECURE") });
		$('.cookie-block').removeClass('cookie-block-visible');
	});
});
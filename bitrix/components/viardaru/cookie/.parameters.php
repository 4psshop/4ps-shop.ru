<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"NOTIFICATION" => Array(
			"NAME" => GetMessage('SECTION_NOTIFICATION'),
			"SORT" => "250",
		),
	),

	"PARAMETERS" => array(
		"JQUERY_EN"  =>  Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("PARAMS_JQUERY_EN"),
			"TYPE" => "LIST",
			"VALUES" => array (
				"nojquery" => GetMessage("PARAMS_JQUERY_NO"),
				"jquery" => GetMessage("PARAMS_JQUERY_EN"),
				"jquery2" => GetMessage("PARAMS_JQUERY2_EN")
			),
		),
		"SCRIPT_EN"  =>  Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("PARAMS_SCRIPT_EN"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y"
		),
		"USE_STANDARD_NOTIFICATION" => Array(
			"PARENT" => "NOTIFICATION",
			"NAME" => GetMessage("PARAMS_USE_STANDARD_NOTIFICATION"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"standard" => GetMessage("PARAMS_SET_STANDARD_NOTIFICATION_TEXT"),
				"custom" => GetMessage("PARAMS_SET_CUSTOM_NOTIFICATION_TEXT")
			),
			"DEFAULT" => "standard",
			"REFRESH" => "Y"
		),
		"SET_COLOR_SCHEME" => Array(
			"PARENT" => "VISUAL",
			"NAME" => GetMessage("PARAMS_SET_COLOR_SCHEME"),
			"TYPE" => "LIST",
			"VALUES" => array(
				"light" => GetMessage("PARAMS_SET_COLOR_SCHEME_LIGHT"),
				"dark" => GetMessage("PARAMS_SET_COLOR_SCHEME_DARK")
			),
			"DEFAULT" => "light"
		),
		"SHOW_ACCEPT_BUTTON" => Array(
			"PARENT" => "VISUAL",
			"NAME" => GetMessage("PARAMS_SHOW_ACCEPT_BUTTON"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "N",
			"REFRESH" => "Y"
		),
	),
);

if ($arCurrentValues["SCRIPT_EN"] == "Y")
{
	$arComponentParameters["PARAMETERS"]["SET_COOKIE_EXPIRES"] = array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("PARAMS_SET_COOKIE_EXPIRES"),
		"TYPE" => "STRING",
		"DEFAULT" => "365"
	);
	$arComponentParameters["PARAMETERS"]["SET_COOKIE_PATH"] = array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("PARAMS_SET_COOKIE_PATH"),
		"TYPE" => "STRING",
		"DEFAULT" => "/"
	);
	$arComponentParameters["PARAMETERS"]["SET_COOKIE_SECURE"] = array(
		"PARENT" => "BASE",
		"NAME" => GetMessage("PARAMS_SET_COOKIE_SECURE"),
		"TYPE" => "LIST",
		"VALUES" => array (
			"true" => GetMessage("PARAMS_SET_COOKIE_SECURE_YES"),
			"false" => GetMessage("PARAMS_SET_COOKIE_SECURE_NO")
		),
		"DEFAULT" => "false"
	);
}

if ($arCurrentValues["SHOW_ACCEPT_BUTTON"] == "Y")
{
	$arComponentParameters["PARAMETERS"]["SET_ACCEPT_BUTTON_NAME"] = array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage("PARAMS_ACCEPT_BUTTON_NAME"),
		"TYPE" => "STRING",
		"DEFAULT" => GetMessage("PARAMS_ACCEPT_BUTTON_NAME_DEFAULT")
	);
	$arComponentParameters["PARAMETERS"]["SET_ACCEPT_BUTTON_BG"] = array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage("PARAMS_ACCEPT_BUTTON_BG"),
		"TYPE" => "COLORPICKER",
		"DEFAULT" => "#232323"
	);
	$arComponentParameters["PARAMETERS"]["SET_ACCEPT_BUTTON_COLOR"] = array(
		"PARENT" => "VISUAL",
		"NAME" => GetMessage("PARAMS_ACCEPT_BUTTON_COLOR"),
		"TYPE" => "COLORPICKER",
		"DEFAULT" => "#FFFFFF"
	);
}

if ($arCurrentValues["USE_STANDARD_NOTIFICATION"] == "standard")
{
	$arComponentParameters["PARAMETERS"]["SET_LINK"] = array(
		"PARENT" => "NOTIFICATION",
		"NAME" => GetMessage("PARAMS_SET_LINK"),
		"TYPE" => "STRING"
	);
}
elseif ($arCurrentValues["USE_STANDARD_NOTIFICATION"] == "custom")
{
	$arComponentParameters["PARAMETERS"]["SET_TEXT"] = array(
		"PARENT" => "NOTIFICATION",
		"NAME" => GetMessage("PARAMS_SET_TEXT"),
		"TYPE" => "STRING"
	);
}

?>

<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("VIARDA_COOKIE_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("VIARDA_COOKIE_COMPONENT_DESCRIPTION"),
	"SORT" => 100,
	"PATH" => array(
		"ID" => "viarda_cookie_components",
		"SORT" => 100,
		"NAME" => GetMessage("VIARDA_COOKIE_COMPONENTS_FOLDER_NAME"),
	),
);

?>
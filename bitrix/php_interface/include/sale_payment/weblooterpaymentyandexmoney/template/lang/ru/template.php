<?php
$MESS['PAYMENT_TARGET_NAME_DEFAULT'] = 'Оплата заказа в интернет-магазине';
$MESS['FIELD_targets'] = 'Оплата заказа';
$MESS['FORM_TOTAL_ORDER_TEXT'] = 'Стоимость заказа ##ORDER_ID# составляет #SUM# руб.';
$MESS['FORM_SUBMIT_BTN'] = 'Оплатить';
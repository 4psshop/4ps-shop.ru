<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

/** @var $params array */

use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

if( $params['COMMISSION_TO_PAYER'] > 0 )
{
    $intCommission = (float)str_replace(',', '.', $params['COMMISSION_TO_PAYER']);
    if( ( is_integer($intCommission) || is_float($intCommission) ) && $intCommission > 0 )
    {
        $params['SUM'] = round( ( $params['SUM'] + ( $params['SUM'] * ( $intCommission / 100 ) ) ), 2 );
    }
}
?>
<form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml" data-weblooter-payment-yandex-money accept-charset="UTF-8">
    <input type="hidden" name="receiver" value="<?=$params['WALLET_ID']?>">
    <input type="hidden" name="quickpay-form" value="shop">
    <input type="hidden" name="targets" value="<?=substr(Loc::getMessage('FIELD_targets').'#'.$params['ORDER_ID'], 0, 150)?>">
    <input type="hidden" name="paymentType" value="<?=( in_array( $params['PAYMENT_TYPE_DEFAULT'], ['PC', 'AC', 'MC'] ) ) ? $params['PAYMENT_TYPE_DEFAULT'] : 'AC'?>">
    <input type="hidden" name="sum" value="<?=$params['SUM']?>" />

    <input type="hidden" name="formcomment" value="<?=substr($params['PAYMENT_COMMENT'],0 , 50)?>">
    <input type="hidden" name="short-dest" value="<?=substr($params['PAYMENT_COMMENT'],0 , 50)?>">
    <input type="hidden" name="label" value="<?=$params['ORDER_ID']?>">
    <input type="hidden" name="successURL" value="<?=$params['SUCCESS_URL']?>">
    <input type="hidden" name="need-fio" value="<?=$params['NEED_FIO'] == 'Y' ? 'true' : 'false'?>">
    <input type="hidden" name="need-email" value="<?=$params['NEED_EMAIL'] == 'Y' ? 'true' : 'false'?>">
    <input type="hidden" name="need-phone" value="<?=$params['NEED_PHONE'] == 'Y' ? 'true' : 'false'?>">
    <input type="hidden" name="need-address" value="<?=$params['NEED_ADDRESS'] == 'Y' ? 'true' : 'false'?>">

    <?=Loc::getMessage('FORM_TOTAL_ORDER_TEXT', ['#ORDER_ID#' => $params['ORDER_ID'], '#SUM#' => number_format($params['SUM'], 0, '.', ' ') ])?><br/>
    <button type="submit"><?=Loc::getMessage('FORM_SUBMIT_BTN')?></button>
</form>
<?
namespace Sale\Handlers\PaySystem;

if( \Bitrix\Main\Loader::includeModule('weblooter.paymentyandexmoney') )
{
    class WeblooterPaymentYandexMoneyHandler extends \Bitrix\Sale\PaySystem\ServiceHandler {
        use \WeblooterModulePaymentSaleYandexMoneyHandler;
    };
}
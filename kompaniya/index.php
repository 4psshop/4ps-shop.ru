<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек");
$APPLICATION->SetPageProperty("title", "О компании 4 ПРО СПОРТ");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт");
$APPLICATION->SetPageProperty("description", "Профессиональный экипировочный центр 4 ПРО СПОРТ");
$APPLICATION->SetTitle("О компании");
?><style>
h1 {
font-size: 200%;
}

h1 {
color: #da4426;
}
</style> <!-- content-box -->
<div class="content-box content-box--about">
	 <!-- content-box__right -->
	<div class="content-box__right">
		 <!-- about -->
		<div class="about">
			<div class="row about__block about__block--border">
				<div class="col-sm-100 col-md-50 about__col about__col--right">
 <span class="about__tagline">Мы – это <br>
					 профессиональный <br>
					 экипировочный центр.</span> <br>
 <br>
					<div class="about__img about__img--indents">
 <img width="400" alt="sport_photo_16111111111111.jpg" src="/upload/medialibrary/f9b/f9b33eff18c4d7d9ec9dcf216e0d5eb2.jpg" height="241" title="sport_photo_16111111111111.jpg"><br>
 <br>
 <img width="400" alt="Tim_Tadder_1122222222.jpg" src="/upload/medialibrary/319/319d498e3bf4de0a2c597d95696dec22.jpg" height="245" title="Tim_Tadder_1122222222.jpg"><br>
 <img width="400" alt="sport_photo_113333333333.jpg" src="/upload/medialibrary/0e0/0e07af23a5a59147b73524ebfb1463d5.jpg" height="244" title="sport_photo_113333333333.jpg"><br>
					</div>
				</div>
				<div class="col-sm-100 col-md-50 about__col about__col--left">
					<p>
 <br>
 <br>
						 Мы – это профессиональный экипировочный центр.&nbsp;&nbsp;<br>
						 Наша специализация - экипировка профессиональных команд в различных видах спорта, включая зимние и летние олимпийские и паралимпийские виды спорта.&nbsp;&nbsp;<br>
						 Ассортимент экипировки очень значительный. Причем многое из того, что мы предлагаем уникально и отсутствует на Российском рынке.<br>
						 Зная требования профессиональных команд, предъявляемые к экипировки, у нас есть свое локальное производство текстиля.<br>
						 В особых случаях мы предоставляем услуги по международной доставке.<br>
 <br>
					</p>
					<h1>Олимпийские виды спорта</h1>
 <br>
					<p>
						 Наша компания осуществляет поставки спортивной экипировки, инвентаря и оборудования для Олимпийских видов спорта. В дополнение к общепринятым видам, таким как, например, бокс, борьба (вольная и греко-римская), тяжелая и легкая атлетика и т.п. наша компания осуществляет поставки инвентаря для таких сложно-технических видов спорта, как, бобслей, скелетон, прыжки с трамплина и т.д. Также мы осуществляем поставки инвентаря, экипировки и оборудования для зрелищного, но пока еще не распространенного в России зимнего олимпийского вида спорта, такого как керлинг. Для осуществления поставок мы имеем ряд прямых контрактов с такими зарубежными поставщиками, как Rass, Balance Plus, Dainis Dukurs, Wilfred Shnaider, Ortema и ing.
					</p>
 <br>
					<p>
						 Каждой команде, каждому спортсмену вне зависимости от вида спорта необходима качественная, функциональная и удобная спортивная экипировка. Тренировочный процесс, переезды и перелеты - экипировка должна не только выдерживать такую жесткую эксплуатацию, но и при этом оставаться презентабельной и отвечать высоким требованиям спортсменов. Исходя из наших знаний в различных видах спорта, мы подберем оптимальный комплект, включающий все необходимые позиции и удовлетворяющий клуб как по бюджету, так и по цветовому решению. Мы сотрудничаем с крупнейшими производителями спортивной экипировки много лет и знаем сроки доступности того или иного товара, что позволяет экипировать клуб в срок и в полном объеме.<br>
					</p>
					<p>
					</p>
					<p>
 <br>
					</p>
				</div>
			</div>
			<div class="row about__block">
				<div class="col-sm-100 col-md-50 about__col about__col--right">
					<h2 class="about__title">
					<div>
					</div>
 </h2>
					<p>
					</p>
				</div>
			</div>
		</div>
		 <!-- about end -->
	</div>
	 <!-- content-box__right end --> <!-- content-box__left -->
	<div class="content-box__left">
		<div class="content-box__left-inner">
			 <!-- side benefits -->
			<div class="side-benefits">
				<div class="side-benefits__inner">
					<div class="side-benefits__heading">
 <span class="side-benefits__title side-benefits__title--weight">Почему выгодно</span> <span class="side-benefits__title">покупать у нас</span>
					</div>
					<ul class="side-benefits__list">
						<li class="side-benefits__item"> <a href="/komanda/"> <span g="" class="icon-knowledge"> <strong class="side-benefits__text">Команда</strong> </span></a> </li>
						<li class="side-benefits__item"> <a href="/znaniya/"> <span g="" class="icon-knowledge"> <strong class="side-benefits__text">Знания</strong> </span></a> </li>
						<li class="side-benefits__item"> <a href="/reputatsiya/"> <span g="" class="icon-hands"> <strong class="side-benefits__text">Репутация</strong> </span></a> </li>
						<li class="side-benefits__item"> <a href="/dostavka/"> <span g="" class="icon-delivery"> <strong class="side-benefits__text">Доставка</strong> </span></a> </li>
					</ul>
				</div>
			</div>
			 <!-- side benefits end --> <!-- side certificate -->
			<div class="side-certificate">
				<div class="side-certificate__inner">
 <img alt="pic" src="/local/templates/4sport/pic/side-certif/img01.jpg">
				</div>
			</div>
			 <!-- side certificate end --> <!-- side usefull information --> <a href="#" class="side-usefull-info">
			<div class="side-usefull-info__inner">
				<div class="side-usefull-info__holder">
 <span g="" class="side-usefull-info__icon icon-info"> <span class="side-usefull-info__title">Полезная<br>
					 информация</span> </span>
				</div>
			</div>
 </a>
			<!-- side usefull information end --> <!-- side-more -->
			<div class="side-more">
 <a href="http://4prosport.ru/catalog/putpdf?put=/Media/images/catalog/pdf/blademasterru.pdf" target="_blank" class="btn btn-black btn-black--font btn--download-left"> <span class="btn-black__inner"> <span class="btn__text btn__text--middle">Загрузить каталог</span> <span class="btn__icon"> <span g="" class="icon-download-orange-small"> </span></span> </span> </a>
			</div>
			 <!-- side-more end -->
		</div>
	</div>
	 <!-- content-box__left end -->
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");


$APPLICATION->RestartBuffer();
$rr = file_get_contents('http://4ps-shop.ru/bitrix/components/bitrix/sale.ajax.locations/search.php?search='.\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('q'));
$rr = str_replace("'", '"', $rr);
$rr = json_decode($rr, true);

$arResult['results'] = array();
foreach ($rr as $arItem)
{
    $address = [$arItem['NAME'], $arItem['REGION_NAME'], $arItem['COUNTRY_NAME']];
    $address = implode(', ', array_diff($address, ['']));
    $arResult['results'][] = array( 'id'=>$arItem['ID'], 'text'=>$address );
}
echo json_encode($arResult);

die();
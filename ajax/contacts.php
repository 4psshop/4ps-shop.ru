<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");


$arResult = ['STATUS'=>'ERROR'];

try
{
    if( !check_bitrix_sessid() )
    {
        throw new \Exception();
    }

    if(isset($_POST['email'])){
        $email =  $_POST['email'];
        $email = trim($email);
        $email = strip_tags($email);
        $email = stripslashes($email);
        $email = htmlentities($email);
    }
    if(isset($_POST['username'])){
        $username =  $_POST['username'];
        $username = trim($username);
        $username = strip_tags($username);
        $username = stripslashes($username);
        // $username = htmlentities($username);
    }
    if(isset($_POST['question'])){
        $question =  $_POST['question'];
        $question = trim($question);
        $question = strip_tags($question);
        $question = stripslashes($question);
        // $question = htmlentities($question);
    }

    CModule::IncludeModule('iblock');
    $el = new CIBlockElement;
    $PROP = array();
    $PROP[41] = $email;
    $PROP[42] = $question;
    if($_POST['prop_id'] > 0){ // ADD SERVICE ITEM
        $PROP[$_POST['prop_id']] = $_POST['prop_val'];
    }
    $arLoadProductArray = Array(
        "IBLOCK_SECTION_ID"   => false,
        "IBLOCK_ID"      		=> 5,
        "PROPERTY_VALUES"		=> $PROP,
        "DATE_ACTIVE_FROM"  	=> date("d.m.Y"),
        "NAME"           		=> $username

    );
    $el->Add($arLoadProductArray);

    $to= "p.sales@4prosport.ru" . ", " ; //обратите внимание на запятую
// $to .= "mebelgold@mebelgold.ru";

    /* тема/subject */
    $subject = "Поступила заявка";

    /* сообщение */
    $message = '
<html>
<body>
<table>
<tr><td>Имя:</td><td>'.$username.'</td></tr>
<tr><td>Ящик:</td><td>'.$email.'</td></tr>
<tr><td>Отзыв:</td><td>'.$question.'</td></tr>
 
</table>
</body>
</html>
';

    /* Для отправки HTML-почты вы можете установить шапку Content-type. */
    $headers= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";

    /* дополнительные шапки */
    $headers .= "From: Магазин 4proSport <4proSport@site.com>\r\n";

    /* и теперь отправим из */
    mail($to, $subject, $message, $headers);


    $arResult = ['STATUS'=>'SUCCESS'];
}
catch (\Exception $e)
{

}

echo json_encode($arResult);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<?
if(isset($_REQUEST['username'])){
	$name = $_REQUEST['username'];
	$name = trim($name);
	// $name = htmlentities($name);
	$name = strip_tags($name);
	$name = stripslashes($name);
}
if(isset($_REQUEST['phone'])){
	$phone = $_REQUEST['phone'];
	$phone = trim($phone);
	// $phone = htmlentities($phone);
	$phone = strip_tags($phone);
	$phone = stripslashes($phone);
}
if(isset($_REQUEST['email'])){
	$email = $_REQUEST['email'];
	$email = trim($email);
	// $email = htmlentities($email);
	$email = strip_tags($email);
	$email = stripslashes($email);
}
if(isset($_REQUEST['comment'])){
	$comment = $_REQUEST['comment'];
	$comment = trim($comment);
	// $comment = htmlentities($comment);
	$comment = strip_tags($comment);
	$comment = stripslashes($comment);
}
if(isset($_REQUEST['prod_id'])){
	$prod_id = $_REQUEST['prod_id'];
	$prod_id = trim($prod_id);
	// $prod_id = htmlentities($prod_id);
	$prod_id = strip_tags($prod_id);
	$prod_id = stripslashes($prod_id);
}



CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
if(!empty($name) && !empty($phone) && !empty($email) && !empty($prod_id)) {
	echo "Спасибо, Ваш запрос отправлен!";

	//Добавляем в инфоблок
	$el = new CIBlockElement;
	$PROP = array();
	$PROP[86] = $phone;
	$PROP[87] = $email;
	$PROP[88] = $prod_id;
	$arLoadProductArray = Array(
	  "IBLOCK_ID"      		=> 14,
	  "PROPERTY_VALUES"		=> $PROP,
	  "DATE_ACTIVE_FROM"  	=> date("d.m.Y"),
	  "NAME"           		=> $name,
	  "ACTIVE"        		=> "N",
	  "PREVIEW_TEXT"   		=> $comment,
	  );
	$id = $el->Add($arLoadProductArray);

	//Отправляем письмо
	$arEventFields = array(
		"NAME"				=> $name,
		"PHONE"				=> $phone,
		"EMAIL"				=> $email,
		"PRODUCT"			=> SITE_SERVER_NAME."/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=2&type=catalog&ID=".$prod_id,
	);
	CEvent::Send("ASK_PRICE", "s1", $arEventFields);
}
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
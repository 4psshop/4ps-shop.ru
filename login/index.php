<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек, купить");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек, купить");
$APPLICATION->SetPageProperty("title", "Вход в магазин 4 ПРО СПОРТ");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт");
$APPLICATION->SetPageProperty("description", "Вход в магазин 4 ПРО СПОРТ: Спортивная одежда, обувь, точильное оборудование Blademaster и SSM, аксессуары");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}

$APPLICATION->SetTitle("Вход в магазин");
?><p class="notetext">Вы зарегистрированы и успешно авторизовались.</p>

<p><a href="/">Вернуться на главную страницу</a></p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек, купить");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек, купить");
$APPLICATION->SetPageProperty("title", "Ваша история заказов в 4 ПРО СПОРТ");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт, купить");
$APPLICATION->SetPageProperty("description", "Ваша история заказов спортивной одежды, обуви Nike, Adidas, Asics, точильное оборудование Blademaster и SSM, аксессуаров в 4 ПРО СПОРТ");
$APPLICATION->SetTitle("История заказов");
?><div class="about">
    <br/>
	<?$APPLICATION->IncludeComponent("bitrix:sale.personal.order", "personal-order", Array(
        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
            "CACHE_TIME" => "3600",	// Время кеширования (сек.)
            "CACHE_TYPE" => "A",	// Тип кеширования
            "COMPONENT_TEMPLATE" => ".default",
            "CUSTOM_SELECT_PROPS" => "",	// Дополнительные свойства инфоблока
            "DETAIL_HIDE_USER_INFO" => array(	// Не показывать в информации о пользователе
                0 => "0",
            ),
            "HISTORIC_STATUSES" => array(	// Перенести в историю заказы в статусах
                0 => "F",
            ),
            "NAV_TEMPLATE" => "",	// Имя шаблона для постраничной навигации
            "ORDERS_PER_PAGE" => "20",	// Количество заказов на одной странице
            "ORDER_DEFAULT_SORT" => "STATUS",	// Сортировка заказов
            "PATH_TO_BASKET" => "/cart/",	// Страница с корзиной
            "PATH_TO_CATALOG" => "/catalog/",	// Путь к каталогу
            "PATH_TO_PAYMENT" => "/order/payment.php",	// Страница подключения платежной системы
            "PROP_1" => "",	// Не показывать свойства для типа плательщика "Физическое лицо" (s1)
            "REFRESH_PRICES" => "N",	// Пересчитывать заказ после смены платежной системы
            "RESTRICT_CHANGE_PAYSYSTEM" => array(	// Запретить смену платежной системы у заказов в статусах
                0 => "0",
            ),
            "SAVE_IN_SESSION" => "Y",	// Сохранять установки фильтра в сессии пользователя
            "SEF_MODE" => "Y",	// Включить поддержку ЧПУ
            "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
            "STATUS_COLOR_F" => "gray",	// Цвет статуса "Выполнен"
            "STATUS_COLOR_N" => "green",	// Цвет статуса "Принят, ожидается оплата"
            "STATUS_COLOR_OT" => "gray",
            "STATUS_COLOR_P" => "yellow",	// Цвет статуса "Оплачен, формируется к отправке"
            "STATUS_COLOR_PR" => "gray",
            "STATUS_COLOR_PSEUDO_CANCELLED" => "red",	// Цвет отменённых заказов
            "STATUS_COLOR_TM" => "gray",
            "STATUS_COLOR_VO" => "gray",
            "PROP_3" => "",	// Не показывать свойства для типа плательщика "ИП" (s1)
            "PROP_4" => "",	// Не показывать свойства для типа плательщика "Юридическое лицо" (s1)
            "SEF_FOLDER" => "/personal/orders/",	// Каталог ЧПУ (относительно корня сайта)
            "SEF_URL_TEMPLATES" => array(
                "list" => "",
                "detail" => "detail/#ID#",
                "cancel" => "cancel/#ID#",
            )
        ),
        false
    );?>
    <br/>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек, купить");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт, купить");
$APPLICATION->SetPageProperty("description", "Команда профессионалов 4 ПРО СПОРТ по экипировке команд, олимпийским видам спорта, спортивной одежде, обуви, точильному оборудованию Blademaster и SSM");
$APPLICATION->SetPageProperty("tags", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, купить");
$APPLICATION->SetPageProperty("title", "Наша команда профессионалов 4 ПРО СПОРТ");
$APPLICATION->SetTitle("Наша команда");
?><style>
   h1 {
    font-size: 250%; /* Размер шрифта в процентах */ 
    color: #da4426
   } 
h2 {
 font-size: 150%; 
}
.nubex1 {
    font-weight: bold;
   }
.thumb img  {
     margin-right: 40px; /* Отступ справа */
    margin-bottom: 30px; /* Отступ снизу */
   }
.tablica {
  display: table; 
  width: 100%; 
  border-spacing: 10px 10px; 
} 

.yacheika {
  display: table-cell; 
  padding: 10px; 
}
 </style>
<h2>Наша команда – это команда единомышленников, профессионалов своего дела, активных, творческих, позитивных и искренне любящих свою работу людей.</h2>
 <br>
 <br>
 <br>
<div style="text-align:center;">
	<h1> <span class="nubex1"> Бухгалтерия</span> </h1>
 <br>
 <br>
	<div style="text-align: center;">
 <img width="300" alt="1.jpg" src="/upload/medialibrary/595/5955b3a2dd5d3f897d759a23a343d24d.jpg" height="300" title="1.jpg"><br>
 <br>
		<h2> <span class="nubex1">Орлова Татьяна</span> </h2>
 <br>
		Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел бухгалтерии для Орловой Т.» <br>
		 тел.: +7-926-235-24-71 <br>
 <br>
 <br>
		<div style="text-align:center;">
 <br>
 <br>
			<h1> <span class="nubex1"> Отдел продаж</span> </h1>
 <br>
 <br>
			<div class="tablica">
				<div class="yacheika">
					 &lt;<img width="300" alt="2.jpg" src="/upload/medialibrary/a7e/a7efcec559fd97d8f04cefc6bd335c0c.jpg" height="300" title="2.jpg">&gt;<br>
 <br>
					<h2> <span class="nubex1"> Пономарев Илья</span> </h2>
 <br>
					Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел продаж для Пономарева И.» <br>
					 тел.: +7-925-411-50-47
				</div>
				<div class="yacheika">
					 &lt;<img width="256" alt="1.jpg" src="/upload/medialibrary/f4d/f4d2d708f770c87a2d82116dfa3df47b.jpg" height="300" title="1.jpg"><br>
 <br>
					<h2> <span class="nubex1"> Савельев Михаил</span> </h2>
 <br>
					Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел продаж для Савельева М.» 
				</div>
				<div class="yacheika">
					 &lt;<img width="260" alt="3.jpg" src="/upload/medialibrary/045/04515c191034464561e09bd771ac88af.jpg" height="300" title="3.jpg"><br>
 <br>
					<h2> <span class="nubex1">Фомин Александр</span> </h2>
 <br>
					Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел продаж для Фомина А.» <br>
					 тел.: +7-915-333-91-46
				</div>
			</div>
			<div style="text-align: center;">
 <br>
 <br>
				<h1> <span class="nubex1"> Транспортный отдел</span> </h1>
 <br>
 <br>
				<div style="text-align: center;">
 <img width="300" alt="1.jpg" src="/upload/medialibrary/721/721c3590e7a6477bb22962d08ccd0490.jpg" height="300" title="1.jpg"><br>
 <br>
					<h2> <span class="nubex1"> Попцов Олег</span> </h2>
 <br>
					 Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В транспортный отдел для Попцова О.» <br>
                                         тел.: +7-903-205-06-79 <br>
					<div style="text-align:center;">
 <br>
 <br>
						<h1> <span class="nubex1"> Ремонт оборудования</span> </h1>
 <br>
 <br>
						<div style="text-align: center;">
 <img width="300" alt="1.jpg" src="/upload/medialibrary/31b/31bab43a36856c3dbd4d0d89036eef06.jpg" height="300" title="1.jpg"><br>
 <br>
							<h2> <span class="nubex1"> Филин Сергей</span> </h2>
 <br>
							Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел ремонта оборудования для Филина С.»
							<div style="text-align: center;">
 <br>
 <br>
								<h1> <span class="nubex1">Отдел IT</span> </h1>
 <br>
 <br>
								<div class="tablica">
									<div class="yacheika">
										 &lt;<img width="300" alt="1.jpg" src="/upload/medialibrary/1fb/1fb3792dad9ec48dcbc4cb08c67b0131.jpg" height="300" title="1.jpg">&gt;<br>
 <br>
										<h2> <span class="nubex1"> Фалынсков Дмитрий</span> </h2>
 <br>
										<p>
										</p>
										Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел IT для Фалынскова Д.»
									</div>
									<div class="yacheika">
										 &lt;<img width="300" alt="1.jpg" src="/upload/medialibrary/308/3087f245b8660e33d546720f968713fb.jpg" height="300" title="1.jpg">&gt;<br>
 <br>
										<h2> <span class="nubex1"> Стефанкова Дарья</span> </h2>
 <br>
										 Для связи пишите письмо на <a href="mailto:info@4prosport.ru">info@4prosport.ru</a> с пометкой «В отдел IT для Стефанковой Д.»<br>
									</div>
									<div style="text-align:left;">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 <br>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"",
Array()
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
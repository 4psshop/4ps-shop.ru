<?php
set_time_limit(60*10);

if( empty( $_SERVER['DOCUMENT_ROOT'] ))
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__FILE__))));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
\CModule::IncludeModule("main");
\CModule::IncludeModule("iblock");


$strFileDir = $_SERVER['DOCUMENT_ROOT'].'/upload/images-update/';

$arImagesFiles = glob($strFileDir.'*');

if( !empty($arImagesFiles) )
{
    $arImagesFiles = array_map(function($v){
        return end( explode('/', $v) );
    }, $arImagesFiles);

    $rs = CIBlockElement::GetList([], ['IBLOCK_ID' => 2, '!PROPERTY_ARTICUL' => false], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_ARTICUL']);
    while ($ar = $rs->Fetch()) {
        // GALLERY
        $ar['PROPERTY_ARTICUL_VALUE'] = trim($ar['PROPERTY_ARTICUL_VALUE']);

        $arFilesToProduct = [];
        foreach ($arImagesFiles as $fileName)
        {
            if( preg_match('/^('.str_replace(['-', '.', '_'], ['\-', '\.', '\_'], $ar['PROPERTY_ARTICUL_VALUE']).')(\_[\d]+)?\./', $fileName) === 1 )
            {
                $arFilesToProduct[] = $fileName;
            }
        }


        if( !empty($arFilesToProduct) )
        {

            $el = new CIBlockElement;
            $el->Update($ar['ID'], [
                'PREVIEW_PICTURE' => \CFile::MakeFileArray($strFileDir.$arFilesToProduct[0]),
                'DETAIL_PICTURE' => \CFile::MakeFileArray($strFileDir.$arFilesToProduct[0])
            ]);
            if( sizeof($arFilesToProduct) > 1 )
            {
                $arFields = [];
                foreach ($arFilesToProduct as $file)
                {
                    $arFields[] = \CFile::MakeFileArray($strFileDir.$file);
                }

                \CIBlockElement::SetPropertyValuesEx($ar['ID'], $ar['IBLOCK_ID'], ['GALLERY' => $arFields]);
            }
        }
    }
}
echo 'Done'."\r\n";
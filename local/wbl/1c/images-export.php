<?
set_time_limit(60*10);

if( empty( $_SERVER['DOCUMENT_ROOT'] ))
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__FILE__))));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");

$strFileDir = $_SERVER['DOCUMENT_ROOT'].'/upload/images-update/';

$rs = CIBlockElement::GetList([], ['IBLOCK_ID' => 2, '!PROPERTY_ARTICUL' => false], false, false, ['ID', 'IBLOCK_ID']);
while ($ob = $rs->GetNextElement())
{
    $ar = $ob->GetFields();
    $ar['PROPERTIES'] = $ob->GetProperties();
    if( !empty($ar['PROPERTIES']['GALLERY']['VALUE']) )
    {
        $arFile = pathinfo( $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($ar['PROPERTIES']['GALLERY']['VALUE'][0]) );
        if( !file_exists( $strFileDir.$ar['PROPERTIES']['ARTICUL']['VALUE'].'.'.$arFile['extension'] ) )
        {
            if( sizeof( $ar['PROPERTIES']['GALLERY']['VALUE'] ) > 1 )
            {
                foreach ($ar['PROPERTIES']['GALLERY']['VALUE'] as $key => $fileId)
                {
                    $arFile = pathinfo(\CFile::GetPath($fileId));
                    copy( $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($fileId), $strFileDir.$ar['PROPERTIES']['ARTICUL']['VALUE'].'_'.($key+1).'.'.$arFile['extension'] );
                }
            }
            else
            {
                copy( $_SERVER['DOCUMENT_ROOT'].\CFile::GetPath($ar['PROPERTIES']['GALLERY']['VALUE'][0]), $strFileDir.$ar['PROPERTIES']['ARTICUL']['VALUE'].'.'.$arFile['extension'] );
            }
        }
    }
}

echo "Done\r\n";
<?
set_time_limit(60*20);

if( empty( $_SERVER['DOCUMENT_ROOT'] ))
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__FILE__))));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");


$currentTime = microtime();

try{

    if(
        file_exists($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog.xml')
        && !file_exists($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog-import-running')
    )
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog-import-running', $currentTime);

        if( file_exists( $_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-catalog.xml' ) )
            unlink($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-catalog.xml' );
        copy($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog.xml', $_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-catalog.xml');

        define('CATALOG_ID', 2);
        define('OFFERS_ID', 15);

        /**
         * Парсим XML<br/>
         * <br/>
         * Возвращает массив<br/>
         * array(<br/>
        &emsp;'ITEMS' => array(<br/>
        &emsp;&emsp;'CATALOG' => array(), // Перечень товаров, которые пойдут в каталог<br/>
        &emsp;&emsp;'OFFERS' => array() // Перечень торг. предложений<br/>
        ),<br/>
        &emsp;'SECTIONS' => array() // Дерево разделов<br/>
        )
         *
         * @return array
         * @throws Exception
         */
        $funPreParseXML = function (){
            $XML = json_decode( json_encode( simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog.xml') ) , true);
            if( empty($XML['ITEM']) )
                throw new \Exception('В XML нет товаров');

            $arResult['ITEMS'] = array('CATALOG'=>array(), 'OFFERS'=>array());
            $arResult['SECTIONS'] = array();

            foreach ($XML['ITEM'] as $arItem){

                $arItem = array_map(function($val)
                {
                    return ( !is_array($val) ) ? trim($val) : $val;
                }, $arItem);

                if( empty($arItem['ITEM_NAME']) ) continue;


                $strNameSection = $arItem['SPORT_GUID'];
                if( !empty($arItem['CATEGORY_GUID']) )
                    $strNameSection = $arItem['CATEGORY_GUID'];
                if( !empty($arItem['SUBCATEGORY_GUID']) )
                    $strNameSection = $arItem['SUBCATEGORY_GUID'];

                /* Соберем дерево разделов */
                // Для раздела 1го уровня
                if( empty($arResult['SECTIONS'][ $arItem['SPORT_GUID'] ]) )
                    $arResult['SECTIONS'][ $arItem['SPORT_GUID'] ] = array(
                        'NAME' => $arItem['SPORT_NAME'],
                        'IBLOCK_SECTION_ID' => '0',
                        'XML_ID' => $arItem['SPORT_GUID']
                    );
                // Для раздела 2го уровня
                if( empty($arResult['SECTIONS'][ $arItem['SPORT_GUID'] ]['PARENT'][ $arItem['CATEGORY_GUID'] ]) )
                    $arResult['SECTIONS'][ $arItem['SPORT_GUID'] ]['PARENT'][ $arItem['CATEGORY_GUID'] ] = array(
                        'NAME' => $arItem['CATEGORY_NAME'],
                        'IBLOCK_SECTION_ID' => $arItem['SPORT_GUID'],
                        'XML_ID' => $arItem['CATEGORY_GUID']
                    );
                // Для раздела 3го уровня
                if( empty($arResult['SECTIONS'][ $arItem['SPORT_GUID'] ]['PARENT'][ $arItem['CATEGORY_GUID'] ]['PARENT'][ $arItem['SUBCATEGORY_GUID'] ]) )
                    $arResult['SECTIONS'][ $arItem['SPORT_GUID'] ]['PARENT'][ $arItem['CATEGORY_GUID'] ]['PARENT'][ $arItem['SUBCATEGORY_GUID'] ] = array(
                        'NAME' => $arItem['SUBCATEGORY_NAME'],
                        'IBLOCK_SECTION_ID' => $arItem['CATEGORY_GUID'],
                        'XML_ID' => $arItem['SUBCATEGORY_GUID']
                    );
                /* / Соберем дерево разделов */

                /* Соберем данные по товару */
                $arResult['ITEMS']['CATALOG'][ $arItem['ITEM_GUID'] ] = array(
                    'IBLOCK_ID' => CATALOG_ID,
                    'NAME' => $arItem['ITEM_NAME'],
                    'CODE' => Cutil::translit($arItem['ITEM_NAME'].'-'.$arItem['ARTICUL'], "ru", array("replace_space" => "-", "replace_other" => "-")),
                    'XML_ID' => $arItem['ITEM_GUID'],
                    'IBLOCK_SECTION_ID' => $strNameSection,
                    'DETAIL_TEXT' => !empty($arItem['DESCRIPTION'])?$arItem['DESCRIPTION']:'',
                    'DETAIL_TEXT_TYPE' => 'text',
                    'PROPERTY_VALUES' => array(
                        'ARTICUL' => !empty($arItem['ARTICUL'])?$arItem['ARTICUL']:'',
                        'BRAND' => !empty($arItem['BRAND'])?$arItem['BRAND']:'',
                        'GENDER' => !empty($arItem['GENDER'])?$arItem['GENDER']:'',
                        'SPORT_NAME' => !empty($arItem['SPORT_NAME'])?$arItem['SPORT_NAME']:'',
                        'SHOW_PRICE' => ( $arItem['SHOW_PRICE'] === true || (string)$arItem['SHOW_PRICE'] == 'true' ) ? 1 : 0
                    )
                );
                
                /* / Соберем данные по товару */

                /* Соберем данные по торговому предложению */
                if( !empty($arItem['VARIATIONS']) ){
                    // Обнаружили баг с варинатами - если там 1 вариант, то не отдается как отдельный массив. Проверим по ключу. Если GUID - значит товар 1 и он и есть предложением.
                    $arItemVariantios = $arItem['VARIATIONS']['VARIATION'];
                    if( array_key_exists('GUID', $arItem['VARIATIONS']['VARIATION']) ){
                        // Одно предложение
                        $arItemVariantios = [$arItemVariantios];
                    }

                    foreach ($arItemVariantios as $arOffer){
                        $arResult['ITEMS']['OFFERS'][ $arItem['ITEM_GUID'] ][ $arOffer['GUID'] ] = array(
                            'IBLOCK_ID' => OFFERS_ID,
                            'NAME' => $arItem['ITEM_NAME'].' ( '.(!empty($arOffer['SIZE1'])?$arOffer['SIZE1']:'').', '.( !empty($arOffer['SIZE2'])?$arOffer['SIZE2']:'' ).' )',
                            'CODE' => Cutil::translit(( $arItem['ITEM_NAME'].' ( '.(!empty($arOffer['SIZE1'])?$arOffer['SIZE1']:'').', '.( !empty($arOffer['SIZE2'])?$arOffer['SIZE2']:'' ).' )' ).'-'.$arItem['ARTICUL'], "ru", array("replace_space" => "-", "replace_other" => "-")),
                            'XML_ID' => $arOffer['GUID'],
                            'PROPERTY_VALUES' => array(
                                'CML2_LINK' => $arItem['ITEM_GUID'],
                                'SIZE1' => !empty($arOffer['SIZE1'])?$arOffer['SIZE1']:'',
                                'SIZE2' => !empty($arOffer['SIZE2'])?$arOffer['SIZE2']:'',
                                'PRICE' => !empty($arOffer['PRICE'])?str_replace(',', '.', $arOffer['PRICE']):'',
                            )
                        );
                    }
                }
                /* / Соберем данные по торговому предложению */

            }

            return $arResult;
        };
        $arPreParseData = $funPreParseXML();
        /* / Парсим XML */

        /**
         * Проводит сверку разделов с сайта и разделов в XML.<br/>
         * Создает новые и обновляет старые<br/>
         * <br/>
         * Изменяет <b>$arPreParseData['SECTIONS']</b> таким образом, что структура массива становится <b>XML_ID => SECTION_ID</b>, <u>затирая старые значения</u>
         * @throws Exception
         */
        $funCheckingSections = function () use (&$arPreParseData){
            if( empty($arPreParseData['SECTIONS']) )
                throw new \Exception('Посре парсинга XML список разделов оказался пуст');

            /* Получим разделы сайта */
            $rsSiteSectionsXMLIDToID = CIBlockSection::GetList(array('ID' => 'ASC'), array('IBLOCK_ID'=>CATALOG_ID), false, array('ID', 'XML_ID'));
            $arSiteSectionsXMLIDToID = array();
            while ($arSect = $rsSiteSectionsXMLIDToID->GetNext()) {
                if( !empty($arSect['XML_ID']) )
                    $arSiteSectionsXMLIDToID[ $arSect['XML_ID'] ] = $arSect['ID'];
            }
            /* / Получим разделы сайта */

            /* Сверим дерево разделов из XML с разделами на сайте */
            function WBLCreateOrUpdateSections ($arSection, $IBLOCK_SECTION_ID = false, &$arSiteSectionsXMLIDToID){

                foreach ($arSection as $arXMLSection){
                    // Проверка корневого раздела
                    if( empty($arSiteSectionsXMLIDToID[ $arXMLSection['XML_ID'] ]) ){
                        // Создать
                        $bs = new CIBlockSection;
                        $arFields = Array(
                            "ACTIVE" => 'Y',
                            "IBLOCK_SECTION_ID" => $IBLOCK_SECTION_ID,
                            "IBLOCK_ID" => CATALOG_ID,
                            "NAME" => $arXMLSection['NAME'],
                            'CODE' => Cutil::translit($arXMLSection['NAME'], "ru", array("replace_space" => "-", "replace_other" => "-")),
                            'XML_ID' => $arXMLSection['XML_ID']
                        );
                        $ID = $bs->Add($arFields);
                        if( $ID > 0 ){
                            // Запишем XML_ID в таблицу соотвествий
                            $arSiteSectionsXMLIDToID[ $arXMLSection['XML_ID'] ] = $ID;
                        }
                        else{
                            // Остановим работу скрипта
                            throw new \Exception($arXMLSection['NAME'].': '.$bs->LAST_ERROR);
                        }
                    }
                    else{
                        // Обновить
                        $bs = new CIBlockSection;
                        $arFields = Array(
                            "IBLOCK_SECTION_ID" => $IBLOCK_SECTION_ID,
                            "IBLOCK_ID" => CATALOG_ID,
                            "NAME" => $arXMLSection['NAME'],
                            'CODE' => Cutil::translit($arXMLSection['NAME'], "ru", array("replace_space" => "-", "replace_other" => "-")),
                            'XML_ID' => $arXMLSection['XML_ID']
                        );
                        $res = $bs->Update($arSiteSectionsXMLIDToID[ $arXMLSection['XML_ID'] ], $arFields);
                        if( !$res ){
                            // Остановим работу скрипта
                            throw new \Exception($arXMLSection['NAME'].': '.$bs->LAST_ERROR);
                        }
                    }

                    // Проверим на дочеркине разделы
                    if( !empty($arXMLSection['PARENT']) ){
                        WBLCreateOrUpdateSections($arXMLSection['PARENT'], $arSiteSectionsXMLIDToID[ $arXMLSection['XML_ID'] ], $arSiteSectionsXMLIDToID);
                    }

                }

            };

            WBLCreateOrUpdateSections($arPreParseData['SECTIONS'], false, $arSiteSectionsXMLIDToID);
            /* / Сверим дерево разделов из XML с разделами на сайте */

            $arPreParseData['SECTIONS'] = $arSiteSectionsXMLIDToID;
        };
        $funCheckingSections();

        /**
         * Продит сверку текущих товаров в каталоге и товаров в XML (не торговых предложений)<br/>
         * Создает новые и обновляет старые<br/>
         * <br/>
         * Изменяет <b>$arPreParseData['ITEMS']['CATALOG']</b> таким образом, что структура массива становится <b>XML_ID => ELEMENT_ID</b>, <u>затирая старые значения</u>
         * @throw \Exception
         */
        $funCheckingCatalogProducts = function () use (&$arPreParseData){
            if( empty($arPreParseData['SECTIONS']) )
                throw new \Exception('В начале работы по обработке товаров обнаружили, что таблица соответствий разделов пуста');

            if( empty($arPreParseData['ITEMS']['CATALOG']) )
                throw new \Exception('В начале работы по обработке товаров обнаружили, что список товаров их XML пуст');

            /* Соберем таблицу соответсткий XML_ID => ELEMENT_ID из товаров на сайте */
            $rsCatalogProductsXMLIDToID = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>CATALOG_ID), false, false, array('ID', 'XML_ID'));
            $arCatalogProductsXMLIDToID = array();
            while ($ar = $rsCatalogProductsXMLIDToID->Fetch()) {
                if( !empty($ar['XML_ID']) )
                    $arCatalogProductsXMLIDToID[ $ar['XML_ID'] ] = $ar['ID'];
            }
            /* / Соберем таблицу соответсткий XML_ID => ELEMENT_ID из товаров на сайте */

            $arFile = pathinfo(__FILE__);
            /* Перебираем товары из XML, обновляем или создаем */
            foreach ($arPreParseData['ITEMS']['CATALOG'] as $arXMLProduct){

                // Проверим раздел и заменим XML_ID на ID или пропустим товар
                if( empty( $arPreParseData['SECTIONS'][ $arXMLProduct['IBLOCK_SECTION_ID'] ] ) ) continue;
                $arXMLProduct['IBLOCK_SECTION_ID'] = $arPreParseData['SECTIONS'][ $arXMLProduct['IBLOCK_SECTION_ID'] ];
                // / Проверим раздел и заменим XML_ID на ID или пропустим товар

                // Начинаем обработку товаров
                if( empty( $arCatalogProductsXMLIDToID[ $arXMLProduct['XML_ID'] ] ) ){
                    // Создаем
                    $el = new CIBlockElement;
                    if ($PRODUCT_ID = $el->Add($arXMLProduct)){
                        // Добавим соответствие XML_ID => ID в таблицу соответствий
                        $arCatalogProductsXMLIDToID[ $arXMLProduct['XML_ID'] ] = $PRODUCT_ID;
                    }
                    else{
                        // При добавлении товара что то пошло не так. Выстреливаем исключение с ошибкой
                        //throw new \Exception($arXMLProduct['NAME'].' ['.$arXMLProduct['PROPERTY_VALUES']['ARTICUL'].']: '.strip_tags($el->LAST_ERROR).'[LINE '.__LINE__.']');
                        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - '.$arXMLProduct['NAME'].' ['.$arXMLProduct['PROPERTY_VALUES']['ARTICUL'].']: '.strip_tags($el->LAST_ERROR).'[LINE '.__LINE__.']'."\r\n", FILE_APPEND);
                    }
                }
                else{
                    // Обновляем
                    $el = new CIBlockElement;
                    if ( !$el->Update($arCatalogProductsXMLIDToID[ $arXMLProduct['XML_ID'] ], $arXMLProduct)){
                        // При добавлении товара что то пошло не так. Выстреливаем исключение с ошибкой
                        throw new \Exception($arXMLProduct['NAME'].' ['.$arXMLProduct['PROPERTY_VALUES']['ARTICUL'].']: '.strip_tags($el->LAST_ERROR).'[LINE '.__LINE__.']');
                    }

                }
                $arPreParseData['ITEMS']['CATALOG'] = $arCatalogProductsXMLIDToID;

            }
            /* / Перебираем товары из XML, обновляем или создаем */

        };
        $funCheckingCatalogProducts();

        /**
         * Проводит сверку текущих торг. предложений и торг. предложений из XML<br/>
         * Создает новые и обновляет старые<br/>
         * @throw \Exception
         */
        $funCheckingOffers = function () use ($arPreParseData){
            if( empty($arPreParseData['SECTIONS']) )
                throw new \Exception('В начале работы по обработке торг. пред. обнаружили, что таблица соответствий разделов пуста');

            if( empty($arPreParseData['ITEMS']['CATALOG']) )
                throw new \Exception('В начале работы по обработке торг. пред. обнаружили, что таблица соответствий товаров пуста');

            if( empty($arPreParseData['ITEMS']['OFFERS']) )
                throw new \Exception('В начале работы по обработке торг. пред. обнаружили, что список торг. пред. их XML пуст');

            /* Соберем таблицу соответсткий XML_ID => ELEMENT_ID из торг. пред. на сайте */
            $rsOffersXMLIDToID = CIBlockElement::GetList(Array(), array('IBLOCK_ID'=>OFFERS_ID), false, false, array('ID', 'XML_ID'));
            $arOffersXMLIDToID = array();
            while ($ar = $rsOffersXMLIDToID->Fetch()) {
                if( !empty($ar['XML_ID']) )
                    $arOffersXMLIDToID[ $ar['XML_ID'] ] = $ar['ID'];
            }
            /* / Соберем таблицу соответсткий XML_ID => ELEMENT_ID из торг. пред. на сайте */


            $arFile = pathinfo(__FILE__);
            foreach ($arPreParseData['ITEMS']['OFFERS'] as $arOffers){
                foreach ($arOffers as $arOffer){

                    // Проверим родитель торг. пред. и либо подменяем, либо пропускаем
                    if( empty( $arPreParseData['ITEMS']['CATALOG'][ $arOffer['PROPERTY_VALUES']['CML2_LINK'] ] ) ) continue;
                    $arOffer['PROPERTY_VALUES']['CML2_LINK'] = $arPreParseData['ITEMS']['CATALOG'][ $arOffer['PROPERTY_VALUES']['CML2_LINK'] ];
                    // / Проверим родитель торг. пред. и либо подменяем, либо пропускаем

                    // Обработаем предложения
                    if( empty( $arOffersXMLIDToID[ $arOffer['XML_ID'] ] ) ){
                        // Создадим
                        $el = new CIBlockElement;
                        $PRODUCT_ID = $el->Add($arOffer);
                        if ( $PRODUCT_ID < 1 ){
                            //throw new \Exception($arOffer.['NAME'].' ['.$arOffer['XML_ID'].']: '.$el->LAST_ERROR);
                            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - '.$arOffer.['NAME'].' ['.$arOffer['XML_ID'].']: '.strip_tags($el->LAST_ERROR).'[LINE '.__LINE__.']'."\r\n", FILE_APPEND);
                        }
                    }
                    else{
                        // Обновим
                        $el = new CIBlockElement;
                        if ( !$el->Update($arOffersXMLIDToID[ $arOffer['XML_ID'] ], $arOffer) ){
                            throw new \Exception($arOffer.['NAME'].' ['.$arOffer['XML_ID'].']: '.strip_tags($el->LAST_ERROR).'[LINE '.__LINE__.']');
                        }
                    }

                }
            }
        };
        $funCheckingOffers();

        $arFile = pathinfo(__FILE__);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - Импорт прошел успешно!'."\r\n", FILE_APPEND);

        unlink($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog.xml');
    }


}
catch (\Exception $e){

    $arFile = pathinfo(__FILE__);
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - '.$e->getMessage()."\r\n", FILE_APPEND);
}
finally
{
    if( file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog-import-running') == $currentTime )
        unlink($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/catalog-import-running');
}

echo "Done\r\n";

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
<?
set_time_limit(60*10);

if( empty( $_SERVER['DOCUMENT_ROOT'] ))
    $_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__FILE__))));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule('sale');
CModule::IncludeModule('catalog');

$currentTime = microtime();

try{

    if(
        file_exists($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price.xml')
        && !file_exists($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price-import-running')
    )
    {
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price-import-running', $currentTime);

        if( file_exists( $_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-price.xml' ) )
            unlink( $_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-price.xml' );
        copy($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price.xml', $_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/last-price.xml' );

        define('CATALOG_ID', 2);
        define('OFFERS_ID', 15);

        /**
         * Перебираем XML и получает остатки и цены
         *
         * @return array
         */
        $funPreparsePriceXML = function (){
            $XML = json_decode( json_encode( simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price.xml') ) , true);
            if( empty($XML['ITEM']) )
                throw new \Exception('В XML нет данныз по ценам');

            $arReturn = [];

            /* Перебираем товары, собирая цены и остатки */
            foreach ($XML['ITEM'] as $arItem){
                if( !empty($arItem['VARIATIONS']['VARIATION']) ){
                    $arVariations = $arItem['VARIATIONS']['VARIATION'];
                    // Обнаружили баг с варинатами - если там 1 вариант, то не отдается как отдельный массив. Проверим по ключу. Если GUID - значит товар 1 и он и есть предложением.
                    if( array_key_exists('GUID', $arItem['VARIATIONS']['VARIATION']) ){
                        // Одно предложение
                        $arVariations = [$arVariations];
                    }
                    foreach ($arVariations as $arOffer){
                        $arReturn[ $arOffer['GUID'] ] = ['QUANTITY'=>$arOffer['QUANTITY'], 'PRICE'=>$arOffer['PRICE']];
                    }
                }
            }

            return $arReturn;

        };
        $arOfferGUIDToPriceAndQuantity = $funPreparsePriceXML();

        /**
         * Получим массив текущих XML_ID предложеник к ID
         *
         * @return array
         */
        $funGetCurrentOffers = function (){
            $arReturn = [];

            $rsElems = CIBlockElement::GetList(Array(), ['IBLOCK_ID'=>OFFERS_ID], false, false, ['ID', 'XML_ID']);
            while ($ob = $rsElems->Fetch()) {
                $arReturn[ $ob['XML_ID'] ] = $ob['ID'];
            }

            return $arReturn;
        };
        $arOfferXMLIDToID = $funGetCurrentOffers();

        /**
         * Обновим цены и остатки
         */
        $funUpdatePriceAndQuantity = function () use ($arOfferGUIDToPriceAndQuantity, $arOfferXMLIDToID){

            if( empty($arOfferGUIDToPriceAndQuantity) || empty($arOfferXMLIDToID) )
                throw new Exception('Подготовленный массив с ценами или массив с ID текущих предложений пуст!');

            foreach ($arOfferGUIDToPriceAndQuantity as $XML_ID=>$arData){
                if( !empty($arOfferXMLIDToID[ $XML_ID ]) ){

                    // Обновим кол-во
                    if( CCatalogProduct::GetByID($arOfferXMLIDToID[ $XML_ID ]) != false ){
                        CCatalogProduct::Update( $arOfferXMLIDToID[ $XML_ID ],  ['QUANTITY'=>10, 'VAT_ID' => 3, 'VAT_INCLUDED' => 'Y']);//$arData['QUANTITY']]);
                    }
                    else{
                        CCatalogProduct::Add( [
                            'ID'=>$arOfferXMLIDToID[ $XML_ID ],
                            'QUANTITY'=>10,//$arData['QUANTITY'],
                            'QUANTITY_RESERVED'=>0,
                            'VAT_ID' => 3,
                            'VAT_INCLUDED' => 'Y'
                        ]);
                    }

                    // Обновим цену
                    CPrice::SetBasePrice($arOfferXMLIDToID[ $XML_ID ], $arData['PRICE'], 'RUB');

                }
            }

        };
        $funUpdatePriceAndQuantity();

        $arFile = pathinfo(__FILE__);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - Импорт прошел успешно!'."\r\n", FILE_APPEND);


        unlink($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price.xml');
    }
}
catch (Exception $e){
    $arFile = pathinfo(__FILE__);
    file_put_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/'.$arFile['filename'].'.log', date('Y-m-d H:i:s').' - '.strip_tags($e->getMessage())."\r\n", FILE_APPEND);
}
finally
{
    if( file_get_contents($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price-import-running') == $currentTime )
        unlink($_SERVER['DOCUMENT_ROOT'].'/local/wbl/1c/price-import-running');
}

echo "Done\r\n";

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
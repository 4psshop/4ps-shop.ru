<?
\CModule::IncludeModule('catalog');
\CModule::IncludeModule('sale');
//AddEventHandler("search", "BeforeIndex", Array("MyWblClass", "BeforeIndexHandler"));

require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';

class MyWblClass
{
    // создаем обработчик события "BeforeIndex"
    function BeforeIndexHandler($arFields)
    {
        if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 2)
        {
            if( \CPrice::GetList([], ['PRODUCT_ID' => $arFields['ITEM_ID']])->SelectedRowsCount() < 1 )
            {
                $arFields['BODY'] = '';
                $arFields['TITLE'] = '';
            }
        }
        elseif($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 15)
        {
            $arFields['BODY'] = '';
            $arFields['TITLE'] = '';
        }
        return $arFields;
    }
}
<?
class WblCouponsAndTokens
{
    private static $instance = null;
    /**
     * @return WblCouponsAndTokens
     */
    public static function getInstance()
    {
        if( static::$instance == null )
            static::$instance = new static();
        return static::$instance;
    }

    /* ************** */
    /*     AGENTS     */
    /* ************** */

    /**
     * Создание нового агента
     * @param $name
     * @param $token
     * @return true
     */
    public function addAgent($name, $token)
    {
        $arAgents = $this->getAgents();

        $arAgents[ \Cutil::translit($name, "ru", array("replace_space" => "_", "replace_other" => "_")) ] = [
            'NAME' => $name,
            'TOKEN' => $token
        ];

        $this->setAgents($arAgents);
        return true;
    }

    /**
     * Редактирование агентов
     * @param $arData array
     * @return true
     */
    public function editAgents($arData)
    {
        /*
         * $arData
         * [
         *   'agent_code' => [
         *     'NAME' => ...,
         *     'TOKEN' => ...,
         *     'DELETE' => 'Y' || null
         *   ]
         * ]
         */
        $arAgents = $this->getAgents();

        foreach ($arData as $agentCode => $arAgentData)
        {
            if( !empty($arAgentData['NAME']) && !empty($arAgentData['TOKEN']) )
            {
                $arAgents[ $agentCode ]['NAME'] = $arAgentData['NAME'];
                $arAgents[ $agentCode ]['TOKEN'] = $arAgentData['TOKEN'];
            }

            if( $arAgentData['DELETE'] == 'Y' )
                unset( $arAgents[ $agentCode ] );
        }

        $this->setAgents($arAgents);
        return true;

    }

    /**
     * Получение агентов
     * @return array
     */
    public function getAgents()
    {
        $arAgents = json_decode(\Bitrix\Main\Config\Option::get('wbl_coupons_and_token', 'agents'), true);
        if( !is_array($arAgents) )
        {
            $arAgents = [];
        }

        return $arAgents;
    }

    /**
     * Сохраняет список агентов
     * @param $arAgents
     * @return true
     */
    private function setAgents($arAgents)
    {
        \Bitrix\Main\Config\Option::set('wbl_coupons_and_token', 'agents', json_encode($arAgents) );
        return true;
    }



    /* *************** */
    /*     COUPONS     */
    /* *************** */
    private $arCouponsList = []; // __construct

    /**
     * Возвращает список купонов
     * @return array
     */
    public function getCouponsList()
    {
        return $this->arCouponsList;
    }

    /**
     * Получене списка купонов по агентам
     * @return array
     */
    public function getAgentCoupons($agentCode)
    {
        $arAgentCoupons = json_decode(\Bitrix\Main\Config\Option::get('wbl_coupons_and_token', 'agent_coupons_'.$agentCode), true);
        if( !is_array($arAgentCoupons) )
        {
            $arAgentCoupons = [];
        }

        return $arAgentCoupons;
    }

    /**
     * Запись списка купонов по агентам
     * @param $arData
     */
    public function setAgentCoupons($arData)
    {
        /*
         * [
         *   'agent_code' => ['...', '...']
         * ]
         */

        foreach ($arData as $agentCode => $arCoupons)
        {
            \Bitrix\Main\Config\Option::set('wbl_coupons_and_token', 'agent_coupons_'.$agentCode, json_encode($arCoupons));
        }
    }



    /* ************* */
    /*     OTHER     */
    /* ************* */
    private function __sleep(){}
    private function __wakeup(){}
    private function __clone(){}
    private function __construct(){

        // get coupons list
        $sql = '
SELECT COUPONS.COUPON as NAME, COUPON_GROUP.NAME as GROUP_NAME
 FROM b_sale_discount_coupon as COUPONS
 INNER JOIN b_sale_discount as COUPON_GROUP
 ON COUPONS.DISCOUNT_ID = COUPON_GROUP.ID';
        $rsCoupons = \Bitrix\Main\Application::getInstance()->getConnectionPool()->getConnection()->query($sql);

        while ($ar = $rsCoupons->fetch())
        {
            $this->arCouponsList[ '['.$ar['GROUP_NAME'].'] '.$ar['NAME'] ] = $ar['NAME'];
        }
        ksort($this->arCouponsList);
    }
}
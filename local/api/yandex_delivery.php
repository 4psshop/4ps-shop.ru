<?
class WBLYaDelivery{

    function __construct()
    {
        $this->arMethodsApiKeys = json_decode('{
    "getPaymentMethods": "4c82d57b529d1c3f944ac31f72e5e5a4b18cd52709ddca786b781d7cfd4eb7ce",
    "getSenderOrders": "4c82d57b529d1c3f944ac31f72e5e5a44c2d3a86dc9f15b27079a8c608700250",
    "getSenderOrderLabel": "4c82d57b529d1c3f944ac31f72e5e5a4b06f606e0a951bf8baebd4e09dfb8a67",
    "getSenderParcelDocs": "4c82d57b529d1c3f944ac31f72e5e5a4a38a12e7104aa57665aa4db9be60ad49",
    "autocomplete": "4c82d57b529d1c3f944ac31f72e5e5a43e1d9fd946ef84688effb73e26fbb7b8",
    "getIndex": "4c82d57b529d1c3f944ac31f72e5e5a412ea0a015596d1fbaef2121a9a013155",
    "createOrder": "4c82d57b529d1c3f944ac31f72e5e5a454aeb805c977056e1941a96f323be4ba",
    "updateOrder": "4c82d57b529d1c3f944ac31f72e5e5a43ba0df815f26a550a9ea05477a540284",
    "deleteOrder": "4c82d57b529d1c3f944ac31f72e5e5a4fab3c79a74c188304532cf8e798eaa33",
    "getSenderOrderStatus": "4c82d57b529d1c3f944ac31f72e5e5a496d7fe480c2799c1f14f1a39dacbde54",
    "getSenderOrderStatuses": "4c82d57b529d1c3f944ac31f72e5e5a49c091f6ed70e5b5e292684bbd94e0149",
    "getSenderInfo": "4c82d57b529d1c3f944ac31f72e5e5a4ea29c271fdc74c73c00c8ff68b4172be",
    "getWarehouseInfo": "4c82d57b529d1c3f944ac31f72e5e5a496d63cfa4bb5514d17282ad471826df9",
    "getRequisiteInfo": "4c82d57b529d1c3f944ac31f72e5e5a48cff56a96315332a2f0ee1e619b54287",
    "getIntervals": "4c82d57b529d1c3f944ac31f72e5e5a4e13671b39a83e5b48fba23a357efaa03",
    "createWithdraw": "4c82d57b529d1c3f944ac31f72e5e5a4cd683aecabf21ec83ee03e0c5a900d62",
    "confirmSenderOrders": "4c82d57b529d1c3f944ac31f72e5e5a4192a35a8af6060388f2d9b61b4418913",
    "updateWithdraw": "4c82d57b529d1c3f944ac31f72e5e5a48506aea4b2d041e47a15c0e49c562436",
    "createImport": "4c82d57b529d1c3f944ac31f72e5e5a4771adb508892ca7cc9f68b3baf4df03c",
    "updateImport": "4c82d57b529d1c3f944ac31f72e5e5a45cf4aa0129260c38aa2706b871122a6e",
    "getDeliveries": "4c82d57b529d1c3f944ac31f72e5e5a475dcd8f867dbe77ba8ecf17b365a2eb3",
    "getOrderInfo": "4c82d57b529d1c3f944ac31f72e5e5a4f87be591e243100ced989a5816ffee72",
    "searchDeliveryList": "4c82d57b529d1c3f944ac31f72e5e5a4dd8545fff1df8a75b16561fb6bbef6c7",
    "confirmSenderParcels": "4c82d57b529d1c3f944ac31f72e5e5a4d9d24847fb1f312e61fbaac636927e8d",
    "searchSenderOrdersStatuses": "4c82d57b529d1c3f944ac31f72e5e5a43072f9464b376e5fcbdbd2337195c07e",
    "getImports": "4c82d57b529d1c3f944ac31f72e5e5a4723686350462cd5902b9c2cba7d9932a",
    "getWithdraws": "4c82d57b529d1c3f944ac31f72e5e5a490a4d28ebd09816ac3e73e45f3af29d8",
    "cancelImport": "4c82d57b529d1c3f944ac31f72e5e5a438e5f66066e04bb6b3f1bf857558ea58",
    "cancelWithdraw": "4c82d57b529d1c3f944ac31f72e5e5a4836b4fff9eff956e42169307e8d9154c"
}', true);

        $this->arClientKeys = json_decode('{
    "client": {
        "id": 51280
    },
    "warehouses": [
        {
            "id": 33211,
            "name": "Основной"
        }
    ],
    "senders": [
        {
            "id": 38275,
            "name": "ООО «Здоровье и Спорт»"
        }
    ],
    "requisites": [
        {
            "id": 18493,
            "name": "«Здоровье и Спорт»"
        }
    ]
}', true);
    }

    var $arMethodsApiKeys = [];
    var $arClientKeys = [];

    /**
     * Собирает cUrl запрос и отправляет его
     *
     * @param $methodName
     * @param $arParams
     * @return mixed
     */
     public function query($methodName, $arParams){
        $arParams = array_merge(
            $arParams,
            [
                'client_id' => $this->arClientKeys['client']['id'],
                'sender_id' => $this->arClientKeys['senders'][0]['id']
            ]
        );

         $GLOBALS['getPostValues'] = &$getPostValues;
         $getPostValues = function ($data)
         {

             if (!is_array($data)) return $data;
             ksort($data);
             return join('', array_map(function($k)
             {
                 return $GLOBALS['getPostValues']($k);
             },
                 $data));
         };

        ksort($arParams);
         $arParams['secret_key'] =  md5($GLOBALS['getPostValues']($arParams) . $this->arMethodsApiKeys[$methodName]);

        $ch = curl_init();
        curl_setopt_array($ch,
            [
                CURLOPT_URL => 'https://delivery.yandex.ru/api/last/'.$methodName,
                CURLOPT_POST => true,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_TIMEOUT => 10,
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/x-www-form-urlencoded'
                ],
                CURLOPT_POSTFIELDS => http_build_query($arParams)
            ]
        );
        $re = curl_exec($ch);
        curl_close($ch);
        return json_decode($re, true);
    }

    /**
     * Парсинг ответа по списку доставок<br/>
     * <b>Ответ:</b><br/>
     [<br/>
    'NAME' => 'СДЭК',<br/>
    'DELIVERY_DATES' => [<br/>
    &middot;&middot;'FROM' => '2018-07-04',<br/>
    &middot;&middot;'TO' => '2018-07-06'<br/>
    &middot;&middot;],<br/>
    'PRICE' => 247,<br/>
    'TIME_INTERVAL' => [<br/>
    &middot;&middot;'FROM' => '09:00:00',<br/>
    &middot;&middot;'TO' => '18:00:00'<br/>
    &middot;&middot;],<br/>
    // В случае доставки курьером - дополнительные поля<br/>
    'DELIVERY_ADDRESS' => '142400, Московская область, Ногинск, ул 2-я Глуховская, 8',<br/>
    'PHONE' => 78002504434<br/>
     'COORDS' => '55.834911, 38.429452'<br/>
    ]<br/>
     *
     * @param $arData
     * @return array
     */
    public function parseDeliveryList($arData){
         $arResult = [];

         if( $arData['status'] == 'ok' ){
             foreach ($arData['data'] as $arDelivery){

                 $arYaDeliveryData = [
                     'tariff' => $arDelivery['tariffId'],
                     'delivery' => $arDelivery['delivery']['id'],
                     'interval' => $arDelivery['deliveryIntervals']['0']['id']
                 ];

                 $arTmp = [
                     'NAME' => $arDelivery['delivery']['name'],
                     'DELIVERY_DATES' => [
                         'FROM' => $arDelivery['delivery_date'][0],
                         'TO' => end($arDelivery['delivery_date'])
                     ],
                     'PRICE' => $arDelivery['costWithRules'],
                     'TIME_INTERVAL' => [
                         'FROM' => $arDelivery['deliveryIntervals'][0]['from'],
                         'TO' => $arDelivery['deliveryIntervals'][0]['to'],
                     ]
                 ];

                 if( empty($arDelivery['pickupPoints']) ){
                     // Самовывоз
                     $arTmp['YA_DELIVERY_DATA'] = $arYaDeliveryData;
                     $arResult[] = $arTmp;
                 }
                 else{
                     // Курьер
                     foreach ($arDelivery['pickupPoints'] as $arPickupPoint){
                         $arYaDeliveryData['pickuppoint'] = $arPickupPoint['id'];
                         $arResult[] = array_merge(
                             $arTmp,
                             [
                                 'DELIVERY_ADDRESS' =>  $arPickupPoint['full_address'],
                                 'PHONE' => $arPickupPoint['phones'][0]['number'],
                                 'DELIVERY_DATES' => [
                                     'FROM' => $arPickupPoint['delivery_date'][0],
                                     'TO' => end($arPickupPoint['delivery_date'])
                                 ],
                                 'COORDS' => $arPickupPoint['lat'].', '.$arPickupPoint['lng'],
                                 'YA_DELIVERY_DATA' => $arYaDeliveryData
                             ]
                         ) ;
                     }
                 }
             }
         }

         return $arResult;
    }


    /**
     * Отправка заказа в ЯДоставку (создает черновик)
     *
     * @param $intOrderID
     * @return mixed
     */
    public function sendOrderToYaDelivery($intOrderID){
        if( $intOrderID ){
            $arParams['order_num'] = $intOrderID;
            $arParams['order_requisite'] = $this->arClientKeys['requisites'][0]['id'];
            $arParams['order_warehouse'] = $this->arClientKeys['warehouses'][0]['id'];

            $strShipDate = '';
            switch (date('w')){
                case 0:
                    $strShipDate = date('Y-m-d', strtotime('now +'.(2-date('w')).'day' ));
                    break;
                case 1:
                    if( date('G') < 16 )
                        $strShipDate = date('Y-m-d', strtotime('now +'.(2-date('w')).'day' ));
                    else
                        $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
                    break;
                case 2:
                case 3:
                    $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
                    break;
                case 4:
                    if( date('G') < 16 )
                        $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
                    else
                        $strShipDate = date('Y-m-d', strtotime('now +'.(9-date('w')).'day' ));
                    break;
                case 5:
                case 6:
                    $strShipDate = date('Y-m-d', strtotime('now +'.(9-date('w')).'day' ));
                    break;
            }
            $arParams['order_shipment_date'] = $strShipDate;

            global $DB;

            /* Получим данные по заказу и передадим данные по стоимость доставки */
            $rsOrder = $DB->Query('SELECT * FROM wbl_order WHERE ID="'.$intOrderID.'"');
            $arOrder = $rsOrder->Fetch();
            $arParams['order_delivery_cost'] = $arOrder['DELIVERY_PRICE'];
            $arParams['is_manual_delivery_cost'] = 1;
            /* / Получим данные по заказу и передадим данные по стоимость доставки */

            /* Получим корзину */
            $rsGetBasket = $DB->Query('SELECT * FROM wbl_basket WHERE ORDER_ID="'.$intOrderID.'"');
            $arBasket = [];
            while ($ar = $rsGetBasket->Fetch()){
                $arBasket[ $ar['PRODUCT_ID'] ] = $ar;
            }
            /* / Получим корзину */

            if( !empty($arBasket) ){

                CModule::IncludeModule('iblock');

                $rsElems = CIBlockElement::GetList(Array(), ['IBLOCK_ID'=>1, 'ID'=>array_keys($arBasket)]);
                while ($ob = $rsElems->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $arFields['PROPERTIES'] = $ob->GetProperties();
                    $arBasket[ $arFields['ID'] ]['PRODUCT'] = $arFields;
                }

                /* Расчитаем вес, габариты, данные о товарах и объявленую стоимость */
                $intWeight = 0;
                $intWidth = 0;
                $intHeight = 0;
                $intLength = 0;
                $intBasketPriceSum = 0;


                $arToCalculate = [];
                foreach ($arBasket as $arItem){

                    $arParams['order_items'][] = [
                        'orderitem_name' => $arItem['PRODUCT']['NAME'],
                        'orderitem_cost' => $arItem['PRICE'],
                        'orderitem_article' => $arItem['PRODUCT']['PROPERTIES']['ARTICLE']['VALUE'],
                        'orderitem_quantity' => $arItem['QUANTITY'],
                        'orderitem_vat_value' => 6 //НДС не облагается
                    ];

                    $arToCalculate[ $arItem['PRODUCT_ID'] ] = [
                        'ID' => $arItem['PRODUCT_ID'],
                        'PRICE' => $arItem['PRICE'],
                        'QUANTITY' => $arItem['QUANTITY'],
                    ];

                    $intBasketPriceSum += ( $arItem['PRICE'] * $arItem['QUANTITY'] );
                    
                }

                $arToCalculate = WBL::calculateOrderSize($arToCalculate);

                $arParams['order_weight'] = $arToCalculate['WEIGHT'];
                $arParams['order_length'] = $arToCalculate['LENGTH'];
                $arParams['order_width'] = $arToCalculate['WIDTH'];
                $arParams['order_height'] = $arToCalculate['HEIGHT'];
                $arParams['order_assessed_value'] = $intBasketPriceSum;

                /* / Расчитаем вес, габариты, данные о товарах и объявленую стоимость */

                /* Получим данные по заказчику */
                $rsUser = $DB->Query('SELECT NAME, LAST_NAME, SECOND_NAME FROM b_user WHERE ID="'.$arOrder['USER_ID'].'"');
                $arUser = $rsUser->Fetch();

                $arParams['recipient']['first_name'] = $arUser['NAME'];
                $arParams['recipient']['last_name'] = $arUser['LAST_NAME'];
                $arParams['recipient']['middle_name'] = (!empty($arUser['SECOND_NAME']))?$arUser['SECOND_NAME']:'Неуказана';
                $arParams['recipient']['phone'] = preg_replace('/[^\d]/', '', $arOrder['PHONE']);
                /* / Получим данные по заказчику */

                /* Получим адрес доставки */
                $ch = curl_init("https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address");
                curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Accept: application/json',
                    'Authorization: Token 8c65e1b5d95946c2f9d106cfedf9b6e62704765b'
                ));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array('query'=>$arOrder['ADDRESS'], 'count'=>'1')));
                $arAddress = curl_exec($ch);
                $arAddress = json_decode($arAddress, true);
                $arAddress = $arAddress['suggestions'][0]['data'];

                preg_match('/(?:\, кв\/офис (.*))$/', $arOrder['ADDRESS'], $matches);

                $arParams['deliverypoint'] = [
                    'index' => $arOrder['ZIP'],
                    'city' => ( !empty($arAddress['settlement']) )?$arAddress['settlement']:$arAddress['city'],
                    'street' => $arAddress['street_with_type'],
                    'house' => $arAddress['house'],
                    'housing' => $arAddress['block'],
                    'floor' => $matches[1]
                ];
                /* / Получим адрес доставки */

                /* Получим данные выбранной службы доставки */
                $rsGetYaDeliveryData = $DB->Query('SELECT DELIVERY_DATA FROM wbl_delivery_yandex WHERE ID="'.$arOrder['DELIVERY_YANDEX_ID'].'"');
                if( $rsGetYaDeliveryData->SelectedRowsCount() > 0 ){
                    $arGetYaDeliveryData = $rsGetYaDeliveryData->Fetch();
                    $arParams['delivery'] = unserialize( base64_decode( $arGetYaDeliveryData['DELIVERY_DATA'] ) );
                }
                /* / Получим данные выбранной службы доставки */

                $arParams['order_amount_prepaid'] = ( $arParams['order_delivery_cost'] + $arParams['order_assessed_value'] );


                $arResult = $this->query('createOrder', $arParams);
                if( !empty($arResult['data']['order']['id']) ){
                    global $DB;
                    $DB->Query('UPDATE wbl_order SET YAD_ORDER_ID="'.$arResult['data']['order']['id'].'" WHERE ID="'.$intOrderID.'"');
                }
                return $arResult;
            }



        }
    }

    /**
     * Получить ярлык для заказа
     *
     * @param $intOrderID
     * @return bool|string
     */
    public function getSenderOrderLabel($intOrderID){

        $arData = '';

        global $DB;
        $intYADOrderID = $DB->Query('SELECT WO.YAD_ORDER_ID, WO.STATUS FROM wbl_order AS WO INNER JOIN wbl_delivery_yandex AS WDY ON WO.DELIVERY_YANDEX_ID=WDY.ID AND WO.ID="'.$intOrderID.'"');
        if( $intYADOrderID->SelectedRowsCount() > 0 ){
            $intYADOrderID = $intYADOrderID->Fetch();
            if( $intYADOrderID['STATUS'] == 'OTMENEN' ){
                $intYADOrderID = '-';
            }
            else{
                $intYADOrderID = $intYADOrderID['YAD_ORDER_ID'];
            }
        }

        /*
        if( !is_numeric($intYADOrderID) && $intYADOrderID != '-' ){
            $arOrders = $this->query('getSenderOrders', []);
            if( !empty($arOrders['data']['orders']) ){
                foreach (array_reverse($arOrders['data']['orders']) as $arOrder){

                    if( !empty($arOrder['delivery']['unique_name']) && !empty($arOrder['recipient']['last_name']) && !empty($arOrder['items']) ){
                        global $DB;
                        $DB->Query('UPDATE wbl_order SET YAD_ORDER_ID="'.$arOrder['order_id'].'" WHERE ID="'.$arOrder['num'].'"');

                        if( $arOrder['num'] == $intOrderID ){
                            $intYADOrderID = $arOrder['order_id'];
                        }
                    }
                }
            }
        }
        */

        if( is_numeric($intYADOrderID) ){
            $arOrderMark = $this->query('getSenderOrderLabel', [
                'order_id' => $intYADOrderID,
                'is_raw' => 'PDF'
            ]);
            if( $arOrderMark['data'] ){
                $arData = $arOrderMark['data'];
            }
        }


        return base64_decode($arData);
    }

    /**
     * Получить статус от ЯДоставки
     *
     * @param $intOrderID
     * @return array|string
     */
    public function getOrderStatus($intOrderID){
        $arData = '';

        global $DB;
        $intYADOrderID = $DB->Query('SELECT WO.YAD_ORDER_ID, WO.STATUS FROM wbl_order AS WO INNER JOIN wbl_delivery_yandex AS WDY ON WO.DELIVERY_YANDEX_ID=WDY.ID AND WO.ID="'.$intOrderID.'"');
        if( $intYADOrderID->SelectedRowsCount() > 0 ){
            $intYADOrderID = $intYADOrderID->Fetch();
            if( $intYADOrderID['STATUS'] == 'OTMENEN' ){
                $intYADOrderID = '-';
            }
            else{
                $intYADOrderID = $intYADOrderID['YAD_ORDER_ID'];
            }
        }

        /*
        if( !is_numeric($intYADOrderID) && $intYADOrderID != '-' ){
            $arOrders = $this->query('getSenderOrders', []);
            if( !empty($arOrders['data']['orders']) ){
                foreach (array_reverse($arOrders['data']['orders']) as $arOrder){

                    if( !empty($arOrder['delivery']['unique_name']) && !empty($arOrder['recipient']['last_name']) && !empty($arOrder['items']) ){
                        global $DB;
                        $DB->Query('UPDATE wbl_order SET YAD_ORDER_ID="'.$arOrder['order_id'].'" WHERE ID="'.$arOrder['num'].'"');

                        if( $arOrder['num'] == $intOrderID ){
                            $intYADOrderID = $arOrder['order_id'];
                        }
                    }
                }
            }
        }
        */

        if( is_numeric($intYADOrderID) ){
            $arOrderMark = $this->query('getSenderOrderStatus', [
                'order_id' => $intYADOrderID
            ]);
            if( $arOrderMark['data'] ){
                $arData = $arOrderMark['data'];
            }
        }

        $arCodeToText = array ( 'DRAFT' => 'заказ доступен для редактирования.', 'CREATED' => 'заказ проверен и отправлен в службу доставки.', 'SENDER_SENT' => 'заказ ожидает подтверждения от службы доставки.', 'DELIVERY_LOADED' => 'заказ подтвержден службой доставки.', 'SENDER_WAIT_FULFILMENT' => 'заказ ожидается на едином складе.', 'SENDER_WAIT_DELIVERY' => 'заказ ожидается в службе доставки.', 'FULFILMENT_LOADED' => 'заказ подтвержден единым складом.', 'FULFILMENT_ARRIVED' => 'заказ находится на едином складе.', 'FULFILMENT_PREPARED' => 'заказ готов к передаче в службу доставки.', 'FULFILMENT_TRANSMITTED' => 'заказ передан в службу доставки.', 'DELIVERY_AT_START' => 'заказ находится на складе службы доставки.', 'DELIVERY_TRANSPORTATION' => 'заказ доставляется.', 'DELIVERY_ARRIVED' => 'заказ находится в населенном пункте получателя.', 'DELIVERY_TRANSPORTATION_RECIPIENT' => 'заказ доставляется по населенному пункту получателя.', 'DELIVERY_ARRIVED_PICKUP_POINT' => 'заказ находится в пункте самовывоза.', 'DELIVERY_DELIVERED' => 'заказ доставлен получателю.', 'RETURN_PREPARING' => 'заказ готовится к возврату.', 'RETURN_ARRIVED_DELIVERY' => 'заказ возвращен на склад службы доставки.', 'RETURN_ARRIVED_FULFILMENT' => 'заказ возвращен на единый склад.', 'RETURN_PREPARING_SENDER' => 'заказ возвращается в магазин.', 'RETURN_RETURNED' => 'заказ возвращен в магазин.', 'LOST' => 'заказ утерян в процессе доставки.', 'UNEXPECTED' => 'статус заказа уточняется.', 'CANCELED' => 'заказ отменен.', 'ERROR' => 'произошла ошибка.', );

        $arData = ['CODE'=>$arData, 'TEXT'=>$arCodeToText[$arData]];
        return $arData;
    }
}
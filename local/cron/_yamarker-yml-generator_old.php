<?
set_time_limit(60*30);

if( empty($_SERVER["DOCUMENT_ROOT"]) )
    $_SERVER["DOCUMENT_ROOT"] = dirname(dirname(dirname(__FILE__)));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');

$retXML = '';

// Получим все продукты и ценовые предложения
$rsProducts = \Bitrix\Catalog\ProductTable::getList([
    'select' => [
        'PRODUCT_'=>'*',
        'PRICE_'=>'PRICE.*'
    ],
    'filter' => [
        '>PRODUCT_QUANTITY' => 0,
        '><PRICE_PRICE'=>[0, 15000]
    ],
    'runtime' => [
        new \Bitrix\Main\Entity\ReferenceField(
            'PRICE',
            '\Bitrix\Catalog\PriceTable',
            [
                '=this.ID'=>'ref.PRODUCT_ID'
            ],
            ['join_type'=>'INNER']
        )
    ]
]);

$arElemIDToProd = [];

if( $rsProducts->getSelectedRowsCount() > 0 )
{
    $retXML .= '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';

    $retXML .= '<yml_catalog date="'.date('Y-m-d H-i').'"><shop>';
    $retXML .= '<name>4ПроСпорт</name>';
    $retXML .= '<company>4ПроСпорт</company>';
    $retXML .= '<url>http://4ps-shop.ru</url>';
    $retXML .= '<currencies><currency id="RUR" rate="1" plus="0"/></currencies>';

    while ($ar = $rsProducts->fetch()){
        $arElemIDToProd[ $ar['PRODUCT_ID'] ] = $ar;
    }

    // Получим все элементы ИБ

    // Получим список всех подразделов
    $arAllowSectionsID = [];

    $retXML .= '<categories>';
    $rsSections = CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), array('IBLOCK_ID' => 2), false, ['ID', 'NAME', 'IBLOCK_SECTION_ID']);
    while ($arSect = $rsSections->Fetch()) {
        $arAllowSectionsID[ $arSect['ID'] ] = $arSect['ID'];

        $retXML .= '<category id="'.$arSect['ID'].'" '.( !empty($arSect['IBLOCK_SECTION_ID'])?'parentId="'.$arSect['IBLOCK_SECTION_ID'].'"':'' ).'>'.$arSect['NAME'].'</category>';
    }
    $retXML .= '</categories>';
    // / Получим список всех подразделов




    // Получим список всех элементов и выберем только те, на которые есть цены
    $arElemIDToSectionID = [];
    $rsElems = CIBlockElement::GetList(Array('IBLOCK_ID'=>'ASC'), ['IBLOCK_ID'=>[2, 15]], false, false, ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK']);
    $arElems = array();
    while ($arFields = $rsElems->Fetch()) {
        switch ($arFields['IBLOCK_ID']){
            case 2:
                if( in_array($arFields['IBLOCK_SECTION_ID'], $arAllowSectionsID)  ){
                    $arElems[ $arFields['IBLOCK_SECTION_ID'] ]['ITEMS'][$arFields['ID']] = ['ID'=>$arFields['ID']];
                    $arElemIDToSectionID[ $arFields['ID'] ] = $arFields['IBLOCK_SECTION_ID'];
                }
                break;

            case 15:
                if( !empty( $arElemIDToSectionID[ $arFields['PROPERTY_CML2_LINK_VALUE'] ] ) && !empty($arElemIDToProd[ $arFields['ID'] ]) ){
                    $arElems[ $arElemIDToSectionID[ $arFields['PROPERTY_CML2_LINK_VALUE'] ] ]['ITEMS'][$arFields['PROPERTY_CML2_LINK_VALUE']]['OFFERS'][ $arFields['ID'] ] = $arFields['ID'];
                }
                break;
        }
    }

    $arValidIDList = [];
    foreach ($arElems as $sectionID=>$arItems1){
        foreach ($arItems1 as $arItems){
            foreach ($arItems as $itemID=>$arItem){

                if( empty($arItem['OFFERS']) ){

                    if( !empty( $arElemIDToProd[ $arItem['ID'] ] ) ){
                        $arValidIDList = array_merge($arValidIDList, [$arItem['ID']]);
                    }

                }
                else{
                    $arValidIDList = array_merge($arValidIDList, [$arItem['ID']], $arItem['OFFERS']);
                }

            }
        }
    }
    unset($arElems, $arElemIDToSectionID, $arAllowSectionsID);
    // Получим список всех элементов и выберем только те, на которые есть цены


    // Получим описания и св-ва элементов и запишем в YML
    $arElements = [];
    $rsElems = CIBlockElement::GetList(Array('IBLOCK_ID'=>'ASC'), ['IBLOCK_ID'=>[2, 15], 'ID'=>$arValidIDList], false, false, ['ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'PREVIEW_TEXT']);
    while ($ob = $rsElems->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arFields['PROPERTIES'] = $ob->GetProperties();
        switch ($arFields['IBLOCK_ID']){
            case 2:
                $arElements[ $arFields['ID'] ] = $arFields;
                break;

            case 15:
                $arElements[ $arFields['PROPERTIES']['CML2_LINK']['VALUE'] ]['OFFERS'][ $arFields['ID'] ] = $arFields;
                break;

        }
    }

    $retXML .= '<offers>';

    foreach ($arElements as $arItem)
    {

        if( !empty($arItem['OFFERS']) )
        {
            // OFFERS
            foreach ($arItem['OFFERS'] as $arOffer)
            {
                $retXML .= "\r\n".'<offer id="'.$arItem['ID'].'.'.$arOffer['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'#'.$arOffer['ID'].'</url>
                <price>'.$arElemIDToProd[ $arOffer['ID'] ]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arOffer['NAME'].'</name>';

                $retXML .= "\r\n".'<description>'.strip_tags( str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT']) ).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


                if( intval($arItem['PREVIEW_PICTURE']) > 0 ){
                    $retXML .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
                }

                if( $arItem['PROPERTIES']['ARTICUL']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';

                if( $arItem['PROPERTIES']['VOLTAGE']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';

                if( $arItem['PROPERTIES']['BRAND']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';

                if( $arItem['PROPERTIES']['GENDER']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';

                if( $arOffer['PROPERTIES']['SIZE1']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Цвет">'.$arOffer['PROPERTIES']['SIZE1']['VALUE'].'</param>';

                if( $arOffer['PROPERTIES']['SIZE2']['VALUE'] )
                    $retXML .= "\r\n".'<param name="Размер">'.$arOffer['PROPERTIES']['SIZE2']['VALUE'].'</param>';


                $retXML .= '</offer>';
            }

        }
        else
        {
            // PRODUCT
            $retXML .= "\r\n".'<offer id="'.$arItem['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'</url>
                <price>'.$arElemIDToProd[ $arItem['ID'] ]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arItem['NAME'].'</name>';

            $retXML .= "\r\n".'<description>'.strip_tags( str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT']) ).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


            if( intval($arItem['PREVIEW_PICTURE']) > 0 ){
                $retXML .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
            }

            if( $arItem['PROPERTIES']['ARTICUL']['VALUE'] )
                $retXML .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';

            if( $arItem['PROPERTIES']['VOLTAGE']['VALUE'] )
                $retXML .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';

            if( $arItem['PROPERTIES']['BRAND']['VALUE'] )
                $retXML .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';

            if( $arItem['PROPERTIES']['GENDER']['VALUE'] )
                $retXML .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';


            $retXML .= '</offer>';
        }
    }

    $retXML .= '</offers>';
    // / Получим описания и св-ва элементов и запишем в YML

    $retXML .= '</shop></yml_catalog>';
}

file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_market_yml.xml', $retXML);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
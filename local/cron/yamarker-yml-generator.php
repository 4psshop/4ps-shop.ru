<?
set_time_limit(60*30);

if( empty($_SERVER["DOCUMENT_ROOT"]) )
    $_SERVER["DOCUMENT_ROOT"] = dirname(dirname(dirname(__FILE__)));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

\Bitrix\Main\Loader::includeModule('iblock');
\Bitrix\Main\Loader::includeModule('catalog');


class YMLGenerator
{

    protected $sXmlHeader = '';
    protected $sXmlFooter = '</shop></yml_catalog>';

    protected $sMarketXml = '';

    protected $aGoodsXml = [];
    protected $aGoodsSectionToRootSection = [];

    /**
     * YMLGenerator constructor.
     */
    public function __construct()
    {
        $this->sXmlHeader .= '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
        $this->sXmlHeader .= '<yml_catalog date="'.date('Y-m-d H-i').'"><shop>';
        $this->sXmlHeader .= '<name>4ПроСпорт</name>';
        $this->sXmlHeader .= '<company>4ПроСпорт</company>';
        $this->sXmlHeader .= '<url>http://4ps-shop.ru</url>';
        $this->sXmlHeader .= '<currencies><currency id="RUR" rate="1" plus="0"/></currencies>';
    }

    /**
     *
     */
    public function execute()
    {
        $this->makeMarketXml();
        $this->makeGoodsXml();
    }

    /**
     *
     */
    protected function makeMarketXml()
    {
        $this->fillMarketSections();
        $this->fillMarketProducts();
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_market_yml.xml', $this->sXmlHeader);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_market_yml.xml', $this->sMarketXml, FILE_APPEND);
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_market_yml.xml', $this->sXmlFooter, FILE_APPEND);
    }

    /**
     *
     */
    protected function fillMarketSections()
    {
        $this->sMarketXml .= '<categories>';
        $rsSections = \CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), array('IBLOCK_ID' => 2), false, ['ID', 'NAME', 'IBLOCK_SECTION_ID']);
        while ($arSect = $rsSections->Fetch()) {

            $this->sMarketXml .= '<category id="'.$arSect['ID'].'" '.(!empty($arSect['IBLOCK_SECTION_ID']) ? 'parentId="'.$arSect['IBLOCK_SECTION_ID'].'"' : '').'>'.$arSect['NAME'].'</category>';
        }
        $this->sMarketXml .= '</categories>';
    }

    /**
     *
     */
    protected function fillMarketProducts()
    {
        $this->sMarketXml .= '<offers>';
        $arElements = $this->getProducts();
        $arElemIDToProd = $this->getPrices();

        foreach ($arElements as $arItem) {

            if( (int)$arItem['PREVIEW_PICTURE'] < 1 )
            {
                continue;
            }

            if (!empty($arItem['OFFERS'])) {
                // OFFERS
                foreach ($arItem['OFFERS'] as $arOffer) {
                    $this->sMarketXml .= "\r\n".'<offer id="'.$arItem['ID'].'.'.$arOffer['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'#'.$arOffer['ID'].'</url>
                <price>'.$arElemIDToProd[$arOffer['ID']]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arOffer['NAME'].'</name>';

                    $this->sMarketXml .= "\r\n".'<description>'.strip_tags(str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT'])).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


                    if (intval($arItem['PREVIEW_PICTURE']) > 0) {
                        $this->sMarketXml .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
                    }

                    if ($arItem['PROPERTIES']['ARTICUL']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['VOLTAGE']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['BRAND']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['GENDER']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';
                    }

                    if ($arOffer['PROPERTIES']['SIZE1']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Цвет">'.$arOffer['PROPERTIES']['SIZE1']['VALUE'].'</param>';
                    }

                    if ($arOffer['PROPERTIES']['SIZE2']['VALUE']) {
                        $this->sMarketXml .= "\r\n".'<param name="Размер">'.$arOffer['PROPERTIES']['SIZE2']['VALUE'].'</param>';
                    }


                    $this->sMarketXml .= '</offer>';
                }

            } else {
                // PRODUCT
                $this->sMarketXml .= "\r\n".'<offer id="'.$arItem['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'</url>
                <price>'.$arElemIDToProd[$arItem['ID']]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arItem['NAME'].'</name>';

                $this->sMarketXml .= "\r\n".'<description>'.strip_tags(str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT'])).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


                if (intval($arItem['PREVIEW_PICTURE']) > 0) {
                    $this->sMarketXml .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
                }

                if ($arItem['PROPERTIES']['ARTICUL']['VALUE']) {
                    $this->sMarketXml .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['VOLTAGE']['VALUE']) {
                    $this->sMarketXml .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['BRAND']['VALUE']) {
                    $this->sMarketXml .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['GENDER']['VALUE']) {
                    $this->sMarketXml .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';
                }


                $this->sMarketXml .= '</offer>';
            }
        }

        $this->sMarketXml .= '</offers>';
    }

    /**
     * @return array
     */
    protected function getProducts()
    {
        static $arElements = null;

        if (is_null($arElements)) {
            $arElements = [];
            $arElemIDToProd = $this->getPrices();

            // Получим список всех элементов и выберем только те, на которые есть цены
            $arElemIDToSectionID = [];
            $rsElems = \CIBlockElement::GetList(Array('IBLOCK_ID' => 'ASC'), ['IBLOCK_ID' => [2, 15]], false, false, ['ID', 'IBLOCK_ID', 'IBLOCK_SECTION_ID', 'PROPERTY_CML2_LINK']);
            $arElems = array();
            while ($arFields = $rsElems->Fetch()) {
                switch ($arFields['IBLOCK_ID']) {
                    case 2:
                        $arElems[$arFields['IBLOCK_SECTION_ID']]['ITEMS'][$arFields['ID']] = ['ID' => $arFields['ID']];
                        $arElemIDToSectionID[$arFields['ID']] = $arFields['IBLOCK_SECTION_ID'];
                        break;

                    case 15:
                        if (!empty($arElemIDToSectionID[$arFields['PROPERTY_CML2_LINK_VALUE']]) && !empty($arElemIDToProd[$arFields['ID']])) {
                            $arElems[$arElemIDToSectionID[$arFields['PROPERTY_CML2_LINK_VALUE']]]['ITEMS'][$arFields['PROPERTY_CML2_LINK_VALUE']]['OFFERS'][$arFields['ID']] = $arFields['ID'];
                        }
                        break;
                }
            }

            $arValidIDList = [];
            foreach ($arElems as $sectionID => $arItems1) {
                foreach ($arItems1 as $arItems) {
                    foreach ($arItems as $itemID => $arItem) {

                        if (empty($arItem['OFFERS'])) {

                            if (!empty($arElemIDToProd[$arItem['ID']])) {
                                $arValidIDList = array_merge($arValidIDList, [$arItem['ID']]);
                            }

                        } else {
                            $arValidIDList = array_merge($arValidIDList, [$arItem['ID']], $arItem['OFFERS']);
                        }

                    }
                }
            }
            unset($arElems, $arElemIDToSectionID, $arAllowSectionsID);
            // Получим список всех элементов и выберем только те, на которые есть цены


            // Получим описания и св-ва элементов и запишем в YML
            $arElements = [];
            $rsElems = \CIBlockElement::GetList(Array('IBLOCK_ID' => 'ASC'), ['IBLOCK_ID' => [2, 15], 'ID' => $arValidIDList], false, false, ['ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_PICTURE', 'IBLOCK_SECTION_ID', 'DETAIL_PAGE_URL', 'PREVIEW_TEXT']);
            while ($ob = $rsElems->GetNextElement()) {
                $arFields = $ob->GetFields();
                $arFields['PROPERTIES'] = $ob->GetProperties();
                switch ($arFields['IBLOCK_ID']) {
                    case 2:
                        $arElements[$arFields['ID']] = $arFields;
                        break;

                    case 15:
                        $arElements[$arFields['PROPERTIES']['CML2_LINK']['VALUE']]['OFFERS'][$arFields['ID']] = $arFields;
                        break;

                }
            }
        }

        return $arElements;
    }

    /**
     * @return array
     */
    protected function getPrices()
    {
        static $aElemIdToPrice = null;

        if (is_null($aElemIdToPrice)) {
            $rsProducts = \Bitrix\Catalog\ProductTable::getList([
                'select' => [
                    'PRODUCT_' => '*',
                    'PRICE_' => 'PRICE.*'
                ],
                'filter' => [
                    '>PRODUCT_QUANTITY' => 0,
                    '><PRICE_PRICE' => [0, 15000]
                ],
                'runtime' => [
                    new \Bitrix\Main\Entity\ReferenceField(
                        'PRICE',
                        '\Bitrix\Catalog\PriceTable',
                        [
                            '=this.ID' => 'ref.PRODUCT_ID'
                        ],
                        ['join_type' => 'INNER']
                    )
                ]
            ]);
            $aElemIdToPrice = [];
            while ($a = $rsProducts->fetch()) {
                $aElemIdToPrice[$a['PRODUCT_ID']] = $a;
            }
        }

        return $aElemIdToPrice;
    }


    protected function makeGoodsXml()
    {
        $this->fillGoodsSections();
        $this->fillGoodsProducts();

        foreach ($this->getRootSections() as $a) {
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_goods_'.$a['ID'].'_yml.xml', $this->sXmlHeader);
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_goods_'.$a['ID'].'_yml.xml', $this->aGoodsXml[$a['ID']], FILE_APPEND);
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/ya_goods_'.$a['ID'].'_yml.xml', $this->sXmlFooter, FILE_APPEND);
        }

    }

    /**
     *
     */
    protected function fillGoodsSections()
    {
        foreach ($this->getRootSections() as $aRootSection) {
            $this->aGoodsXml[$aRootSection['ID']] .= '<categories>';
            $rsSections = \CIBlockSection::GetList(array('DEPTH_LEVEL' => 'ASC'), array(
                'IBLOCK_ID' => 2,
                '>=LEFT_MARGIN' => $aRootSection['LEFT_MARGIN'],
                '<RIGHT_MARGIN' => $aRootSection['RIGHT_MARGIN']
            ), false, ['ID', 'NAME', 'IBLOCK_SECTION_ID']);
            while ($arSect = $rsSections->Fetch()) {

                $this->aGoodsXml[$aRootSection['ID']] .= '<category id="'.$arSect['ID'].'" '.(!empty($arSect['IBLOCK_SECTION_ID']) ? 'parentId="'.$arSect['IBLOCK_SECTION_ID'].'"' : '').'>'.$arSect['NAME'].'</category>';
                $this->aGoodsSectionToRootSection[$arSect['ID']] = $aRootSection['ID'];
            }
            $this->aGoodsXml[$aRootSection['ID']] .= '</categories>';
        }
    }

    /**
     * @return array
     */
    protected function getRootSections()
    {
        $rsRootSections = \Bitrix\Iblock\SectionTable::getList([
            'filter' => [
                'DEPTH_LEVEL' => 1,
                'IBLOCK_ID' => 2,
                'ACTIVE' => 'Y'
            ],
            'select' => [
                'ID',
                'NAME',
                'LEFT_MARGIN',
                'RIGHT_MARGIN',
            ]
        ]);
        $aRootSections = [];
        while ($a = $rsRootSections->fetch()) {
            yield $a;
        }
    }

    /**
     *
     */
    protected function fillGoodsProducts()
    {
        foreach ($this->getRootSections() as $aRootSection) {
            $this->aGoodsXml[$aRootSection['ID']] .= '<offers>';
        }

        $arElements = $this->getProducts();
        $arElemIDToProd = $this->getPrices();

        foreach ($arElements as $arItem) {
            $iRootSection = $this->aGoodsSectionToRootSection[$arItem['IBLOCK_SECTION_ID']];

            if( (int)$arItem['PREVIEW_PICTURE'] < 1 )
            {
                continue;
            }

            if (!empty($arItem['OFFERS'])) {

                // OFFERS
                foreach ($arItem['OFFERS'] as $arOffer) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<offer id="'.$arItem['ID'].'.'.$arOffer['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'#'.$arOffer['ID'].'</url>
                <price>'.$arElemIDToProd[$arOffer['ID']]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arOffer['NAME'].'</name>';

                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<description>'.strip_tags(str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT'])).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


                    if (intval($arItem['PREVIEW_PICTURE']) > 0) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
                    }

                    if ($arItem['PROPERTIES']['ARTICUL']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['VOLTAGE']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['BRAND']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';
                    }

                    if ($arItem['PROPERTIES']['GENDER']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';
                    }

                    if ($arOffer['PROPERTIES']['SIZE1']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Цвет">'.$arOffer['PROPERTIES']['SIZE1']['VALUE'].'</param>';
                    }

                    if ($arOffer['PROPERTIES']['SIZE2']['VALUE']) {
                        $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Размер">'.$arOffer['PROPERTIES']['SIZE2']['VALUE'].'</param>';
                    }


                    $this->aGoodsXml[$iRootSection] .= '</offer>';
                }

            } else {
                // PRODUCT
                $this->aGoodsXml[$iRootSection] .= "\r\n".'<offer id="'.$arItem['ID'].'" available="true">
                <url>http://4ps-shop.ru'.$arItem['DETAIL_PAGE_URL'].'</url>
                <price>'.$arElemIDToProd[$arItem['ID']]['PRICE_PRICE'].'</price>
                <currencyId>RUR</currencyId>
                <categoryId>'.$arItem['IBLOCK_SECTION_ID'].'</categoryId>
                <delivery>true</delivery>
				<name>'.$arItem['NAME'].'</name>';

                $this->aGoodsXml[$iRootSection] .= "\r\n".'<description>'.strip_tags(str_replace(array('"', '&', "'"), array('&quot;', '&amp;', "&gt;"), $arItem['PREVIEW_TEXT'])).'</description>
                <manufacturer_warranty>true</manufacturer_warranty>
                <sales_notes>Предоплата 100%</sales_notes>';


                if (intval($arItem['PREVIEW_PICTURE']) > 0) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<picture>http://4ps-shop.ru'.CFIle::GetPath($arItem['PREVIEW_PICTURE']).'</picture>';
                }

                if ($arItem['PROPERTIES']['ARTICUL']['VALUE']) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Артикул">'.$arItem['PROPERTIES']['ARTICUL']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['VOLTAGE']['VALUE']) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Напряжение">'.$arItem['PROPERTIES']['VOLTAGE']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['BRAND']['VALUE']) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Бренд">'.$arItem['PROPERTIES']['BRAND']['VALUE'].'</param>';
                }

                if ($arItem['PROPERTIES']['GENDER']['VALUE']) {
                    $this->aGoodsXml[$iRootSection] .= "\r\n".'<param name="Пол">'.$arItem['PROPERTIES']['GENDER']['VALUE'].'</param>';
                }


                $this->aGoodsXml[$iRootSection] .= '</offer>';
            }
        }

        foreach ($this->getRootSections() as $aRootSection) {
            $this->aGoodsXml[$aRootSection['ID']] .= '</offers>';
        }
    }
}

(new YMLGenerator())->execute();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
<?
header('Access-Control-Allow-Origin: *');
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
\Bitrix\Main\Loader::includeModule('sale');


require $_SERVER['DOCUMENT_ROOT'].'/local/api/yandex_delivery.php';

if( !empty($_REQUEST['TYPE_OF_DELIVERY']) && !empty($_REQUEST['ADDRESS']) ){

//    $strCity = htmlspecialcharsbx($_REQUEST['ADDRESS']);
    $strCity = \Bitrix\Sale\Location\Admin\LocationHelper::getLocationPathDisplay($_REQUEST['ADDRESS']);

    $strShipDate = '';
    switch (date('w')){
        case 0:
            $strShipDate = date('Y-m-d', strtotime('now +'.(2-date('w')).'day' ));
            break;
        case 1:
            if( date('G') < 16 )
                $strShipDate = date('Y-m-d', strtotime('now +'.(2-date('w')).'day' ));
            else
                $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
            break;
        case 2:
        case 3:
            $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
            break;
        case 4:
            if( date('G') < 16 )
                $strShipDate = date('Y-m-d', strtotime('now +'.(5-date('w')).'day' ));
            else
                $strShipDate = date('Y-m-d', strtotime('now +'.(9-date('w')).'day' ));
            break;
        case 5:
        case 6:
            $strShipDate = date('Y-m-d', strtotime('now +'.(9-date('w')).'day' ));
            $strShipDate = date('Y-m-d', strtotime('now +'.(9-date('w')).'day' ));
            break;
    }

    $arGetTail = [
        'city_from' => 'Москва',
        'city_to' => $strCity,
        'delivery_type' => ( (htmlspecialcharsbx($_POST['TYPE_OF_DELIVERY'])==70) )?'todoor':'pickup',
        //'index_city' => 142400,
        'weight' => ( !empty(htmlspecialcharsbx($_REQUEST['TOTAL_WEIGHT'])) ? htmlspecialcharsbx($_REQUEST['TOTAL_WEIGHT']) : 5 ),
        'width' => ( !empty(htmlspecialcharsbx($_REQUEST['TOTAL_WIDTH'])) ? htmlspecialcharsbx($_REQUEST['TOTAL_WIDTH']) : 50 ),
        'length' => ( !empty(htmlspecialcharsbx($_REQUEST['TOTAL_LENGTH'])) ? htmlspecialcharsbx($_REQUEST['TOTAL_LENGTH']) : 50 ),
        'height' => ( !empty(htmlspecialcharsbx($_REQUEST['TOTAL_HEIGHT'])) ? htmlspecialcharsbx($_REQUEST['TOTAL_HEIGHT']) : 20 ),
        'assessed_value' => htmlspecialcharsbx($_REQUEST['ORDER_SUM'])?htmlspecialcharsbx($_REQUEST['ORDER_SUM']):10000,
        'order_cost' => htmlspecialcharsbx($_REQUEST['ORDER_SUM'])?htmlspecialcharsbx($_REQUEST['ORDER_SUM']):10000,
        'ship_date' => $strShipDate,
        'to_yd_warehouse' => '1'
    ];

    $WBLYaDelivery = new WBLYaDelivery;
    $arDelivery = $WBLYaDelivery->parseDeliveryList( $WBLYaDelivery->query('searchDeliveryList', $arGetTail) );

    switch ($arGetTail['delivery_type']){

        case 'todoor':
            // Курьер

            if( !empty($arDelivery) ){
                $strReturn = '';

                $arTmpDelivery = []; $i = 0;
                foreach ($arDelivery as $arItem){
                    ++$i;
                    $arTmpDelivery[ $arItem['PRICE'].'.'.$i ] = $arItem;
                }
                ksort($arTmpDelivery);

                foreach ($arTmpDelivery as $arItem){
                    $arItem['PRICE'] += 50;

                    switch (mb_strtoupper($arItem['NAME'])){
                        case 'ПЭК':
                        case 'ПОЧТА':
                        case 'ПОЧТА РОССИИ':
                        case 'ПОЧТА ОНЛАЙН':
                        case 'PICKPOINT':
                            $arItem['NAME'] .= ' (проверка товара только после оплаты)';
                            break;
                    }

                    $strReturn .= '<label><input type="radio" name="FORM_RESULT[DELIVERY_YANDEX]" '.( $_REQUEST['NOT_REQUIRED'] != 'Y' ? 'required' : '' ).' value="'.$arItem['PRICE'].'#|#'.$arItem['DELIVERY_ADDRESS'].'#|#'.$arItem['PHONE'].'#|#'.$arItem['NAME'].'#|#'.base64_encode(serialize($arItem['YA_DELIVERY_DATA'])).'"> '.$arItem['NAME'].', <b>Стоимость:</b> '.number_format($arItem['PRICE'], '0', '.', ' ').' руб., <b>Доставка:</b> '.( ($arItem['DELIVERY_DATES']['FROM']!=$arItem['DELIVERY_DATES']['TO'])? FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['FROM'])).' - '.FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['TO'])) : FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['FROM'])) ).', <b>Время:</b> '.date('H:i', strtotime($arItem['TIME_INTERVAL']['FROM'])).' - '.date('H:i', strtotime($arItem['TIME_INTERVAL']['TO'])).'</label>';
                    $strReturn .= '<hr/>';
                }
                echo $strReturn;
            }
            else{
                echo '<p>К сожалению расчитать стоимость доставки автоматизированно не удалось. После оформления заказа мы расчитаем стоимость доставки и сообщим Вам стоимость.</p>
<input type="radio" name="DELIVERY_YANDEX" required checked value="0#|##|##|#Ожидайте расчета#|#YTowOnt9" > Расчитать стоимость после оформления заказа';
            }

            break;

        case 'pickup':
            // Пунт самовывоза
            if( !empty($arDelivery) ){
                ?>
                <div id="ya_delivery_map" style="width: 100%; height: 400px"></div>
                <input name="FORM_RESULT[DELIVERY_YANDEX]" <?=( $_REQUEST['NOT_REQUIRED'] != 'Y' ? 'required' : '' )?> value="" type="hidden" />


                <p data-wbl-ya-delivery-selected class="hide" style="margin-top: 25px;"></p>
                <script type="text/javascript">
                    ymaps.ready(init);

                    function init(){
                        var myMap;
                        ymaps.geocode('<?=$_REQUEST['ADDRESS']?>').then(function (res) {
                            myMap = new ymaps.Map("ya_delivery_map", {
                                center: res.geoObjects.get(0).geometry.getCoordinates(),
                                zoom: 14
                            });

                            var objects = [];
                            <?foreach ($arDelivery as $key=>$arItem):?>
                            <?
                            switch (mb_strtoupper($arItem['NAME'])){
                                case 'ПЭК':
                                case 'ПОЧТА':
                                case 'ПОЧТА РОССИИ':
                                case 'ПОЧТА ОНЛАЙН':
                                case 'PICKPOINT':
                                    $arItem['NAME'] .= ' (проверка товара только после оплаты)';
                                    break;
                            }
                            ?>
                            <?$arItem['PRICE'] += 50;?>
                            var myPlacemark<?=$key?> = new ymaps.Placemark([<?=$arItem['COORDS']?>], {
                                hintContent: '"<?=$arItem['NAME']?>", <?=number_format($arItem['PRICE'], '0', '.', ' ')?> руб.<br/>',
                                balloonContent: '<b>ТК</b>: <?=$arItem['NAME']?><br/>'+
                                '<b>Адрес</b>: <?=$arItem['DELIVERY_ADDRESS']?><br/>'+
                                '<b>Дата доставки</b>: <?=( ($arItem['DELIVERY_DATES']['FROM']!=$arItem['DELIVERY_DATES']['TO'])? FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['FROM'])).' - '.FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['TO'])) : FormatDate('d.M', strtotime($arItem['DELIVERY_DATES']['FROM'])) )?><br/>'+
                                '<b>Рабочее время</b>: <?=date('H:i', strtotime($arItem['TIME_INTERVAL']['FROM'])).' - '.date('H:i', strtotime($arItem['TIME_INTERVAL']['TO']))?><br/>'+
                                '<b>Стоимость</b>: <?=number_format($arItem['PRICE'], '0', '.', ' ')?> руб.<br/>'+
                                '<b>Телефон пункта:</b>: <?=preg_replace('/(\d)(\d{3,3})(\d{3,3})(\d{2,2})(\d{2,2})/', '+$1 ($2) $3-$4-$5', $arItem['PHONE'])?><br/>'+
                                '<a href="javascript:void(0)" class="wbl-ya-map-button" onclick="$(\'[name=\\\'FORM_RESULT[DELIVERY_YANDEX]\\\']\').val(\'<?=$arItem['PRICE'].'#|#'.$arItem['DELIVERY_ADDRESS'].'#|#'.$arItem['PHONE'].'#|#'.$arItem['NAME'].'#|#'.base64_encode(serialize($arItem['YA_DELIVERY_DATA']))?>\').trigger(\'change\'); $(\'[data-wbl-ya-delivery-selected]\').html(\'<b>Выбран:</b> <?=$arItem['NAME'].', '.$arItem['DELIVERY_ADDRESS']?>\').removeClass(\'hide\'); if( $(\'[name=\\\'HOUSE\\\']\').length > 0 ){ $(\'[name=\\\'HOUSE\\\']\').val(\'\') } alert(\'Пункт выдачи заказов успешно выбран!\');">Выбрать</a>'
                            });

                            objects.push(myPlacemark<?=$key?>);
                            <?endforeach;?>
                            objects = ymaps.geoQuery(objects);

                            objects.searchInside(myMap).addToMap(myMap);

                            myMap.events.add('boundschange', function () {
                                var visibleObjects = objects.searchInside(myMap).addToMap(myMap);
                                objects.remove(visibleObjects).removeFromMap(myMap);
                            });

                        });
                    }
                </script>
                <?
            }
            else{
                echo '<p>К сожалению по данному адресу нельзя доставить заказ данным способом. Смените тип доставки.</p>';
            }
            break;
    }

}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
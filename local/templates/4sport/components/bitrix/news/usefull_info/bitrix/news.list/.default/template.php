<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style>
	#articles .item {
	    background: #ffffff;
	    display: block;
	    height: 395px;
	    position: relative;
	    margin: 0 0 10px;
	    -moz-box-sizing: border-box;
	    -webkit-box-sizing: border-box;
	    box-sizing: border-box;
	    float: left;
	    width: 255px;
	    margin: 0 30px 30px 0;
	    position: relative;
	}
	a {
	    color: #231f20;
	    cursor: pointer;
	    background: transparent;
	    text-decoration: none;
	}
	a > img {
	    vertical-align: bottom;
	}
#articles .item .name {
    font-size: 22px;
    /*font-family: 'Roboto Condensed', sans-serif;*/
    font-weight: 700;
}	
	#articles .item .txt {
	    padding: 19px;
	}
#articles .item .anons {
    font-size: 12px;
    line-height: 18px;
    padding-top: 10px;
    padding-bottom: 35px;
}	
#articles .item .detail_link:hover {
    text-decoration: underline;
}
.strelka{
	color: #EF3425;
} 					
</style>
<div class="content">
	<section id="articles">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			 <? $file = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array('width'=>255, 'height'=>'129'), BX_RESIZE_IMAGE_EXACT, true); ?>    			
			<div class="item">
				<div class="item-wrapper">
					<div class="pic">
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>">
							<img src="<?=$file["src"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>">
						</a>
					</div>
					<div class="txt">
						<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="name">
							<?echo $arItem["NAME"]?>
						</a>
						<div class="anons">
								<p><?echo $arItem["PREVIEW_TEXT"];?></p>
						</div>
					</div>
				</div>
				<div class="article-detail">
					<a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="detail_link">подробнее <span class="strelka">></span></a>
				</div>
			</div>
		<?endforeach;?>		
		<div class="clearfix"></div>
	</section>
</div>

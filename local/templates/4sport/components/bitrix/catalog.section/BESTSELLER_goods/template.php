<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style type="text/css">
	.avaliable_text {
    text-align: right;
    /* padding: 5px 2px; */
    color: #d73e20;
    font-family: "Bender";
    font-weight: 700;
    font-style: italic;
}
</style>
<?
if (!empty($arResult['ITEMS']))
{
	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
	?>
	<div class="products-related">
		<div class="products-related__head">
			<div class="products-related__head-left">
			<h2 class="products-related__head-title">
				Хиты 
				<span class="products-related__head-title--regular">
				продаж
				</span>
			</h2>
			</div>
			<div class="products-related__head-right">
				<div class="products-slider__controls"></div>
			</div>
		</div>
		<div class="products-related__content">
			<div class="products-slider-wrap">
				<div class="products-slider js-products-slider">
					<div class="products-slider__item">
						<ul class="products-list">
							<?
							foreach ($arResult['ITEMS'] as $key => $arItem)
							{
								$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
								$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
								$strMainID = $this->GetEditAreaId($arItem['ID']);
								$arItemIDs = array(
								'ID' => $strMainID,
								'PICT' => $strMainID.'_pict',
								'SECOND_PICT' => $strMainID.'_secondpict',
								'STICKER_ID' => $strMainID.'_sticker',
								'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
								'QUANTITY' => $strMainID.'_quantity',
								'QUANTITY_DOWN' => $strMainID.'_quant_down',
								'QUANTITY_UP' => $strMainID.'_quant_up',
								'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
								'BUY_LINK' => $strMainID.'_buy_link',
								'BASKET_ACTIONS' => $strMainID.'_basket_actions',
								'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
								'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
								'COMPARE_LINK' => $strMainID.'_compare_link',
								'PRICE' => $strMainID.'_price',
								'DSC_PERC' => $strMainID.'_dsc_perc',
								'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
								'PROP_DIV' => $strMainID.'_sku_tree',
								'PROP' => $strMainID.'_prop_',
								'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
								'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
								);
								$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
								$productTitle = (
								isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
								? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
								: $arItem['NAME']
								);
								$imgTitle = (
								isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
								? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
								: $arItem['NAME']
								);
								$minPrice = false;
								if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
								$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);
								?>
								<li class="products-list__item" id="<? echo $strMainID; ?>">
									<div class="product" itemscope itemtype="http://schema.org/Product">
										<div class="product__top">
											<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="product__image" id="<? echo $arItemIDs['SECOND_PICT']; ?>">
												<span class="product__image-cell">
													<img class="product__image-img" itemprop="image" src="<? echo $arItem['PREVIEW_PICTURE']['SRC']; ?>" alt="<? echo $imgTitle; ?>" style="max-width: 221px;">
												</span>
											</a>
										</div>
					
										<div class="product__middle">
											<div class="product__middle-left">
												<h2 class="product__title" itemprop="name">
													<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>" class="product__title-link">
														<? echo $productTitle; ?>
													</a>
												</h2>
											</div>
											<span class="hrenShow" itemprop="description">
												<? echo $productTitle; ?>
											</span>
											<div class="product__middle-right" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
												<div class="product__price" itemprop="price" id="<? echo $arItemIDs['PRICE']; ?>">
													<?
													if (!empty($minPrice))
													{
														if ('N' == $arParams['PRODUCT_DISPLAY_MODE'] && isset($arItem['OFFERS']) && !empty($arItem['OFFERS']))
														{
															echo $minPrice['PRINT_DISCOUNT_VALUE'];
															/*
															echo GetMessage(
																'CT_BCS_TPL_MESS_PRICE_SIMPLE_MODE',
																array(
																'#PRICE#' => $minPrice['PRINT_DISCOUNT_VALUE'],
																'#MEASURE#' => GetMessage(
																	'CT_BCS_TPL_MESS_MEASURE_SIMPLE_MODE',
																	array(
																		'#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
																		'#UNIT#' => $minPrice['CATALOG_MEASURE_NAME']
																		)
																	)
																)
															);
															*/
														}
														else
														{
															echo $minPrice['PRINT_DISCOUNT_VALUE'];
														}
														/*
														if ('Y' == $arParams['SHOW_OLD_PRICE'] && $minPrice['DISCOUNT_VALUE'] < $minPrice['VALUE'])
														{
														?> 
															<span>
																<? echo $minPrice['PRINT_VALUE']; ?>
															</span>
														<?
														}
														*/
													}
													unset($minPrice);
													?>
													<span class="hrenShow" itemprop="priceCurrency">RUB</span>
												</div>
											</div>
										</div>
										<!--buy-->
										<?
										if($arItem['CATALOG_SUBSCRIBE'] == 'Y')
											$showSubscribeBtn = true;
										else
											$showSubscribeBtn = false;
										if (!isset($arItem['OFFERS']) || empty($arItem['OFFERS']))
										{
										?>
										<div class="product__bottom">
											<?
											if ($arItem['CAN_BUY'])
											{
												if ('Y' == $arParams['USE_PRODUCT_QUANTITY'])
												{
												?>
													
													<div class="product__bottom-left">
														<div class="spinner product__bottom-left-spinner" >
															<div class="spinner__control spinner__control--left js-spinner-minus" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">
															</div>
															<input type="text" class="spinner__input js-spinner" id="<? echo $arItemIDs['QUANTITY']; ?>" name="<? echo $arParams["PRODUCT_QUANTITY_VARIABLE"]; ?>" value="1">
															<div class="spinner__control spinner__control--right js-spinner-plus" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">
															</div>
														</div>
		
													</div>
												<?
												}
												?>
												<div id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" class="product__bottom-right">
													<a id="<? echo $arItemIDs['BUY_LINK']; ?>" class="btn btn-red btn--cart-left product__bottom-right-btn" href="javascript:void(0)" rel="nofollow">
														<span class="btn-black__inner">
															<span class="btn__icon">
																<svg class="icon-cart-white-medium">
																	<use xlink:href="#cart"></use>
																</svg>
															</span>
															<span class="btn__text btn__text--middle">
																В корзину
															</span>
														</span>
													</a>
												</div>
											<?
											}
											
											else
											{
											?>			
												<div id="<? echo $arItemIDs['NOT_AVAILABLE_MESS']; ?>" class="avaliable_text">
													<span class="bx_notavailable">
														<?
														echo ('' != $arParams['MESS_NOT_AVAILABLE'] ? $arParams['MESS_NOT_AVAILABLE'] : GetMessage('CT_BCS_TPL_MESS_PRODUCT_NOT_AVAILABLE'));
														?>
													</span>
												</div>
												<?if($showSubscribeBtn):	
												echo '<div class="btn btn-red btn--cart-left product__bottom-right-btn">';
												echo '
												<span class="btn-black__inner">
												<span class="btn__icon">
												<svg class="icon-cart-white-medium">
												<use xlink:href="#cart"></use>
												</svg>
												</span>
													<span class="btn__text btn__text--middle">
													';
													$APPLICATION->includeComponent('bitrix:catalog.product.subscribe','',
														array(
															'PRODUCT_ID' => $arItem['ID'],
															'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
															'BUTTON_CLASS' => 'bx_bt_button bx_medium',
															'DEFAULT_DISPLAY' => true,
														),
													$component, array('HIDE_ICONS' => 'Y')
													);
													echo "
													</span>
												</span>";
												echo "</div>";
												endif;
												?>
											<?
											}
											?>
										</div>
										<?
										$emptyProductProperties = empty($arItem['PRODUCT_PROPERTIES']);
										$arJSParams = array(
										'PRODUCT_TYPE' => $arItem['CATALOG_TYPE'],
										'SHOW_QUANTITY' => ($arParams['USE_PRODUCT_QUANTITY'] == 'Y'),
										'SHOW_ADD_BASKET_BTN' => false,
										'SHOW_BUY_BTN' => true,
										'SHOW_ABSENT' => true,
										'SHOW_OLD_PRICE' => ('Y' == $arParams['SHOW_OLD_PRICE']),
										'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
										'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
										'SHOW_DISCOUNT_PERCENT' => ('Y' == $arParams['SHOW_DISCOUNT_PERCENT']),
										'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
										'PRODUCT' => array(
										'ID' => $arItem['ID'],
										'NAME' => $productTitle,
										'PICT' => ('Y' == $arItem['SECOND_PICT'] ? $arItem['PREVIEW_PICTURE_SECOND'] : $arItem['PREVIEW_PICTURE']),
										'CAN_BUY' => $arItem["CAN_BUY"],
										'SUBSCRIPTION' => ('Y' == $arItem['CATALOG_SUBSCRIPTION']),
										'CHECK_QUANTITY' => $arItem['CHECK_QUANTITY'],
										'MAX_QUANTITY' => $arItem['CATALOG_QUANTITY'],
										'STEP_QUANTITY' => $arItem['CATALOG_MEASURE_RATIO'],
										'QUANTITY_FLOAT' => is_double($arItem['CATALOG_MEASURE_RATIO']),
										'SUBSCRIBE_URL' => $arItem['~SUBSCRIBE_URL'],
										'BASIS_PRICE' => $arItem['MIN_BASIS_PRICE']
										),
										'BASKET' => array(
										'ADD_PROPS' => ('Y' == $arParams['ADD_PROPERTIES_TO_BASKET']),
										'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
										'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
										'EMPTY_PROPS' => $emptyProductProperties,
										'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
										'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
										),
										'VISUAL' => array(
										'ID' => $arItemIDs['ID'],
										'PICT_ID' => ('Y' == $arItem['SECOND_PICT'] ? $arItemIDs['SECOND_PICT'] : $arItemIDs['PICT']),
										'QUANTITY_ID' => $arItemIDs['QUANTITY'],
										'QUANTITY_UP_ID' => $arItemIDs['QUANTITY_UP'],
										'QUANTITY_DOWN_ID' => $arItemIDs['QUANTITY_DOWN'],
										'PRICE_ID' => $arItemIDs['PRICE'],
										'BUY_ID' => $arItemIDs['BUY_LINK'],
										'BASKET_PROP_DIV' => $arItemIDs['BASKET_PROP_DIV'],
										'BASKET_ACTIONS_ID' => $arItemIDs['BASKET_ACTIONS'],
										'NOT_AVAILABLE_MESS' => $arItemIDs['NOT_AVAILABLE_MESS'],
										'COMPARE_LINK_ID' => $arItemIDs['COMPARE_LINK'],
										'SUBSCRIBE_ID' => $arItemIDs['SUBSCRIBE_LINK'],
										),
										'LAST_ELEMENT' => $arItem['LAST_ELEMENT']
										);
										unset($emptyProductProperties);
										?>
										<script type="text/javascript">
										var <? echo $strObName; ?> = new JCCatalogSection(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
										</script>
										<?
										}

										?>
									</div>
								</li>	
							<?
							}
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
	});
	</script>
<?
}
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<nav class="header__nav">
	    <div class="header__nav-inner">
	        <ul class="header__nav-list js-swipe-list">
				<?
				foreach($arResult as $arItem):
				if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
				continue;
				?>
	            <li class="header__nav-list-item"><a href="<?=$arItem["LINK"]?>" class="header__nav-link"><?=$arItem["TEXT"]?></a></li>

	            <?endforeach?>
	        </ul>
	    </div>
	</nav>
<?endif?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
	<div class="topbar__nav">
        <ul class="topbar__nav-list">
			<?
			foreach($arResult as $arItem):
				if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
				continue;
			?>
		            <li class="topbar__nav-list-item">
		                <a href="<?=$arItem["LINK"]?>" class="topbar__nav-link"><?=$arItem["TEXT"]?></a>
		            </li>
            
            <?endforeach?>
        </ul>
    </div>
<?endif?>    
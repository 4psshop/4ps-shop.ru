<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="wbl-steps-wrapper">
    <?foreach($arResult as $arItem):?>
    <div class="wbl-step-layer active">
        <div class="wbl-step-text">
            <a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
        </div>
    </div>
    <?endforeach;?>
</div>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<style type="text/css">
    .content-box .side-menu__item-content {padding-right: 20px;}
    .content-box .side-menu__item-content-list > li {margin-top: 12px;}
    .content-box .side-menu-list__item + .side-menu-list__item {border: none;}
    .content-box .side-menu-list > .side-menu-list__item + .side-menu-list__item {border-top: 2px solid #EFEFEF;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent > .side-menu__item > .side-menu__item-head:before {right: 0; top: 8px;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent > .side-menu__item > .side-menu__item-head:after {right: 2px; top: 8px;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent > .side-menu__item > .side-menu__item-head a {padding: 0; font-weight: 400;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent > .side-menu__item > .side-menu__item-head .icon-arrow-right-gray-light-small {position: absolute; left: -14px; top: 6px;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent {margin-top: 12px; padding: 0 0 0 14px;}
    .content-box .side-menu-list__item.js-accord-parent .side-menu-list__item.js-accord-parent .side-menu__item-content {padding-left: 5px; padding-right: 0; padding-bottom: 0;}
    .content-box .side-menu-list__item.active > .side-menu__item > .side-menu__item-head > .side-menu__item-head-title {color: #d94223;}
    .content-box .side-menu-list__item.active .side-menu__item-head-title {color: #000;}

    .content-box .side-menu-list__item.active > .side-menu__item > .side-menu__item-content {display: block !important;}
    .content-box .side-menu__item-content-list-item.active > a {color: #d94223;}
</style>
<?
$arSections = array(); global $LastSection; $LastSection = array();
foreach ($arResult['SECTIONS'] as $arSect) {
    global $LastSection;
    if( $arSect['DEPTH_LEVEL'] == 1 ){
        $arSections[ $arSect['ID'] ] = $arSect;
        $LastSection[ $arSect['DEPTH_LEVEL'] ] =& $arSections[ $arSect['ID'] ];
    }else{
        $LastSection[ $arSect['DEPTH_LEVEL']-1 ]['CHILDS'][ $arSect['ID'] ] = $arSect;
        $LastSection[ $arSect['DEPTH_LEVEL'] ] =& $LastSection[ $arSect['DEPTH_LEVEL']-1 ]['CHILDS'][ $arSect['ID'] ];
    }
}
?>
<?
function WBLCreateSectionAsideTree($arSections){
    ?>
    <?foreach ($arSections as $arSection):?>
        <?if( empty($arSection['CHILDS']) ):?>
            <li class="side-menu__item-content-list-item <?=( $arSection['SELECTED']=='Y' )?'active':''?>">
                <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="side-menu__item-content-list-item-link">
                    <?=$arSection['NAME']?>
                </a>
                <span class="icon-arrow-right-gray-light-small"></span>
            </li>
        <?else:?>
            <li class="side-menu-list__item js-accord-parent <?=( $arSection['SELECTED']=='Y' )?'active':''?>">
                <div class="side-menu__item">
                    <div class="side-menu__item-head">
                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="side-menu__item-head-title js-accord-link">
                            <?=$arSection['NAME']?>
                        </a>
                        <?if( $arSection['DEPTH_LEVEL'] > 1 ):?>
                            <span class="icon-arrow-right-gray-light-small"></span>
                        <?endif;?>
                    </div>

                    <div class="side-menu__item-content js-accord-content">
                        <ul class="side-menu__item-content-list">
                            <?WBLCreateSectionAsideTree($arSection['CHILDS']);?>
                        </ul>
                    </div>

                </div>
            </li>
        <?endif;?>
    <?endforeach;?>
    <?
}
?>

<div class="side-menu">
    <ul class="side-menu-list">
        <?WBLCreateSectionAsideTree($arSections);?>
    </ul>
</div>

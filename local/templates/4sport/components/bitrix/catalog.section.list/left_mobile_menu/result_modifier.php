<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
	'VIEW_MODE' => 'LIST',
	'SHOW_PARENT_NAME' => 'Y',
	'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
	$arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
	if ('LIST' != $arParams['VIEW_MODE'])
	{
		$boolClear = false;
		$arNewSections = array();
		foreach ($arResult['SECTIONS'] as &$arOneSection)
		{
			if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
			{
				$boolClear = true;
				continue;
			}
			$arNewSections[] = $arOneSection;
		}
		unset($arOneSection);
		if ($boolClear)
		{
			$arResult['SECTIONS'] = $arNewSections;
			$arResult['SECTIONS_COUNT'] = count($arNewSections);
		}
		unset($arNewSections);
	}
}

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
	{
		reset($arResult['SECTIONS']);
		$arCurrent = current($arResult['SECTIONS']);
		if (!isset($arCurrent['PICTURE']))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}
		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			if (!isset($arMap[$arSection['ID']]))
				continue;
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
	}
}
?>

<!-- Filter -->
<?
if ( CModule::IncludeModule( 'iblock' ) )  
    {  
          
        // Массив сортировки  
        // LEFT_MARGIN - так называемая "левая граница".   
        // Порядок такой - берётся первый раздел (по полю SORT и ID).  
        // Если у этого раздела, есть вложенные разделы, то берём первого из них и с ним делаем то же самое.  
        // Если нет "детишек", то переходим к следующему разделу в корне.  
        $arOrder = Array(  
            'LEFT_MARGIN' => 'ASC',  
        );  
          
          
        // Массив фильтрации  
        // В IBLOCK_ID ставим идентификатор выбранного в параметрах инфоблока.  
        // ACTIVE - выбираем только активные разделы. Необязательно, но лучше привыкать к использованию активности.  
        // DEPTH_LEVEL - берём разделы только первых двух уровней вложенности. Как я уже говорил,  
        // нужно писать компоненты под конкретную задачу.   
        // Задача нашего компонента - вывести первые два уровня, значит и выбираем только два уровня разделов.  
        $arFilter = Array(  
            'IBLOCK_ID' => $arParams['IBLOCK_ID'],  
            'ACTIVE' => 'Y',  
            '<=DEPTH_LEVEL' => 2  
        );  
          
        // Массив выбираемых полей  
        // NAME - название раздела  
        // CODE - символьный код. Потребуется для генерации ссылки на раздел.   
        // Нам ведь нужно, чтобы пункты меню куда-то вели? :)  
        // DEPTH_LEVEL - уровень вложенности  
        $arSelect = Array(  
            'NAME',  
            'CODE',
            'SECTION_PAGE_URL', 
            'DEPTH_LEVEL'  
        );  
          
        // Функция выборки разделов инфоблока  
        // Обратите внимание, что в этой функции массив выбираемых полей $arSelect передаётся 4-ым параметром  
        // в отличие от функции CIBlockElement::GetList(), где он идёт 5-ым.  
        // Будьте аккуратнее с этим.  
        $dbSections = CIBlockSection::GetList( $arOrder, $arFilter, false, $arSelect, false );  
        while ( $arSection = $dbSections->Fetch() )  
        {  
          
            // Разделы в корне инфоблока просто запихиваем в arResult  
            if ( $arSection['DEPTH_LEVEL'] == 1 )  
            {  
                $arResult['TRALEX'][] = Array(  
                    'NAME' => $arSection['NAME'],  
                    // В качестве кода "базового" раздела я взял "catalog". Просто для примера...  
                    'LINK' => '/catalog/' . $arSection['CODE'] . '/'  
                );  
                  
                // Запоминаем индекс раздела, под которым он попадает в arResult  
                $key = count( $arResult['TRALEX'] ) - 1;  
            }  
            elseif ( $arSection['DEPTH_LEVEL'] == 2 )  
            {  
                  
                // Разделы второго уровня запихиваем в их родительский раздел  
                // Тут-то нам и пригодился индекс родительского раздела  
                $arResult['TRALEX'][$key]['CHILDS'][] = Array(  
                    'NAME' => $arSection['NAME'],  
                    // Для генерации ссылки используем ссылку родительского раздела и код текущего  
                    'LINK' => $arResult['TRALEX'][$key]['LINK'] . $arSection['CODE'] . '/'  
                );  
  
            }  
        }
}
?>       
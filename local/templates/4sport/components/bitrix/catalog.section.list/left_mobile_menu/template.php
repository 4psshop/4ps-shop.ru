<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
// Search section
$curPage = $APPLICATION->GetCurPage(true);
$pieces = explode("/", $curPage);
$dir =  $pieces[2]; // кусок2
?>
<?if(count($arResult['TRALEX'])):?>  
<div class="side-menu side-menu--header">
    <ul class="side-menu-list">
 <? foreach ($arResult['TRALEX'] as $arFirstItem ): ?>
        <?
        $pos = strpos($arFirstItem['LINK'], $dir);
        if ($pos === false) {
             echo '<li class="side-menu-list__item js-accord-parent">';
        } else {
            echo '<li class="side-menu-list__item js-accord-parent active">';
        }?>             
        <!-- <li class="side-menu-list__item js-accord-parent"> -->
            <div class="side-menu__item">
                <div class="side-menu__item-head">
                <? if (count($arFirstItem['CHILDS'])):?> 
                    <a href="<?=$arFirstItem['LINK']?>" class="side-menu__item-head-title js-accord-link">
                <?else:?>
                    <a href="<?=$arFirstItem['LINK']?>" class="side-menu__item-head-title ">    
                <? endif; ?> 
                    <?=$arFirstItem['NAME']?>
                    </a>
                </div>
                 <? if (count($arFirstItem['CHILDS'])):?>  
                <?
                $pos = strpos($arFirstItem['LINK'], $dir);
                if ($pos === false) {
                     echo '<div class="side-menu__item-content js-accord-content">';
                } else {
                    echo '<div class="side-menu__item-content js-accord-content" style="display:block;">';
                }?>           
                <!-- <div class="side-menu__item-content js-accord-content"> -->
                    <ul class="side-menu__item-content-list">
                     <?foreach ($arFirstItem['CHILDS'] as $arSecondItem ):?>  
                        <li class="side-menu__item-content-list-item">
                            <a href="<?=$arSecondItem['LINK']?>" class="side-menu__item-content-list-item-link"> <?=$arSecondItem['NAME']?></a>
                            <span class="icon-arrow-right-gray-light-small"></span>
                        </li>
                      <?endforeach;?>     
                    </ul>
                </div>
                <? endif; ?>   
            </div>
        </li>
         <? endforeach; ?>  
    </ul>
</div>
<?endif;?> 





 
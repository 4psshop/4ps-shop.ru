<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<div class="footer__catalog">
	<h4 class="footer__catalog-title">
		Каталог
	</h4>
	<div class="footer__catalog-inner">
		<div class="row">
		<?
		// Вывод в два столбика
		$round = round(count($arResult['SECTIONS']) / 2);
		$disjoint = array_chunk($arResult['SECTIONS'], $round);
		// Цикл по столбикам
		foreach($disjoint as $array)
		{
			echo '<div class="col-sm-100 col-md-50">';
				echo '<ul class="footer__catalog-list">';
				//Цикл по элементам
					foreach($array as $arSection)
					{
						echo '
						<li class="footer__catalog-list-item">
							<a href="'.$arSection['SECTION_PAGE_URL'].'" class="footer__catalog-link">'.
								$arSection['NAME']
							.'</a>
						</li>';
					}
				echo "</ul>";
			echo '</div>';
		}
		?>
		</div>
	</div>
</div>
<?
}
?>
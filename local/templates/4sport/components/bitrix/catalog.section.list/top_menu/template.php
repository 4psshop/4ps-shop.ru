<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(count($arResult['TRALEX'])):?>  
	<nav class="header__nav">
        <div class="header__nav-inner" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
            <ul class="header__nav-list js-swipe-list">
            <? foreach ($arResult['TRALEX'] as $arFirstItem ): ?>
                <li class="header__nav-list-item">
                    <table class="header__nav_link-wrap-table">
                        <tr>
                            <td class="header__nav_link-wrap">
                                <a href="<?=$arFirstItem['LINK']?>" class="header__nav-link"><?=$arFirstItem['NAME']?></a>
                            </td>
                        </tr>
                    </table>
                    <? if (count($arFirstItem['CHILDS'])):?>  
                    <div class="header__nav-submenu">
                        <div class="side-menu__item-content side-menu__item-content--header">
                            <ul class="side-menu__item-content-list">
                            <?foreach ($arFirstItem['CHILDS'] as $arSecondItem ):?>  
                                <li class="side-menu__item-content-list-item">
                                    <a href="<?=$arSecondItem['LINK']?>" itemprop="discussionUrl" class="side-menu__item-content-list-item-link"><?=$arSecondItem['NAME']?></a>
                                    <span class="icon-arrow-right-gray-light-small"></span>
                                </li>
                                <?endforeach;?> 
                            </ul>
                        </div>
                    </div>
                    <? endif; ?>  
                </li>
                <? endforeach; ?>  
            </ul>
        </div>
    </nav>
<?endif;?> 
 
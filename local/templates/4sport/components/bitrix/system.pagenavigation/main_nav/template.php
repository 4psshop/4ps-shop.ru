<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$ClientID = 'navigation_'.$arResult['NavNum'];

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
?>
<div class="filters" style="padding: 6px 5px;">
	<div class="filters__bottom-bottom">
		<div class="filters__bottom-bottom-right">
			<div class="pagination">
				<span class="label label--inline pagination__label">Страница:</span>
				<ul class="pagination__list">
<?
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

	// to show always first and last pages
	$arResult["nStartPage"] = 1;
	$arResult["nEndPage"] = $arResult["NavPageCount"];

	$sPrevHref = '';
	if ($arResult["NavPageNomer"] > 1)
	{
		$bPrevDisabled = false;

		if ($arResult["bSavePage"] || $arResult["NavPageNomer"] > 2)
		{
			$sPrevHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]-1);
		}
		else
		{
			$sPrevHref = $arResult["sUrlPath"].$strNavQueryStringFull;
		}
	}
	else
	{
		$bPrevDisabled = true;
	}

	$sNextHref = '';
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"])
	{
		$bNextDisabled = false;
		$sNextHref = $arResult["sUrlPath"].'?'.$strNavQueryString.'PAGEN_'.$arResult["NavNum"].'='.($arResult["NavPageNomer"]+1);
	}
	else
	{
		$bNextDisabled = true;
	}
	?>
		<?/*?><div class="navigation-arrows">
			<span class="arrow">&larr;</span><span class="ctrl"> ctrl</span>&nbsp;<?if ($bPrevDisabled):?><span class="disabled"><?=GetMessage("nav_prev")?></span><?else:?><a href="<?=$sPrevHref;?>" id="<?=$ClientID?>_previous_page"><?=GetMessage("nav_prev")?></a><?endif;?>&nbsp;<?if ($bNextDisabled):?><span class="disabled"><?=GetMessage("nav_next")?></span><?else:?><a href="<?=$sNextHref;?>" id="<?=$ClientID?>_next_page"><?=GetMessage("nav_next")?></a><?endif;?>&nbsp;<span class="ctrl">ctrl </span><span class="arrow">&rarr;</span>
		</div>
		<?*/?>


	<?
	$bFirst = true;
	$bPoints = false;

	do
	{	?>

		<?if (($arResult["nStartPage"] <= 2) && ($bFirst)):?>
			<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?echo $arResult["nStartPage"]-1?>""><span><</span></a></li>
		<?endif;?>


		<?
		if ($arResult["nStartPage"] <= 2 || $arResult["nEndPage"]-$arResult["nStartPage"] <= 1 || abs($arResult['nStartPage']-$arResult["NavPageNomer"])<=2)
		{

			if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
	?>
			<li class="pagination__list-item current"><span class="pagination__list-item-link"><?=$arResult["nStartPage"]?></span></li>
	<?
			elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
	?>
			<li class="pagination__list-item">
				<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">
					<span><?=$arResult["nStartPage"]?></span>
				</a>
			</li>

	<?
			else:
	?>
			<li class="pagination__list-item">
				<a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>">
					<span>
						<?=$arResult["nStartPage"]?>
					</span>
				</a>
			</li>
	<?
			endif;
			$bFirst = false;
			$bPoints = true;
		}
		else
		{
			if ($bPoints)
			{
			?>
			<?
			if ( $arResult["NavPageNomer"] < $arResult["nStartPage"]) {
				$number_dot = round(($arResult["nEndPage"]-1-$arResult["NavPageNomer"])/2)+$arResult["NavPageNomer"];
			} else {
				$number_dot = round(($arResult["NavPageNomer"] - 2)/2);
			}
			?>
			<li class="pagination__list-item">
				<a class="pagination_dots" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$number_dot?>">
					<span>...</span>
				</a>
			</li>


			<?
				$bPoints = false;
			}
		}?>

		<?if (($arResult["NavPageNomer"] < $arResult["nEndPage"]) && ($arResult["nStartPage"] == $arResult["nEndPage"])):?>
			<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?echo $arResult["nStartPage"]+1?>""><span>></span></a></li>
		<?endif;?>

		<?$arResult["nStartPage"]++;

	} while($arResult["nStartPage"] <= $arResult["nEndPage"]);

?>
				</ul>
			</div>
		</div>
	</div>
</div>


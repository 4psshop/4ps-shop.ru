<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

 
?>
<?
/***CustomPageNav start***/
$nPageWindow = 3; //количество отображаемых страниц 
if ($arResult["NavPageNomer"] > floor($nPageWindow/2) + 1 && $arResult["NavPageCount"] > $nPageWindow)
    $nStartPage = $arResult["NavPageNomer"] - floor($nPageWindow/2);
else
    $nStartPage = 1;
 
if ($arResult["NavPageNomer"] <= $arResult["NavPageCount"] - floor($nPageWindow/2) && $nStartPage + $nPageWindow-1 <= $arResult["NavPageCount"])
    $nEndPage = $nStartPage + $nPageWindow - 1;
else
{
    $nEndPage = $arResult["NavPageCount"];
    if($nEndPage - $nPageWindow + 1 >= 1)
        $nStartPage = $nEndPage - $nPageWindow + 1;
}
$arResult["nStartPage"] = $arResult["nStartPage"] = $nStartPage;
$arResult["nEndPage"] = $arResult["nEndPage"] = $nEndPage;
/***CustomPageNav end***/
?> 



<div class="filters" style="padding: 6px 5px;">
	<div class="filters__bottom-bottom">
		<div class="filters__bottom-bottom-right">
			<div class="pagination">
				<span class="label label--inline pagination__label">Страница:</span>
				<ul class="pagination__list">
				<?if($arResult["bDescPageNumbering"] === true):?>

					<?if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
						<?if($arResult["bSavePage"]):?>
							<li class="pagination__list-item"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span><</span></a></li>
							<li class="pagination__list-item">
							<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
							<span class="pagination__list-item-link">1</span>
							</a>
							</li>
						<?else:?>
							<?if (($arResult["NavPageNomer"]+1) == $arResult["NavPageCount"]):?>
								<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><</span></a></li>
							<?else:?>
								<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>"><span><</span></a></li>
							<?endif?>
							<li class="pagination__list-item"><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="pagination__list-item-link"><span>1</span></a></li>
						<?endif?>
					<?else:?>
							<li class="pagination__list-item"><span class="pagination__list-item-link"><</span></li>
							<li class="pagination__list-item current"><span class="pagination__list-item-link">1</span></li>
					<?endif?>

					<?
					$arResult["nStartPage"]--;
					while($arResult["nStartPage"] >= $arResult["nEndPage"]+1):
					?>
						<?$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;?>

						<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
							<li class="pagination__list-item current"><span class="pagination__list-item-link"><?=$NavRecordGroupPrint?></span></li>
						<?else:?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"><span><?=$NavRecordGroupPrint?></span></a></li>
						<?endif?>

						<?$arResult["nStartPage"]--?>
					<?endwhile?>

					<?if ($arResult["NavPageNomer"] > 1):?>
						<?if($arResult["NavPageCount"] > 1):?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><span><?=$arResult["NavPageCount"]?></span></a></li>
						<?endif?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span>></span></a></li>
					<?else:?>
						<?if($arResult["NavPageCount"] > 1):?>
							<li class="pagination__list-item current"><span class="pagination__list-item-link"><?=$arResult["NavPageCount"]?></span></li>
						<?endif?>
							<li class="pagination__list-item"><span class="pagination__list-item-link">></span></li>
					<?endif?>

				<?else:?>

					<?if ($arResult["NavPageNomer"] > 1):?>
						<?if($arResult["bSavePage"]):?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span><</span></a></li>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><span>1</span></a></li>
						<?else:?>
							<?if ($arResult["NavPageNomer"] > 2):?>
								<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>"><span><</span></a></li>
							<?else:?>
								<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span><</span></a></li>
							<?endif?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>"><span>1</span></a></li>
						<?endif?>
					<?else:?>
							<li class="pagination__list-item"><span class="pagination__list-item-link"><</span></li>
							<li class="pagination__list-item current"><span class="pagination__list-item-link">1</span></li>
					<?endif?>

					<?
					$arResult["nStartPage"]++;
					while($arResult["nStartPage"] <= $arResult["nEndPage"]-1):
					?>
						<?if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
							<li class="pagination__list-item current"><span class="pagination__list-item-link"><?=$arResult["nStartPage"]?></span></li>
						<?else:?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>">
							<span><?=$arResult["nStartPage"]?></span></a></li>
						<?endif?>
						<?$arResult["nStartPage"]++?>
					<?endwhile?>

					<?if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):?>
						<?if($arResult["NavPageCount"] > 1):?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">
							<span><?=$arResult["NavPageCount"]?></span></a></li>
						<?endif?>
							<li class="pagination__list-item"><a class="pagination__list-item-link" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>">
							<span>></span></a></li>
					<?else:?>
						<?if($arResult["NavPageCount"] > 1):?>
							<li class="pagination__list-item current"><span class="pagination__list-item-link"><?=$arResult["NavPageCount"]?></span></li>
						<?endif?>
							<li class="pagination__list-item"><span class="pagination__list-item-link">></span></li>
					<?endif?>
				<?endif?>

				<?if ($arResult["bShowAll"]):?>
					<?if ($arResult["NavShowAll"]):?>
							<li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0" rel="nofollow"><span>Страницы</span></a></li>
					<?else:?>
							<li class="bx-pag-all"><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1" rel="nofollow"><span>Все</span></a></li>
					<?endif?>
				<?endif?>
				</ul> 
			</div>
		</div>
	</div>
</div>
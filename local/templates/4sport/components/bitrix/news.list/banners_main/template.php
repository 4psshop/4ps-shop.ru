<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<div class="col-sm-100 col-md-50">
		<a href="<?=$arItem['PROPERTIES']['LINK']['VALUE'];?>" target="_blank">
			<div class="video">
				<div class="video__image">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="video__image-img" alt="">
				</div>
				<div class="icon-play-round-red video__btn">
				</div>
			</div>
		</a>	
	</div>
	<?endforeach;?>
</div>









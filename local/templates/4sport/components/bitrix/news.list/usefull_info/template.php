<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<!-- content-box -->
<div class="content-box content-box--about">
	<!-- content-box__left -->
	<div class="content-box__left content-box__left--wide">
		<!-- video -->
		<div class="video">
			<ul class="video__list">
				<li class="video__item">
					<!-- video container -->
					<div class="video-container">
						<?=htmlspecialcharsBack($arResult["ITEMS"][0]["PROPERTIES"]["VIDEO1"]["VALUE"]["TEXT"])?>
					</div>
					<!-- video container end -->
				</li>
				<li class="video__item">
					<!-- video container -->
					<div class="video-container">
			    		<?=htmlspecialcharsBack($arResult["ITEMS"][0]["PROPERTIES"]["VIDEO2"]["VALUE"]["TEXT"])?>
					</div>
					<!-- video container end -->
				</li>
			</ul>
		</div>
		<!-- video end -->
	</div>
	<!-- content-box__left end -->

	<!-- content-box__right -->
	<div class="content-box__right content-box__right--tight">
		<div class="side-contacts">
			<div class="contacts contacts--no-decor">
				<div class="contacts__block">
					<span class="contacts__text"><?=htmlspecialcharsBack($arResult["ITEMS"][0]["PROPERTIES"]["TEXT"]["VALUE"]["TEXT"])?></span>
				</div>
			</div>
		</div>
	</div>
	<!-- content-box__right end -->
	
</div>
<!-- content-box end -->
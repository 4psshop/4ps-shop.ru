<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- big text prosport -->
<?/*?><span class="intro__bg-text" style="margin-top: 335px;">prosport</span><?*/ //текст на заднем фоне?>
<div class="single-slide main-banner-slider">
	<!-- slides -->
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<div class="intro__holder">
			<?if(!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])):?>
				<a href="<?echo $arItem["PROPERTIES"]["LINK"]["VALUE"];?>">
			<?endif;?>
			<?if($arItem["PROPERTIES"]["FULL"]["VALUE"] != "Да"):?>
				<span
					class="intro__bg-img"
					style="
						background: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);
						background-size: 100% auto;
						background-repeat: no-repeat;
						background-position: center bottom;
				">
				</span>
			<?else:?>
				<style>
				.single-slide {
					min-height: 511px;
					height: 100%;
				}
				.intro__bg-img1 {
					bottom: 0;
					margin: 0px;
				}
				.intro__holder.slick-slide {
					min-height: 511px;
					height: 100%;
				}
				@media (max-width: 868px) {
					.intro__holder.slick-slide {
						min-height: 211px;
						height: 100%;
					}
					.intro__bg-img1 {
						bottom: 0;
						margin: 0px;
						height:300px;
					}
					.single-slide {
						min-height: 211px;
						height: 100%;
					}
					.intro__holder.slick-slide {
						max-height: 211px;
					}
					.intro__bg-img1{
						display:block;
					}
				}
				</style>
				<span
					id="sps_img"
					class="intro__bg-img1"
					style="
						background: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>);
						background-size: 100% auto;
						background-repeat: no-repeat;
						background-position: center bottom;
				">
				</span>
			<?endif;?>
			<?if($arItem["PROPERTIES"]["FULL"]["VALUE"] != "Да"):?>
				<div class="intro__content">
					<?echo $arItem["PREVIEW_TEXT"];?>
				</div>
			<?else:?>
				<div class="intro__content" id="sps">
					<?echo $arItem["PREVIEW_TEXT"];?>
				</div>
			<?endif;?>
			<?if(!empty($arItem["PROPERTIES"]["LINK"]["VALUE"])):?>
				</a>
			<?endif;?>
		</div>
 	<?endforeach;?>
</div>
<script>
// slick slider
$(document).ready(function(){
  $('.single-slide').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: <?=$arResult['TIME'];?>,
		infinite: true,
		// speed: 500,
		fade: true,
		dots: true,
		 dotsClass: "my-dots",
		centerMode: true,
		cssEase: 'linear',
		prevArrow: '',
		nextArrow: '',
		//adaptiveHeight: true,
  });
});
</script>
<style type="text/css">
	div#sps {
    display: none;
}
@media (max-width: 1023px){
div#sps {
    display: block;
}
}
</style>
<style type="text/css">
.my-dots {
  position: absolute;
  bottom: -45px;
  display: block;
  width: 100%;
  padding: 0;
  list-style: none;
  text-align: center;
}
.my-dots li {
  position: relative;
  display: inline-block;
  width: 20px;
  height: 20px;
  margin: 0 5px;
  padding: 0;
  cursor: pointer;
}
.my-dots li button {
  font-size: 0;
  line-height: 0;
  display: block;
  width: 20px;
  height: 20px;
  padding: 5px;
  cursor: pointer;
  color: transparent;
  border: 0;
  outline: none;
  background: transparent;
}
.my-dots li button:hover,
.my-dots li button:focus {
  outline: none;
}
.my-dots li button:hover:before,
.my-dots li button:focus:before {
  opacity: 1;
}
.my-dots li button:before {
  font-family: 'slick';
  font-size: 6px;
  line-height: 20px;
  position: absolute;
  top: 0;
  left: 0;
  width: 20px;
  height: 20px;
  content: url('https://upload.wikimedia.org/wikipedia/commons/b/b7/Rule_Segment_-_Circle_open_-_20px.svg');
  text-align: center;
  opacity: .25;
  color: black;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.my-dots li.slick-active button:before {
  opacity: .75;
  color: black;
}
.slick-dotted.slick-slider {
    margin-bottom: 60px;
}


/* Корректировки стилей - делаем слайдер под адаптив, пропорциональное уменьшение */

		.main-banner-slider.slick-dotted.slick-slider {
		    padding-top: 20px;
		}
		.main-banner-slider.single-slide {
		    min-height: 0;
		}
		.main-banner-slider .intro__holder.slick-slide {
		    min-height: 0;
		}
		.main-banner-slider .intro__holder {
		    padding: 0;
		    position: relative;
		}
		.main-banner-slider .intro__bg-img1{
		  position: static;
		  display: block;
		  padding-top: 41%;
		  height: auto;
		}

		@media (max-width: 1125px){
		  .main-banner-slider .slick-track .intro__holder.slick-slide, 
		  .main-banner-slider.single-slide {
		      min-height: 0;
		  }
		}

		@media (max-width: 868px){
		  .main-banner-slider .intro__holder.slick-slide {
		      max-height: none;
		  }  
		  .main-banner-slider .my-dots {
		    bottom: -40px;
		  }
		}



</style>
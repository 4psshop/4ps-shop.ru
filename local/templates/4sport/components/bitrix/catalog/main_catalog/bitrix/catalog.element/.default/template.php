<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arEquipmentSections = [];
$obCache = \Bitrix\Main\Application::getInstance()
    ->getCache();
if ($obCache->startDataCache(60 * 60 * 24, md5(__FILE__.__LINE__))) {

    $rsSections = CIBlockSection::GetTreeList(['IBLOCK_ID' => 2], ['ID', 'IBLOCK_SECTION_ID', 'DEPTH_LEVEL', 'NAME']);
    $arSections = [];
    global $LastSection;
    $LastSection = [];
    while ($arSect = $rsSections->GetNext()) {
        global $LastSection;
        if ($arSect['DEPTH_LEVEL'] == 1) {
            $arSections[$arSect['ID']] = $arSect;
            $LastSection[$arSect['DEPTH_LEVEL']] =& $arSections[$arSect['ID']];
        } else {
            $LastSection[$arSect['DEPTH_LEVEL'] - 1]['CHILDS'][$arSect['ID']] = $arSect;
            $LastSection[$arSect['DEPTH_LEVEL']] =& $LastSection[$arSect['DEPTH_LEVEL'] - 1]['CHILDS'][$arSect['ID']];
        }
    }

    function wblReq($arResult)
    {
        $arTmp = [$arResult['ID']];
        if (!empty($arResult['CHILDS'])) {
            foreach ($arResult['CHILDS'] as $arLvl) {
                $arTmp = array_merge($arTmp, wblReq($arLvl));
            }
        }

        return $arTmp;
    }

    $arEquipmentSections = wblReq($arSections[54]);
    $obCache->endDataCache($arEquipmentSections);
} else {
    $arEquipmentSections = $obCache->getVars();
}

$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES'])) {
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
    // 'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
    //'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);
$strMainID = $this->GetEditAreaId($arResult['ID']);
$arItemIDs = array(
    'ID' => $strMainID,
    'PICT' => $strMainID.'_pict',
    'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
    'STICKER_ID' => $strMainID.'_sticker',
    'BIG_SLIDER_ID' => $strMainID.'_big_slider',
    'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
    'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
    'SLIDER_LIST' => $strMainID.'_slider_list',
    'SLIDER_LEFT' => $strMainID.'_slider_left',
    'SLIDER_RIGHT' => $strMainID.'_slider_right',
    'OLD_PRICE' => $strMainID.'_old_price',
    'PRICE' => $strMainID.'_price',
    'DISCOUNT_PRICE' => $strMainID.'_price_discount',
    'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
    'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
    'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
    'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
    'QUANTITY' => $strMainID.'_quantity',
    'QUANTITY_DOWN' => $strMainID.'_quant_down',
    'QUANTITY_UP' => $strMainID.'_quant_up',
    'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
    'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
    'BASIS_PRICE' => $strMainID.'_basis_price',
    'BUY_LINK' => $strMainID.'_buy_link',
    'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
    'BASKET_ACTIONS' => $strMainID.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
    'COMPARE_LINK' => $strMainID.'_compare_link',
    'PROP' => $strMainID.'_prop_',
    'PROP_DIV' => $strMainID.'_skudiv',
    'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
    'OFFER_GROUP' => $strMainID.'_set_group_',
    'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
    'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;
$strTitle = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
    : $arResult['NAME']
);
$strAlt = (
isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
    ? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
    : $arResult['NAME']
);
?>


<?
$jsonToYA = [
    'id' => $arResult['ID'],
    'name' => $arResult['NAME'],
    'price' => $arResult['MIN_BASIS_PRICE']['DISCOUNT_VALUE_VAT']
];
?>
    <div class="product-full" itemscope itemtype="http://schema.org/Product" id="<? echo $arItemIDs['ID']; ?>">
        <!-- name -->
        <span class="hrenShow" itemprop="name">
		<?=$arResult['NAME'];?>
	 </span>
        <div class="product-full__top">
            <!-- product-full__top-left -->
            <div class="product-full__top-left">
                <div class="product-full__images">
                    <!-- product-full__images-large-slider -->
                    <div class="product-full__images-large-slider js-product-full__images-large-slider" id="<? echo $arItemIDs['BIG_SLIDER_ID']; ?>">
                        <div class="product-full__images-large-slider-item">
                            <a href="<? echo $arResult["PREVIEW_PICTURE"]['SRC']; ?>" class="product-full__images-large-image js-image-link">
							<span class="product-full__images-large-image-cell">
								<img itemprop="image" id="<? echo $arItemIDs['PICT']; ?>" src="<? echo $arResult["PREVIEW_PICTURE"]['SRC']; ?>" alt="<? echo $strAlt; ?>" title="<? echo $strTitle; ?>" style='display: none;'>
								<img src="<? echo $arResult["PREVIEW_PICTURE"]['SRC']; ?>" alt="" class="product-full__images-large-image-img" style="width:474px!important;">
							</span>
                                <svg class="icon-zoom product-full__images-large-image-zoom">
                                    <use xlink:href="#zoom"></use>
                                </svg>
                            </a>
                        </div>
                        <? foreach ($arResult["GALLERY_SRC"] as $src_sl): ?>
                            <div class="product-full__images-large-slider-item">
                                <a href="<? echo $src_sl; ?>" class="product-full__images-large-image js-image-link">
								<span class="product-full__images-large-image-cell">
									<img src="<? echo $src_sl; ?>" alt="" class="product-full__images-large-image-img" style="width:474px!important;">
								</span>
                                    <svg class="icon-zoom product-full__images-large-image-zoom">
                                        <use xlink:href="#zoom"></use>
                                    </svg>
                                </a>
                            </div>
                        <? endforeach ?>
                    </div>
                    <!-- product-full__images-large-slider end -->
                    <div class="product-full__images-small">
                        <div class="product-full__images-small-slider-wrap">
                            <div class="product-full__images-small-slider js-product-full__images-small-slider">
                                <div class="product-full__images-small-slider-item product-full__images-small-slider-item--active">
                                    <div class="product-full__images-small-image">
									<span class="product-full__images-small-image-cell">
										<img src="<? echo $arResult["PREVIEW_PICTURE"]['SRC']; ?>" alt="" class="product-full__images-small-image-img" width="54">
									</span>
                                    </div>
                                </div>
                                <? foreach ($arResult["GALLERY_SRC"] as $src_sl): ?>
                                    <div class="product-full__images-small-slider-item">
                                        <div class="product-full__images-small-image">
										<span class="product-full__images-small-image-cell">
											<img src="<? echo $src_sl; ?>" alt="" class="product-full__images-small-image-img" width="54">
										</span>
                                        </div>
                                    </div>
                                <? endforeach ?>
                            </div>
                        </div>
                    </div>
                    <!-- product-full__images-small-slider end -->
                </div>
            </div>
            <!-- product-full__top-right -->
            <div class="product-full__top-right">
                <!-- product-full__info -->
                <div class="product-full__info">
                    <div class="product-full__info-top">
                        <div class="product-full__info-index">Артикул:<? echo $arResult['PROPERTIES']['ARTICUL']['VALUE']; ?></div>
                        <div class="product-full__info-developer"><? echo $arResult['PROPERTIES']['BRAND']['VALUE']; ?></div>
                        <div class="product-full__info-all">Все товары <a href="/catalog?BRAND=<? echo $arResult['PROPERTIES']['BRAND']['VALUE']; ?>"><? echo $arResult['PROPERTIES']['BRAND']['VALUE']; ?></a></div>
                    </div>
                    <div class="product-full__info-content" itemprop="description">
                        <?
                        if (!empty($arResult['PREVIEW_TEXT'])) {
                            echo $arResult['PREVIEW_TEXT'];
                        } else {
                            echo $arResult['DETAIL_TEXT'];
                        }
                        ?>
                    </div>

                    <!-- block price					 -->
                    <div class="product-full__info-buttons">
                        <?
                        // Точильное оборудование
                        if (in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections)) {
                            ?>

                            <div class="product-full__info-buttons-top">
                                <!-- old price -->
                                <div class="product-full__info-buttons-top-left">
                                    <?
                                    $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
                                    $boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
                                    ?>
                                    <?
                                    if ($arParams['SHOW_OLD_PRICE'] == 'Y' && $minPrice['VALUE'] < 15000) {
                                        ?>
                                        <span class="product-full__info-price-old" id="<? echo $arItemIDs['OLD_PRICE']; ?>">
								<? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?>
								</span>
                                        <?
                                    }
                                    ?>
                                </div>
                                <!-- nalichie -->
                                <div class="product-full__info-buttons-top-right">
                                    <?
                                    if ($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID'] == 'calc' || empty($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID'])) {
                                        ?><?
                                        if (empty($arResult['OFFERS'])):?>
                                            <?
                                            if (in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $minPrice['DISCOUNT_VALUE'] >= 15000):?>
                                                <span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Под заказ</span>
                                    </span>
                                            <?
                                            elseif (
                                                (!in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $arResult['CATALOG_QUANTITY'] > 0)
                                                || (in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $minPrice['DISCOUNT_VALUE'] < 15000 && $arResult['CATALOG_QUANTITY'] > 0)
                                            ):?>
                                                <span class="product-full__info-available">
                                        <svg class="icon-check-round-green product-full__info-available-icon">
                                            <use xlink:href="#check-round"></use>
                                        </svg>
                                        <span class="product-full__info-available-text">в наличии <?$rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $arParams["ELEMENT_ID"], 'STORE_ID' => 1), false, false, array('AMOUNT'));
						if ($arStore = $rsStore->Fetch())
						{
							echo $arStore['AMOUNT'];
						}?></span>
                                    </span>
                                            <? else: ?>
                                                <span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Нет в наличии</span>
                                    </span>
                                            <? endif; ?>
                                        <? endif; ?><?
                                    } else {
                                        switch ($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID']) {
                                            case 'in':
                                                ?>
                                                <span class="product-full__info-available">
                                        <svg class="icon-check-round-green product-full__info-available-icon">
                                            <use xlink:href="#check-round"></use>
                                        </svg>
                                        <span class="product-full__info-available-text">в наличии <?$rsStore = CCatalogStoreProduct::GetList(array(), array('PRODUCT_ID' => $arParams["ELEMENT_ID"], 'STORE_ID' => 1), false, false, array('AMOUNT'));
						if ($arStore = $rsStore->Fetch())
						{
							echo $arStore['AMOUNT'];
						}?></span>
                                    </span>
                                                <?
                                                break;
                                            case 'out':
                                                ?><span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Под заказ</span>
                                    </span><?
                                                break;
                                        }
                                    }
                                    ?>


                                </div>
                            </div>
                            <!-- price -->
                            <div class="product-full__info-buttons-bottom">
                                <?
                                if ($minPrice['DISCOUNT_VALUE'] < 15000):?>
                                    <div class="product-full__info-buttons-bottom-left">
                                        <div class="product-full__info-buttons-bottom-left-inner">
                                            <span class="product-full__info-price" id="<? echo $arItemIDs['PRICE']; ?>">от <? echo $minPrice['DISCOUNT_VALUE']; ?> р.</span>
                                        </div>
                                    </div>
                                <? endif; ?>

                                <!--Указывается схема Offer.-->
                                <div itemprop="offers" class="hrenShow" itemscope itemtype="http://schema.org/Offer">

                                    <!--В поле price указывается цена товара.-->
                                    <span itemprop="price"><? echo $minPrice['DISCOUNT_VALUE']; ?></span>

                                    <!--В поле priceCurrency указывается валюта.-->
                                    <span itemprop="priceCurrency">RUB</span>
                                </div>

                                <div class="product-full__info-buttons-bottom-right" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">

                                    <?
                                    if (empty($arResult['OFFERS'])):?>
                                        <!-- quantity -->
                                        <span class="item_buttons_counter_block" style="display: none;">
                                    <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
                                    <input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo(isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
                                        ? 1
                                        : $arResult['CATALOG_MEASURE_RATIO']
                                    ); ?>">
                                    <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
                                    <span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo(isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
                                </span>
                                        <?
                                        if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
                                            $canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
                                        } else {
                                            $canBuy = $arResult['CAN_BUY'];
                                        }
                                        $buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
                                        $addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
                                        $notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
                                        $showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
                                        // $showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
                                        if ($arResult['CATALOG_SUBSCRIBE'] == 'Y') {
                                            $showSubscribeBtn = true;
                                        } else {
                                            $showSubscribeBtn = false;
                                        }
                                        $compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));
                                        ?>

                                        <? //Если товар дороже 15000 выводит кнопку для вызова popup
                                        ?>
                                        <?
                                        if ($canBuy && ($minPrice['DISCOUNT_VALUE'] > 15000)):?>
                                            <a href="#" class="btn btn-red btn--cart-left myBtn_product" prod="<?=$arResult['ID']?>">
                                    <span class="btn-black__inner">
                                        <span class="btn__text btn__text--middle">
                                            Цена по запросу
                                        </span>
                                    </span>
                                            </a>
                                            <? /*$APPLICATION->IncludeComponent(
                                    "bitrix:main.include",
                                    "",
                                    Array(
                                        "AREA_FILE_SHOW" => "file",
                                        "AREA_FILE_SUFFIX" => "inc",
                                        "COMPOSITE_FRAME_MODE" => "A",
                                        "COMPOSITE_FRAME_TYPE" => "AUTO",
                                        "EDIT_TEMPLATE" => "standard.php",
                                        "PATH" => "/include/4ps/modal_product.php"
                                    )
                                );*/
                                            ?>
                                        <? else: ?>
                                            <?
                                            if ($showBuyBtn) {
                                                ?>
                                                <span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo($canBuy ? '' : 'none'); ?>;">
                                    <!-- buy link -->
                                        <a href="javascript:void(0);" class="btn btn-red btn--cart-left" id="<? echo $arItemIDs['BUY_LINK']; ?>">
                                            <span class="btn-black__inner">
                                                <span class="btn__icon">
                                                    <svg class="icon-cart-white-medium">
                                                        <use xlink:href="#cart"></use>
                                                    </svg>
                                                </span>
                                                <span class="btn__text btn__text--middle">
                                                В корзину
                                                </span>
                                            </span>
                                        </a>
                                    </span>
                                                <?
                                            }
                                            ?>
                                            <?
                                            if ($showSubscribeBtn) {
                                                ?>
                                                <span class="btn btn-red btn--cart-left" style="display: <? echo($canBuy ? 'none' : ''); ?>;">
                                        <?
                                        $APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe', '',
                                            array(
                                                'PRODUCT_ID' => $arResult['ID'],
                                                'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                                                'BUTTON_CLASS' => 'bx_big bx_bt_button',
                                                'DEFAULT_DISPLAY' => !$canBuy,
                                            ),
                                            $component, array('HIDE_ICONS' => 'Y')
                                        );
                                        ?>
                                    </span>
                                                <?
                                            }
                                            ?>
                                            <?
                                            unset($showAddBtn, $showBuyBtn);
                                            ?>
                                            <a href="#inline" class="modalbox product-full__info-oneclick" id="myBuyOne">Купить в один клик</a>
                                        <? endif; ?>

                                    <? endif; ?>

                                </div>
                            </div>
                            <span class="product-full__info-buttons-attention">Точная стоимость может варьироваться от комплектации и аксессуаров</span>
                            <?
                        } // остальное
                        else {
                            ?>

                            <div class="product-full__info-buttons-top">
                                <!-- old price -->
                                <div class="product-full__info-buttons-top-left">
                                    <?
                                    $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']);
                                    $boolDiscountShow = (0 < $minPrice['DISCOUNT_DIFF']);
                                    ?>
                                    <?
                                    if ($arParams['SHOW_OLD_PRICE'] == 'Y' && $arResult['PROPERTIES']['SHOW_PRICE']['VALUE'] == '1') {
                                        ?>
                                        <span class="product-full__info-price-old" id="<? echo $arItemIDs['OLD_PRICE']; ?>">
                                    <? echo($boolDiscountShow ? $minPrice['PRINT_VALUE'] : ''); ?>
                                    </span>
                                        <?
                                    }
                                    ?>
                                </div>
                                <!-- nalichie -->
                                <div class="product-full__info-buttons-top-right">
                                    <?
                                    if ($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID'] == 'calc' || empty($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID'])) {

                                        ?>
                                        <?
                                        if (empty($arResult['OFFERS'])): ?>
                                            <?
                                            if (in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $minPrice['DISCOUNT_VALUE'] >= 15000):?>
                                                <span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Под заказ</span>
                                    </span>
                                            <?
                                            elseif (
                                                (!in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $arResult['CATALOG_QUANTITY'] > 0)
                                                || (in_array($arResult['IBLOCK_SECTION_ID'], $arEquipmentSections) && $minPrice['DISCOUNT_VALUE'] < 15000 && $arResult['CATALOG_QUANTITY'] > 0)
                                            ):?>
                                                <span class="product-full__info-available">
                                        <svg class="icon-check-round-green product-full__info-available-icon">
                                            <use xlink:href="#check-round"></use>
                                        </svg>
                                        <span class="product-full__info-available-text">в наличии</span>
                                    </span>
                                            <? else: ?>
                                                <span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Нет в наличии</span>
                                    </span>
                                            <? endif; ?>
                                        <? endif; ?>

                                        <?

                                    } else {
                                        switch ($arResult['PROPERTIES']['STOCK_STATUS']['VALUE_XML_ID']) {
                                            case 'in':
                                                ?>
                                                <span class="product-full__info-available">
                                        <svg class="icon-check-round-green product-full__info-available-icon">
                                            <use xlink:href="#check-round"></use>
                                        </svg>
                                        <span class="product-full__info-available-text">в наличии </span>
                                    </span>
                                                <?
                                                break;
                                            case 'out':
                                                ?>
                                                <span class="product-full__info-available">
                                        <span class="basket-table__availability basket-table__availability--absent">Под заказ</span>
                                    </span>
                                                <?
                                                break;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <!-- price -->
                            <div class="product-full__info-buttons-bottom">
                                <?
                                if ($arResult['PROPERTIES']['SHOW_PRICE']['VALUE'] == '1'):?>
                                    <div class="product-full__info-buttons-bottom-left">
                                        <div class="product-full__info-buttons-bottom-left-inner">
                                            <span class="product-full__info-price" id="<? echo $arItemIDs['PRICE']; ?>">от <? echo $minPrice['DISCOUNT_VALUE']; ?> р.</span>
                                        </div>
                                    </div>
                                <? endif; ?>

                                <!--Указывается схема Offer.-->
                                <div itemprop="offers" class="hrenShow" itemscope itemtype="http://schema.org/Offer">

                                    <!--В поле price указывается цена товара.-->
                                    <span itemprop="price"><? echo $minPrice['DISCOUNT_VALUE']; ?></span>

                                    <!--В поле priceCurrency указывается валюта.-->
                                    <span itemprop="priceCurrency">RUB</span>
                                </div>

                                <div class="product-full__info-buttons-bottom-right" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>">

                                    <?
                                    if (empty($arResult['OFFERS'])):?>
                                        <!-- quantity -->
                                        <span class="item_buttons_counter_block" style="display: none;">
                                    <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_DOWN']; ?>">-</a>
                                    <input id="<? echo $arItemIDs['QUANTITY']; ?>" type="text" class="tac transparent_input" value="<? echo(isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])
                                        ? 1
                                        : $arResult['CATALOG_MEASURE_RATIO']
                                    ); ?>">
                                    <a href="javascript:void(0)" class="bx_bt_button_type_2 bx_small bx_fwb" id="<? echo $arItemIDs['QUANTITY_UP']; ?>">+</a>
                                    <span class="bx_cnt_desc" id="<? echo $arItemIDs['QUANTITY_MEASURE']; ?>"><? echo(isset($arResult['CATALOG_MEASURE_NAME']) ? $arResult['CATALOG_MEASURE_NAME'] : ''); ?></span>
                                </span>
                                        <?
                                        if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
                                            $canBuy = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['CAN_BUY'];
                                        } else {
                                            $canBuy = $arResult['CAN_BUY'];
                                        }
                                        $buyBtnMessage = ($arParams['MESS_BTN_BUY'] != '' ? $arParams['MESS_BTN_BUY'] : GetMessage('CT_BCE_CATALOG_BUY'));
                                        $addToBasketBtnMessage = ($arParams['MESS_BTN_ADD_TO_BASKET'] != '' ? $arParams['MESS_BTN_ADD_TO_BASKET'] : GetMessage('CT_BCE_CATALOG_ADD'));
                                        $notAvailableMessage = ($arParams['MESS_NOT_AVAILABLE'] != '' ? $arParams['MESS_NOT_AVAILABLE'] : GetMessageJS('CT_BCE_CATALOG_NOT_AVAILABLE'));
                                        $showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
                                        // $showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
                                        if ($arResult['CATALOG_SUBSCRIBE'] == 'Y') {
                                            $showSubscribeBtn = true;
                                        } else {
                                            $showSubscribeBtn = false;
                                        }
                                        $compareBtnMessage = ($arParams['MESS_BTN_COMPARE'] != '' ? $arParams['MESS_BTN_COMPARE'] : GetMessage('CT_BCE_CATALOG_COMPARE'));
                                        ?>

                                        <? //Если товар дороже 15000 выводит кнопку для вызова popup
                                        ?>12
                                        <?
                                        if ($canBuy && ($arResult['PROPERTIES']['SHOW_PRICE']['VALUE'] != '1')):?>
                                            <a href="#" class="btn btn-red btn--cart-left myBtn_product" prod="<?=$arResult['ID']?>">
                                            <span class="btn-black__inner">
                                                <span class="btn__text btn__text--middle">
                                                    Цена по запросу
                                                </span>
                                            </span>
                                            </a>
                                        <? else: ?>
                                            <?
                                            if ($showBuyBtn) {
                                                ?>
                                                <span class="item_buttons_counter_block" id="<? echo $arItemIDs['BASKET_ACTIONS']; ?>" style="display: <? echo($canBuy ? '' : 'none'); ?>;">
                                    <!-- buy link -->
                                        <a href="javascript:void(0);" class="btn btn-red btn--cart-left" id="<? echo $arItemIDs['BUY_LINK']; ?>">
                                            <span class="btn-black__inner">
                                                <span class="btn__icon">
                                                    <svg class="icon-cart-white-medium">
                                                        <use xlink:href="#cart"></use>
                                                    </svg>
                                                </span>
                                                <span class="btn__text btn__text--middle">
                                                В корзину
                                                </span>
                                            </span>
                                        </a>
                                    </span>
                                                <?
                                            }
                                            ?>
                                            <?
                                            if ($showSubscribeBtn) {
                                                ?>
                                                <span class="btn btn-red btn--cart-left" style="display: <? echo($canBuy ? 'none' : ''); ?>;">
                                        <?
                                        $APPLICATION->IncludeComponent('bitrix:catalog.product.subscribe', '',
                                            array(
                                                'PRODUCT_ID' => $arResult['ID'],
                                                'BUTTON_ID' => $arItemIDs['SUBSCRIBE_LINK'],
                                                'BUTTON_CLASS' => 'bx_big bx_bt_button',
                                                'DEFAULT_DISPLAY' => !$canBuy,
                                            ),
                                            $component, array('HIDE_ICONS' => 'Y')
                                        );
                                        ?>
                                    </span>
                                                <?
                                            }
                                            ?>
                                            <?
                                            unset($showAddBtn, $showBuyBtn);
                                            ?>
                                            <a href="#inline" class="modalbox product-full__info-oneclick" id="myBuyOne">Купить в один клик</a>
                                        <? endif; ?>
                                    <? elseif (!empty($arResult['OFFERS']) && $arResult['PROPERTIES']['SHOW_PRICE']['VALUE'] != '1'): ?>
                                        <a href="#" class="btn btn-red btn--cart-left myBtn_product" prod="<?=$arResult['ID']?>">
                                            <span class="btn-black__inner">
                                                <span class="btn__text btn__text--middle">
                                                    Цена по запросу
                                                </span>
                                            </span>
                                        </a>
                                    <? endif; ?>

                                </div>
                            </div>
                            <span class="product-full__info-buttons-attention">Точная стоимость может варьироваться от комплектации и аксессуаров</span>
                            <?
                        }
                        ?>
                    </div>
                    <div id="inline" style="display: none;">
                        <h2>Купить в один клик</h2>

                        <form action="" method='POST' id="myFormBuyOne">
                            <!-- Modal content -->
                            <div class="modal-contentBuyOne">
                                <div class="form-row basket-form__row-indent">
                                    <div class="label basket-form__label">
                                        <label for="basket_form_name">Имя получателя</label>
                                    </div>
                                    <input type="text" name="basket_form_name" id="basket_form_name" class="input" placeholder="Ваше имя" required>
                                </div>
                                <div class="row basket-form__row">
                                    <div class="col-sm-100 col-md-50 basket-form__col">
                                        <div class="form-row basket-form__row-indent">
                                            <div class="label basket-form__label">
                                                <label for="basket_form_phone">Телефон</label>
                                            </div>
                                            <input type="text" name="basket_form_phone" id="basket_form_phone" class="input js-phone-mask" placeholder="7 (___) ___-__-__" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-100 col-md-50 basket-form__col">
                                        <div class="form-row basket-form__row-indent">
                                            <div class="label basket-form__label">
                                                <label for="basket_form_mail">E-mail</label>
                                            </div>
                                            <input type="email" name="basket_form_mail" id="basket_form_mail" class="input" placeholder="Ваш e-mail" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="basket-actions">
                                    <button type="submit" class="btn btn-red btn--cart-left basket-actions__btn">
									<span class="btn-black__inner">
										<span class="btn__icon">
											<svg class="icon-cart-white-large">
												<use xlink:href="#cart"></use>
											</svg>
										</span>
										<span class="btn__text btn__text--middle">Купить</span>
									</span>
                                    </button>
                                </div>
                                <div id="result"></div>
                            </div>
                            <!-- name goods -->
                            <input type="hidden" value="<? echo $arResult['NAME']; ?>" id="name_goods">
                        </form>
                    </div>

                    <script>
                        $(document).ready(function() {
                            $('.modalbox').fancybox();
                            $('#myFormBuyOne').submit(function() {
                                $.ajax({
                                    type: 'POST',
                                    url: '/ajax/buy_one_click.php',
                                    data: 'username=' + $('#basket_form_name').val() + '&email=' + $('#basket_form_mail').val() + '&phone=' + $('#basket_form_phone').val() + '&name_goods=' + $('#name_goods').val(),
                                    success: function(html) {
                                        // alert(123);
                                        $('#result').html(html);
                                        //show mod
                                        // modal.style.display = "block";
                                    }
                                });
                                return false;
                            });
                        });
                    </script>
                    <!-- end modal -->
                    <div class="product-full__info-bottom">
                        <div class="product-full__info-back">
                            Назад в <a href="#" onclick="history.back();return false;">каталог</a>
                        </div>
                    </div>
                </div>
                <!-- product-full__info end -->
            </div>
            <!-- product-full__top-right end -->
        </div>

        <!-- product-full__bottom -->
        <div class="product-full__bottom">

            <?

            if (!empty($arResult['OFFERS']) && $arResult['PROPERTIES']['SHOW_PRICE']['VALUE'] == '1'):

                // Соберм св-ва, которые являются ключевыми
                $arWblPropsTable = [];
                if (!empty($arResult['OFFERS'][0]['PROPERTIES'])) {
                    foreach ($arResult['OFFERS'][0]['PROPERTIES'] as $arOfferProps) {
                        if ($arOfferProps['SORT'] == 1488) {
                            if (!empty($arOfferProps['VALUE'])) {
                                $arWblPropsTable[] = ['CODE' => $arOfferProps['CODE'], 'NAME' => $arOfferProps['NAME']];
                            }
                        }
                    }
                }

                // Соберем таблицу значений по принципу $ar[ PROP_1 ][ PROP_2 ][ ID ] = QUANTITY
                $arWblOfferTable = [];
                foreach ($arResult['OFFERS'] as $arOffer) {
                    if (!empty($arOffer['PROPERTIES'])) {
                        $arWblOfferTable[$arOffer['PROPERTIES'][$arWblPropsTable[0]['CODE']]['VALUE']][$arOffer['PROPERTIES'][$arWblPropsTable[1]['CODE']]['VALUE']] = ['ID' => $arOffer['ID'], 'QUANTITY' => $arOffer['CATALOG_QUANTITY']];
                    }
                }

                // Соберем все возможные варианты зн-ний св-в
                $arProp1 = array_keys($arWblOfferTable);
                ksort($arProp1);
                $arProp2 = [];
                foreach ($arWblOfferTable as $prop2) {
                    $arProp2 = array_merge($arProp2, array_keys($prop2));
                }
                $arProp2 = array_unique($arProp2);
                ksort($arProp2);
                ?>


                <style type="text/css">
                    .wbl-item-offers-wrapper {
                        background: #fff;
                        margin: 0px 0 20px 20px;
                        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
                        -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
                        -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12);
                    }

                    .wbl-item-offers-wrapper .wbl-title-layer {
                        padding: 22px 30px;
                        border-bottom: 2px solid #F6F6F6;
                        font-family: "Bender";
                        color: #333333;
                        font-size: 24px;
                        font-weight: 900;
                        font-style: italic;
                    }

                    .wbl-item-offers-wrapper .wbl-title-layer .wbl-aside {
                        width: 120px;
                        display: inline-block;
                        border-right: 2px solid #F6F6F6;
                    }

                    .wbl-item-offers-wrapper .wbl-title-layer .wbl-center {
                        display: inline-block;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer {
                        font-family: "Bender";
                        color: #333333;
                        font-size: 16px;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wbl-aside {
                        width: 150px;
                        float: left;
                        border-right: 2px solid #F6F6F6;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wbl-aside .line {
                        padding: 10px 0 10px 30px;
                        border-bottom: 2px solid #F6F6F6;
                        height: 42px;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wbl-center {
                        width: calc(100% - 152px);
                        float: left;
                        overflow-x: auto;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wb-detail-table {
                        margin-top: -1px;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wb-detail-table tr {
                        margin: 0;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wb-detail-table td {
                        padding: 5px 10px 5px 10px;
                        border-bottom: 2px solid #F6F6F6;
                        border-right: 2px solid #F6F6F6;
                        height: 42px;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .wb-detail-table .wbl-input {
                        text-align: center;
                    }

                    .wbl-item-offers-wrapper .wbl-offers-layer .btn-red {
                        margin: 20px 0;
                        display: inline-block;
                    }

                    .wbl-desk {
                        display: block;
                    }

                    .wbl-mobile {
                        display: none;
                        padding: 20px
                    }

                    @media (max-width: 980px) {
                        .wbl-desk {
                            display: none;
                        }

                        .wbl-mobile {
                            display: block;
                        }
                    }

                    .wbl-select {
                        width: 100%;
                        height: 52px;
                        background: #f4f4f4;
                        border: 1px solid #f4f4f4;
                        font-family: 'Bender';
                        font-style: italic;
                        font-size: 16px;
                        color: #050505;
                        border-radius: 3px;
                        -webkit-border-radius: 3px;
                        -moz-border-radius: 3px;
                        padding: 0 25px;
                        margin-bottom: 20px;
                    }

                    .wbl-mobile-title {
                        font-size: 24px;
                        font-weight: 900;
                        font-style: italic;
                        color: #333333;
                        font-family: "Bender";
                        padding-bottom: 20px;
                    }

                    .wbl-mobile .spinner {
                        margin-bottom: 20px;
                    }

                    .wbl-row .wbl-col-6 {
                        text-align: center;
                        width: 50%;
                        float: left;
                    }
                </style>

                <div class="wbl-item-offers-wrapper">

                    <div class="wbl-desk">

                        <div class="wbl-title-layer">
                            <div class="wbl-aside">
                                <?=$arWblPropsTable[0]['NAME']?>
                            </div>
                            <div class="wbl-center">
                                <?=$arWblPropsTable[1]['NAME']?>
                            </div>
                        </div>
                        <div class="wbl-offers-layer">
                            <div class="wbl-aside">
                                <div class="line">&nbsp;</div>
                                <? foreach ($arProp1 as $val): ?>
                                    <div class="line"><?=$val?></div>
                                <? endforeach; ?>
                            </div>
                            <div class="wbl-center">
                                <table class="wb-detail-table" id="wbl-offers-table">
                                    <tr>
                                        <? foreach ($arProp2 as $val): ?>
                                            <td><?=$val?></td>
                                        <? endforeach; ?>
                                    </tr>
                                    <? foreach ($arProp1 as $prop1Val): ?>
                                        <tr>
                                            <? foreach ($arProp2 as $prop2Val): ?>
                                                <td>
                                                    <? if (!empty($arWblOfferTable[$prop1Val][$prop2Val]) && $arWblOfferTable[$prop1Val][$prop2Val]['QUANTITY'] > 0): ?>


                                                        <? if ($minPrice['DISCOUNT_VALUE'] <= 15000): ?>
                                                            <nobr>
                                                                <div class="spinner">
                                                                    <a href="javascript:void(0);" class="spinner__control spinner__control--left js-spinner-minus"></a>
                                                                    <input type="text" class="spinner__input js-spinner" id="WBL_OFFERS_<?=$arWblOfferTable[$prop1Val][$prop2Val]['ID']?>" class="wbl-input" size="3" data-wbl-max-amount="<?=$arWblOfferTable[$prop1Val][$prop2Val]['QUANTITY']?>" data-wbl-item-id="<?=$arWblOfferTable[$prop1Val][$prop2Val]['ID']?>" value="0" />
                                                                    <a href="javascript:void(0);" class="spinner__control spinner__control--right js-spinner-plus"></a>
                                                                </div>
                                                            </nobr>
                                                        <? else: ?>
                                                            Цена по запросу
                                                        <? endif; ?>

                                                    <? else: ?>
                                                        -
                                                    <? endif; ?>
                                                </td>
                                            <? endforeach; ?>
                                        </tr>
                                    <? endforeach; ?>
                                </table>
                            </div>

                            <div style="clear: both;"></div>

                            <div style="text-align: center">

                                <? if ($minPrice['DISCOUNT_VALUE'] <= 15000): ?>
                                    <a href="javascript:void(0);" class="btn btn-red btn--cart-left" data-wbl-add-offers-2-basket="Y">
                            <span class="btn-black__inner">
                                <span class="btn__icon">
                                    <svg class="icon-cart-white-medium">
                                        <use xlink:href="#cart"></use>
                                    </svg>
                                </span>
                                <span class="btn__text btn__text--middle">
                                В корзину
                                </span>
                            </span>
                                    </a>
                                <? else: ?>
                                    <a href="javascript:void(0);" class="btn btn-red btn--cart-left myBtn_product" prod="<?=$arResult['ID']?>">
                            <span class="btn-black__inner">
                                <span class="btn__icon">
                                    <svg class="icon-cart-white-medium">
                                        <use xlink:href="#cart"></use>
                                    </svg>
                                </span>
                                <span class="btn__text btn__text--middle">
                                Запросить цену
                                </span>
                            </span>
                                    </a>
                                <? endif; ?>

                            </div>

                            <div style="clear: both;"></div>

                        </div>

                    </div>
                    <div class="wbl-mobile">
                        <a name="mobile-basket"></a>

                        <?
                        $arWblSelectValues = [];
                        $arWblOfferIDToQuantity = [];
                        foreach ($arResult['OFFERS'] as $arOffer) {
                            $arWblSelectValues[$arOffer['ID']] = ['TEXT' => $arOffer['PROPERTIES'][$arWblPropsTable[0]['CODE']]['VALUE'].' / '.$arOffer['PROPERTIES'][$arWblPropsTable[1]['CODE']]['VALUE'], 'QUANTITY' => $arOffer['CATALOG_QUANTITY']];
                            $arWblOfferIDToQuantity[$arOffer['ID']] = ($arOffer['CATALOG_QUANTITY'] > 0) ? $arOffer['CATALOG_QUANTITY'] : 0;
                        }
                        asort($arWblSelectValues);
                        ?>
                        <div class="wbl-mobile-title">
                            <?=$arWblPropsTable[0]['NAME']?> / <?=$arWblPropsTable[1]['NAME']?>
                        </div>

                        <select class="wbl-select" name="WBL_OFFER_ID">
                            <option value="0" disabled selected>-- Выберите параметры --</option>
                            <? foreach ($arWblSelectValues as $valueOffID => $arVal): ?>
                                <option value="<?=$valueOffID?>" <?=($arVal['QUANTITY'] < 1) ? 'disabled' : ''?>><?=$arVal['TEXT']?></option>
                            <? endforeach; ?>
                        </select>

                        <div class="wbl-row">
                            <div class="wbl-col-6">

                                <nobr>
                                    <div class="spinner">
                                        <a href="javascript:void(0);" class="spinner__control spinner__control--left js-spinner-minus"></a>
                                        <input type="text" class="spinner__input js-spinner wbl-input" id="WBL_OFFER_COUNT" size="5" data-wbl-max-amount="0" value="0" />
                                        <a href="javascript:void(0);" class="spinner__control spinner__control--right js-spinner-plus"></a>
                                    </div>
                                </nobr>

                            </div>
                            <div class="wbl-col-6">

                                <a href="javascript:void(0);" class="btn btn-red btn--cart-left" data-wbl-add-offers-2-basket="Y">
                                <span class="btn-black__inner">
                                    <span class="btn__icon">
                                        <svg class="icon-cart-white-medium">
                                            <use xlink:href="#cart"></use>
                                        </svg>
                                    </span>
                                    <span class="btn__text btn__text--middle">
                                    В корзину
                                    </span>
                                </span>
                                </a>

                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>

                </div>

                <div style="clear: both;"></div>

                <script type="text/javascript">
                    (() => {
                        // DESK
                        document.querySelector('.wbl-desk [data-wbl-add-offers-2-basket="Y"]').onclick = function(e) {
                            e.preventDefault();
                            let $this = this;

                            $this.querySelector('.btn__text').innerText = 'Добавляем...';

                            setTimeout(function() {
                                let boolWasAdd2Basket = false;
                                document.querySelectorAll('#wbl-offers-table input[id^="WBL_OFFERS_"]').forEach(function(html, i, val) {
                                    let inputVal = val[i].value;
                                    if (inputVal > 0) {
                                        let xmr = new XMLHttpRequest();
                                        xmr.open('GET', '/catalog/?' + 'action=ADD2BASKET&id=' + val[i].getAttribute('data-wbl-item-id') + '&quantity=' + val[i].value, false);
                                        xmr.send();
                                        boolWasAdd2Basket = true;
                                    }
                                });

                                $this.querySelector('.btn__text').innerText = 'В корзину';

                                if (boolWasAdd2Basket) {

                                    window.dataLayer.push({
                                        'ecommerce': {
                                            'add': {
                                                'products': [<?=json_encode($jsonToYA)?>]
                                            }
                                        }
                                    });

                                    window.location.href = window.location.pathname;
                                }
                            }, 10);

                        };

                        let funWblInpitChanged = function($this) {
                            let maxVal = $this.getAttribute('data-wbl-max-amount').toString().replace(/([^\d\-])/, '') - 0,
                                curVal = $this.value.toString().replace(/([^\d\-])/, '') - 0;

                            if (curVal < 0) {
                                curVal = 0;
                            }

                            if (curVal > maxVal) {
                                curVal = maxVal;
                            }

                            $this.value = curVal;
                        };

                        let WBLInputs = document.querySelectorAll('.wbl-desk [id^="WBL_OFFERS_"]');
                        for (i = 0; i < WBLInputs.length; i++) {
                            WBLInputs[i].addEventListener('change', function() {
                                funWblInpitChanged(this);
                            });
                            WBLInputs[i].addEventListener('keyup', function() {
                                funWblInpitChanged(this);
                            });
                        }

                        let WBLInputControl = document.querySelectorAll('.wbl-desk .spinner__control');
                        for (i = 0; i < WBLInputControl.length; i++) {
                            WBLInputControl[i].addEventListener('click', function() {

                                if (this.classList.contains('spinner__control--left')) {
                                    this.parentNode.querySelector('input[id^="WBL_OFFERS_"]').value = (this.parentNode.querySelector('input[id^="WBL_OFFERS_"]').value - 0 - 1);
                                }
                                else if (this.classList.contains('spinner__control--right')) {
                                    this.parentNode.querySelector('input[id^="WBL_OFFERS_"]').value = (this.parentNode.querySelector('input[id^="WBL_OFFERS_"]').value - 0 + 1);
                                }
                                this.parentNode.querySelector('input[id^="WBL_OFFERS_"]').dispatchEvent(new Event('change'));

                            });
                        }

                        // MOBILE
                        let jsonOfferIDToOfferPrice = JSON.parse('<?=json_encode($arWblOfferIDToQuantity)?>');
                        document.querySelector('.wbl-mobile [data-wbl-add-offers-2-basket="Y"]').onclick = function(e) {
                            e.preventDefault();

                            let offerID = document.querySelector('[name="WBL_OFFER_ID"]').value,
                                intQuantity = document.querySelector('.wbl-mobile #WBL_OFFER_COUNT').value;

                            if (offerID > 0 && intQuantity > 0) {

                                let xmr = new XMLHttpRequest();
                                xmr.open('GET', '/catalog/?' + 'action=ADD2BASKET&id=' + offerID + '&quantity=' + intQuantity, false);
                                xmr.send();

                                window.dataLayer.push({
                                    'ecommerce': {
                                        'add': {
                                            'products': [<?=json_encode($jsonToYA)?>]
                                        }
                                    }
                                });

                                alert('Товар добавлен в корзину');
                                jsonOfferIDToOfferPrice[offerID] = ((jsonOfferIDToOfferPrice[offerID] - 0) - (intQuantity - 0));
                                funSetMaxQuantityMobile(jsonOfferIDToOfferPrice[offerID]);
                                document.querySelector('.wbl-mobile #WBL_OFFER_COUNT').value = 0;

                            }

                        };

                        let funSetMaxQuantityMobile = function(max) {
                            document.querySelector('#WBL_OFFER_COUNT').setAttribute('data-wbl-max-amount', max);
                            document.querySelector('#WBL_OFFER_COUNT').value = 0;
                        };

                        document.querySelector('[name="WBL_OFFER_ID"]').onchange = function(e) {
                            funSetMaxQuantityMobile(jsonOfferIDToOfferPrice[this.value]);
                        };

                        document.querySelector('.wbl-mobile #WBL_OFFER_COUNT').addEventListener('change', function() {
                            funWblInpitChanged(this);
                        });
                        document.querySelector('.wbl-mobile #WBL_OFFER_COUNT').addEventListener('keyup', function() {
                            funWblInpitChanged(this);
                        });

                        let WBLInputControlMobile = document.querySelectorAll('.wbl-mobile .spinner__control');
                        for (i = 0; i < WBLInputControlMobile.length; i++) {
                            WBLInputControlMobile[i].addEventListener('click', function() {

                                if (this.classList.contains('spinner__control--left')) {
                                    this.parentNode.querySelector('#WBL_OFFER_COUNT').value = (this.parentNode.querySelector('#WBL_OFFER_COUNT').value - 0 - 1);
                                }
                                else if (this.classList.contains('spinner__control--right')) {
                                    this.parentNode.querySelector('#WBL_OFFER_COUNT').value = (this.parentNode.querySelector('#WBL_OFFER_COUNT').value - 0 + 1);
                                }
                                this.parentNode.querySelector('#WBL_OFFER_COUNT').dispatchEvent(new Event('change'));

                            });
                        }
                    })();
                </script>

            <? endif; ?>

            <div style="clear: both;"></div>

            <!-- product-full__bottom-left -->
            <div class="product-full__bottom-left">
                <!-- product-full__characteristics -->
                <div class="product-full__characteristics">
                    <div class="product-full__characteristics-head">
                        <h2 class="product-full__characteristics-head-title">Характеристики</h2>
                    </div>
                    <div class="product-full__characteristics-content">
                        <table class="characteristics-table">
                            <tbody class="characteristics-table__content">
                            <? if (!empty($arResult['PROPERTIES']['WEIGHT']['VALUE'])): ?>
                                <tr class="characteristics-table__row">
                                    <td class="characteristics-table__cell characteristics-table__cell--title"><?=$arResult['PROPERTIES']['WEIGHT']['NAME'];?></td>
                                    <td class="characteristics-table__cell characteristics-table__cell--value"><?=$arResult['PROPERTIES']['WEIGHT']['VALUE'];?></td>
                                </tr>
                            <? endif; ?>
                            <? if (!empty($arResult['PROPERTIES']['VOLTAGE']['VALUE'])): ?>
                                <tr class="characteristics-table__row">
                                    <td class="characteristics-table__cell characteristics-table__cell--title"><?=$arResult['PROPERTIES']['VOLTAGE']['NAME'];?></td>
                                    <td class="characteristics-table__cell characteristics-table__cell--value"><?=$arResult['PROPERTIES']['VOLTAGE']['VALUE'];?></td>
                                </tr>
                            <? endif; ?>
                            <? if (!empty($arResult['PROPERTIES']['WIDTH']['VALUE'])): ?>
                                <tr class="characteristics-table__row">
                                    <td class="characteristics-table__cell characteristics-table__cell--title"><?=$arResult['PROPERTIES']['WIDTH']['NAME'];?></td>
                                    <td class="characteristics-table__cell characteristics-table__cell--value"><?=$arResult['PROPERTIES']['WIDTH']['VALUE'];?></td>
                                </tr>
                            <? endif; ?>
                            <? if (!empty($arResult['PROPERTIES']['LENGTH']['VALUE'])): ?>
                                <tr class="characteristics-table__row">
                                    <td class="characteristics-table__cell characteristics-table__cell--title"><?=$arResult['PROPERTIES']['LENGTH']['NAME'];?></td>
                                    <td class="characteristics-table__cell characteristics-table__cell--value"><?=$arResult['PROPERTIES']['LENGTH']['VALUE'];?></td>
                                </tr>
                            <? endif; ?>
                            <? if (!empty($arResult['PROPERTIES']['HEIGHT']['VALUE'])): ?>
                                <tr class="characteristics-table__row">
                                    <td class="characteristics-table__cell characteristics-table__cell--title"><?=$arResult['PROPERTIES']['HEIGHT']['NAME'];?></td>
                                    <td class="characteristics-table__cell characteristics-table__cell--value"><?=$arResult['PROPERTIES']['HEIGHT']['VALUE'];?></td>
                                </tr>
                            <? endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- product-full__characteristics end -->
            </div>
            <!-- product-full__bottom-left end -->
            <!-- product-full__bottom-right -->
            <div class="product-full__bottom-right">
                <!-- product-full__equipment -->
                <div class="product-full__equipment">
                    <div class="product-full__equipment-head">
                        <h2 class="product-full__equipment-head-title">Комплектация</h2>
                    </div>
                    <div class="product-full__equipment-content">
                        <table class="equipment-table">
                            <tbody class="equipment-table__content">
                            <? foreach ($arResult['KOMPLEKT'] as $Komp): ?>
                                <tr class="equipment-table__row">
                                    <? if ($Komp['ID'] < 10): ?>
                                        <td class="equipment-table__cell equipment-table__cell--number">0<?=$Komp['ID'];?>.</td>
                                    <? else: ?>
                                        <td class="equipment-table__cell equipment-table__cell--number"><?=$Komp['ID'];?>.</td>
                                    <? endif; ?>
                                    <td class="equipment-table__cell equipment-table__cell--title"><?=$Komp['NAME'];?></td>
                                    <td class="equipment-table__cell equipment-table__cell--count"><?=$Komp['QUAN'];?>.</td>
                                </tr>
                            <? endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- product-full__equipment end -->
            </div>
            <!-- product-full__bottom-right end -->

            <div style="clear: both;"></div>


        </div>
    </div>
    <!-- product-full end -->
    <!-- help -->
    <div class="help">
        <div class="help__logo">
            <div class="help__logo-icon">
                <svg class="icon-phone">
                    <use xlink:href="#phone"></use>
                </svg>
            </div>
        </div>
        <div class="help__inner">
            <div class="help__left">
                <div class="help__text">
                    <div class="help__text-title">Есть<br> вопросы?</div>
                    <div class="help__text-desc">Наши менеджеры<br> с радостью вам ответят</div>
                </div>
            </div>
            <div class="help__right">
                <div class="help__contacts">
                    <div class="help__contacts-time">ежедневно с 10:00 до 21:00</div>
                    <div class="help__contacts-phone">+7 499 <span class="help__contacts-phone-strong">649 49 84</span></div>
                </div>
            </div>
        </div>
    </div>
    <!-- help end -->
    <!-- instructions -->
    <div class="instructions">
        <div class="instructions__head">
            <h2 class="instructions__head-title">Инструкции</h2>
        </div>
        <div class="instructions__content">
            <div class="instructions__content-inner">
                <ul class="instructions-list">
                    <? foreach ($arResult['INSTRUCTION'] as $inst): ?>
                        <li class="instructions-list__item">
                            <div class="instruction">
                                <div class="instruction__image">
                                    <img src="<? echo SITE_TEMPLATE_PATH; ?>/pic/instruction/instruction-01.jpg" alt="" class="instruction__image-img">
                                </div>
                                <div class="instruction__buttons">
                                    <div class="instruction__buttons-left">
                                        <a href="<?=$inst['SRC'];?>" class="btn btn-white btn-white--small btn--callback-left" target="_blank">
									<span class="btn-white__inner">
										<span class="btn__icon">
											<svg class="icon-download-gray-small">
											  <use xlink:href="#download"></use>
											</svg>
										</span>
										<span class="btn__text btn__text--middle">Скачать</span>
									</span>
                                        </a>
                                    </div>
                                    <div class="instruction__buttons-right">
                                        <div class="instruction__file-info"><?=$inst['TYPE'];?>,<br> <?=$inst['WEIGHT'];?> кб</div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- instructions end -->
    <!-- products-related -->
<?
$GLOBALS['arrFilter'] = array("ID" => $arResult['PROPERTIES']['LINK_GOODS']['VALUE']);
?>
<? $APPLICATION->IncludeComponent(
    "bitrix:catalog.section",
    "accessories",
    Array(
        "ACTION_VARIABLE" => "action",
        "ADD_PICT_PROP" => "-",
        "ADD_PROPERTIES_TO_BASKET" => "Y",
        "ADD_SECTIONS_CHAIN" => "N",
        "ADD_TO_BASKET_ACTION" => "ADD",
        "AJAX_MODE" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "BACKGROUND_IMAGE" => "-",
        "BASKET_URL" => "/personal/cart/index.php",
        "BROWSER_TITLE" => "-",
        "CACHE_FILTER" => "N",
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "COMPONENT_TEMPLATE" => "accessories",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "DETAIL_URL" => "",
        "DISABLE_INIT_JS_IN_COMPONENT" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_TOP_PAGER" => "N",
        "ELEMENT_SORT_FIELD" => "sort",
        "ELEMENT_SORT_FIELD2" => "id",
        "ELEMENT_SORT_ORDER" => "asc",
        "ELEMENT_SORT_ORDER2" => "desc",
        "FILTER_NAME" => "arrFilter",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "2",
        "IBLOCK_TYPE" => "catalog",
        "INCLUDE_SUBSECTIONS" => "Y",
        "LABEL_PROP" => "-",
        "LINE_ELEMENT_COUNT" => "3",
        "MESSAGE_404" => "",
        "MESS_BTN_ADD_TO_BASKET" => "В корзину",
        "MESS_BTN_BUY" => "Купить",
        "MESS_BTN_DETAIL" => "Подробнее",
        "MESS_BTN_SUBSCRIBE" => "Подписаться",
        "MESS_NOT_AVAILABLE" => "Нет в наличии",
        "META_DESCRIPTION" => "-",
        "META_KEYWORDS" => "-",
        "OFFERS_LIMIT" => "5",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "Товары",
        "PAGE_ELEMENT_COUNT" => "30",
        "PARTIAL_PRODUCT_PROPERTIES" => "N",
        "PRICE_CODE" => array(0 => "BASE",),
        "PRICE_VAT_INCLUDE" => "Y",
        "PRODUCT_ID_VARIABLE" => "id",
        "PRODUCT_PROPERTIES" => array(),
        "PRODUCT_PROPS_VARIABLE" => "prop",
        "PRODUCT_QUANTITY_VARIABLE" => "quant",
        "PRODUCT_SUBSCRIPTION" => "N",
        //"PROPERTY_CODE" => array(0=>"",1=>"NEW",2=>"",),
        "ID_ELEMENT" => $arResult['ID'],
        "SECTION_CODE" => "",
        "SECTION_ID" => "",
        "SECTION_ID_VARIABLE" => "SECTION_ID",
        "SECTION_URL" => "",
        "SECTION_USER_FIELDS" => array(0 => "", 1 => "",),
        "SEF_MODE" => "N",
        "SET_BROWSER_TITLE" => "Y",
        "SET_LAST_MODIFIED" => "N",
        "SET_META_DESCRIPTION" => "Y",
        "SET_META_KEYWORDS" => "Y",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_404" => "N",
        "SHOW_ALL_WO_SECTION" => "Y",
        "SHOW_CLOSE_POPUP" => "N",
        "SHOW_DISCOUNT_PERCENT" => "N",
        "SHOW_OLD_PRICE" => "N",
        "SHOW_PRICE_COUNT" => "1",
        "TEMPLATE_THEME" => "blue",
        "USE_MAIN_ELEMENT_SECTION" => "N",
        "USE_PRICE_COUNT" => "N",
        "USE_PRODUCT_QUANTITY" => "Y"
    )
); ?>
<?
if (isset($arResult['OFFERS']) && !empty($arResult['OFFERS'])) {
    foreach ($arResult['JS_OFFERS'] as &$arOneJS) {
        if ($arOneJS['PRICE']['DISCOUNT_VALUE'] != $arOneJS['PRICE']['VALUE']) {
            $arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['PRICE']['DISCOUNT_DIFF_PERCENT'];
            $arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arOneJS['BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
        }
        $strProps = '';
        if ($arResult['SHOW_OFFERS_PROPS']) {
            if (!empty($arOneJS['DISPLAY_PROPERTIES'])) {
                foreach ($arOneJS['DISPLAY_PROPERTIES'] as $arOneProp) {
                    $strProps .= '<dt>'.$arOneProp['NAME'].'</dt><dd>'.(
                        is_array($arOneProp['VALUE'])
                            ? implode(' / ', $arOneProp['VALUE'])
                            : $arOneProp['VALUE']
                        ).'</dd>';
                }
            }
        }
        $arOneJS['DISPLAY_PROPERTIES'] = $strProps;
    }
    if (isset($arOneJS)) {
        unset($arOneJS);
    }
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribeBtn,
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'NAME' => $arResult['~NAME']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $arSkuProps
    );
    if ($arParams['DISPLAY_COMPARE']) {
        $arJSParams['COMPARE'] = array(
            'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
            'COMPARE_PATH' => $arParams['COMPARE_PATH']
        );
    }
} else {
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ($arResult['MIN_PRICE']['DISCOUNT_VALUE'] != $arResult['MIN_PRICE']['VALUE']) {
        $arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_PRICE']['DISCOUNT_DIFF_PERCENT'];
        $arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'] = -$arResult['MIN_BASIS_PRICE']['DISCOUNT_DIFF_PERCENT'];
    }
    $arJSParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => (isset($arResult['MIN_PRICE']) && !empty($arResult['MIN_PRICE']) && is_array($arResult['MIN_PRICE'])),
            'SHOW_DISCOUNT_PERCENT' => ($arParams['SHOW_DISCOUNT_PERCENT'] == 'Y'),
            'SHOW_OLD_PRICE' => ($arParams['SHOW_OLD_PRICE'] == 'Y'),
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'SHOW_BASIS_PRICE' => ($arParams['SHOW_BASIS_PRICE'] == 'Y'),
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => ($arParams['SHOW_CLOSE_POPUP'] == 'Y'),
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribeBtn,
        ),
        'VISUAL' => array(
            'ID' => $arItemIDs['ID'],
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'PICT' => $arFirstPhoto,
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'PRICE' => $arResult['MIN_PRICE'],
            'BASIS_PRICE' => $arResult['MIN_BASIS_PRICE'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_double($arResult['CATALOG_MEASURE_RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['CATALOG_MEASURE_RATIO'],
        ),
        'BASKET' => array(
            'ADD_PROPS' => ($arParams['ADD_PROPERTIES_TO_BASKET'] == 'Y'),
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    unset($emptyProductProperties);
}
?>
    <script type="text/javascript">
        var <? echo $strObName; ?> =;
        new JCCatalogElement(<? echo CUtil::PhpToJSObject($arJSParams, false, true); ?>);
        BX.message({
            ECONOMY_INFO_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO'); ?>',
            BASIS_PRICE_MESSAGE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_BASIS_PRICE') ?>',
            TITLE_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR') ?>',
            TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS') ?>',
            BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
            BTN_SEND_PROPS: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS'); ?>',
            BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT') ?>',
            BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE'); ?>',
            BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
            TITLE_SUCCESSFUL: '<? echo GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK'); ?>',
            COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK') ?>',
            COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
            COMPARE_TITLE: '<? echo GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE') ?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
            PRODUCT_GIFT_LABEL: '<? echo GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL') ?>',
            SITE_ID: '<? echo SITE_ID; ?>'
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            window.dataLayer.push({
                'ecommerce': {
                    'detail': {
                        'products': [
                            <?=json_encode($jsonToYA)?>
                        ]
                    }
                }
            });
        });
    </script>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "COMPOSITE_FRAME_MODE" => "A",
        "COMPOSITE_FRAME_TYPE" => "AUTO",
        "EDIT_TEMPLATE" => "standard.php",
        "PATH" => "/include/4ps/modal_product.php"
    )
); ?>
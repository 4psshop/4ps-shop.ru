<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;
if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);
	$bDelayColumn  = false;
	$bDeleteColumn = false;
	$bWeightColumn = false;
	$bPropsColumn  = false;
	$bPriceType    = false;
if ($normalCount > 0):
?>
<!-- basket block -->
<div class="basket">
	<div class="basket__holder">
		<!-- basket table -->
		<div class="basket-table">
			<table id="basket_items" class="basket-table__grid">
				<thead>
					<tr>
						<th>Название</th>
						<th class="basket-table__wide"></th>
						<th>Цена</th>
						<th>Количество</th>
						<th>Стоимость</th>
						<th class="basket-table__small"></th>
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
							$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
							if ($arHeader["name"] == '')
								$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
							$arHeaders[] = $arHeader["id"];
							// remember which values should be shown not in the separate columns, but inside other columns
							if (in_array($arHeader["id"], array("TYPE")))
							{
								$bPriceType = true;
								continue;
							}
							if ($arHeader["id"] == "DELETE")
							{
								$bDeleteColumn = true;
								continue;
							}
							?> 
						<?
						endforeach;
						if ($bDeleteColumn || $bDelayColumn):
						?>
							<th class="basket-table__small"></th>
						<?
						endif;
						?>
					</tr>
				</thead>
				<tbody>
				<?
				foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
					if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
				?>
					<tr id="<?=$arItem["ID"]?>">
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
							if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
								continue;
							if ($arHeader["name"] == '')
								$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
							if ($arHeader["id"] == "NAME"):
							?>
								<td data-heading="Название">
									<?
									if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
										$url = $arItem["PREVIEW_PICTURE_SRC"];
									elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
										$url = $arItem["DETAIL_PICTURE_SRC"];
									else:
										$url = $templateFolder."/images/no_photo.png";
									endif;
									?>
									<div class="basket-table__img">
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?>
											<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="basket-table__img-link">
												<span class="basket-table__img-cell">
													<img src="<?=$url?>" alt="pic">
												</span>
											</a>
										<?endif;?>	
									</div>
								</td>
								<td class="basket-table__align">
									<strong>
										<?=$arItem["NAME"]?>
									</strong>
								</td>
								<?
								elseif ($arHeader["id"] == "PRICE"):
								?>
								<td data-heading="Цена">
									<em id="current_price_<?=$arItem["ID"]?>">
										<?=$arItem["PRICE_FORMATED"]?>
									</em>
									<em id="old_price_<?=$arItem["ID"]?>">
										<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
											<?=$arItem["FULL_PRICE_FORMATED"]?>
										<?endif;?>
									</em>
								</td>
								<?
								elseif ($arHeader["id"] == "QUANTITY"):
								?>
								<td data-heading="Количество">
									<div class="spinner">
										<?
										$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
										$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
										$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
										$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
										?>
										<a 
											href="javascript:void(0);" 
											class="spinner__control spinner__control--left js-spinner-minus"  
											onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"
										>
										</a>
										<input
											type="text"
											size="3"
											id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
											name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
											size="2"
											maxlength="18"
											class="spinner__input js-spinner"
											min="0"
											<?=$max?>
											step="<?=$ratio?>"
											style="max-width: 50px"
											value="<?=$arItem["QUANTITY"]?>"
											onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
										>
										<a href="javascript:void(0);"  
										class="spinner__control spinner__control--right js-spinner-plus" 
										onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"
										>
										</a>
									</div>
									<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
								</td> 
								<?
								elseif ($arHeader["id"] == "PRICE"):
								?>
								<td class="basket-table__black" data-heading="Стоимость">
									<?
									if ($arHeader["id"] == "SUM"):
									?>
										<em><strong id="sum_<?=$arItem["ID"]?>">
									<?
									endif;
									echo $arItem[$arHeader["id"]];
									if ($arHeader["id"] == "SUM"):
									?>
										</em></strong>
									<?
									endif;
									?>
								</td>
								<?
								else:
								?>
								<td class="basket-table__black" data-heading="Стоимость">
									<?
									if ($arHeader["id"] == "SUM"):
									?>
										<div id="sum_<?=$arItem["ID"]?>">
									<?
									endif;
									echo "<strong><em>";
									echo $arItem[$arHeader["id"]];
									if ($arHeader["id"] == "SUM"):
									?>
										</em>
										</strong>
										</div>
									<?
									endif;
									?>
								</td>
								<?
								endif;
							endforeach;
							if ($bDelayColumn || $bDeleteColumn):
							?>
							<td class="basket-table__small">
								<?
								if ($bDeleteColumn):
								?>
									<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>">
										<span class="basket-table__remove">
										<svg class="icon-cross-blue-small">
										<use xlink:href="#cross"></use>
										</svg>
									</a>
								<?
								endif;
								?> 
							</td>
							<?
							endif;
							?>
						</tr>
						<?
						endif;
					endforeach;
					?>
				</tbody>
			</table>
		</div>
		<!-- basket table end -->
		<!-- basket common -->
		<div class="basket-common">
			<div class="basket-common__content">
				<span class="basket-common__text">Итого:</span>
				<span class="basket-common__price"><span class="basket-common__price-inner" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></span>
			</div>
		</div>
		<!-- basket common end -->
	</div>	
	<!-- basket actions -->
	<div class="basket-actions">
		<a href="javascript:history.back()" class="basket-actions__link">
			<svg class="icon-arrow-right-red-medium basket-actions__link-icon">
				<use xlink:href="#arrow-right"></use>
			</svg>
			Продолжить покупки
		</a>
		<a href="/order/" class="btn btn-red btn--cart-left basket-actions__btn">
			<span class="btn-black__inner">
				<span class="btn__icon">
					<svg class="icon-cart-white-large">
						<use xlink:href="#cart"></use>
					</svg>
				</span>
				<span onclick="checkOut();" class="btn__text btn__text--middle">
					Оформить заказ
				</span>
			</span>
		</a>
	</div>
	<!-- basket actions end -->
	</div>
	</div>
	<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
	<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
</div>
<?
else:
?>
<div id="basket_items_list">
	<table>
		<tbody>
			<tr>
				<td style="text-align:center">
					<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<?
endif;
?>
<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;
if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);
	$bDelayColumn  = false;
	$bDeleteColumn = false;
	$bWeightColumn = false;
	$bPropsColumn  = false;
	$bPriceType    = false;
if ($normalCount > 0):
?>

<div class="basket_block">
	<div class="basket_left_block">
		<!-- basket block -->
		<div class="basket">
			<div class="basket__holder">



				<div class="bx_ordercart_order_pay_left" id="coupons_block">
				<?
				if ($arParams["HIDE_COUPON"] != "Y")
				{
				?>
					<div class="bx_ordercart_coupon">
						<div class="coupon_text">
							<span><?=GetMessage("STB_COUPON_PROMT")?></span>
						</div>
						<div class="coupon_input">
							<input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">
						</div>
						<?/*?><a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?=GetMessage('SALE_COUPON_APPLY'); ?></a><?*/?>
						<div class="coupon_res">
							<?
							if (!empty($arResult['COUPON_LIST']))
							{
								$cupon_bool = false;
								foreach ($arResult['COUPON_LIST'] as $oneCoupon)
								{
									$couponClass = 'disabled';
									switch ($oneCoupon['STATUS'])
									{
										case DiscountCouponsManager::STATUS_NOT_FOUND:
										case DiscountCouponsManager::STATUS_FREEZE:
											$couponClass = 'bad';
											break;
										case DiscountCouponsManager::STATUS_APPLYED:
											$couponClass = 'good';
											break;
									}
									?>
										<?/*?>
										<input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>">
										<span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span>
										<?*/?>

											<?if (isset($oneCoupon['CHECK_CODE_TEXT'])) {?>
												<?

												if (is_array($oneCoupon['CHECK_CODE_TEXT'])) {
													if (in_array("применен", $oneCoupon['CHECK_CODE_TEXT'])) {
														$cupon_bool = true;
													}
												} else {
													$pos_find = strripos($oneCoupon['CHECK_CODE_TEXT'], "применен");
													if ($pos != false) {
														$cupon_bool = true;
														echo $oneCoupon['CHECK_CODE_TEXT'];
													}
												}
												?>
											<?}?>
										<?
								}
								unset($couponClass, $oneCoupon);
								if ($cupon_bool == true) {
									?>
									<div class="bx_ordercart_coupon_notes">
									промокод активирован! <i class="fa fa-check"></i>
									</div>
									<?
								}
							}
							}
							else
							{
								?>&nbsp;<?
							}
							?>
						</div>
				</div>

		</div>



				<!-- basket table -->
				<div class="basket-table">
					<table id="basket_items" class="basket-table__grid">
						<thead>
							<tr>
								<th>Изображение</th>
								<th class="basket-table__wide">Название</th>
								<th>Цена</th>
								<th>Количество</th>
								<th>Стоимость</th>
								<!-- <th class="basket-table__small"></th> -->
								<?
								foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
									$arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
									if ($arHeader["name"] == '')
										$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
									$arHeaders[] = $arHeader["id"];
									// remember which values should be shown not in the separate columns, but inside other columns
									if (in_array($arHeader["id"], array("TYPE")))
									{
										$bPriceType = true;
										continue;
									}
									if ($arHeader["id"] == "DELETE")
									{
										$bDeleteColumn = true;
										continue;
									}
									?>
								<?
								endforeach;
								if ($bDeleteColumn || $bDelayColumn):
								?>
									<th class="basket-table__small"></th>
								<?
								endif;
								?>
							</tr>
						</thead>
						<tbody>
						<?
						foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):
							if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
						?>
							<tr id="<?=$arItem["ID"]?>">
								<?
								foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
									if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
										continue;
									if ($arHeader["name"] == '')
										$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
									if ($arHeader["id"] == "NAME"):
									?>
										<td data-heading="Название">
											<?
											if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
												$url = $arItem["PREVIEW_PICTURE_SRC"];
											elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
												$url = $arItem["DETAIL_PICTURE_SRC"];
											else:
												$url = $templateFolder."/images/no_photo.png";
											endif;
											?>
											<?
											$url_big = CFile::GetPath($arItem['PREVIEW_PICTURE']);
											if (!empty($url_big)) {
												$url = $url_big ;
											}?>
											<div class="basket-table__img">
												<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?>
													<a href="<?=$arItem["DETAIL_PAGE_URL"] ?>" class="basket-table__img-link">
														<span class="basket-table__img-cell">
															<img src="<?=$url?>" alt="pic">
														</span>
													</a>
												<?endif;?>
											</div>
										</td>
										<td class="basket-table__align">
											<strong>
												<?=$arItem["NAME"]?>
											</strong>
										</td>
										<?
										elseif ($arHeader["id"] == "PRICE"):
										?>
										<td data-heading="Цена">
											<em id="current_price_<?=$arItem["ID"]?>">
												<?=$arItem["PRICE_FORMATED"]?>
											</em>
											<?/*?><em id="old_price_<?=$arItem["ID"]?>">
												<?if (floatval($arItem["DISCOUNT_PRICE_PERCENT"]) > 0):?>
													<?=$arItem["FULL_PRICE_FORMATED"]?>
												<?endif;?>
											</em><?*/?>
										</td>
										<?
										elseif ($arHeader["id"] == "QUANTITY"):
										?>
										<td data-heading="Количество">
											<div class="spinner">
												<?
												$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
												$max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
												$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
												$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
												?>
												<a
													href="javascript:void(0);"
													class="spinner__control spinner__control--left js-spinner-minus"
													onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);"
												>
												</a>
												<input
													type="text"
													size="3"
													id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
													name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
													size="2"
													maxlength="18"
													class="spinner__input js-spinner"
													min="0"
													<?=$max?>
													step="<?=$ratio?>"
													style="max-width: 50px"
													value="<?=$arItem["QUANTITY"]?>"
													onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>)"
												>
												<a href="javascript:void(0);"
												class="spinner__control spinner__control--right js-spinner-plus"
												onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);"
												>
												</a>
											</div>
											<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
										</td>
										<?
										elseif ($arHeader["id"] == "PRICE"):
										?>
										<td class="basket-table__black" data-heading="Стоимость">
											<?
											if ($arHeader["id"] == "SUM"):
											?>
												<em><strong id="sum_<?=$arItem["ID"]?>">
											<?
											endif;
											echo $arItem[$arHeader["id"]];
											if ($arHeader["id"] == "SUM"):
											?>
												</em></strong>
											<?
											endif;
											?>
										</td>
										<?
										else:
										?>
										<td class="basket-table__black" data-heading="Стоимость">
											<?
											if ($arHeader["id"] == "SUM"):
											?>
												<div id="sum_<?=$arItem["ID"]?>">
											<?
											endif;
											echo "<strong><em>";
											echo $arItem[$arHeader["id"]];
											if ($arHeader["id"] == "SUM"):
											?>
												</em>
												</strong>
												</div>
											<?
											endif;
											?>
										</td>
										<?
										endif;
									endforeach;
									if ($bDelayColumn || $bDeleteColumn):
									?>
									<td class="basket-table__small">
										<?
										if ($bDeleteColumn):
										?>
											<a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>">
												<span class="basket-table__remove">
												<svg class="icon-cross-blue-small">
												<use xlink:href="#cross"></use>
												</svg>
											</span>
											</a>
										<?
										endif;
										?>
									</td>
									<?
									endif;
									?>
								</tr>
								<?
								endif;
							endforeach;
							?>
						</tbody>
					</table>
				</div>
				<!-- basket table end -->
				<!-- basket actions -->
				<div class="backet_back_block">
					<div class="basket-actions">
						<a href="javascript:history.back()" class="basket-actions__link">
							<svg class="icon-arrow-right-red-medium basket-actions__link-icon">
								<use xlink:href="#arrow-right"></use>
							</svg>
							Продолжить покупки
						</a>
					</div>
				</div>
			<!-- basket actions end -->
			</div>

			</div>

			<input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
			<input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
			<input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
			<input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
			<input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
			<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
		</div>
		<?
		else:
		?>
		<div id="basket_items_list">
			<table>
				<tbody>
					<tr>
						<td style="text-align:center">
							<div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<?
		endif;
		?>
	</div>

	<div class="basket_right_block">
		<div class="basket-common">
			<div class="basket-common__content">
				<span class="basket-common__text">Итого:</span>
			</div>
		</div>

		<div class="basket_info_block">
			<div class="basket_info_item">
				<div class="text_basket_info">
					Товаров
				</div>
				<div class="value_basket_info">
					<?echo count($arResult['ITEMS']['AnDelCanBuy']);?>
				</div>
			</div>

			<div class="basket_info_item">
				<div class="text_basket_info">
					Стоимость
				</div>
				<div class="value_basket_info">
					<span class="" id="PRICE_WITHOUT_DISCOUNT">
						<?=$arResult['PRICE_WITHOUT_DISCOUNT']?>
					</span>
				</div>
			</div>

			<?if ($arResult['DISCOUNT_PRICE_ALL'] <= 0):?>
				<div class="promocode_block hide">
			<?endif;?>
					<div class="basket_info_item promocode">
						<div class="text_basket_info">
							Промокод
						</div>
						<div class="value_basket_info">
							<?=$arResult['APPLIED_DISCOUNT_LIST'][0]['COUPON']['COUPON']?>
						</div>
					</div>

					<div class="basket_info_item">
						<div class="text_basket_info">
							Стоимость
							со скидкой
						</div>
						<div class="value_basket_info">
							<span class="" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span>
						</div>
					</div>
			<?if ($arResult['DISCOUNT_PRICE_ALL'] <= 0):?>
				</div>
			<?endif;?>
		</div>
		<div class="basket_info_block basket_block_buy_button">
			<?
			CModule::IncludeModule("iblock");
			$arSelect = Array("ID","PROPERTY_MIN_PRICE");
			$arFilter = Array("IBLOCK_CODE"=>'catalog_settings', "ACTIVE"=>"Y", "CODE" => "settings");
			$res_set = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($cart_set = $res_set->fetch()){
				$min_sum_to_buy = (int)$cart_set['PROPERTY_MIN_PRICE_VALUE'];
			}
			?>

			<?if ($arResult["allSum"] <= $min_sum_to_buy):?>
				<p>Минимальная сумма для заказа <?=$min_sum_to_buy?> рублей</p>
			<?else:?>
				<a href="/order/" class="btn btn-red btn--cart-left basket-actions__btn">
					<span class="btn-black__inner">
						<?/*?><span class="btn__icon">
							<svg class="icon-cart-white-large">
								<use xlink:href="#cart"></use>
							</svg>
						</span><?*/?>
						<span onclick="checkOut();" class="btn__text btn__text--middle">
							Оформить заказ
						</span>
					</span>
				</a>
			<?endif;?>
		</div>
		<span class="min_to_but" style="display: none;"><?=$min_sum_to_buy?></span>
	</div>
</div>
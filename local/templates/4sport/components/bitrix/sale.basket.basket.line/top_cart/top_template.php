<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();
$compositeStub = (isset($arResult['COMPOSITE_STUB']) && $arResult['COMPOSITE_STUB'] == 'Y');
?>
<svg class="icon-cart-red-large header__cart-icon">
	<use xlink:href="#cart"></use>
</svg>
<?
	if (!$arResult["DISABLE_USE_BASKET"])
	{
		?>
		<i class="fa fa-shopping-cart"></i>
		<a href="<?= $arParams['PATH_TO_BASKET'] ?>" class='header__cart-title'>
		<?= GetMessage('TSB1_CART') ?>
		</a>
		<?
	}
	if (!$compositeStub)
	{
		if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] > 0 ))
		{
			echo '<div class="header__cart-text">'; 
			echo $arResult['NUM_PRODUCTS'].' '.$arResult['PRODUCT(S)'];
			echo "</div>";
		}
		if ($arParams['SHOW_NUM_PRODUCTS'] == 'Y' && ($arResult['NUM_PRODUCTS'] == 0 ))
		{
			echo '<div class="header__cart-text">'; 
			echo 'В корзине нет товаров';
			echo "</div>";
		}
		//echo '<div class="header__cart-amount">0</div>';
	}
		?>
 
 
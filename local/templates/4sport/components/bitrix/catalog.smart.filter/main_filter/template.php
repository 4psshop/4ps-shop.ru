<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$templateData = array(
    'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
    'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);

if (isset($templateData['TEMPLATE_THEME'])) {
    $this->addExternalCss($templateData['TEMPLATE_THEME']);
}
// $this->addExternalCss("/bitrix/css/main/bootstrap.css");
// $this->addExternalCss("/bitrix/css/main/font-awesome.css");
$_SESSION["pages"] = $_REQUEST["pages"];
$_SESSION["sort"] = $_REQUEST["sort"];
?>

<style type="text/css">
    .bx-filter.bx-green .bx-ui-slider-pricebar-v {
        background: #D8E4E9 !important;
    }
</style>

<div class="bx-filter <?=$templateData["TEMPLATE_CLASS"]?> bx-filter-horizontal">

    <form name="<? echo $arResult["FILTER_NAME"]."_form" ?>" action="<? echo $arResult["FORM_ACTION"] ?>" method="get" class="smartfilter" id="hns-catalog-filter">
        <? foreach ($arResult["HIDDEN"] as $arItem): ?>
            <input type="hidden" name="<? echo $arItem["CONTROL_NAME"] ?>" id="<? echo $arItem["CONTROL_ID"] ?>" value="<? echo $arItem["HTML_VALUE"] ?>" />
        <? endforeach; ?>


        <div class="filters">
            <!-- Изначальное место filters__top -->
            <div class="filters__bottom">
                <div class="filters__bottom-top" style="padding-bottom: 0px;">
                    <? foreach ($arResult["ITEMS"] as $key => $arItem)//prices
                    {
                        $key = $arItem["ENCODED_ID"];
                        if (isset($arItem["PRICE"])):
                            if ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0) {
                                continue;
                            }
                            $precision = 2;
                            if (Bitrix\Main\Loader::includeModule("currency")) {
                                $res = CCurrencyLang::GetFormatDescription($arItem["VALUES"]["MIN"]["CURRENCY"]);
                                $precision = $res['DECIMALS'];
                            }
                            ?>
                            <div class="filters__bottom-top-left" data-role="bx_filter_block">
                                <div class="label label--inline">
                                    <span><?=$arItem["NAME"]?>:</span>
                                </div>
                                <div class="filters__input">
                                    <input
                                            class="input js-slider-range-from js-mask-numbers"
                                            type="text"
                                            name="<?
                                            echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"] ?>"
                                            id="<?
                                            echo $arItem["VALUES"]["MIN"]["CONTROL_ID"] ?>"
                                            value="<?
                                            echo $arItem["VALUES"]["MIN"]["HTML_VALUE"] ?>"
                                            placeholder="от"
                                            onkeyup="smartFilter.keyup(this)"
                                    />
                                </div>
                                <div class="filters__input">
                                    <input
                                            class="input js-slider-range-to js-mask-numbers"
                                            type="text"
                                            name="<?
                                            echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"] ?>"
                                            id="<?
                                            echo $arItem["VALUES"]["MAX"]["CONTROL_ID"] ?>"
                                            value="<?
                                            echo $arItem["VALUES"]["MAX"]["HTML_VALUE"] ?>"
                                            placeholder="до"
                                            onkeyup="smartFilter.keyup(this)"
                                    />
                                </div>
                            </div>
                            <div class="filters__bottom-top-right">
                                <div class="bx-filter-parameters-box bx-active">
                                    <div class="bx-filter-block " data-role="bx_filter_block">
                                        <div class=" bx-filter-parameters-box-container">


                                            <div class="bx-ui-slider-track-container">
                                                <div class="bx-ui-slider-track" id="drag_track_<?=$key?>">
                                                    <?
                                                    $precision = $arItem["DECIMALS"] ? $arItem["DECIMALS"] : 0;
                                                    $step = ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"]) / 4;
                                                    $price1 = number_format($arItem["VALUES"]["MIN"]["VALUE"], $precision, ".", "");
                                                    $price2 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step, $precision, ".", "");
                                                    $price3 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 2, $precision, ".", "");
                                                    $price4 = number_format($arItem["VALUES"]["MIN"]["VALUE"] + $step * 3, $precision, ".", "");
                                                    $price5 = number_format($arItem["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
                                                    ?>
                                                    <div class="bx-ui-slider-part p1"><span><?=$price1?></span></div>
                                                    <div class="bx-ui-slider-part p2"><span><?=$price2?></span></div>
                                                    <div class="bx-ui-slider-part p3"><span><?=$price3?></span></div>
                                                    <div class="bx-ui-slider-part p4"><span><?=$price4?></span></div>
                                                    <div class="bx-ui-slider-part p5"><span><?=$price5?></span></div>

                                                    <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$key?>"></div>
                                                    <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$key?>"></div>
                                                    <div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                                                    <div class="bx-ui-slider-pricebar-v" style="left: 0;right: 0;" id="colorAvailableActive_<?=$key?>"></div>
                                                    <div class="bx-ui-slider-range" id="drag_tracker_<?=$key?>" style="left: 0%; right: 0%;">
                                                        <a class="bx-ui-slider-handle left" style="left:0;" href="javascript:void(0)" id="left_slider_<?=$key?>"></a>
                                                        <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$key?>"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?
                        $arJsParams = array(
                            "leftSlider" => 'left_slider_'.$key,
                            "rightSlider" => 'right_slider_'.$key,
                            "tracker" => "drag_tracker_".$key,
                            "trackerWrap" => "drag_track_".$key,
                            "minInputId" => $arItem["VALUES"]["MIN"]["CONTROL_ID"],
                            "maxInputId" => $arItem["VALUES"]["MAX"]["CONTROL_ID"],
                            "minPrice" => $arItem["VALUES"]["MIN"]["VALUE"],
                            "maxPrice" => $arItem["VALUES"]["MAX"]["VALUE"],
                            "curMinPrice" => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
                            "curMaxPrice" => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
                            "fltMinPrice" => intval($arItem["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItem["VALUES"]["MIN"]["VALUE"],
                            "fltMaxPrice" => intval($arItem["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItem["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItem["VALUES"]["MAX"]["VALUE"],
                            "precision" => $precision,
                            "colorUnavailableActive" => 'colorUnavailableActive_'.$key,
                            "colorAvailableActive" => 'colorAvailableActive_'.$key,
                            "colorAvailableInactive" => 'colorAvailableInactive_'.$key,
                        );
                        ?>
                            <script type="text/javascript">
                                BX.ready(function() {
                                    window['trackBar<?=$key?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
                                });
                            </script>
                        <?endif;
                    }
                    ?>

                    <div class="filters__top">
                        <?
                        //not prices
                        foreach ($arResult["ITEMS"] as $key => $arItem) {
                            if (
                                empty($arItem["VALUES"])
                                || isset($arItem["PRICE"])
                            ) {
                                continue;
                            }

                            if (
                                $arItem["DISPLAY_TYPE"] == "A"
                                && (
                                    $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
                                )
                            ) {
                                continue;
                            }
                            ?>
                            <div class="label label--inline">
                                <span>Бренд:</span>
                            </div>
                            <div class="filters__tags">
                                <ul class="filters__tags-list">
                                    <?
                                    $arCur = current($arItem["VALUES"]);
                                    switch ($arItem["DISPLAY_TYPE"]) {
                                        default://CHECKBOXES
                                            ?>
                                            <?
                                            foreach ($arItem["VALUES"] as $val => $ar):?>
                                                <li class="filters__tags-list-item">
                                                    <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="filter-tag <? echo $ar["DISABLED"] ? 'disabled' : '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">

                                                        <input
                                                                type="checkbox"
                                                                class="filter-tag__checkbox"
                                                                value="<? echo $ar["HTML_VALUE"] ?>"
                                                                name="<? echo $ar["CONTROL_NAME"] ?>"
                                                                id="<? echo $ar["CONTROL_ID"] ?>"
                                                            <? echo $ar["CHECKED"] ? 'checked="checked"' : '' ?>
                                                                onclick="smartFilter.click(this)"
                                                        />
                                                        <span class="btn btn-white btn-white--small btn--check-round-left filter-tag__item">
														<span class="btn-white__inner">
															<span class="btn__icon">
																<span class="icon-check-round-gray"></span>
															</span>
															<span class="btn__text btn__text--middle" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?>
															<span class="btn__text-gray">
													<?
                                                    if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                                        ?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                                    endif; ?>
															</span>
															</span>
														</span>
													</span>

                                                    </label>
                                                </li>
                                            <?endforeach; ?>
                                        <?
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?
                        }
                        ?>
                    </div>

                    <div class="filters__bottom-bottom">
                        <div class="filters__bottom-bottom-left">
                            <div class="label label--inline">
                                <span>Сортировка:</span>
                            </div>
                            <div class="filters__select">
                                <div class="select-item">
                                    <select class="select-item__select" name="sort" onchange="smartFilter.click(this)">
                                        <option value="priceup" <? if ($_REQUEST['sort'] == "priceup") {
                                            echo "selected";
                                        } ?>>Сначала дорогие
                                        </option>
                                        <option value="pricedowm" <? if ($_REQUEST['sort'] == "pricedowm") {
                                            echo "selected";
                                        } ?>>Сначала дешевые
                                        </option>
                                        <option value="popular" <? if ($_REQUEST['sort'] == "popular") {
                                            echo "selected";
                                        } ?>>По популярности
                                        </option>
                                    </select>
                                    <div class="select-item__divider"></div>
                                    <div class="select-item__icon"></div>
                                </div>
                            </div>
                        </div>
                        <div class="filters__bottom-bottom-right">
                            <!-- 							<div class="pagination">
                                                            <span class="label label--inline pagination__label">Страница:</span>
                                                            <ul class="pagination__list">
                                                                <li class="pagination__list-item current">
                                                                    <span class="pagination__list-item-link">1</span>
                                                                </li>
                                                                <li class="pagination__list-item">
                                                                    <a href="#" class="pagination__list-item-link">2</a>
                                                                </li>
                                                                <li class="pagination__list-item">
                                                                    <a href="#" class="pagination__list-item-link">3</a>
                                                                </li>
                                                            </ul>
                                                        </div> -->

                            <div class="pages">
                                <span class="label label--inline pages__label">По:</span>
                                <div class="pages__select">

                                    <div class="select-item">
                                        <select class="select-item__select" name="pages" onchange="smartFilter.click(this)">
                                            <option value="12" <? if ($_REQUEST['pages'] == 12) {
                                                echo "selected";
                                            } ?>>12
                                            </option>
                                            <option value="24" <? if ($_REQUEST['pages'] == 24) {
                                                echo "selected";
                                            } ?>>24
                                            </option>
                                            <option value="36" <? if ($_REQUEST['pages'] == 36) {
                                                echo "selected";
                                            } ?>>36
                                            </option>
                                        </select>
                                        <div class="select-item__divider"></div>
                                        <div class="select-item__icon"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" id="set_filter"
                           name="set_filter" value="Применить" />

                    <?/* if (
                    !empty(\Bitrix\Main\Application::getInstance()
                        ->getContext()
                        ->getRequest()
                        ->get('set_filter'))
                    ): ?>
                        <a href="<?=\Bitrix\Main\Application::getInstance()
                                        ->getContext()
                                        ->getRequest()
                                        ->getRequestedPageDirectory().'/'?>"
                           style="
                                font-family: 'Bender', Arial, Verdana, sans-serif;
                                font-size: 16px;
                                font-weight: 700;
                                font-style: italic;
                                margin-left: 14px;
                                padding: 15px 20px;
                                border-radius: 12px 12px 12px 0;
                                -webkit-transform: skewX(-15deg);
                                -ms-transform: skewX(-15deg);
                                 transform: skewX(-15deg);
                                 background: #EBF1F4;
                                color: #000;
                                margin-top: 15px;
                                border: none;
                                display: inline-block;
                                line-height: 21px;
                            ">
                            <?=GetMessage("CT_BCSF_DEL_FILTER")?>
                        </a>
                    <? endif; */?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12 bx-filter-button-box">
                <div class="bx-filter-block">
                    <div class="bx-filter-parameters-box-container">
                        <div class="bx-filter-popup-result <? if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL")
                            echo $arParams["POPUP_POSITION"] ?>" id="modef" <? if (!isset($arResult["ELEMENT_COUNT"])) {
                            echo 'style="display:none"';
                        } ?> style="display: inline-block;">
                            <? echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>')); ?>
                            <span class="arrow"></span>
                            <br />
                            <a href="<? echo $arResult["FILTER_URL"] ?>" target=""><? echo GetMessage("CT_BCSF_FILTER_SHOW") ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </form>

</div>
<script type="text/javascript">
    var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>
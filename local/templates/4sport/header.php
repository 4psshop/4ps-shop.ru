<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CJSCore::Init(array("fx"));
CJSCore::Init(array("jquery"));
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html>
<html xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>

	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?$APPLICATION->ShowTitle(false)?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<meta name="MobileOptimized" content="320">
	<meta name="yandex-verification" content="543e4b0e93ac8fb4" />

    <?$APPLICATION->ShowHead();?>
	<!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,400i&amp;subset=cyrillic-ext" rel="stylesheet">
	<!-- Styles -->
    <?if(
            stripos($APPLICATION->GetCurDir(), '/order-new/') !== false
            || stripos($APPLICATION->GetCurDir(), '/order/') !== false
    ):?>
	    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/order-main.css">
	<?else:?>
        <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/main.css">
	<?endif;?>
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/slick.css">
	<link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/slick-theme.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet">
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var s = document.createElement('script');s.type='text/javascript';
            document.body.appendChild(s);s.src='https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js';
        })
    </script>

	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/slick.min.js"></script>
	<!-- Modals -->
	<link rel="stylesheet" type="text/css" media="all" href="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.css">
	<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/fancybox/jquery.fancybox.js"></script>
	<meta name="locale" content="ru_RU">
	<meta name="DC.title" content="<?$APPLICATION->ShowTitle(false)?>" />
	<meta name="DC.subject" content="4ps-shop.ru, коньки, станки для заточки коньков, blademaster" >
	<meta name="DC.type" content="text" >
	<meta name="DC.source" content="<?=$curPage;?>" >
	<meta name="DC.relation" content="<?=$curPage;?>" >
	<meta name="DC.coverage" content="Moscow,Russia" >
	<meta name="DC.coverage" content="1999-<?=date('Y');?>" >
	 <meta name="DC.creator" content="http://4ps-shop.ru/" >
	<meta name="DC.publisher" content="http://4ps-shop.ru/" >
	<meta name="DC.contributor" content="http://4ps-shop.ru/" >
	<meta name="DC.rights" content="http://4ps-shop.ru/" >
	<meta name="DC.date" content="2000-<?=date('Y');?>" >
	<meta name="DC.format" content="digital" >
	<meta name="DC.identifier" content="<?=$curPage;?> " >
	<meta name="DC.language" content="ru-RU" >
	<meta name="DC.provenance" content="http://4ps-shop.ru/" >
	<meta name="DC.rightsHolder" content="http://4ps-shop.ru/" >
	<meta name="DC.accrualPeriodicity" content="weekly" >
	<meta name="DC.accrualPolicy" content="Active" >
	<meta name="geo.region" content="RU-MOW" />
	<meta name="geo.placename" content="Москва" />
	<meta name="geo.position" content="55.882520, 37.549840" />
	<meta name="ICBM" content="55.882520, 37.549840" />
	<meta property="og:locale" content="ru_RU" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="<?$APPLICATION->ShowTitle(false)?>" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="http://4ps-shop.ru/" />
	<meta property="og:site_name" content="http://4ps-shop.ru/" />
	<meta property="og:image" content="<?=SITE_TEMPLATE_PATH?>/img/logo.png"/>
	<!--icons-->
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="/apple-touch-icon-57x57.png" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/apple-touch-icon-114x114.png" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/apple-touch-icon-72x72.png" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/apple-touch-icon-144x144.png" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60" href="/apple-touch-icon-60x60.png" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/apple-touch-icon-120x120.png" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/apple-touch-icon-76x76.png" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/apple-touch-icon-152x152.png" />
	<link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
	<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
	<link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />
	<meta name="application-name" content="&nbsp;"/>
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<?
	$LastModified_unix = 1294844676; // время последнего изменения страницы
	$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
	$IfModifiedSince = false;
	if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
		$IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));
	if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
		$IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
	if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
		header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
		exit;
	}
	header('Last-Modified: '. $LastModified);
	?>
    <script src="<?=SITE_TEMPLATE_PATH?>/qs.js"></script>


    <!-- Yandex.Metrika eCommerce -->
    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
    </script>
    <!-- /Yandex.Metrika eCommerce -->
<meta name="yandex-verification" content="461b2ff99a6b07b2" />
<meta name="prmoney" content="vj6IzMmXTHcZ35fhuiUct4qQjzRRMvPt5aQcD8cR" />
</head>
<body itemscope="" itemtype="http://schema.org/WebPage">
<div id="panel"><?$APPLICATION->ShowPanel();?></div>
<!-- svg sprites -->
<div style="display:none;">
    <!-- include inc/svg_base_template.html  -->
	<?echo '<?xml version="1.0" encoding="UTF-8"?>';?>
    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><symbol viewBox="0 0 13 17" id="arrow-left"><title>arrow-left</title><path class="acls-1" d="M13 3.9L8.92 0 0 8.5 8.92 17 13 13.1 8 9l5-5.1z" data-name="&lt;"/></symbol><symbol viewBox="0 0 6 9" id="arrow-right"><title>arrow-right</title><path class="bcls-1" d="M1.89 0L0 2.07l2.31 2.17L0 6.93 1.89 9 6 4.5 1.89 0z" data-name=">"/></symbol><symbol id="arrow-right2" viewBox="0 0 13 17"><defs><style>.ccls-1{fill-rule:evenodd}</style></defs><title>arrow-right2</title><path class="ccls-1" d="M4.09 0L0 3.9 5 8l-5 5.09L4.09 17 13 8.5 4.09 0z" id="c_" data-name=">"/></symbol><symbol viewBox="0 0 14 15" id="bubble"><path class="dst0" d="M12 0H2C.9 0 0 .9 0 2v8c0 1.1.9 2 2 2h2l3 3 3-3h2c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2zM4 6.5c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-1c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v1zm4 0c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-1c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v1zm4 0c0 .3-.2.5-.5.5h-1c-.3 0-.5-.2-.5-.5v-1c0-.3.2-.5.5-.5h1c.3 0 .5.2.5.5v1z"/></symbol><symbol id="callback" viewBox="0 0 17 16.97"><defs><style>.ecls-1{fill:#3a3c40;fill-rule:evenodd}</style></defs><title>callback</title><g id="etel"><g id="eПерезвоните_мне" data-name="Перезвоните мне"><path class="ecls-1" d="M1148.51 81.48a8.49 8.49 0 1 0 0 12 8.51 8.51 0 0 0 0-12zm-.48 3l-1.29 1.29a.49.49 0 0 1-.7 0l-.22-.22-.42-.41a1.4 1.4 0 0 0-1.46-.33l-3.63 3.62a1.38 1.38 0 0 0 .34 1.45l.44.44.21.21a.49.49 0 0 1 0 .7l-1.3 1.35a.48.48 0 0 1-.76 0l-.09-.18a5.74 5.74 0 0 1-.48-3.61 7 7 0 0 1 5.55-5.54 5.78 5.78 0 0 1 3.52.43h.05l.2.1a.48.48 0 0 1 .01.74z" transform="translate(-1133.99 -79)" id="eФигура_7" data-name="Фигура 7"/></g></g></symbol><symbol viewBox="0 0 25 23" id="cart"><title>cart</title><path class="fcls-1" d="M22.9 10.73L19 1.86A3.13 3.13 0 0 0 16.13 0H8.82a3.13 3.13 0 0 0-2.87 1.86l-3.88 8.88H0v3.06h1.17l3.14 8.21a1.56 1.56 0 0 0 1.46 1h13.46a1.56 1.56 0 0 0 1.46-1l3.14-8.21H25v-3.07h-2.1zm-10.44 9.34a3.1 3.1 0 1 1 3.16-3.1 3.13 3.13 0 0 1-3.16 3.1zm-7-9.34l3.35-7.67h7.32l3.35 7.67h-14z" data-name="Фигура 13"/></symbol><symbol id="check-round" viewBox="0 0 12.93 13"><defs><style>.gcls-1{fill:#62b10c}</style></defs><title>check-round-green</title><g id="gОписание"><g id="gBuy"><g id="gв_наличии" data-name="в наличии"><path class="gcls-1" d="M1444.46 744a6.5 6.5 0 1 0 6.46 6.5 6.48 6.48 0 0 0-6.46-6.5zm-.77 9l-.23-.23-.23.23-2.27-2.28.74-.74 1.76 1.77 3.75-3.77.74.74z" transform="translate(-1438 -744)" id="gIcon"/></g></g></g></symbol><symbol id="cross" viewBox="0 0 8.2 8.4"><style>.hst0{fill-rule:evenodd;clip-rule:evenodd}</style><path class="hst0" d="M6.5 0l1.6 1.7-6.4 6.6L.1 6.6 6.5 0z"/><path class="hst0" d="M0 1.8L1.7 0l6.4 6.6-1.7 1.8L0 1.8z"/></symbol><symbol viewBox="0 0 64 52" id="delivery"><title>delivery</title><image data-name="Векторный смарт-объект Изображение" width="64" height="52" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAAA0CAYAAAA62j4JAAAACXBIWXMAAAsSAAALEgHS3X78AAAC6ElEQVRoQ+2ZsW7bMBCG7QBBlg4pOnhKjI4ZM2Xt5sXoGnRz9jyDnyBD5455hnTOlKlTgxToecped+lS1EDh3kFHmFIpHUUeKdtKgA+JSN7dzx8kRccDePd2vQvgzyAFA6nwtiBNJBTbgOMtZAy5DJAGdgGbkFTfiwGpC8SwNwZg/q/Id+RcGluJ230D7Ekgv5ErKcYVK40NJbcBhk/IUZtYaWwouQ24Qf7y31+QsW+sVCeU3BchmtAE+cnP9HvSFwMekSFPbAzFCqB2WhFz09elAdKtLJbSBPH5CIqzwNT/jLzuzABpYCqw9hUUbwfS8a3S5zpAfXlGXkn199kA4qNUvzMDoOUWAHmL2bznGDpfLpp07O0hiDG3HPuEHG6DAQRNKOo16FHDxdzHAGlZxWBqRF+ErFxt+IOcNRrQJCQWh6Dgq7CVQzLdcMfjH5CDao1WBtSJqXt2iI7+MNRGL48/RX5x3HW1v1VCSUyDAWofh9votWKuOY6MOLX7ghLmQtGAAyi2AMXe2X1BCXOhZQDHnUFxGFL8B9MenDAHmgZw7JzjfyBvqC0qYWoSGHAIxcWIctxSW1TC1GgbwPEXsLmLTOxXVIlqwdxIBijx3HcDSglL11ZpKeWgyQAp1jfvfwk1Cmix0wZYKyuIqtA6fb566+oEJ5SQJiixzm2AS0DX+BjgizW3MVhfu/fRgNJhH5wwB4kMcG8BKUEXvBiQ6F4CIVsAx5xA8T+9BbJiFtx2IsWH4GNAiC6nAQ6OrYAZbL68cEF9s0qRurGxqOoSDeAipu0emSIjZsptpn+WywBQ0iUtQVpexmFaUkPHmCH3rXlsku2QSpdUyCS4dxWpFDOO30gTiEVTl1RowcFTq+0SWTKXVvuUxy6kCcSiqUsqtOLgkdW2hM0eWlrtI25bSROIRVNXtkKaaOqSCqktNU00dUmF1A4bTTR1SYXUXjeaaOryKTaDzd4iN70uHKnR0jWwBvWS3hvwD5+avk2XUoqWAAAAAElFTkSuQmCC"/></symbol><symbol viewBox="0 0 16 16" id="download"><path class="jst0" d="M12 8l-1 2h3v2H2v-2h3L4 8H0v8h16V8h-4z"/><path class="jst0" d="M8 10l4-4H9V0H7v6H4l4 4z"/></symbol><symbol viewBox="0 0 64 56" id="hadns"><title>hadns</title><image data-name="Векторный смарт-объект Изображение" width="64" height="56" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAAA4CAYAAABNGP5yAAAACXBIWXMAAAsSAAALEgHS3X78AAADo0lEQVRoQ+WZwUrcMRDGd09eC1710MOCF8GLN5G+gngvfbB9kb0UH0JYB6Q+g0UKFdp/E5vsZrOTfDOTZD14+CEyXzLfzD+JIc6maZppoC+f5457x4PjBOlH4z0EL/dIywEFTMJ5SDg5Ht+zCaH4x+DlAek5oECQ+F2a0MsDFIw2YKFnbig4lBEpvXNCAaK3oUPnggIJI4wdKgcUSBllcPTcUKBhhNERc6ZAgZaehnvOVQIKLGTG10hfmWc9sngPFFhxhhfB/IS0lTmmwAJprUCBlQ/dgA+9BbLiezKkCVCgYWDxw5oABVKy4neM1mISWsfXgAIJI4vvPU8OFCAOUfyo+Tx5AtVzV0vxWXxDS04whn024xogeu4CxR9bipc0AOUG2r1nM/UAiQH3+zLEfoDi/U9/YfqkaYDEQ9DAD1qbnF0ywsQvIX4lHattAJovxOGWnpUEYfBckzDRvQbNmXSspQHCeUt1vH3gGXXY84x2FXR3vgmOa8dTbay1AQZvqfZBNFiiyfQXjl+xqITaqokaLv9ZiL0KCxPXAQdLJq40wa8Evx38mbB0HFf0pfy++LsQW4Gc6jqqg2sTSiFmDwrMc/gVdaGcB9YhNeG/4iVK3kohv8+9khSfzHNJ24O4ugVLJtbJoN+lwSOgyteSkI2P3qdQk+we0NOQBWtOyzg4qXXiVrQ5XfxIo49AQckQ0muhhotX0G6u06R4Q4QCxtBbEqTVQJUrq7QJlDzC+magnBEoYBKNakDxRoqawMSPUM4IFDBmY5dFe0yKocgTyTgEFDBGzcmyedR7nolftvqBAsb4OW0vGaak1LDns3iTDw8UMOaXrcmp7573Ps5RzhJQwJiPjx03NZMIZZGl+FOIL1G+ElDAGN88diCT2bgeez6PX4fYC/JdAgpyaP+x45vjb60J1G/P78RJ8E6AgIIcKj92FJtA/ff8DSneCWpAAQftPnbEBnxVFGGJn2b5IqJ3ghJQgCDFmdAh7v8EP9N2xX1vKd4DBQhSngmCIiXxpxA3n/4RKECQ7UyQFJnGT7N48+kfgQIJNP5M2LnsUIfTPwIFWqjvmZAefD8dV9Tp9I9AgRZqOxP2rrXu91va316eptM/AgVayHYmpF/6OY3Tdrn/of/XcPUrcQ0osEC2MyH90v6/yt2XOwcUtELCM4G2XzqulO7LnQMKWqH9M+E2aQr3pf3lZkkDljsHFLRC5TOh+7XWAhT0gPh/lC4O+aVL/AOTSTrvhIh5ugAAAABJRU5ErkJggg=="/></symbol><symbol viewBox="0 0 54 96" id="info"><title>info</title><image data-name="Векторный смарт-объект Изображение" width="54" height="96" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADYAAABgCAYAAABWkoE3AAAACXBIWXMAAAsSAAALEgHS3X78AAAGXElEQVR4Xu2bP0hfVxTHTZVgIBYxoWaIexfBwcEhg4tQQpdiRwkODsmSIYguDkfpIIWCVAIBB3ELhGRy6ODwGzpUOnXvENJSMhSatpaWEHJ6v3qf97z7u+937n3vXQMPD3xA7vnec+/5vfvn/bkOMfNQi4wY5gxrhgPDseG14YSdndiyY6tZs3VGIuJHowoiQIfuGp4a/uL6hrqIgViNk1QFA7hueGT4jds3xERstKH1o7XEPjLcN/zOYfvbcGjYMCwapg1ThnHLlC1btJpDWydkaANtoU2tX40SQ4d+5H77h8/mywLXG0Yjtu6BjeUb2pyOiHOOKhCsGP7lsr0xkGEion4sEzbmGy4b2l6JqH+KKjAMG55w2d4b9gw3IurX5YZt4z2XDX0Z1uprwa8annuBMbHntcCGUcOsYcmwymdXgezfS9Y3GhFnnvsXKPTp6qB6gwIOc39SPcMnA+pgGD0wHBn+Y92ggRZ1Bg1ntNnz6qJvlVduUGL+8HvG1b/SbcMu98/BFENdxECsUBto+5lX50mFtjKxFS8AAoZ+Haxm6xxeyWBYrnuGfcOOZd+WVW0XiIWYodUVffCTCy4ooaSwrMpfHp0IXSn8st9zv700bBlmDFcC9QquWM2WreMbYoeuHvrSEzr0tW8r8CthI5T7FCZtaE4h0K9ctl8My1x/H1u2MaShjdD+hT7JBQV9Lm3ifoX7Qoxldt7zF0n9wWXDWB8LaFMZ4/65jbZCyaFvciu4L/1SiPsyOe73AsEwNOSVemu4F9A15Z6NXRjaDA3LPaFB38/vLaXokRBh1/c3XwwXOafQ8OeepgoS9XYi9EM2tkwObfvDHH2UdyiPCp/stByz5AUA61y2lCtVJ7Eh24a09YBGxkYOp8kXzrvCieXW3yxvc3lJr9w/KqibGJBzDn3wh+QEl/uGXM4TeyocB15FsCv8WLnaWChiGePyarkb0BwIP3I5TQyXTj75LniV8IvIfW2Z+wPnZpmdoS/+iFoQfuQygsI5UYgHPn+CPhD+lwH/RTDC5U38QcAvH1bnULgmCg65P+iR8G8F/FUQ65Yy37ZEvaOA/1D411Agx+eGJ8ZjhbxLn+H+gBeV2Iyohz75jzwbwn+AgmNRsOiJZ4UPG+Cge7/ciaFteQMx6/kXhe8YBa9FgX/rsiR8Pc+XAok4Kcn49EScJc83LXyvUSBfZk554lXh2/d8KZCI0ySxfRFn1fNNCd/JEJdt3BO31aG24uyIOOT5xoXvtECan9im8DXpELUUJymxzg7Fzi4euZZ7CYk4dRNLXu5zbdASEjHqJpa8Qee6pZKQiFE3seRbqou4CSYRo05itW6Ccz22zLN7rd0T9X8Q5Z9FxAHLon70YwvI8aBJrFvM1av9oAlyvBpoK7FGrwZyvMzBMNtR+EKJ0fhlDsj5+q0Orbx+A519YQo6+YobdPajRNHpzn1GKljhsnXiw19BJz/Vgs5+XAdtHofYtHzw4xAFnTzAIsEk7dSRIwmW1c4dEivo5LG+go8N3xr+5HyG2GgDbWn9aSUxNPQTX5yhreTkVEGAb/jiDW1q/WqcmNxTvjbcZDd/2uKmjV0Y2tT61Tgxabci9HW5xWXT9CVUQQD/itU+aT2A6/wBrlhn51hnV8UiOfyKOf6ZoDDERhvJSXGDxCTXWF/lqthmZ9ui/FqgnSRUQWaInVGEPhpVkBliZxShj0YVJELsbDNRTxH6aFRBIsTOKIM+GlWQCLEzyqCPRhUkQuyMMuijUQWJEDujDPpoVEEixM4ogz4aVZAIsTPKoI9GFSRC7Iwy6KNRBYkQO6MM+mhUQSLEzqiifCdC3xhVkAixM6oov0ysCaogEWJnJMrx8SH0SFKlb4wqSITYGWXQR6MKEiF2Rhn00aiCRIidUUV5p+aYLL9MrAmqIBFiZyTKL1fFtlAFiRA7owz6aFRBIsTOqKK8U3NMll8m1gRVkAixM8qgj0YVJELsbDtCL9/dU4Q+GlWQyEN29iJC/0LoH0boo1EFidxhZziIMjlAO8nlwyp3BmiTUQWJ4IzTK3b2HYcPgo1aX2GvOOJ8VAqqoAbyX7Ng+CL5JZ9doUn7t/9F1P+XqcaogprI09WahU6LN0YV1ARnn74yvONqe2c1yeekYlAFDfnU8NjwM5+do39r/35sfVr92vwPRdKuE4ksBOkAAAAASUVORK5CYII="/></symbol><symbol viewBox="0 0 64 64" id="knowledge"><title>knowledge</title><image data-name="Векторный смарт-объект Изображение" width="64" height="64" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAsSAAALEgHS3X78AAAGT0lEQVR4Xu2aQWicRRTHm21SUlrbIKVIq7ahkQYsaQ9pc7G6koMtCB6EKnjx0kZQgqKHHgoq5OBBqpCKQhA0jVVIUj0Vi1Qi9NCCULA1+rC5mIuFpFRTbEnTfr7HvOGb/TIzb77Zb3fDxsMPdue9939v5vu++WZmd02SJGvqDZQ71yNvIn8y9Hm9FFcLRIciwU5uRI4jN5Akww22bZR0ikR0KALsVAfyHnLT6PAM8hozY7TfZN8OSbcIRIdqwE5sQT5AFowOTiOvIq2GXyu3TRt+Cxy7RcpTDaJDDFj0NuRj5I7RoSvIy0jJE1dinytG3B3W2ibljUF0yAMWuRP5DFk0OnAJeQFpkeINnRaOuWToLLL2Tik+D6JDCFjUbuQLZMkoeAp5ToqVIA3W0rpLnGu3FBuC6CAU14N8gzwwCvweeVqKzQtpsrbO84Bz90ixPkQHRzEHkO+MYhL+fkCKrZaic4sOmeQ1uQoxQEF3n+jAyWr6HFYDVDn/+ITrNhMXAUS+gWxCdX8XFwnkXIOYgQ1bjdUCCFyFkuM6qMF6HGO3Is8j7yCnkHNcwF/IXSOX5i7bptn3FMeSxlYpn6cO3z5kHTlcMwxzELkjw5gNyIvIKHLd0sFquc7alGODVI+lPr0TnTM0r5HhqtFwDxlHypKgIbwf+RoqnzniPvILF/0+qFuvjOwDNWE9AurqEA9z2xPIM8groIodZQ1zhk84F+XcL9XHNdKE/iwykdG6SsY25CXkp0ySX5HXkYccor3Ij5mYP0A9d/2uuBhAHaCQ5ofIb5mcVEOvI24T8gZUzmsJ95X63JYN2JNxJGgS+QR50hjNd0FdYbLTa+dzZJ/UkaIAtRocgfSVd59rajH68SlUToAL3LbH1LKJ6wDbyE2BWnTo75PIdqngrHbodwn0fRw5Y9TzJSy/k6e5L5tsGt4iwf3sEG9LBfq0Q76HAupM0axtiWum2r3bcLFIo307qMksqshawhdKd55qDL4rbWLeDkr2RhFbV24hyd4oYuvKLSTZHTH0qi0jQ8gPyDykb5GEP8+zbYh92yTdausicgtJ9oxvFzIMlauvUOY4tkvKk7cuk9xCkp19aEc2BpVvjn+Mz78jA8b3AW6z+S6xlncnGlKXjdxCAfaBTAdoYzNkfD8JvBsztUDtRk8afkMcaw7KQGxdLnILuezY1g6Vi5LLoJbLO5C/uW1Y0gJ12yccs4M1Lhu6lKM9tC6J4A4KRW+GdAVGtyxtZEpsmzQGpDVAq9Xo8CS3lVhTP1KUa7OkFUJQB312UFded56uWr9h6+N2muWXHZy6cmHbXkjfEn1Gez+kdxPlbE8ELQmxgxKQ3vZ0oHE4Y5tg26gj1pkL1FaY7BOZ9sOQHqickeqTMIVpn35eF8WfvTs8qJzJ9ZWmd/kg8hSo8wVqtx6bCwPQw/Z7rDXI2ub6gXBOjCGYnf83I5xwm3UQQL3q9GxP5/MXLfHELVD7+GOgjtfp9n4U1EGI9ungtr3sc4xjbjk0L3JO+kw1RB/W6s7oKz+FdDNT3HbeMQBjbKcJS094naB2ZnSlsrvHalhiTdLu5FwlSCfLMamj0gDog4Vuo4Pd3LZo6XwXpB1cdhoDamLUzynN3h8hZ5GfkVlw322z7HOWY46zjbRsr75eSAcoaMVY1ADod/U5myi2H2T7rC+57rzgM8t+Bx12vVgattkltEjwIwBqY6PX9occRQ2yfdyXPHAAxtlv0GE/xHaqKdcGitAiwZMgqJ0a2egMf62jqBH2OeFLHjgAJ9hvxGFfy7WQT9mnZcMU0q/BRcb6GoR0XX/aU/QF9jniSx44AEfY74LH5zT7DPm0bIgOlmT6mTvq8QH26XP5sF/IAOjVJHh8jrKPdU7yITpYkumf0KyTEvvMs88uQStkAHax37zHR0+6Mz4tG6KDJdltTvaYx0fPId7fFgMHoEPyo1rY57ZPy4boYEmWnShXFFL9/w9AAQOw6h+BVT8JrvrX4KpfCJU5WXMthUOBZtwM5QWabTucF6j9gci3UM8DkRgw4VecvJ5HYm9BLY7EYoBmOhSNBZrlWLwaoFl+GIkFVshPY7GIDiHACvhxNBbRIRRo8M/jsYgOeYEG/UEiFtEhBmjAX2RiER2qAer4J6lYRIcigDr8TS6W/wBHqCbk/FUMhAAAAABJRU5ErkJggg=="/></symbol><symbol viewBox="0 0 38 64" id="phone"><title>phone</title><image data-name="Векторный смарт-объект Изображение" width="38" height="64" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAABACAYAAAB2kAXpAAAACXBIWXMAAAsSAAALEgHS3X78AAABaElEQVRoQ+2aMY7CMBBFQ01DtznAXouD5JyptuQaNF5/Kz9AFGvGlm0Sa770JBtmPE+AaOLBOTcsXD2TZ/Y8XbtgFmZiNhyCD6V+PX/x3maBA1wGflKUenjuntFza8S4zMRsBC5XiE3LC3jjx72+2tZgNuUmvDAvm7uiuTZwQGZs+EMfFY2De2W7lyKdC8al9rk3SGJbr410Lri8F6c0tiBkXSgaWhGyLhQNrQhZF9s3vpDzi0n/2KVg1GLSb6EUjIlpYbLFpLrUff9itWBMTAuTLSbVpe77F6sFY2JamGwxqS51379YLRgT08Jki0l1qfv+xWrBmJgWJltMqov1SeyekzJAqov1SeyeU3JALoyJaWFMTAtjYloYE9PC9Ccm1aXu+xerBWNiWpjzi7XOecW+Tci6UDS0IgSL1If10cPeItXH+HhYX+J6wzZSfYyP6w2HvRASu0IjHVSK6BUacMhLR+RQ17T+AcuRjntcHjXZAAAAAElFTkSuQmCC"/></symbol><symbol id="search" viewBox="0 0 14 15"><defs><style>.ocls-1{fill:#3a3c40;fill-rule:evenodd}</style></defs><title>search</title><g id="oПоиск"><path class="ocls-1" d="M819.89 92.77a1.56 1.56 0 0 0-.34-.54l-2.55-2.7a6.13 6.13 0 0 0 .76-1.92 6.67 6.67 0 0 0 .12-2.05 6.49 6.49 0 0 0-.53-2 6.07 6.07 0 0 0-1.17-1.75 5.88 5.88 0 0 0-2-1.35 5.69 5.69 0 0 0-4.46 0 5.87 5.87 0 0 0-2 1.35 6.09 6.09 0 0 0-1.31 2.06 6.49 6.49 0 0 0 0 4.67 6.08 6.08 0 0 0 1.31 2.06 5.82 5.82 0 0 0 1.56 1.17 5.89 5.89 0 0 0 1.77.59 5.52 5.52 0 0 0 1.84 0 5.77 5.77 0 0 0 1.77-.62l2.68 2.8a1.48 1.48 0 0 0 .51.36 1.55 1.55 0 0 0 1.17 0 1.48 1.48 0 0 0 .51-.36 1.56 1.56 0 0 0 .34-.54 1.76 1.76 0 0 0 .02-1.23zM815 87.51a3.37 3.37 0 0 1-.72 1.14 3.2 3.2 0 0 1-4.67 0 3.37 3.37 0 0 1-.72-1.14 3.62 3.62 0 0 1-.24-1.3 3.68 3.68 0 0 1 .24-1.3 3.34 3.34 0 0 1 .72-1.15 3.1 3.1 0 0 1 1.09-.76 3.3 3.3 0 0 1 2.49 0 3.1 3.1 0 0 1 1.09.75 3.34 3.34 0 0 1 .72 1.15 3.68 3.68 0 0 1 .24 1.3 3.62 3.62 0 0 1-.24 1.31z" transform="translate(-806 -80)" id="oSearch_Icon" data-name="Search Icon"/></g></symbol><symbol viewBox="0 0 64 64" id="service"><title>delivery</title><image data-name="Векторный смарт-объект Изображение" width="64" height="64" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADW1JREFUeNrsWwlUlNUeZ4ZhG1YXtgAVBBXNRE0yLaHEhVxDnmaCael7mD31acsx65iVdSzNpY33KnfTTE1tIUMTKK1wJStkX2VHZhgGmP3d32Xu9AnM9jGI59R3zj3DMPf+///f/7vf/a+fQKfT2XXnpaq+0b8lK3OCsiRvqLK0cIiyrGiQprGhj66l2U3b2uyKOUJnsVzgIm6y9+hV7xgUnOvYL+S6Y/+wP1xGRGY4+AaUdKd8ApsrQKcTtPyaOaHxzJfzmy/9GKOqKB3YFXIOd/UrEI9+4LTHxBkHXe6JzLATCHR3pAI00oa+kuN7lzd+e3SRqqp8gOEHob3GOXxEpvPg4RcdA8nd7R+aLerjXSl0cZMJxa4yTNE2y921LU3u6vpaf2VJfriyvGhQa861e1uzsyLttBp7gzL8Aos9ps7Z7TV74fv2nr3q7ggFqG/W+jV89tGz0pOfJhm2NAHmFhV7xH1C7FGXEfelC8n25kNbSx6TlqxfomQZKXOa0lPioSj2yHjOfDy517ylm0W9vau6uGN1vIZWrRY1HN21Iu+R4dKc6GAdRvGSaVekqccXaFtbXPjSNcqP0ARt8GD8wBsyQBa+dHktai24PpwrSPHSGZeazp+ZrtNqBbYG3mEQHuAFnlzFQyY+9Kx+BKRff7akZscr7+qUCmehm4ek71NrXvIi2xHPeqePSH2Nf2PqF4nNmRlTFPnZERqZpLe9V+9atwenHnOPfuRzccR9aVhb8lTsr4rCnOH27l43nULDr4ojJ5zymPToPlEfn8rOnw+NvYQ8dnUfb96olcs8BY5OrT4rXvm357R5H3fLGaBTqxyr31mX3JhyZDG+i+99INVv7ZaFxp5BPK91n2x5XXpy/zKdWu1gjC6U4RoZndL43bGFHYQTiVSeMxM+hJLZgdnZGVT15pq9zRd/nITvHrHxu3xXb0wSiByUNlOATtEivrHunyeaL52LsRMItX0Xr1rfO+HpN/B3Z/Nh82+sXfqlqrI0BN+dh4782T162mHxyLFncUcVRbl3y9K++UfTD9/GaSQ3vblrg7YfimrN+W20LO3rua1/XBlLT3//foUBb340A75B5wJqhTf3f/Bi3a5tG/C3ePT40wEb/zdL4OTS3OVDkJzs4tIV89LxrOXGDFLI0lPmmJqvKM4dmj9rVC3mF8wdVyq/kDHZ6HyN2l5+6dzEqi3rkvNnj64pfjI2i3uOYC1ogBZogrYp3pANMmI+ZIbsXToEtSqlY9mahFR24sqv/BRtar5G3uReOD+qAPNLkmb/opbc7NvVQw80QAs0QRs8TM2HjMwyQXZg4K2Ayk3P7WR33hx4jOodG7azO28L8FwlsJ0AHubmQ1a2E4CBlwIkXx1aQs3MQwM15rY9hqqu2j83JkyJNSa3Pc8Bmm03I0wJXubmQ2bIjjXAYpUCYFNzJw9pweL6ve++ZImA9QeTn6db/+m4n9r/BptdOO+BYsmJ/UnmtqSpAdpUJsLLIpmI7FRpBIsxP0HY4VDUaEQwK7DzMHX0tLfggp3HJ0779sGRIu+3UYgKq7e+/GHRguh86ckDSTCr1nqtjDbjZe6C7MAALBQTwdZ+TgcFSE/se1qR/0cEnBzYeWOmrv0FJ4f6B8TU3Wpo/4zeRD7+ZeqayiC+imC0GS/zRl6oBQahq7sUmIDNpB8Ap6I4cWKOtrnJw2flhme8Zie+b6lwuQ+F6JjNVtdWBIWl5jqCae1/N21qvvjDZPwWlprj1PjN4SfrD3zwIhRhUnbiBLlE3H/W+18vvOAUOvRq3qRBSpH3XWXMtxh0tlBgqWyS4/uW12xf/55Q7NY4YN+ZwVzn7ZYdgKgO4J3C7r5M3VsLr8ZvjywyJECIgPD8AL70mfjzDLxTyOBr8M48Zy5IDj6QFur7n9eWiXzuKjXueaodsBY0QAvfGfj2PM1dwAJMwAaMnTpCMDV5U4c14dCggY2FB5M05fNFLCgpWzX/LNaSneRb9uzCU9QhWflYmqIkf0ingRL5n0Ym9epsYA3WUrqEFmiCNngwfuBtqZxYS/0ZgpFrog0T6nZvW88iK0ujOlV1RRCzFjUfbNzMXcdMIgXP89THWmb6uEoDL3a6QwZLo0gWwQLrrVaAnNTI5OBPJBksTTuRWHwltRYRY9O8k9Y+x13HAiDHfgOv881VOAaF5HBpsUMVvMATvCGDZQeiQEex0ceHYCWYDWcAcnhIYyHicn9wyjFLBZSfPz2TKm3uki22ztW1tyAdwIAnRwaLzCjBBozACswGBSCBiU+ksQROzi0WZ3z1uT/nIfdcsLvNF+N5S/7RnD4JNmDkYqYKQPaWamhC7FFbC6ptavTqibVGd4EeI8MshIdGU9dCew0SmFalrP0Ci/HZev3XMR08LH0iFKaHtwL0aztLqjKeTAZLL4qRYAVmYBeiaEG3VPiITGuzt67jYk7Sw/Dwx2vYoWJQgNiNZnA0TTJP3ql2/VpGi+teU54cGSy9gBFY6dlHsAuRvaEKGDz8orUC9pqzeDtycc1Xf46uTX7zba4SHAL659Nn9EZxGO+qkn4to8XAgxd4gjdksPr80GMFdiHKVdTkBAbnWksIvj08OrYLylcv+F7+0/fTNQ11vsz8KYpy7uarALYWtEATtMGD3f02b9K/zGrzqscK7CLU6ug/+4dm8xHSY2r8bnwiwMFdwcB3xBJtJvbCg3wVwNY6DQzPKoiLrDLG22oF6LECuxCFSno3+3hX8hUUggTv+34QbLNj4IA8BDIscsNzxio61h2Acnd2PoEWaII2s/9duRhWip35/xa7lFaMosSJ16nPfupYorVrsQZrQaP9bywW4CsXsLK4QMit59na5npMmbOHng+ff7K6vZXImxLeghBafv7MjM4qzHQNh8af3mfbfByAfOUyFGUJdgE0gS+hX17thSSILRWAinHR41EFsOd+azc/4TE5bi/7reqt53eyIgsv5cbG7/J7/q0n+TpY+TMiGqgyUGllz5ytdwBK2L0Tl2/E3zU7NuxQlhUOZr/5kkMSIKy9k5hPqz/6Q5afg/Vnldku/9ExVTRsNVN06EoVmT2zBfH3l7fm/R5htOhKfsMcNl+rUjl0h0zASostBLsIbSmwsWhOMFp66kpAZ2+vNqTc6qoCSpfNzkQB0z1m9gG0wjB7LDt9fAEKr9zQFyd/dwRSwEp3KMEuQk8O7d8pyQ8Xjxp3pjsjOFSDUROUnDiwDMPUnO6UA1jb8g3BuSLDXShvc4i68/Jf/95cr/jL46RfHVraQhwmlT4x6kC8OZeIsWme0x/7yGXYqPOytJBu7dxiWIFdxLY9enJuRxwPgBh2PXgxrMAuRCsa/Wd2ViR6cmzNTJaeEs8sgjXWg7vWpvkJgpE2X9HQODJDiD48tKKh4wINSbZihIJH/d53X656feWnNDU9K+FDi9PY+rlYCxp8qkhG4wtgJFiBGdhpRgh9eFTjGSlzuuT4SOp90OlR+drKg4Vx91XW79r6KjvVZWe/nquurQo0e0KTOZjLkqGgAVqgCdrg0aUdqcfIMNM0NrRStmp+GlzEgccu+FqcFySaxHaSZ6bHyn8++wix46Pau7y3ZJCI1oO2HYoWefuVGwNftuqxNJPNlQKBzjls2GXXsQ994xoZlUKTG0b6kzrsSkWrS0HcmGo4QkHbDkYjOyTQFwcExGUtRILRb93WBI+YWQdMxul5v49sOLZnhfzc6ZloejImnAMxMwIinFpy0/vGC4tSAMyYErjgMSdg0+5YkVfvWh1RsoqErcaUjKYq1/ExJ3vFPbHDKWzYFZMVrNTjCVVvrN6HNFrwp+khNOtsTWFE09ToWfnGmj3MU6Pe1IyR9RWvrjgoPXV0obqhzsdoBFZTGVj4eFQ+7fQgn/huyW8dmiUID/ACT/DmygLZIKM1hRGLS2PK8uLQwgXReQZmr6/a3/L75bHkDgktDkM5QDsb5sB3BKURQgbIYqBBZISsVpfGMFjJCQ0NaGAygK8oCWE+Oj6br10czzsWJwCLFsZktweP/1kFvt2ATFwZITO3GYs1VtISnrEOEVV9jR9rMGr4Yu9ytu2LEh7OoYpZPOWaJe0pPTUgG2SkCiUys8cBWFijFzCabJFB7y2dPH1EAyZXvLL8MGt86sodum1KIDKyhirITm/qtHsk9KYSbGZbZdFGUpo06wJq8khGKgqyRyCiC3rvyHjnIW359Dv9ar2eFVn2TPw5YGEY0GTRL/nEGG50ekt/gLEmKXpq7nzn1e64W13N7ZkadTu3bmD0rWqSomnokMHX0HjMbLtjP34p8568aF1CX10GFmDq1K8y1SvM8nZobQl4e88U1ORvh/Cs38jYZa4/CLWJG889cQoxhLncodAUIXRdo/EYhCrQLK0vetzJF2SErJAZsgODaf/Yxs3SPXkG2LxZmqsE1jSN9lPaPWqFB9jtCiCy0K5QfWssbZK2ALxVr8ygxZU1T7d1biV+196p6BG7T2SALAYXnchoTTuu1QzReMxMJJwl6jFy3ObbNghP8GZODmQy1RR9R740xesRsPFLUz362pw1Cuiu1+b+8i9O/v3q7N8vT//FX5//vwADANS0P6WXhlsBAAAAAElFTkSuQmCC"/></symbol><symbol id="zoom" viewBox="0 0 24.17 23"><defs><style>.qcls-1{fill:#afc4ca}</style></defs><title>zoom</title><g id="qфото"><g id="qPicture"><path class="qcls-1" d="M703 390a9 9 0 1 0-2.56 3.1l6.87 6.87 2.83-2.83zm-8 3a7 7 0 1 1 7-7 7 7 0 0 1-7 7zm1-11h-2v3h-3v2h3v3h2v-3h3v-2h-3v-3z" transform="translate(-686 -377)" id="qZoom"/></g></g></symbol></svg>
</div>
<!-- svg sprites end -->
<!--[if lt IE 9]>
	<p class="browserupgrade" style="color: #fff;background: #000;padding: 20px 15px; text-align: center;">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- wrapper -->
<div class="wrapper">
	<!-- topbar -->
<div class="topbar">
    <div class="container">
        <div class="topbar__inner">
            <div class="topbar__left">
                <span class="topbar__burger js-topbar-trigger">
                    <span class="topbar__burger-line"></span>
                </span>
                <div class="topbar__nav" " itemscope="" itemtype="http://schema.org/SiteNavigationElement">
				<!--top menu-->
					<?$APPLICATION->IncludeComponent("bitrix:menu", "top", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "top",	// Тип меню для остальных уровней
							"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
							"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "N",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
							"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					);?>
                     <div class="topbar__mobile">
                        <!-- side-menu -->
						<?
						$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "left_mobile_menu", Array(
							"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
								"CACHE_GROUPS" => "Y",	// Учитывать права доступа
								"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
								"CACHE_TYPE" => "A",	// Тип кеширования
								"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
								"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
								"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
								"IBLOCK_ID" => "2",	// Инфоблок
								"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
								"SECTION_CODE" => "",	// Код раздела
								"SECTION_FIELDS" => array(	// Поля разделов
									0 => "",
									1 => "",
								),
								"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
								"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
								"SECTION_USER_FIELDS" => array(	// Свойства разделов
									0 => "",
									1 => "",
								),
								"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
								"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
								"VIEW_MODE" => "TEXT",	// Вид списка подразделов
							),
							false
						);
						?>
						<!-- side-menu end -->
                    </div>
                </div>
            </div>
            <div class="topbar__right">
            	<div class="header_icon_soc">
        			<a href="https://www.youtube.com/channel/UCScXp2SqnQChv4nbqnfvbuA/featured" target="_blank">
        				<i class="fa fa-youtube"></i>
        			</a>
					<a href="https://www.instagram.com/4prosport/" target="_blank">
						<i class="fa fa-instagram"></i>
					</a>
            	</div>
                <a href="http://4prosport.ru" class="btn btn-black btn--arrow-right">
                    <span class="btn-black__inner">
                        <span class="btn__text btn__text--middle">на сайт 4ProSport</span>
                        <span class="btn__icon">
                            <svg class="icon-arrow-right-red-small">
                                <use xlink:href="#arrow-right"></use>
                            </svg>
                        </span>
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- topbar end -->
	<!-- intro -->
	<?if($curPage == '/index.php'):?>
	<div class="intro">
	<?endif;?>
		<!-- header -->
		<div class="header" role="banner">
		    <div class="container">
		        <div class="header__top">
		            <div class="header__top-left" itemscope="" itemtype="http://schema.org/ImageObject">
		                <a href="/" class="header__logo">
		                    <img class="header__logo-img" src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" itemprop="contentUrl" >
		                </a>
		            </div>
		            <div class="header__top-middle-left">
						<?$APPLICATION->IncludeComponent("bitrix:search.form", "top_search", Array(
							"PAGE" => "#SITE_DIR#search/",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
							"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
							),
							false
						);?>
		            </div>
		            <div class="header__top-middle-right">
		                <div class="header__contacts">
		                    <div class="header__contacts-item">
		                        <div class="header__contacts-time">ежедневно с 10:00 до 21:00</div>
		                        <div class="header__contacts-phone">
		                        <a href="tel:+74996494984" style="color: #3a3c40;">
		                        +7 499 649 49 84
		                        </a>
		                        </div>
		                    </div>
		                    <div class="header__contacts-item">
		                         <a href="#" class="btn btn-white btn--callback-left" id="myBtn">
		                            <span class="btn-white__inner">
		                                <span class="btn__icon">
		                                    <svg class="icon-callback">
		                                        <use xlink:href="#callback"></use>
		                                    </svg>
		                                </span>
		                                <span class="btn__text btn__text--middle">Перезвоните мне</span>
		                            </span>
		                        </a>
		                    </div>
		                </div>
		            </div>
		            <!-- modal recall -->
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"COMPOSITE_FRAME_MODE" => "A",
							"COMPOSITE_FRAME_TYPE" => "AUTO",
							"EDIT_TEMPLATE" => "standard.php",
							"PATH" => "/include/4ps/modal_recall.php"
						)
					);?>

		            <div class="header__top-right">
                        <div class="wbl-auth-personal-wrapper">
                            <div class="wbl-auth-personal">
                                <a href="/personal/"><?=( $GLOBALS['USER']->IsAuthorized() ? '<i class="icon-user"></i> Личный кабинет' : '<i class="icon-login"></i> Авторизоваться' )?></a>
                            </div>
                        </div>
						<?$APPLICATION->IncludeComponent(
							"bitrix:sale.basket.basket.line",
							"top_cart",
							array(
								"HIDE_ON_BASKET_PAGES" => "Y",
								"PATH_TO_BASKET" => SITE_DIR."cart/",
								"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
								"PATH_TO_PERSONAL" => SITE_DIR."personal/",
								"PATH_TO_PROFILE" => SITE_DIR."personal/",
								"PATH_TO_REGISTER" => SITE_DIR."login/",
								"POSITION_FIXED" => "N",
								"SHOW_AUTHOR" => "N",
								"SHOW_EMPTY_VALUES" => "Y",
								"SHOW_NUM_PRODUCTS" => "Y",
								"SHOW_PERSONAL_LINK" => "N",
								"SHOW_PRODUCTS" => "N",
								"SHOW_TOTAL_PRICE" => "Y",
								"COMPONENT_TEMPLATE" => "top_cart",
								"COMPOSITE_FRAME_MODE" => "A",
								"COMPOSITE_FRAME_TYPE" => "AUTO"
							),
							false
						);?>
		            </div>
		        </div>

				<?
				$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "top_menu", Array(
					"ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
					"CACHE_GROUPS" => "Y",	// Учитывать права доступа
					"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
					"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
					"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
					"IBLOCK_ID" => "2",	// Инфоблок
					"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
					"SECTION_CODE" => "",	// Код раздела
					"SECTION_FIELDS" => array(	// Поля разделов
					0 => "",
					1 => "",
					),
					"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
					"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
					"SECTION_USER_FIELDS" => array(	// Свойства разделов
					0 => "",
					1 => "",
					),
					"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
					"TOP_DEPTH" => "1",	// Максимальная отображаемая глубина разделов
					"VIEW_MODE" => "TEXT",	// Вид списка подразделов
					),
					false
				);
				?>
		    </div>
		</div>
		<!-- header end -->
		<?if($curPage == '/index.php'):?>
			<div class="container">
				<!-- slider -->
				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"slider_main",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "slider_main",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "10",
		"IBLOCK_TYPE" => "baners",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "FULL",
			1 => "LINK",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	),
	false
);?>
<!-- 				<div class="intro__holder">
					<span class="intro__bg-text">prosport</span>
					<span class="intro__bg-img"></span>
					<div class="intro__content">
						<h1 class="intro__title"><em>Компания<span class="intro__title-weight">«4prosport»</span></em></h1>
						<span class="intro__text">Мы – это профессиональный экипировочный центр. Основным видом деятельности является экипировка профессиональных спортивных команд и организаций. Мы более 10 лет успешно работаем на Российском рынке и динамично развиваемся.</span>
					</div>
				</div> -->
				<!-- side benefits -->
				<div class="side-benefits side-benefits--line">
					<div class="side-benefits__heading">
						<span class="side-benefits__title side-benefits__title--weight">Почему выгодно</span>
						<span class="side-benefits__title">покупать у нас</span>
					</div>
					<ul class="side-benefits__list">
						<li class="side-benefits__item">
							<a href="/znaniya/">
								<svg class="icon-knowledge">
									<use xlink:href="#knowledge"></use>
								</svg>
								<strong class="side-benefits__text">Знания</strong>
							</a>
						</li>
						<li class="side-benefits__item">
							<a href="/servis/">
								<svg class="icon-service">
									<use xlink:href="#service"></use>
								</svg>
								<strong class="side-benefits__text">Сервис</strong>
							</a>
						</li>
						<li class="side-benefits__item">
							<a href="/reputatsiya/">
								<svg class="icon-hands">
									<use xlink:href="#hadns"></use>
								</svg>
								<strong class="side-benefits__text">Репутация</strong>
							</a>
						</li>
						<li class="side-benefits__item">
							<a href="/dostavka/">
								<svg class="icon-delivery">
									<use xlink:href="#delivery"></use>
								</svg>
								<strong class="side-benefits__text">Доставка</strong>
							</a>
						</li>
					</ul>
				</div>
				<!-- side benefits end -->
			</div>
			<?endif;?>
		<?if($curPage == '/index.php'):?>
		</div>
		<?endif;?>
	<!-- intro end -->
	<!-- content -->
	<div class="content" role="main">
		<div class="container">
		<?if($curPage != '/index.php'):?>
		<!-- content-top -->
			<div class="content-top">
				<div class="container">
					<!-- breadcrumbs -->
					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "navigation", Array(
						"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
						"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
						"PATH" => "",	// Путь, для которого будет построена навигационная цепочка (по умолчанию, текущий путь)
						"SITE_ID" => "s1",	// Cайт (устанавливается в случае многосайтовой версии, когда DOCUMENT_ROOT у сайтов разный)
						"START_FROM" => "0",	// Номер пункта, начиная с которого будет построена навигационная цепочка
						),
						false
					);?>
					<!-- breadcrumbs end -->

					<h1 class="content-top__title"><?$APPLICATION->ShowTitle()?></h1>
				</div>
			</div>
			<!-- content-top end -->
		<?endif;?>
        <?if( preg_match('/^\/personal\//', $_SERVER['PHP_SELF'] ) === 1):?>
			<?$APPLICATION->IncludeComponent("bitrix:menu", "personal-menu", Array(
				"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
				"CHILD_MENU_TYPE" => "",	// Тип меню для остальных уровней
				"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
				"COMPOSITE_FRAME_TYPE" => "AUTO",	// Содержимое компонента
				"DELAY" => "N",	// Откладывать выполнение шаблона меню
				"MAX_LEVEL" => "1",	// Уровень вложенности меню
				"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
					0 => "",
				),
				"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
				"MENU_CACHE_TYPE" => "N",	// Тип кеширования
				"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
				"ROOT_MENU_TYPE" => "personal",	// Тип меню для первого уровня
				"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
			),
				false
			);?>
        <?endif;?>

/* placeholder */
function placeholder(objInputs){
	if (objInputs.length) objInputs.placeholder();
}
/* placeholder end */

/* autoFooter */
function autoFooter() {
	if ( $('.footer-spacer,.footer').length ){
		var footer_spacer = $('.footer-spacer');
		var footer = $('.footer');
		var footer_height = footer.innerHeight();
		footer.css('margin-top',-footer_height);
		footer_spacer.css('height',footer_height);
	}
}
/* autoFooter end */

/* sliders */
function sliders(){
	// js-products-slider
	if ( $('.js-products-slider').length ) {
		$('.js-products-slider').on('init', function(event, slick, direction){
			$('.products-slider-wrap').addClass('products-slider-wrap--visible');
		});

		$('.js-products-slider').on('beforeChange edge swipe', function(event, slick, direction){
			$('.products-slider-wrap').removeClass('products-slider-wrap--visible');
		});
		$('.js-products-slider').on('afterChange', function(event, slick, direction){
			$('.products-slider-wrap').addClass('products-slider-wrap--visible');
		});

		products_slider = $('.js-products-slider').slick({
			dots: false,
			infinite: false,
			speed: 500,
  			prevArrow: '<div class="products-slider__control">\
							<svg class="icon-arrow-left-black-large">\
				                <use xlink:href="#arrow-left"></use>\
				            </svg>\
						</div>',
			nextArrow: '<div class="products-slider__control">\
				<svg class="icon-arrow-right-black-large">\
	                <use xlink:href="#arrow-right2"></use>\
	            </svg>\
			</div>',
  			appendArrows: $('.products-slider__controls')
		});
	}

	// js-product-full__images-large-slider
	if ( $('.js-product-full__images-large-slider').length ) {
		products_full_images_large_slider = $('.js-product-full__images-large-slider').slick({
			arrows: false,
			swipe: false,
			dots: false,
			infinite: false,
			speed: 500,
			fade: true
		});
	}

	// js-product-full__images-small-slider
	if ( $('.js-product-full__images-small-slider').length ) {
		products_full_images_small_slider = $('.js-product-full__images-small-slider').slick({
			dots: false,
			infinite: false,
			speed: 500,
			slidesToShow: 4,
  			slidesToScroll: 1,
  			//focusOnSelect: true,
  			//asNavFor: '.js-product-full__images-large-slider',
  			prevArrow: '<div class="products-slider__control">\
							<svg class="icon-arrow-left-black-large">\
				                <use xlink:href="#arrow-left"></use>\
				            </svg>\
						</div>',
			nextArrow: '<div class="products-slider__control">\
				<svg class="icon-arrow-right-black-large">\
	                <use xlink:href="#arrow-right2"></use>\
	            </svg>\
			</div>',
  			//appendArrows: $('.products-slider__controls')
		});

		$('.product-full__images-small-image').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.product-full__images-small-slider-item:eq(0)');
			var index = parent.index();
			parent.addClass('product-full__images-small-slider-item--active').siblings('.product-full__images-small-slider-item--active').removeClass('product-full__images-small-slider-item--active');
			products_full_images_large_slider[0].slick.slickGoTo(parseInt(index));
			e.preventDefault();
		});
	}
}
/* sliders end */

/* map */
// function map() {
// 	if ( $('#js-map').length ) {
// 		var markArray = new Array();
// 		markArray = [
// 			{
// 				left: 55.785788,
// 				right: 37.626586,
// 				hintContent: 'улица Щепкина, 61/2с4',
// 				help_hint: 'улица Щепкина, 61/2с4',
// 				balloonContent: 'улица Щепкина, 61/2с4'
// 			}
// 		];
// 		ymaps.ready(function () {
// 			// Create map
// 			var myMap = new ymaps.Map("js-map", {
// 				center: [55.785788, 37.626586],
// 				zoom: 16,
// 				controls: []
// 			});

// 			$.each(markArray, function(index, element){
// 				var left = element.left;
// 				var right = element.right;
// 				var hintContent = element.hintContent;
// 				var help_hint = element.help_hint;
// 				var balloonContent = element.balloonContent;
// 				var myPlacemark = new ymaps.Placemark([left, right], {
// 					hintContent: hintContent,
// 					help_hint: help_hint,
// 					balloonContent: balloonContent
// 				},{
// 					hideIconOnBalloonOpen: false,
// 					zIndexActive: 0,
// 					zIndex: 1,
// 					balloonCloseButton: false
// 				});
// 				myMap.geoObjects.add(myPlacemark);
// 			});
// 			myMap.behaviors.disable('scrollZoom');
// 			myMap.balloon.open(myMap.getCenter(), 'улица Щепкина, 61/2с4', {
// 				closeButton: false
// 	    });
// 		});
// 	}
// }
/* accordMulti */
function accordMulti(){
	if ( $('.js-accord-link').length ) {
		$('.js-accord-link').on('click',function(e){
			var cur = $(this);
			var parent = cur.parents('.js-accord-parent:eq(0)');
			var block = parent.find('.js-accord-content:eq(0)');

			if ( !parent.hasClass('active') ){
				parent.addClass('active').siblings('.active').removeClass('active').find('.js-accord-content').slideUp();
				block.slideDown();
			} else {
				parent.removeClass('active');
				block.slideUp();
			}
			e.preventDefault();
		});
	}
}
/* accordMulti */

// add space in numbers
function val_space(n) {
	var str = '';
	n = n + ' ';
	str = n.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
	return str;
}

// /* sliderRange */
// function sliderRange() {
// 	// js-slider-range-price
// 	if ($('.js-slider-range-price').length) {
// 		var slider_range_price = $('.js-slider-range-price'),
// 			slider_range_from = $('.js-slider-range-from'),
// 			slider_range_to = $('.js-slider-range-to');

// 		var slider_range_price_min = 0;
// 		var slider_range_price_max = 150000;

// 		// slider
// 		slider_range_price.slider({
// 			range: true,
// 			min: slider_range_price_min,
// 			max: slider_range_price_max,
// 			values: [ 50, 90000 ],
// 			create: function() {
// 				var start_values = slider_range_price.slider('option','values');
// 				slider_range_price.find('.ui-slider-handle:eq(0)').append('<span class="ui-slider-handle__value">'+val_space(start_values[0])+'</span>');
// 				slider_range_price.find('.ui-slider-handle:eq(1)').append('<span class="ui-slider-handle__value">'+val_space(start_values[1])+'</span>');
// 		    },
// 			slide: function( event, ui ) {
// 				slider_range_price.find('.ui-slider-handle:eq(0) .ui-slider-handle__value').html(val_space(ui.values[0]));
// 				slider_range_from.val(val_space(ui.values[0])+'р.')
// 				slider_range_price.find('.ui-slider-handle:eq(1) .ui-slider-handle__value').html(val_space(ui.values[1]));
// 				slider_range_to.val(val_space(ui.values[1])+'р.');
// 			},
// 			change: function( event, ui ) {
// 				slider_range_price.find('.ui-slider-handle:eq(0) .ui-slider-handle__value').html(val_space(ui.values[0]));
// 				slider_range_from.val(val_space(ui.values[0])+'р.')
// 				slider_range_price.find('.ui-slider-handle:eq(1) .ui-slider-handle__value').html(val_space(ui.values[1]));
// 				slider_range_to.val(val_space(ui.values[1])+'р.');
// 			}
// 		}).draggable();

// 		// slider_range_from
// 		slider_range_from.on('blur',function(){
// 			var cur_val = $(this).val();
// 			var cur_values = slider_range_price.slider('option','values');
// 			var cur_values1 = cur_values[0];
// 			var cur_values2 = cur_values[1];

// 			if ( cur_val > cur_values2 ){
// 				slider_range_price.slider('option', 'values', [ cur_val, slider_range_price_max ]);
// 			} else {
// 				slider_range_price.slider('option', 'values', [ cur_val, cur_values2 ]);
// 			}
// 		});

// 		// slider_range_to
// 		slider_range_to.on('blur',function(){
// 			var cur_val = $(this).val();
// 			var cur_values = slider_range_price.slider('option','values');
// 			var cur_values1 = cur_values[0];
// 			var cur_values2 = cur_values[1];

// 			if ( cur_val < cur_values1 ){
// 				slider_range_price.slider('option', 'values', [ slider_range_price_min, cur_val ]);
// 			} else {
// 				slider_range_price.slider('option', 'values', [ cur_values1, cur_val ]);
// 			}
// 		});
// 	}
// }
// /* sliderRange end */


/* fancybox */
function fancybox(){
	// image
	if ( $('.js-image-link').length ) {
		$('.js-image-link').fancybox({
			wrapCSS		: 'fancy-image'
		});
	}
}
/* fancybox end */

/* search trigger */
function searchTrigger() {
	if($('.js-search-trigger').length) {
		var open = false;
		$('.js-search-trigger').on('click', function() {
			if($(window).width()<=767) {
				var value = $(this).closest('.header__search').find('.header__search-input').val();
	      if (value==0) {
	        $(this).closest('.header__search').toggleClass('is-active');
	        open = !open;
	      }
      }
		})
		$(document).on('click touchend', function(e) {
			if(open) {
				var isBtn = !!$(e.target).closest('.js-search-trigger').length,
						isMenu = !!$(e.target).closest('.header__search').length;
				if(!isBtn && !isMenu) {
					$('.header__search').removeClass('is-active');
					open = false;
				}
			}
		})
	}
}
/* search trigger end*/

/* mobile nav list */
function mobileSwipeList() {
	if($('.js-swipe-list').length) {
		var swipeList = $('.js-swipe-list'),
				swipeListItem = swipeList.find('.header__nav-list-item'),
				startWidth = 0;
		if($(window).width()<=1023 && !$('.js-swipe-list.is-swiped').length) {
			for (var i=0; i < swipeListItem.length; i++) {
				var itemWidth = swipeListItem.eq(i).outerWidth();
				startWidth+=itemWidth;
			}
			swipeList.width(startWidth+1);
			swipeList.addClass('is-swiped')
		} else if($(window).width()>1023 && $('.js-swipe-list.is-swiped').length) {
			swipeList.removeClass('is-swiped')
			swipeList.css('width','');
		}
	}
}
/* mobile nav list end*/


/*topbar menu*/
function topbarMenu() {
	if($('.js-topbar-trigger').length) {
		var open = false;
		$('.js-topbar-trigger').on('click',function() {
			$(this).closest('.topbar__inner').toggleClass('is-active');
			open = !open;
			if($(this).closest('.topbar__inner.is-active').length) {
				$('body').css('overflow','hidden');
			} else {
				$('body').css('overflow','');
			}
		})
		$(document).on('click touchend', function(e) {
			if(open) {
				var isBtn = !!$(e.target).closest('.js-topbar-trigger').length,
						isMenu = !!$(e.target).closest('.topbar__inner').length;
				if(!isBtn && !isMenu) {
					$('.topbar__inner').removeClass('is-active');
					open = false;
				}
			}
		})
	}
}
/*topbar menu end*/

/*block flow fix*/
function blockFlowFix() {
	if($('.inspection-tablet').is(':visible') && $('.topbar__nav-list').closest('.topbar__left').length) {
		$('.topbar__nav-list').prependTo($('.footer__top').find('.container'));
	} else if(!$('.inspection-tablet').is(':visible') && !$('.topbar__nav-list').closest('.topbar__left').length) {
		$('.topbar__nav-list').insertAfter('.topbar__burger');
	}
}
/*block flow fix end*/

/* inputMask */
function inputMask(){
	$('.js-phone-mask').mask('7 (999) 999 - 99 - 99');
}
/* inputMask end */

/* spinnerFn */
// function spinnerFn() {
// 	if($('.js-spinner').length) {
// 		$('.js-spinner-plus').on('click', function() {
// 			var valueInput = $(this).siblings('input').val(),
// 					plus
// 				if (valueInput >= 0) {
// 					plus = parseInt(valueInput) +1;
// 					$(this).siblings('input').val(plus);
// 				}
// 		})
// 		$('.js-spinner-minus').on('click', function() {
// 			var valueInput = $(this).siblings('input').val(),
// 					minus
// 				if (valueInput > 1) {
// 					minus = parseInt(valueInput) -1;
// 					$(this).siblings('input').val(minus);
// 				}
// 		})
// 		$('.js-spinner').bind("change keyup input click", function(event) {
// 		    if (this.value.match(/[^0-9]/g)) {
// 		        this.value = this.value.replace(/[^0-9]/g, '');
// 		    }
// 		    if ( event.keyCode == 38 ) {
// 		    	var valueInput = $(this).val(),plus;
// 				if (valueInput >= 0) {
// 					plus = parseInt(valueInput) +1;
// 					$(this).val(plus);
// 				}
// 		    }
// 		    if ( event.keyCode == 40 ) {
// 		    	var valueInput = $(this).val(),minus;
// 				if (valueInput > 1) {
// 					minus = parseInt(valueInput) -1;
// 					$(this).val(minus);
// 				}
// 		    }
// 		});
// 		$('.js-spinner').bind("blur", function(event) {
// 			var valueInput = $(this).val();
// 			if(valueInput == '' || valueInput == 0)
// 				$(this).val(1);
// 		});
// 	}
// }
/* spinnerFn */

/* inputMaskNumbers */
function inputMaskNumbers(){
	$('.js-mask-numbers').bind("change keyup input", function(event) {
	    if (this.value.match(/[^0-9]/g)) {
	        this.value = this.value.replace(/[^0-9]/g, '');
	    }
	});
}
/* inputMaskNumbers */

$(document).ready(function(){
	placeholder($('input[placeholder], textarea[placeholder]'));
	accordMulti();
	// sliderRange();
	//map(); 
	fancybox();
	mobileSwipeList();
	searchTrigger();
	topbarMenu();
	inputMask();
	//spinnerFn();
	inputMaskNumbers();
	blockFlowFix();
});
$(window).load( function(){
	autoFooter();
	sliders();
});
$(window).resize(function(){
	autoFooter();
	mobileSwipeList();
	blockFlowFix();
});
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
	<!-- content end -->
		<p id="back-top">
	<a href="#top"><span></span>Вверх</a>
</p>
		</div>
	</div>

	<div class="footer-spacer"></div>
</div>
<!-- wrapper end -->

<!-- footer -->
<footer class="footer" role="contentinfo">
	<div class="footer__top">
		<div class="container">
			<div class="footer__top-left">
				<?$APPLICATION->IncludeComponent("bitrix:catalog.section.list", "bottom_menu", Array(
					"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
						"CACHE_GROUPS" => "Y",	// Учитывать права доступа
						"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
						"CACHE_TYPE" => "A",	// Тип кеширования
						"COMPOSITE_FRAME_MODE" => "A",	// Голосование шаблона компонента по умолчанию
						"COMPOSITE_FRAME_TYPE" => "AUTO", 	// Содержимое компонента
						"COUNT_ELEMENTS" => "Y",	// Показывать количество элементов в разделе
						"IBLOCK_ID" => "2",	// Инфоблок
						"IBLOCK_TYPE" => "catalog",	// Тип инфоблока
						"SECTION_CODE" => "",	// Код раздела
						"SECTION_FIELDS" => array(	// Поля разделов
							0 => "",
							1 => "",
						),
						"SECTION_ID" => $_REQUEST["SECTION_ID"],	// ID раздела
						"SECTION_URL" => "",	// URL, ведущий на страницу с содержимым раздела
						"SECTION_USER_FIELDS" => array(	// Свойства разделов
							0 => "",
							1 => "",
						),
						"SHOW_PARENT_NAME" => "Y",	// Показывать название раздела
						"TOP_DEPTH" => "2",	// Максимальная отображаемая глубина разделов
						"VIEW_MODE" => "TEXT",	// Вид списка подразделов
					),
					false
				);?>
			</div>
			<div class="footer__top-right">
				<div class="footer__subscribe">
					<h4 class="footer__subscribe-title">Новости</h4>
					<div class="footer__subscribe-desc">Присоединяйтесь<br> на наши новости</div>
					<?$APPLICATION->IncludeComponent("bitrix:sender.subscribe", "feedback", Array(
					"AJAX_MODE" => "N",	// Включить режим AJAX
					"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
					"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
					"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
					"AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
					"CACHE_TIME" => "3600",	// Время кеширования (сек.)
					"CACHE_TYPE" => "A",	// Тип кеширования
					"CONFIRMATION" => "Y",	// Запрашивать подтверждение подписки по email
					"SET_TITLE" => "N",	// Устанавливать заголовок страницы
					"SHOW_HIDDEN" => "N",	// Показать скрытые рассылки для подписки
					"USE_PERSONALIZATION" => "N",	// Определять подписку текущего пользователя
					),
					false
					);?>
					<div class="footer__social">
						<ul class="footer__social-list">
							<li class="footer__social-list-item"><iframe src="https://yandex.ru/sprav/widget/rating-badge/77056386061" width="150" height="50" frameborder="0"></iframe></li>
							<?/*?><li class="footer__social-list-item"><a href="https://ru-ru.facebook.com" class="footer__social-link icon-soc-fb"></a></li>
							<li class="footer__social-list-item"><a href="https://twitter.com" class="footer__social-link icon-soc-tw"></a></li>
							<li class="footer__social-list-item"><a href="https://plus.google.com" class="footer__social-link icon-soc-gplus"></a></li>
							<li class="footer__social-list-item"><a href="https://vk.com" class="footer__social-link icon-soc-tam"></a></li>
							<li class="footer__social-list-item"><a href="https://vk.com" class="footer__social-link icon-soc-yt"></a></li>
							<li class="footer__social-list-item"><a href="https://vk.com" class="footer__social-link icon-soc-inst"></a></li>
							<li class="footer__social-list-item"><a href="https://vk.com" class="footer__social-link icon-soc-rss"></a></li><?*/?>
						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
	<div class="footer__bottom">
		<div class="container">
			<div class="footer__bottom-inner">
				<div class="footer__bottom-left">
					<?/*?>
					<div class="footer__designed">
						<a href="#" class="footer__designed-logo">
							<img src="<?=SITE_TEMPLATE_PATH?>/img/designed.png" alt="" class="footer__designed-logo-img">
						</a>
						<span class="footer__designed-text">Качественный<br> интернет маркетинг</span>
					</div>
					<?*/?>
					<div class="footer__copyright">&copy; Все права защищены.</div>
					<div class="footer__copyright"><a href="https://4ps-shop.ru/poleznaya-informatsiya/zashchita-informatsii.php"> Защита информации.</a></div>
				</div>
				<div class="footer__bottom-right">
					<a href="#" class="footer__scrolltop">Вверх</a>
				</div>
			</div>
		</div>
	</div>
	<!-- special predlochenie -->
	<?$APPLICATION->IncludeComponent(
		"tralex:special_ads",
		".default",
		Array(
		),
		false
	);?>
<?$APPLICATION->IncludeComponent(
	"viardaru:cookie",
	"",
	Array(
		"JQUERY_EN" => "nojquery",
		"SCRIPT_EN" => "N",
		"SET_ACCEPT_BUTTON_BG" => "#232323",
		"SET_ACCEPT_BUTTON_COLOR" => "#FFFFFF",
		"SET_ACCEPT_BUTTON_NAME" => "Принять",
		"SET_COLOR_SCHEME" => "light",
		"SET_LINK" => "https://4ps-shop.ru/poleznaya-informatsiya/zashchita-informatsii.php",
		"SET_TEXT" => "",
		"SHOW_ACCEPT_BUTTON" => "Y",
		"USE_STANDARD_NOTIFICATION" => "standard"
	)
);?>
</footer>
<!-- footer end -->
<div class="inspection-tablet"></div>
<!-- Scripts load -->
<!--<script>window.jQuery || document.write('<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-1.11.3.min.js"><\/script>')</script>-->
<script src="<?=SITE_TEMPLATE_PATH?>/js/jquery-ui.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/widgets.js"></script>
<?$APPLICATION->AddHeadScript('/local/templates/4sport/js/functions.js');?>
<!-- <script src="<?=SITE_TEMPLATE_PATH?>/js/functions.js"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		// hide #back-top first
		$("#back-top").hide();
		// fade in #back-top
		$(function () {
			$(window).scroll(function () {
				if ($(this).scrollTop() > 50) {
					$('#back-top').fadeIn();
				} else {
					$('#back-top').fadeOut();
				}
			});
			// scroll body to 0px on click
			$('#back-top a').click(function () {
				$('body,html').animate({
					scrollTop: 0
				}, 800);
				return false;
			});
		});
	});
</script>
<div class="vcard hrenShow">
	<div>
		<span class="category">Интернет мазанин</span>
		<span class="fn org">4ps-shop.ru</span>
	</div>
	<div class="adr">
		<span class="locality">г. Москва</span>,
		<span class="street-address"> Дмитровское ш.100/2 БЦ Норд Хаус, оф. 4734</span>
	</div>
	<div>Телефон: <span class="tel">+7-499-649-49-84, +7-925-411-50-47</span></div>
	<div>Мы работаем <span class="workhours">ежедневно с 09:00 до 18:00</span>
		<span class="url">
		<span class="value-title" title="http://4ps-shop.ru/"> </span>
		</span>
	</div>
</div>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(57518047, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/57518047" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter —>

</body>
</html>
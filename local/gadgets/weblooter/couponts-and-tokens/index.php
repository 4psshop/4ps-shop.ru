<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
require \Bitrix\Main\Application::getDocumentRoot().'/local/api/wbl_coupons_and_tokens.php';

$wblGetPost = function ()
{
    return \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('WBL_COUPON_AND_TOKEN');
};

if( $wblGetPost()['SEND'] == 'Y' && check_bitrix_sessid() )
{
    // edit old agents
    if( !empty( $wblGetPost()['AGENT'] ) )
    {
        \WblCouponsAndTokens::getInstance()->editAgents( $wblGetPost()['AGENT'] );
    }

    // add agent
    if(
        !empty( $wblGetPost()['NEW_AGENT']['NAME'] )
        && !empty( $wblGetPost()['NEW_AGENT']['TOKEN'] )
    )
    {
        \WblCouponsAndTokens::getInstance()->addAgent($wblGetPost()['NEW_AGENT']['NAME'], $wblGetPost()['NEW_AGENT']['TOKEN']);
    }

    // set agent coupons
    if( !empty( $wblGetPost()['AGENT_COUPONS'] ) )
    {
        \WblCouponsAndTokens::getInstance()->setAgentCoupons($wblGetPost()['AGENT_COUPONS']);
    }

    \Bitrix\Main\Application::getInstance()->getContext()->getResponse()->addHeader('Location: '.\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri() );
}
?>
<style type="text/css">
    .wbl-block .flex-row-center {display: flex; align-items: flex-start; flex-direction: row; justify-content: center; margin: 0 -10px 20px;}
    .wbl-block .flex-row-center > div {width: 100%; padding: 0 10px;}
    .wbl-block .flex-row-center [type=text],
    .wbl-block .flex-row-center select {width: 100%; box-sizing: border-box;}
</style>
<div class="bx-gadgets-info">
	<div class="bx-gadgets-content-padding-rl" style="padding: 20px 20px;">
        <form action="<?=\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getRequestUri()?>" method="post">
            <?=bitrix_sessid_post()?>
            <input type="hidden" name="WBL_COUPON_AND_TOKEN[SEND]" value="Y" />
            <div class="wbl-block">
                <h3>Список контрагентов и их токенов</h3>

                <?
                foreach (\WblCouponsAndTokens::getInstance()->getAgents() as $argentCode => $arAgent)
                {
                    ?>
                    <div class="flex-row-center">
                        <div>
                            <input type="text" name="WBL_COUPON_AND_TOKEN[AGENT][<?=$argentCode?>][NAME]" placeholder="Название контрагента" value="<?=$arAgent['NAME']?>" />
                        </div>
                        <div>
                            <input type="text" name="WBL_COUPON_AND_TOKEN[AGENT][<?=$argentCode?>][TOKEN]" placeholder="Токен" value="<?=$arAgent['TOKEN']?>" />
                        </div>
                        <div>
                            <label>
                                <input type="checkbox" name="WBL_COUPON_AND_TOKEN[AGENT][<?=$argentCode?>][DELETE]" value="Y" /> Удалить
                            </label>
                        </div>
                    </div>
                    <?
                }
                ?>
                <label><b>Создать нового</b></label><br/>
                <div class="flex-row-center">
                    <div>
                        <input type="text" name="WBL_COUPON_AND_TOKEN[NEW_AGENT][NAME]" placeholder="Название контрагента" value="" />
                    </div>
                    <div>
                        <input type="text" name="WBL_COUPON_AND_TOKEN[NEW_AGENT][TOKEN]" placeholder="Токен" value="" />
                    </div>
                </div>


                <hr/>
                <h3>Список промокодов контрагентов</h3>
                <?
                foreach (\WblCouponsAndTokens::getInstance()->getAgents() as $agentCode => $arAgent)
                {

                    $arAgentCoupons = \WblCouponsAndTokens::getInstance()->getAgentCoupons($agentCode);
                    ?>
                    <div class="flex-row-center">
                        <div>
                            <?=$arAgent['NAME']?>
                        </div>
                        <div>
                            <select name="WBL_COUPON_AND_TOKEN[AGENT_COUPONS][<?=$agentCode?>][]" multiple size="6">
                                <?
                                foreach (WblCouponsAndTokens::getInstance()->getCouponsList() as $nameCoupon => $coupon)
                                {
                                    ?>
                                    <option value="<?=$coupon?>" <?=( in_array($coupon, $arAgentCoupons) )?'selected':''?> ><?=$nameCoupon?></option>
                                    <?
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <?
                }
                ?>

                <hr/>
                <h3>Путь к restfull api</h3>
                <p>
                    Путь - <code>/local/restfull-api/get-orders/#COUPON#/#AGENT_TOKEN#/</code><br/>
                    Пример - <code>/local/restfull-api/get-orders/SL-6T47L-VOZBFU5/W097DG9ZVAASA607/</code>
                </p>

                <button class="adm-btn">Сохранить</button>
            </div>
        </form>
	</div>	
</div>
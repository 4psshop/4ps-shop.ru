<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<style type="text/css">
.pp-header, .pp-footer{
    padding: 10px;
    background: #fe4017;
    box-shadow: inset 0px -3px 0px rgba(0, 0, 0, 0.2);
}
.pp-header h3{
    margin:0;
    font: normal 1.7em/1.3 Verdana;
    color: #fff;
    text-shadow: 1px 2px 2px rgba(0, 0, 0, 0.7);
    text-align: center;
}
.pp-content{ background: #fff; padding: 20px 10px;}
.pp-content-main{
    max-width: 340px;
    min-width: 140px;
    padding: 0 0 10px 10px;
    margin-right: 10px;
    display: inline-block;
    vertical-align: top;
    width: 50%;
}
.pp-content-sidebar img {
    min-width: 120px;
    max-width: 200px;
    width: 100%;
}
.pp-content-main ul,
.pp-content-main h4 {padding-left: 0; margin: 0;}
.pp-content-main h4 { font-size: 18px; margin:0 0 5px 20px;}
.pp-content-main li {list-style: none;}
.pp-content-main li i {margin-right: 5px;}
.pp-content-sidebar{
    /*width: 130px;*/
    display: inline-block;
    vertical-align: top;
}
.pp-content-sidebar i {font-size: 184px; color: #036;}
.pp-footer { background: #EAEAEC; }
.pp-footer input {
    padding: 8px 12px;
    border-radius: 3px;
    outline: none;
    border: 1px solid #dfdfdf;
}
#pp-bg {
    position: fixed;
    top:0; left:0;
    width:100%; height: 100%;
    background: rgba(30,30,30,0.5);
    z-index:99999;
    display: none;
}
.btnsp, .btnsp:visited {
    position: relative;
    display: inline-block;
    outline: none; border: none;
    padding: 5px 10px 6px;
    background: #fe4017;
    box-shadow: inset 0px -3px 0px rgba(0, 0, 0, 0.2);
    color: #fff;
    cursor: pointer;
}
.btnsp:hover { background-color: #fe4017; }
.btnsp:active { top: 1px; }
@media (max-width: 468px) {
.pp-content-main {
    width: 100%;
}
}

#promo2, #promo2 .pp-content{
    width: 30vw;
    background: transparent;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    flex-wrap: wrap;
    flex-direction: column;
}
#promo2, #promo2 .pp-content p{
    font-size: 1.1em;
}
</style>

	    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/js/jquery-latest.min.js"></script>
    <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH;?>/js/jquery.cookie.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){

		    function pp_show(){
                $("a.fancybox").fancybox().trigger('click');
            }
            function look_yes_func(){
                $("a.look_yes_facny").fancybox().trigger('click');
            }
            setTimeout(function(){  look_yes_func(); }, <?=$arResult["TIME"]*100;?>);
            setTimeout(function(){  pp_show(); }, <?=$arResult["TIME"]*1000;?>);

		});
	</script>

<? if ($_COOKIE['OLD_USER'] != "Y"):
   setcookie("OLD_USER", 'Y', time()+3600*24*30, '/');
   ?>
   <div style="display: none;">
      <a href="#promo" id="click-this" class="fancybox">link</a>
      <div id="promo" style="display:none">
        <div class="pp-header">
            <?=htmlspecialcharsBack($arResult["TITLE"]["TEXT"]);?>
        </div>
        <div class="pp-content">
            <div class="pp-content-main">
                 <?=htmlspecialcharsBack($arResult["TEXT"]);?>
            </div>
            <?if(!empty($arResult['PICTURE'])):?>
                <div class="pp-content-sidebar">
                    <img src="<?=$arResult['PICTURE'];?>">
                </div>
             <?endif;?>
        </div>
        <div class="pp-footer">
            <form action="" method="POST" class="form_podpiska_new">
                <input type="text" name="name" placeholder="Ваше имя" required/>
                <input type="text" name="email" placeholder="email@email.com" required/>
                <input type="hidden" name="nameAc" value="<?=$arResult['NAME'];?>" required>
                <input class="btnsp" name="sub" type="submit" value="Подписаться" />
            </form>
        </div>
      </div>
    </div>
<?else:?>
    <?if ( $_REQUEST['okmessage'] == "show" ){?>
        <div style="display: none;">
            <a href="#promo2" id="click-this2" class="look_yes_facny">link2</a>

            <div id="promo2" style="display:none">
                <div class="pp-content">
                    <p>Спасибо, Вы подписаны!</p>
                </div>
            </div>
        </div>
    <?}?>
<?endif;?>




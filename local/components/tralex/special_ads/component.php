<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$arSelect = Array("ID", "NAME","PREVIEW_TEXT","PREVIEW_PICTURE","PROPERTY_TIME","PROPERTY_TITLE");
$arFilter = Array("IBLOCK_ID"=>IntVal(11), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
while($ob = $res->GetNext()){
  $file = CFile::ResizeImageGet($ob["PREVIEW_PICTURE"], array('width'=>'200', 'height'=>'200'), BX_RESIZE_IMAGE_EXACT, true);
  $arResult = array(
    'NAME' => $ob["NAME"],
    'TIME' => $ob["PROPERTY_TIME_VALUE"],
    'TITLE' => $ob["PROPERTY_TITLE_VALUE"],
    'TEXT' => $ob["PREVIEW_TEXT"],
    'PICTURE' => $file['src'],
  );
}


if (isset($_REQUEST['name'])) {
  $name = $_REQUEST['name'];
  $name = strip_tags($name);
  $name = trim($name);
  // $name = htmlentities($name);
  $name = stripslashes($name);
}
if (isset($_REQUEST['email'])) {
  $email = $_REQUEST['email'];
  $email = strip_tags($email);
  $email = trim($email);
  // $email = htmlentities($email);
  $email = stripslashes($email);
}
if (isset($_REQUEST['nameAc'])) {
  $nameAc = $_REQUEST['nameAc'];
  $nameAc = strip_tags($nameAc);
  $nameAc = trim($nameAc);
  // $nameAc = htmlentities($nameAc);
  $nameAc = stripslashes($nameAc);
}
// echo $name;
// echo $email;
// echo $nameAc;
  // echo '<pre>';
  // print_r($_COOKIES);
  // echo '</pre>';
if (!empty($_REQUEST['name']) && !empty($_REQUEST['email']) && !empty($_REQUEST['nameAc'])) {
  # code...
  $el = new CIBlockElement;

  $PROP = array();

  $arLoadProductArray = Array(
    "MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
    "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
    "IBLOCK_ID"      => 12,
    "PROPERTY_VALUES"=> $PROP,
    "NAME"           => $nameAc,
    "ACTIVE"         => "Y",            // активен
    "PREVIEW_TEXT"   => "Имя: ".$name." Почта: ".$email
    );

  if($PRODUCT_ID = $el->Add($arLoadProductArray)){
    //echo "New ID: ".$PRODUCT_ID;
    LocalRedirect("?okmessage=show");
  }
  else{
    echo "Error: ".$el->LAST_ERROR;
  }
}
//echo "ok";
$this->IncludeComponentTemplate();

?>
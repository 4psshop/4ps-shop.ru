<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if ( $this->StartResultCache( 86400 ) )  
{  
?>
<!-- sanitaze variables -->
<?
  if (isset($_REQUEST['basket_form_name'])) {
    $name = $_REQUEST['basket_form_name'];
    $name = strip_tags($name);
    $name = trim($name);
    // $name = htmlentities($name);
    $name = stripslashes($name);          
  }
  if (isset($_REQUEST['basket_form_phone'])) {
    $phone = $_REQUEST['basket_form_phone'];
    $phone = strip_tags($phone);
    $phone = trim($phone);
    // $phone = htmlentities($phone);
    $phone = stripslashes($phone);
  }
  if (isset($_REQUEST['basket_form_mail'])) {
    $mail = $_REQUEST['basket_form_mail'];
    $mail = strip_tags($mail);
    $mail = trim($mail);
    // $mail = htmlentities($mail);
    $mail = stripslashes($mail);
  }
  if (isset($_REQUEST['action_type'])) {
    $action_type = $_REQUEST['action_type'];
    $action_type = strip_tags($action_type);
    $action_type = trim($action_type);
    // $action_type = htmlentities($action_type);
    $action_type = stripslashes($action_type);
  }
  if (isset($_REQUEST['basket_form_master'])) {
    $basket_form_master = $_REQUEST['basket_form_master'];
    $basket_form_master = strip_tags($basket_form_master);
    $basket_form_master = trim($basket_form_master);
    // $basket_form_master = htmlentities($basket_form_master);
    $basket_form_master = stripslashes($basket_form_master);
  }
  if (isset($_REQUEST['basket_form_team'])) {
    $basket_form_team = $_REQUEST['basket_form_team'];
    $basket_form_team = strip_tags($basket_form_team);
    $basket_form_team = trim($basket_form_team);
    // $basket_form_team = htmlentities($basket_form_team);
    $basket_form_team = stripslashes($basket_form_team);
  }  
  if (isset($_REQUEST['basket_form_town'])) {
    $basket_form_town = $_REQUEST['basket_form_town'];
    $basket_form_town = strip_tags($basket_form_town);
    $basket_form_town = trim($basket_form_town);
    // $basket_form_town = htmlentities($basket_form_town);
    $basket_form_town = stripslashes($basket_form_town);
  }
  if (isset($_REQUEST['action_delivery'])) {
    $action_delivery = $_REQUEST['action_delivery'];
    $action_delivery = strip_tags($action_delivery);
    $action_delivery = trim($action_delivery);
    // $action_delivery = htmlentities($action_delivery);
    $action_delivery = stripslashes($action_delivery);
  }
  if (isset($_REQUEST['basket_form_adress'])) {
    $basket_form_adress = $_REQUEST['basket_form_adress'];
    $basket_form_adress = strip_tags($basket_form_adress);
    $basket_form_adress = trim($basket_form_adress);
    // $basket_form_adress = htmlentities($basket_form_adress);
    $basket_form_adress = stripslashes($basket_form_adress);
  }
  if (isset($_REQUEST['action_pay'])) {
    $action_pay = $_REQUEST['action_pay'];
    $action_pay = strip_tags($action_pay);
    $action_pay = trim($action_pay);
    // $action_pay = htmlentities($action_pay);
    $action_pay = stripslashes($action_pay);
  }
  // basket current users
  $arBasketItems = array();
  $dbBasketItems = CSaleBasket::GetList(
          array(
                  "NAME" => "ASC",
                  "ID" => "ASC"
              ),
          array(
                  "FUSER_ID" => CSaleBasket::GetBasketUserID(),
                  "LID" => SITE_ID,
                  "ORDER_ID" => "NULL"
              ),
          false,
          false,
          array("ID", "NAME","CALLBACK_FUNC", "MODULE", 
                "PRODUCT_ID", "QUANTITY", "DELAY", 
                "CAN_BUY", "PRICE", "WEIGHT")
      );
      while ($arItems = $dbBasketItems->Fetch())
      {
          if (strlen($arItems["CALLBACK_FUNC"]) > 0)
          {
              CSaleBasket::UpdatePrice($arItems["ID"], 
                                       $arItems["CALLBACK_FUNC"], 
                                       $arItems["MODULE"], 
                                       $arItems["PRODUCT_ID"], 
                                       $arItems["QUANTITY"]);
              $arItems = CSaleBasket::GetByID($arItems["ID"]);
          }
          // $arBasketItems[] = $arItems;
          $name_goods[] = $arItems['NAME'].' - количество '.$arItems['QUANTITY'];
          $arResult['ID'][] = $arItems['PRODUCT_ID'];
          $arResult['QUANTITY'][] = $arItems['NAME']." - количество ".$arItems['QUANTITY']." шт";
          $comma_goods = implode(" ", $name_goods);

          $arBasketItems[] = $arItems; 
      // Печатаем массив, содержащий актуальную на текущий момент корзину
      }
      // kolichestvo tovarov 
      $arResult['BASKET_QUANTITY'] = count($arBasketItems);
      // summa zakaza
      foreach ($arBasketItems as $key => $arBasket) {
        $total_sum[] = (int)$arBasket['PRICE'];
      }
      $arResult['TOTAL_PRICE'] = array_sum($total_sum);
 
    // add to cart
    if (!empty($name) && !empty($phone) && !empty($mail)) 
    {
      if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale"))
      { 
        $el = new CIBlockElement;
        $PROP = array();
        // type order
        $PROP[59] = 18;
        // name 
        $PROP[60] = $name;
        // phone        
        $PROP[61] = $phone;
        // email        
        $PROP[62] = $mail;
        // city        
        // $PROP[63] = $city;
        // adress        
        // $PROP[64] = $addres;
        // kol-vo tovari        
        $PROP[65] = $name_goods;        
        // status order
        $PROP[66] = 20;
        // tovari
        $PROP[68] = $arResult['ID'];
        // type-client
        if ($action_type == 'team') {
          $PROP[74] = 27;
        }
        if ($action_type == 'masterskaya') {
          $PROP[74] = 26;
        }
        // name masterskaya/komandi
        $PROP[75] = $basket_form_master;
         // name masterskaya/komandi
        $PROP[80] = $basket_form_team;
        // gorod masterskoi/komandi
        $PROP[76] = $basket_form_town;
        // sposob dostavki
        if ($action_delivery == 'samovivoz') {
          $PROP[77] = 28;
        }
        if ($action_delivery == 'del_trans') {
          $PROP[77] = 29;
        }
        // address delivery
        $PROP[78] = $basket_form_adress;
        // sposob oplati
        if ($action_pay == 'nal') {
          $PROP[79] = 30;
        }
        if ($action_pay == 'bez_nal') {
          $PROP[79] = 32;
        }
        if ($action_pay == 'credit_card') {
          $PROP[79] = 31;
        }            
        // foreach ($arBasketItems as $basket) {
        // $PROP[66] = array("VALUE"=>array("NAME"=>$basket['QUANTITY']));
        // }
        $arLoadProductArray = Array(
          "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
          "IBLOCK_ID"      => 6,
          "PROPERTY_VALUES"=> $PROP,
          "NAME"           => "Заказ от ".date("d.m.Y"),
          "ACTIVE"         => "Y"
          );
        if($PRODUCT_ID = $el->Add($arLoadProductArray)){
         CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID());
          header( 'Refresh: 0; url=/complete_order/finish/index.php?COMPLETE=Y&ORDER='.$PRODUCT_ID.''); 
        }
        else{
          echo "Error: ".$el->LAST_ERROR;
        }
        // send email
        /* получатели */
    // $to= "stankevich.alexandr@yandex.ru" . ", " ; //обратите внимание на запятую
        $to= "p.sales@4prosport.ru" . ", " ; //обратите внимание на запятую
        // $to .= "mebelgold@mebelgold.ru";

        /* тема/subject */
        $subject = "Поступил заказ";

        /* сообщение */
        $message = '
        <html>
        <head>
        <title>Поступил заказ</title>
        </head>
        <body>
        <p>Поступил заказ</p>
        <table border="1">
        <tr>
        <th>Телефон</th><th>Имя</th><th>Почта</th><th>Товар</th>
        </tr>
        <tr>
        <td>'.$phone.'</td><td>'.$name.'</td><td>'.$mail.'</td><td>'.$comma_goods.'</td>
        </tr>
        </table>
        </body>
        </html>
        ';

        /* Для отправки HTML-почты вы можете установить шапку Content-type. */
      $headers= "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=utf-8\r\n";

        /* дополнительные шапки */
        $headers .= "From: Магазин 4proSport <4proSport@site.com>\r\n";

        /* и теперь отправим из */
        mail($to, $subject, $message, $headers);
        
      }
    }
    else  
    {  
        // Также останавливаем кэширование, если не подключен модуль "Информационные блоки"  
        $this->AbortResultCache();  
    }    
?>
<?
$this->IncludeComponentTemplate();
}
?>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!-- basket block -->
<?if(!empty($arResult['ID'])):?>
	<div class="basket"> 
		<div class="basket__holder basket__holder--indents">
			<form action="" method="POST" id="OrderForm">
				<div class="basket__inner">
					<!-- basket form -->
					<div class="basket__left basket-form">
						<div class="basket-form__block">
							<div class="row">
								<div class="col-sm-100 col-md-50">
									<h2 class="basket-form__title">Контактная информация:</h2>
									<div class="form-row basket-form__row-indent">
										<div class="label basket-form__label">
											<label for="basket_form_name">Имя получателя</label>
										</div>
										<input type="text" name="basket_form_name" id="basket_form_name" required class="input" placeholder="Ваше имя">
									</div>
									<div class="row basket-form__row">
										<div class="col-sm-100 col-md-50 basket-form__col">
											<div class="form-row basket-form__row-indent">
												<div class="label basket-form__label">
													<label for="basket_form_phone">Телефон</label>
												</div>
												<input type="text" name="basket_form_phone" required id="basket_form_phone" class="input js-phone-mask" placeholder="7 (___) ___-__-__">
											</div>
										</div>
										<div class="col-sm-100 col-md-50 basket-form__col">
											<div class="form-row basket-form__row-indent">
												<div class="label basket-form__label">
													<label for="basket_form_mail">E-mail</label>
												</div>
												<input type="email" required name="basket_form_mail" id="basket_form_mail" class="input" placeholder="Ваш e-mail">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-100 col-md-50">
									<h2 class="basket-form__title">Получить скидку:</h2>
									<div class="form-row basket-form__row-indent">
										<div class="label basket-form__label">
											<span>Выберите один из вариантов</span>
										</div>
										<ul class="radioboxes-inline-list basket-form__list basket-form__list--indent">
											<li class="radioboxes-inline-list__item basket-form__item basket-form__item--half basket-form__item--no-indent">
												<label class="radiobox radiobox--text">
													<input type="radio" class="radiobox__check team_check" name="action_type" value="team" checked>
													<span class="radiobox__inner radiobox__inner--left">
														<span class="radiobox__text">Для команды</span>
													</span>
												</label>
											</li>
											<li class="radioboxes-inline-list__item basket-form__item basket-form__item--half basket-form__item--no-indent">
												<label class="radiobox radiobox--text">
													<input type="radio" class="radiobox__check mas_check" value="masterskaya" name="action_type">
													<span class="radiobox__inner radiobox__inner--right">
														<span class="radiobox__text">Для сер. мастерской</span>
													</span>
												</label>
											</li>
										</ul>
									</div>
									<div class="row basket-form__row">
										<div class="col-sm-100 col-md-50 basket-form__col">
											<div class="form-row basket-form__row-indent" id="mas_team">
												<div class="label basket-form__label">
													<label for="basket_form_master">Название мастерской</label>
												</div>
												<input type="text" name="basket_form_master" id="basket_form_master" class="input" placeholder="Название">
											</div>
											<div class="form-row basket-form__row-indent" id="block_team">
												<div class="label basket-form__label">
													<label for="basket_form_master">Название команды</label>
												</div>
												<input type="text" name="basket_form_team" id="basket_form_master" class="input" placeholder="Название">
											</div>											
										</div>
										<div class="col-sm-100 col-md-50 basket-form__col">
											<div class="form-row basket-form__row-indent">
												<div class="label basket-form__label">
													<label for="basket_form_town">Укажите Ваш город</label>
												</div>
												<input type="text" name="basket_form_town" id="basket_form_town" class="input" placeholder="Ваш город">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="basket-form__block">
							<div class="row">
								<div class="col-sm-100 col-md-50">
									<h2 class="basket-form__title">Выберите способ доставки:</h2>
									<ul class="radioboxes-list">
										<li class="radioboxes-list__item basket-form__item">
											<label class="radiobox">
												<input type="radio" class="radiobox__check" value="samovivoz" name="action_delivery" checked>
												<span class="radiobox__icon">
													<span class="radiobox__icon-image"></span>
												</span>
												<span class="radiobox__inner">
													<span class="radiobox__text">Самовывоз</span>
												</span>
											</label>
										</li>
										<li class="radioboxes-list__item basket-form__item">
											<label class="radiobox">
												<input type="radio" class="radiobox__check" value="del_trans" name="action_delivery">
												<span class="radiobox__icon">
													<span class="radiobox__icon-image"></span>
												</span>
												<span class="radiobox__inner">
													<span class="radiobox__text">Доставка транспортной компанией</span>
												</span>
											</label>
										</li>
									</ul>
								</div>
								<div class="col-sm-100 col-md-50">
									<div class="form-row basket-form__row-indent basket-form__row-indent--top">
									<div class="label basket-form__label basket-form__label--indent">
										<label for="basket_form_adress">г. Москва, Дмитровское ш. 100/2 БЦ Норд Хаус, оф. 4734</label>
									</div>
									<input type="text" name="basket_form_adress" id="basket_form_adress" class="input" placeholder="Адрес доставки">
								</div>
								</div>
							</div>
						</div>
						<div class="basket-form__block">
							<h2 class="basket-form__title">Выберите способ оплаты:</h2>
							<ul class="radioboxes-inline-list basket-form__list">
								<li class="radioboxes-inline-list__item basket-form__item basket-form__item--half">
									<label class="radiobox">
										<input type="radio" class="radiobox__check" name="action_pay" value="nal" checked>
										<span class="radiobox__icon">
											<span class="radiobox__icon-image"></span>
										</span>
										<span class="radiobox__inner">
											<span class="radiobox__text">Наличными при получении</span>
										</span>
									</label>
								</li>
								<li class="radioboxes-inline-list__item basket-form__item basket-form__item--half">
									<label class="radiobox">
										<input type="radio" class="radiobox__check" name="action_pay" value="bez_nal">
										<span class="radiobox__icon">
											<span class="radiobox__icon-image"></span>
										</span>
										<span class="radiobox__inner">
											<span class="radiobox__text">Безналичный расчет</span>
										</span>
									</label>
								</li>
								<li class="radioboxes-inline-list__item basket-form__item basket-form__item--half">
									<label class="radiobox">
										<input type="radio" class="radiobox__check" name="action_pay" value="credit_card">
										<span class="radiobox__icon">
											<span class="radiobox__icon-image"></span>
										</span>
										<span class="radiobox__inner">
											<span class="radiobox__text">Кредитная карта</span>
										</span>
									</label>
								</li>
							</ul>
						</div>
					</div>
					<!-- basket form end -->
					<div class="basket__right">
						<div class="basket__burger">
							<span class="basket__burger-decor">
								<span class="basket__burger-decor-line"></span>
							</span>
							<div class="basket__burger-inner">
								<a href="#" class="basket__back" onclick="history.back();return false;">
									<span class="basket__back-icon icon-arrow-right-gray-light-small"></span>
									<span class="basket__back-text">Назад</span>
									<span class="basket__back-text basket__back-text--color">на шаг 1</span>
								</a>
								<!-- backet click -->
								<div class="basket-click">
									<span class="basket-click__heading">
										<svg class="icon-cart-orange-small basket-click__heading-icon">
											<use xlink:href="#cart"></use>
										</svg>
										<span class="basket-click__heading-text">У вас <span class="basket-click__heading-text--color">
										<?=$arResult['BASKET_QUANTITY'];?>
										</span>товара, на сумму:</span>
									</span>
									<span class="basket-click__price">
										<span class="basket-click__price-inner"><?=$arResult['TOTAL_PRICE'];?> р.</span>
									</span>
									<a href="#" class="btn btn-red basket-click__link" id="myBuyOneCart">
										<span class="btn-black__inner">
											<span class="btn__text btn__text--middle">Купить в один клик</span>
										</span>
									</a>
									<span class="basket-click__text">От вас потребуется только <br>имя и телефон для связи</span>
								</div>
								<!-- backet click end -->
							</div>
						</div>
					</div>
				</div>
				<!-- basket common -->
				<div class="basket-common">
					<div class="basket-common__content">
						<span class="basket-common__text">Итого:</span>
						<span class="basket-common__price">
							<span class="basket-common__price-inner">
								<?=$arResult['TOTAL_PRICE'];?> р.
							</span>
						</span>
					</div>
				</div>
				<!-- basket common end -->
			</form>
		</div>
		<!-- basket actions -->
		<div class="basket-actions">
			<a href="#" class="btn btn-red btn--cart-left basket-actions__btn" id="comp_button">
				<span class="btn-red__inner">
					<span class="btn__icon">
						<svg class="icon-cart-white-large">
							<use xlink:href="#cart"></use>
						</svg>
					</span>
					<span class="btn__text btn__text--middle">
						Подтвердить заказ
					</span>
				</span>
			</a>
		</div>
		<!-- basket actions end -->
	</div>
<?else:?>
	<p style="text-align: center;font-size: 15pt">
		Корзина пуста
	</p>
<?endif;?>
<!-- The Modal -->
<div id="myModalBuyOneClick" class="modalBuyOneClick">
	<form action="" method='POST'  id="myFormBuyOne">
		<!-- Modal content -->
		<div class="modal-contentBuyOne">
			<span class="close">&times;</span>
			<div class="form-row basket-form__row-indent">
				<div class="label basket-form__label">
					<label for="basket_form_name_modal">Имя получателя</label>
				</div>
				<input type="text" name="basket_form_name_modal" id="basket_form_name_modal" class="input" placeholder="Ваше имя" required>
			</div>
			<div class="row basket-form__row">
				<div class="col-sm-100 col-md-50 basket-form__col">
					<div class="form-row basket-form__row-indent">
						<div class="label basket-form__label">
							<label for="basket_form_phone_modal">Телефон</label>
						</div>
						<input type="text" name="basket_form_phone_modal" id="basket_form_phone_modal" class="input js-phone-mask" placeholder="7 (___) ___-__-__" required>
					</div>
				</div>
				<div class="col-sm-100 col-md-50 basket-form__col">
					<div class="form-row basket-form__row-indent">
						<div class="label basket-form__label">
							<label for="basket_form_mail_modal">E-mail</label>
						</div>
						<input type="email" name="basket_form_mail_modal" id="basket_form_mail_modal" class="input" placeholder="Ваш e-mail" required>
					</div>
				</div>
			</div>
			<div class="basket-actions">
				<button type="submit" class="btn btn-red btn--cart-left basket-actions__btn" id="buy_one">
					<span class="btn-black__inner">
						<span class="btn__icon">
							<svg class="icon-cart-white-large">
								<use xlink:href="#cart"></use>
							</svg>
						</span>
						<span class="btn__text btn__text--middle">Купить</span>
					</span>
				</button>
			</div>
			<div id="result"></div>
		</div>
		<!-- name goods -->
		<?
 		$json_id = json_encode($arResult['ID']); 
 		$json_name = json_encode($arResult['QUANTITY']); 
		?>
		<input type="hidden" value="<?echo $json_id;?>" id="name_goods_modal">
		<input type="hidden" value="<?echo $json_name;?>" id="id_goods_modal">
	</form>
</div>
<style type="text/css">
		/* The Modal (background) */
	.modalBuyOneClick {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 250px!important; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}
	/* Modal Content */
	.modal-contentBuyOne {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    border: 1px solid #888;
	    width: 500px;
	    /*margin-top: 256px;*/
	}
	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}
	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
</style>
<script>
	// Get the modal
	var modal = document.getElementById('myModalBuyOneClick');
	// Get the button that opens the modal
	var btn = document.getElementById("myBuyOneCart");
	// Get the <span> element that closes the modal
	var span = document.getElementsByClassName("close")[0];
	// When the user clicks the button, open the modal 
	btn.onclick = function() {
	    modal.style.display = "block";
	}
	// When the user clicks on <span> (x), close the modal
	span.onclick = function() {
	    modal.style.display = "none";
	    location.reload();
	}
	// When the user clicks anywhere outside of the modal, close it
	window.onclick = function(event) {
	    if (event.target == modal) {
	        modal.style.display = "none";
	    }
	}
</script>
<script>
$(document).ready(function(){
	$("#mas_team").hide();
	// click master/team
	$(".mas_check").click(function(){
	 	$("#mas_team").show();
	 	$("#block_team").hide();
	});
	$(".team_check").click(function(){
	 	$("#block_team").show();
	 	$("#mas_team").hide();
	});		
	var id_goods = <?php echo $json_id;?>;
	var json_name = <?php echo $json_name;?>;
	$( "#comp_button" ).click(function() {
		$( "#OrderForm" ).submit();
	});		
	$('#myFormBuyOne').submit(function(){
		$.ajax({
			type: "POST",
			url: "/ajax/buy_one_click_cart.php",
			data: "username="+$("#basket_form_name_modal").val()+"&email="+$("#basket_form_mail_modal").val()+"&phone="+$("#basket_form_phone_modal").val() + "&name_goods=" + json_name + "&id_goods=" + id_goods,
			success: function(html){
				// alert(123);
				$("#result").html(html);
				//show mod
				 // modal.style.display = "block";
			}
		});
		return false;
	});
});
</script>						














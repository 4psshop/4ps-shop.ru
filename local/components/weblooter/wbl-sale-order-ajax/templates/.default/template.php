<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

$APPLICATION->SetAdditionalCSS($this->getFolder().'/bootstrap-grid.min.css');
?>
<script src="<?=$this->getFolder().'/jquery.mask.min.js'?>"></script>
<?
$APPLICATION->SetTitle('Оформление заказа');
$APPLICATION->SetPageProperty('title', 'Оформление заказа');
$wblCreateInput = function ($arItem, $keyName = 'ORDER_PROPS') use ($arResult){
    ?>
    <div class="wbl-input-row">
        <div class="wbl-input-name">
            <?=$arItem['NAME']?> <?=($arItem['REQUIED']=='Y')?'<span class="required">*</span>':''?>
        </div>
        <?switch ($arItem['TYPE']){
            case 'TEXT':
                ?>
                <input type="text" name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]" value="<?=$arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]?>" <?=($arItem['REQUIED']=='Y')?'required':''?> autocomplete="nope<?=rand(100000000, 9000000000)?>" data-<?=$keyName?> <?=(!empty($arItem['DESCRIPTION']))?'placeholder=\''.$arItem['DESCRIPTION'].'\'':''?> />
                <?
                break;
            case 'LOCATION':
                ?>

                <?/*$GLOBALS["APPLICATION"]->IncludeComponent(
                "bitrix:sale.ajax.locations",
                "wbl-popup",
                array(
                    "AJAX_CALL" => "N",
                    "COUNTRY_INPUT_NAME" => "COUNTRY",
                    "REGION_INPUT_NAME" => "REGION",
                    "CITY_INPUT_NAME" => "CITY",
                    "CITY_OUT_LOCATION" => "Y",
                    "LOCATION_VALUE" => $arResult['FORM_RESULT']['ORDER_PROPS'][$arItem['ID']], // Значение свойства местоположение
                    "ORDER_PROPS_ID" => $arItem['ID'], // АйДи свойства местоположени,
                    'WBL_HIDDEN_INPUT_NAME' => 'FORM_RESULT['.$keyName.']['.$arItem['ID'].']'
                ),
                null,
                array('HIDE_ICONS' => 'Y')
            );*/?>
                <select name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]">
                    <?if( !empty($arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]) ):?>
                        <option value="<?=$arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]?>"><?=$arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]?></option>
                    <?endif;?>
                </select>
                <script type="text/javascript">
                    $(document).ready(function () {
                        <?if( !empty($arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]) ):?>
                            $('[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]').html('<option value="<?=$arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]?>">'+(localStorage.city)+'</option>');
                        <?endif;?>
                        $("[name='FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]']").select2(
                            {
                                "minimumInputLength":1,
                                "language":{
                                    "errorLoading":function () {
                                        return 'Подготавливаем список...';
                                    },
                                    "searching":function () {
                                        return 'Подготавливаем список...';
                                    },
                                    'inputTooShort': function () {
                                        return 'Варианты появятся сразу после ввода первых букв населенного пункта';
                                    },
                                    'noResults' : function () {
                                        return 'Такого населенного пункта в базе не найдено';
                                    }
                                },
                                "ajax":{
                                    "url":"/ajax/wbl_get_locations_list.php",
                                    "dataType":"json",
                                    "data":function(params) {
                                        return {q:params.term};
                                    }
                                },
                                "escapeMarkup":function (markup) {
                                    return markup;
                                },
                                "templateResult":function(city) {
                                    return city.text;
                                },
                                "templateSelection":function (city) {
                                    return city.text;
                                },
                                "width":"100%",
                                "placeholder":"--Выберите населенный пункт--"
                            });
                        $('[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]').on('change', function () {
                            console.log( $(this).val() );
                            localStorage.setItem('city', $('.select2-selection__rendered').text() );
                        })
                    })
                </script>

                <?
                break;
            default:
                ?>
                <input type="text" name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]" value="<?=$arResult['FORM_RESULT'][$keyName][ $arItem['ID'] ]?>" autocomplete="nope<?=rand(100000000, 9000000000)?>" data-<?=$keyName?> <?=(!empty($arItem['DESCRIPTION']))?'placeholder=\''.$arItem['DESCRIPTION'].'\'':''?> />
                <?
                break;
        }?>
    </div>
    <?if( $arItem['CODE'] == 'PHONE' ):?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]').mask('+7 (999) 999-99-99');
            })
        </script>
    <?endif;?>
    <?if( $arItem['CODE'] == 'EMAIL' ):?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('body')
                    .on('change', '[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]', function () {
                        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                        if( !pattern.test($(this).val()) )
                            $(this).val('');
                        checkFormValid($(this).parents('[data-step-block]').attr('data-step-block'))
                    })
            })
        </script>
    <?endif;?>
    <?if( $arItem['IS_LOCATION'] == 'Y' ):?>
        <script type="text/javascript">
            /*
            $(document).ready(function () {
                $('body')
                    .on('change', '#CITY_val', function () {
                        $.ajax({
                            url:'/bitrix/components/bitrix/sale.ajax.locations/search.php',
                            data:({
                                search: $('#CITY_val').val()
                            }),
                            async:false,
                            type:'GET',
                            dataType: 'html',
                            success: function(ResultAjax){
                                var Cities = JSON.parse( ResultAjax.replace(/'/g, '"') );
                                if( Cities.length > 0 ){
                                    var addressCtr = [];

                                    if( typeof Cities[0].NAME != 'undefined' )
                                        addressCtr.push(Cities[0].NAME);
                                    if( typeof Cities[0].REGION_NAME != 'undefined' )
                                        addressCtr.push(Cities[0].REGION_NAME);
                                    if( typeof Cities[0].COUNTRY_NAME != 'undefined' )
                                        addressCtr.push(Cities[0].COUNTRY_NAME);

                                    addressCtr = addressCtr.join(', ');

                                    $('#CITY_val').val( addressCtr );
                                    $('[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]').val( Cities[0].ID );
                                }
                                else{
                                    $('#CITY_val').val('');
                                    $('[name="FORM_RESULT[<?=$keyName?>][<?=$arItem['ID']?>]"]').val('');
                                }
                            }
                        });
                    })
            })
            */
        </script>
    <?endif;?>

<?}?>

<form action="<?=$APPLICATION->GetCurDir()?>" method="post" data-wbl-sale-order-ajax-form  autocomplete="off">
    <input type="hidden" name="STEP" value="<?=$arResult['STEP']?>" />
    <?=bitrix_sessid_post()?>

    <div class="row">
        <?if( $arResult['ORDER_ID'] < 1 ):?>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 visible-xs-block visible-sm-block visible-md-block hidden-lg / wbl-aside-total wbl-total-mobile">
                <div class="wbl-white-bg">

                    <div class="wbl-header">Итого</div>

                    <div class="wbl-list-wrap">
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Товаров:</div>
                            <div class="wbl-val"><?=$arResult['TOTAL']['ITEMS']?></div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Доставка:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['DELIVERY'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Стоимость:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['BASE_AMOUNT'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Стоимость со скидкой:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['AMOUNT'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Конечная стоимость заказа:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['AMOUNT']+$arResult['TOTAL']['DELIVERY'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9 / main-layer">

                <div class="wbl-white-bg">

                    <div class="wbl-steps-wrapper">
                        <div class="wbl-step-layer <?=($arResult['STEP']==1)?'active':''?>">
                            <div class="wbl-step-num">Шаг 1</div>
                            <div class="wbl-step-text">Основная информация</div>
                        </div>
                        <div class="wbl-step-layer <?=($arResult['STEP']==2)?'active':''?>">
                            <div class="wbl-step-num">Шаг 2</div>
                            <div class="wbl-step-text">Способ доставки</div>
                        </div>
                        <div class="wbl-step-layer <?=($arResult['STEP']==3)?'active':''?>">
                            <div class="wbl-step-num">Шаг 3</div>
                            <div class="wbl-step-text">Способ оплаты</div>
                        </div>
                    </div>


                    <!-- Тип плательщика и контактные данные -->
                    <div style="display: <?=($arResult['STEP']=='1')?'block':'none'?>" data-step-block="1">

                        <div class="wbl-block wbl-bottom-border">
                            <h3>Выберите тип плательщика</h3>

                            <?if( empty($arResult['FORM_DATA']['PERSON_TYPES']) && empty($arResult['FORM_DATA']['ORDER_GROUP']) ):?>
                                <?=( !empty($arResult['LAST_ERROR']) )?'<p style="color: #ff1a1a; font-family: \'Bender\'; font-size: 16px;">'.$arResult['LAST_ERROR'].'</p>':''?>
                            <?endif;?>

                            <?if( !empty($arResult['FORM_DATA']['PERSON_TYPES']) ):?>
                                <div class="wbl-select-person">
                                    <?foreach ($arResult['FORM_DATA']['PERSON_TYPES'] as $arItem):?>
                                        <div class="radiobox-layer">
                                            <input type="radio" name="FORM_RESULT[PERSON_TYPE]" id="FORM_RESULT_PERSON_TYPE_<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" <?=( $arResult['FORM_RESULT']['PERSON_TYPE'] == $arItem['ID'] )?'checked':''?> />
                                            <label for="FORM_RESULT_PERSON_TYPE_<?=$arItem['ID']?>"><span><?=$arItem['NAME']?></span></label>
                                        </div>
                                    <?endforeach;?>
                                </div>
                                <?if( !empty($arResult['FORM_DATA']['PERSON_TYPES']) && empty($arResult['FORM_DATA']['ORDER_GROUP']) ):?>
                                    <?=( !empty($arResult['LAST_ERROR']) )?'<p style="color: #ff1a1a; font-family: \'Bender\'; font-size: 16px;">'.$arResult['LAST_ERROR'].'</p>':''?>
                                <?endif;?>
                            <?endif;?>
                        </div>


                        <?if( !empty($arResult['FORM_DATA']['ORDER_GROUP']) ):?>
                            <div class="wbl-block">
                                <h3><?=$arResult['FORM_DATA']['ORDER_GROUP']['NAME']?></h3>
                                <h4>Поля, отмеченные звездочкой (<span class="wbl-required">*</span>) обязательны к заполнению</h4>

                                <?if( !empty($arResult['FORM_DATA']['ORDER_PROPS']) ):?>
                                    <?foreach ($arResult['FORM_DATA']['ORDER_PROPS'] as $arItem):?>
                                        <?$wblCreateInput($arItem);?>
                                    <?endforeach;?>
                                <?endif;?>
                                <div class="wbl-inputs-error" style="display: none;">Заполните обязательные поля чтобы продолжить</div>
                            </div>
                            <?=( !empty($arResult['LAST_ERROR']) )?'<p style="color: #ff1a1a; font-family: \'Bender\'; font-size: 16px;">'.$arResult['LAST_ERROR'].'</p>':''?>


                            <div class="wbl-form-footer">
                                <a class="wbl-back" href="/cart/">Вернуться в корзину</a>
                                <button class="wbl-next" name="STEP" value="2">К следующему шагу</button>
                                <div class="clearfix"></div>
                            </div>
                        <?endif;?>

                    </div>
                    <!-- / Тип плательщика и контактные данные -->

                    <!-- Способ доставки и данные по доставке -->
                    <div style="display: <?=($arResult['STEP']=='2')?'block':'none'?>" data-step-block="2">


                        <div class="wbl-block">
                            <h3>Выберите способ доставки</h3>
                            <?if( !empty($arResult['FORM_DATA']['DELIVERY']) ):?>
                                <div class="wbl-select-delivery row">
                                    <?$i=0;?>
                                    <?foreach ($arResult['FORM_DATA']['DELIVERY'] as $arItem): ++$i;?>
                                        <div class="radiobox-layer col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                            <input type="radio" name="FORM_RESULT[DELIVERY]" id="FORM_RESULT_DELIVERY_<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" <?=$arResult['FORM_RESULT']['DELIVERY']==$arItem['ID']?'checked':''?> <?=$arResult['STEP']=='2'?'required':''?> />
                                            <label for="FORM_RESULT_DELIVERY_<?=$arItem['ID']?>"><span><?=$arItem['NAME']?></span></label>
                                            <?
                                            $srtDesc = array();
                                            if( !empty($arItem['PRICE']) )
                                                $srtDesc[] = '<b>Стоимость:</b> '.$arItem['PRICE'].' руб.';
                                            if( !empty($arItem['TRANSIT']) )
                                                $srtDesc[] = '<b>Доставка:</b> '.$arItem['TRANSIT'];
                                            if( !empty($arItem['DESCRIPTION']) )
                                                $srtDesc[] = $arItem['DESCRIPTION'];

                                            if( !empty($srtDesc) )
                                                $srtDesc = implode('; ', $srtDesc);
                                            if( !empty($srtDesc) ):
                                            ?>
                                            <p><?=$srtDesc?></p>
                                            <?endif;?>
                                        </div>
                                        <?if( !is_float($i/3) && $i != 0 ):?>
                                            <div class="clearfix hidden-xs hidden-sm hidden-md visible-lg-block "></div>
                                        <?endif;?>
                                    <?endforeach;?>
                                    <div class="clearfix"></div>
                                </div>
                            <?endif;?>

                            <?if( !empty($arResult['FORM_RESULT']['DELIVERY']) && !empty( $arResult['FORM_DATA']['DELIVERY_PROPS'] ) ):?>


                                <?
                                $obOrder = \Bitrix\Sale\Order::create(SITE_ID);
                                $obOrder->setPersonTypeId($arResult['FORM_RESULT']['PERSON_TYPE']);
                                $obPropsColl = $obOrder->getPropertyCollection();
                                $locationPropId = $obPropsColl->getDeliveryLocation()->getPropertyId();
                                $strVal = '';
                                if( !empty( $arResult['FORM_RESULT']['ORDER_PROPS'][ $locationPropId ] ) )
                                {
                                    $strVal = $arResult['FORM_RESULT']['ORDER_PROPS'][ $locationPropId ];
                                }
                                elseif( !empty( $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $locationPropId ] ) )
                                {
                                    $strVal = $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $locationPropId ];
                                }
                                ?>

                                <?if( $arResult['FORM_RESULT']['DELIVERY'] == 70 ):?>
                                    <h3>Выберите транспортную компанию и заполните данные доставки</h3>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $.ajax({
                                                url:'/local/templates/4sport/function/get_delivety_methods.php',
                                                data:({
                                                    ADDRESS: '<?=$strVal?>',
                                                    TYPE_OF_DELIVERY: '70',
                                                    NOT_REQUIRED: '<?=$arResult['STEP']!=2?'Y':''?>'
                                                }),
                                                async:false,
                                                type:'POST',
                                                dataType: 'html',
                                                success: function(ResultAjax){
                                                    $('#wwweee').html(ResultAjax)
                                                }
                                            });
                                        })
                                    </script>
                                    <div id="wwweee"></div>
                                    <br/>
                                    <br/>
                                    <?foreach ($arResult['FORM_DATA']['DELIVERY_PROPS'] as $arItem):?>
                                        <?$wblCreateInput($arItem, 'DELIVERY_PROPS');?>
                                    <?endforeach;?>

                                <?elseif( $arResult['FORM_RESULT']['DELIVERY'] == 71 ):?>
                                    <h3>Выберите пункт выдачи заказов</h3>

                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $.ajax({
                                                url:'/local/templates/4sport/function/get_delivety_methods.php',
                                                data:({
                                                    ADDRESS: '<?=$strVal?>',
                                                    TYPE_OF_DELIVERY: '71',
                                                    NOT_REQUIRED: '<?=$arResult['STEP']!=2?'Y':''?>'
                                                }),
                                                async:false,
                                                type:'POST',
                                                dataType: 'html',
                                                success: function(ResultAjax){
                                                    $('#wwweee').html(ResultAjax)
                                                }
                                            });
                                        })
                                    </script>
                                    <div id="wwweee"></div>
                                    <br/>
                                    <br/>

                                <?else:?>
                                    <h3><?=$arResult['FORM_DATA']['DELIVERY_GROUP']['NAME']?></h3>

                                    <?foreach ($arResult['FORM_DATA']['DELIVERY_PROPS'] as $arItem):?>
                                        <?$wblCreateInput($arItem, 'DELIVERY_PROPS');?>
                                    <?endforeach;?>

                                <?endif;?>
                            <?endif;?>
                            <input type="hidden" name="FORM_RESULT[YADELIVERY]" value="<?=($arResult['FORM_RESULT']['DELIVERY_YANDEX'])?$arResult['FORM_RESULT']['DELIVERY_YANDEX']:$arResult['FORM_RESULT']['YADELIVERY']?>">

                            <?if( !empty($arResult['FORM_RESULT']['DELIVERY']) ):?>
                                <div class="wbl-input-row">
                                    <div class="wbl-input-name">
                                        Комментарий
                                    </div>
                                    <textarea name="FORM_RESULT[ORDER_PROPS][COMMENTS]" rows="5"><?=$arResult['FORM_RESULT']['ORDER_PROPS']['COMMENTS']?></textarea>
                                </div>
                            <?endif;?>
                            <div class="wbl-inputs-error" style="display: none;">Заполните обязательные поля чтобы продолжить</div>
                        </div>
                        <?=( !empty($arResult['LAST_ERROR']) )?'<p style="color: #ff1a1a; font-family: \'Bender\'; font-size: 16px;">'.$arResult['LAST_ERROR'].'</p>':''?>


                        <div class="wbl-form-footer">
                            <button name="BACK_TO_STEP" class="wbl-back" value="1">К предыдущему шагу</button>
                            <?if( !empty($arResult['FORM_RESULT']['DELIVERY']) ):?>
                                <button name="STEP" class="wbl-next" value="3">К следующему шагу</button>
                            <?endif;?>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <!-- / Способ доставки и данные по доставке -->

                    <?if( $arResult['STEP'] == 3 ):?>
                        <?foreach ($_REQUEST['FORM_RESULT']['DELIVERY_PROPS'] as $intPropId=>$propVal):?>
                            <input type="hidden" name="FORM_RESULT[DELIVERY_PROPS][<?=$intPropId?>]" value="<?=$propVal?>" />
                        <?endforeach;?>
                    <?endif;?>

                    <!-- Способ оплаты -->
                    <div style="display: <?=($arResult['STEP']=='3')?'block':'none'?>" data-step-block="3">

                        <?if( !empty($arResult['FORM_DATA']['PAYMENT']) ):?>
                            <div class="wbl-block">
                                <h3>Выберите способ оплаты</h3>


                                <div class="wbl-select-delivery row">
                                    <?$i=0;?>
                                    <?foreach ($arResult['FORM_DATA']['PAYMENT'] as $arItem): ++$i;?>
                                        <div class="radiobox-layer col-xs-12 col-sm-12 col-md-12 col-lg-4">
                                            <input type="radio" name="FORM_RESULT[PAYMENT]" id="FORM_RESULT_PAYMENT_<?=$arItem['ID']?>" value="<?=$arItem['ID']?>" <?=($arResult['FORM_RESULT']['PAYMENT']==$arItem['ID'])?'checked':''?> <?=$arResult['STEP']=='3'?'required':''?> />
                                            <label for="FORM_RESULT_PAYMENT_<?=$arItem['ID']?>"><span><?=$arItem['NAME']?></span></label>
                                            <p><?=$arItem['DESCRIPTION']?></p>
                                        </div>
                                        <?if( !is_float($i/3) && $i != 0 ):?>
                                            <div class="clearfix hidden-xs hidden-sm hidden-md visible-lg-block "></div>
                                        <?endif;?>
                                    <?endforeach;?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        <?endif;?>
                        <div class="wbl-block">
                            <?=( !empty($arResult['LAST_ERROR']) )?'<p style="color: #ff1a1a; font-family: \'Bender\'; font-size: 16px;">'.$arResult['LAST_ERROR'].'</p>':''?>
                        </div>

                        <div class="wbl-form-footer">
                            <button name="BACK_TO_STEP" class="wbl-back" value="2">К предыдущему шагу</button>
                            <?if( !empty($arResult['FORM_RESULT']['PAYMENT']) ):?>
                                <button name="STEP" class="wbl-next" value="make_order">Оформить заказ</button>
                            <?endif;?>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                    <!-- / Способ оплаты -->
                </div>

            </div>

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 hidden-xs hidden-sm hidden-md visible-lg / wbl-aside-total">
                <div class="wbl-white-bg">

                    <div class="wbl-header">Итого</div>

                    <div class="wbl-list-wrap">
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Товаров:</div>
                            <div class="wbl-val"><?=$arResult['TOTAL']['ITEMS']?></div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Доставка:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['DELIVERY'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Стоимость:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['BASE_AMOUNT'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Стоимость со скидкой:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['AMOUNT'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                        <div class="wbl-list-layer">
                            <div class="wbl-name">Конечная стоимость заказа:</div>
                            <div class="wbl-val"><?=number_format($arResult['TOTAL']['AMOUNT']+$arResult['TOTAL']['DELIVERY'], 0, ',', ' ')?> р.</div>
                            <div  class="clearfix"></div>
                        </div>
                    </div>

                </div>

            </div>

        <?endif;?>
    </div>

    <?if( $arResult['ORDER_ID'] > 0 ):?>

        <div class="col-xs-12 / wbl-white-bg wbl-end">
            Ваш заказ #<?=$arResult['ORDER_ID']?> успешно оформлен!
            <br/><br/>
            <?
            $ORDER_ID = $arResult['ORDER_ID']; // Id заказа
            $paymentId = $arResult['FORM_RESULT']['PAYMENT']; // Id опталы безналом
            ?>
            <a class="wbl-payment-btn" href="/order/payment.php?ORDER_ID=<?=$ORDER_ID?>&PAYMENT=<?=$paymentId?>">Оплатить</a>
        </div>

        <script type="text/javascript">
            $(window).load(function(){
                window.dataLayer.push({
                    "ecommerce": {
                        "currencyCode": "RUB",
                        "purchase": {
                            "actionField": {
                                "id" : "<?=$arResult['ORDER_ID']?>",
                                "goal_id": 40667662
                            },
                            "products": [
                                <?foreach ($arResult['YA_EC'] as $key=>$arItem):?>
                                <?=( $key > 0 )?',':''?>
                                {
                                    "id": "<?=$arItem['id']?>",
                                    "name": "<?=$arItem['name']?>",
                                    "price": "<?=$arItem['price']?>",
                                    "quantity": "<?=$arItem['quantity']?>"
                                }
                                <?endforeach;?>
                            ]
                        }
                    }
                });
            })
        </script>

    <?endif;?>

    <button data-wbl-form-send style="display: none;"></button>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

$arResult = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('result');
if( empty($arResult) )
    die();

$GLOBALS['USER']->Authorize(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('intUserID'));

/* ЯДоставка */
$intDeliveryPrice = $arResult['FORM_DATA']['DELIVERY'][ $arResult['FORM_RESULT']['DELIVERY'] ]['PRICE'];
$arYADelivery = [];
if( !empty($arResult['FORM_RESULT']['YADELIVERY']) )
{
    $arYADelivery = explode('#|#', $arResult['FORM_RESULT']['YADELIVERY']);
    $intDeliveryPrice = $arYADelivery[0];
}
/* / ЯДоставка */



$arResult['YA_EC'] = [];

\Bitrix\Sale\DiscountCouponsManager::init();
//$arCoupons = \Bitrix\Sale\DiscountCouponsManager::get();
$arCoupons = \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('arCoupons');
foreach ($arCoupons as $strCoupon => $arCoupon)
{
    \Bitrix\Sale\DiscountCouponsManager::add($strCoupon);
}

$obOrder = \Bitrix\Sale\Order::create(SITE_ID, \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('intUserID'));
$obOrder->setPersonTypeId($arResult['FORM_RESULT']['PERSON_TYPE']);
$obOrder->setField('USER_DESCRIPTION', $arResult['FORM_RESULT']['ORDER_PROPS']['COMMENTS']);

/**
 * @var \Bitrix\Sale\Basket $obBasket
 * @var \Bitrix\Sale\BasketItem $obBasketItem
 */
//$obBasket = \Bitrix\Sale\Basket::loadItemsForFUser( \Bitrix\Sale\Fuser::getId(), SITE_ID );
$obBasket = \Bitrix\Sale\Basket::loadItemsForFUser( \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('Fuser'), SITE_ID );

$obDiscount = \Bitrix\Sale\Discount::loadByBasket($obBasket);
$obBasket->refreshData(array('PRICE', 'COUPONS'));
$obDiscount->calculate();
$arDiscountResult = $obDiscount->getApplyResult();
$obBasket->save();

$obOrder->setBasket($obBasket);
$obBasket = $obOrder->getBasket();

foreach ($obBasket->getBasketItems() as $obBasketItem)
{
    $arResult['YA_EC'][] = [
        'id' => $obBasketItem->getProductId(),
        'name' => $obBasketItem->getProductId(),
        'price' => $obBasketItem->getPrice(),
        'quantity' => $obBasketItem->getQuantity()
    ];
}

/**
 * @var \Bitrix\Sale\ShipmentCollection $obShipmentColl
 * @var \Bitrix\Sale\Shipment $obShipment
 * @var \Bitrix\Sale\ShipmentItemCollection $obShipmentItemColl
 */
$obShipmentColl = $obOrder->getShipmentCollection();
$obShipment =  $obShipmentColl->createItem( \Bitrix\Sale\Delivery\Services\Manager::getObjectByCode($arResult['FORM_RESULT']['DELIVERY'] ) );
$obShipment->setBasePriceDelivery($intDeliveryPrice);
$obShipment->setField('CURRENCY', $obOrder->getCurrency() );
$obShipmentItemColl = $obShipment->getShipmentItemCollection();

foreach ($obBasket->getBasketItems() as $obBasketItem)
{
    $item = $obShipmentItemColl->createItem($obBasketItem);
    $item->setQuantity( $obBasketItem->getQuantity() );
}

/**
 * @var \Bitrix\Sale\PaymentCollection $obPaymentColl
 * @var \Bitrix\Sale\Payment $obPayment
 */
$obPaymentColl = $obOrder->getPaymentCollection();
$obPayment = $obPaymentColl->createItem( \Bitrix\Sale\PaySystem\Manager::getObjectById( $arResult['FORM_RESULT']['PAYMENT'] ) );
$obPayment->setField('SUM', $obOrder->getPrice());
$obPayment->setField('CURRENCY', $obOrder->getCurrency());


/**
 * @var \Bitrix\Sale\PropertyValueCollection $obPropsColl
 * @var \Bitrix\Sale\PropertyValue $obProp
 */
$obPropsColl = $obOrder->getPropertyCollection();
foreach ($obPropsColl as $obProp)
{
    switch ($obProp->getPropertyId())
    {
        case '55':
            if( !empty($arResult['FORM_RESULT']['YADELIVERY']) )
                $obProp->setValue( $arYADelivery[1] );
            break;
        case '56':
            if( !empty($arResult['FORM_RESULT']['YADELIVERY']) )
                $obProp->setValue( $arYADelivery[3] );
            break;
        case '57':
            if( !empty($arResult['FORM_RESULT']['YADELIVERY']) )
                $obProp->setValue( $arYADelivery[4] );
            break;
        case $obPropsColl->getDeliveryLocation()->getPropertyId():
            $strVal = '';
            if( !empty( $arResult['FORM_RESULT']['ORDER_PROPS'][ $obProp->getPropertyId() ] ) )
            {
                $strVal = $arResult['FORM_RESULT']['ORDER_PROPS'][ $obProp->getPropertyId() ];
            }
            elseif( !empty( $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $obProp->getPropertyId() ] ) )
            {
                $strVal = $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $obProp->getPropertyId() ];
            }

            $strCode = \Bitrix\Sale\Location\LocationTable::getList([ 'filter'=>['=ID'=>$strVal] ])->fetchRaw();
            $strCode = $strCode['CODE'];

            $obProp->setValue( $strCode );
            break;

        default:
            $strVal = '';
            if( !empty( $arResult['FORM_RESULT']['ORDER_PROPS'][ $obProp->getPropertyId() ] ) )
            {
                $strVal = $arResult['FORM_RESULT']['ORDER_PROPS'][ $obProp->getPropertyId() ];
            }
            elseif( !empty( $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $obProp->getPropertyId() ] ) )
            {
                $strVal = $arResult['FORM_RESULT']['DELIVERY_PROPS'][ $obProp->getPropertyId() ];
            }

            $obProp->setValue( $strVal );
            break;
    }
}


$obBasket = $obOrder->getBasket();

foreach ($obBasket->getBasketItems() as $obBasketItem)
{
    $arResult['YA_EC'][] = [
        'id' => $obBasketItem->getProductId(),
        'name' => $obBasketItem->getProductId(),
        'price' => $obBasketItem->getPrice(),
        'quantity' => $obBasketItem->getQuantity()
    ];
}


$res = $obOrder->save();
if( $res->isSuccess() )
{
    unset($_SESSION['WBL_FORM_RESULT']);
    $arResult['ORDER_ID'] = $res->getId();


    $user = new CUser;
    $user->Update(\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getPost('intUserID'), ['PERSONAL_NOTES' => $obPropsColl->getPayerName()->getValue()]);

}
else
{
    unset($arResult['YA_EC']);
    $arResult['ORDER_CREATE_ERROR'] = implode('; ', $res->getErrorMessages() );
}

echo json_encode($arResult);
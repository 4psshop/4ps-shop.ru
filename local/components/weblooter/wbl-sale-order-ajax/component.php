<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

\Bitrix\Main\Loader::includeModule('sale');
\Bitrix\Main\Loader::includeModule('catalog');

//unset($_SESSION['WBL_FORM_RESULT']);


$arCoupons = \Bitrix\Sale\DiscountCouponsManager::get();

$arResult = array();
try{

    if( !check_bitrix_sessid() && !empty($_REQUEST['FORM_RESULT']) )
        throw new \Exception('Во время проверки сессии была обнаружена ошибка');

    if( !empty($_REQUEST['BACK_TO_STEP']) ){
        switch ($_REQUEST['BACK_TO_STEP']){
            case '1':
                global $_REQUEST;
                $_REQUEST['FORM_RESULT']['DELIVERY'] = '';
                unset($_REQUEST['FORM_RESULT']['DELIVERY_PROPS']);
                unset($_SESSION['WBL_FORM_RESULT_DELIVERY_PROPS']);
                $_REQUEST['FORM_RESULT']['PAYMENT'] = '';
                $_REQUEST['STEP'] = 1;
                break;
            case '2':
                global $_REQUEST;
                $_REQUEST['FORM_RESULT']['PAYMENT'] = '';
                $_REQUEST['STEP'] = 2;
                break;
        }
    }
    else
    {
        unset($_SESSION['WBL_FORM_RESULT']);
    }

    /** Получим корзину */
    $dbBasketItems = CSaleBasket::GetList(Array("NAME" => "ASC", "ID" => "ASC"), Array("FUSER_ID" => CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "DELAY" => "N", "CAN_BUY" => "Y"), false, false, Array("ID", "CALLBACK_FUNC", "MODULE", "PRODUCT_ID", "QUANTITY", "PRODUCT_PROVIDER_CLASS", 'PRICE', 'DISCOUNT_PRICE'));

    while ($arItems = $dbBasketItems->Fetch()) {
        if ('' != $arItems['PRODUCT_PROVIDER_CLASS'] || '' != $arItems["CALLBACK_FUNC"]) {
            $arResult['BASKET_ITEMS'][ $arItems["ID"] ] = $arItems;
        }
    }


    if (!empty($arResult['BASKET_ITEMS'])) {

        $arOrder = array(
            'SITE_ID' => SITE_ID,
            'USER_ID' => $GLOBALS['USER']->GetID(),
            'BASKET_ITEMS' => $arResult['BASKET_ITEMS']
        );
        $arOptions = array();
        $arErrors = array();
        CSaleDiscount::DoProcessOrder($arOrder, $arOptions, $arErrors);

        $arResult['BASKET_ITEMS'] = $arOrder['BASKET_ITEMS'];
    }

    if( !empty($arResult['BASKET_ITEMS']) ){
        foreach ($arResult['BASKET_ITEMS'] as $arItem){
            $arResult['TOTAL']['ITEMS'] += $arItem['QUANTITY'];
            $arResult['TOTAL']['BASE_AMOUNT'] += $arItem['BASE_PRICE']*$arItem['QUANTITY'];
            $arResult['TOTAL']['AMOUNT'] += $arItem['PRICE']*$arItem['QUANTITY'];
            $arResult['TOTAL']['DISCOUNT_AMOUNT'] += $arItem['DISCOUNT_PRICE']*$arItem['QUANTITY'];
        }
    }

    if( empty($arResult['BASKET_ITEMS']) )
        throw new \Exception('Ваша корзина пуста!');
    /** / Получим корзину */


    /* Подтаскиваем данные из сессии */
    if( empty($_REQUEST['FORM_RESULT']) && !empty($_SESSION['WBL_FORM_RESULT']['FORM_RESULT']) ){
        global $_REQUEST;
        $_REQUEST['FORM_RESULT'] = $_SESSION['WBL_FORM_RESULT']['FORM_RESULT'];
        $_REQUEST['STEP'] = $_SESSION['WBL_FORM_RESULT']['STEP'];
    }
    /* / Подтаскиваем данные из сессии */

    /* Получаем входные значения */
    $arResult['FORM_RESULT'] = $_REQUEST['FORM_RESULT'];
    $arResult['STEP'] = $_REQUEST['STEP'];


    /** Сверим что поменялось и внесем изменения */
    if( $_SESSION['WBL_FORM_RESULT']['FORM_RESULT']['DELIVERY'] != $arResult['FORM_RESULT']['DELIVERY'] ){
        unset($arResult['FORM_RESULT']['DELIVERY_PROPS']);
    }
    /** / Сверим что поменялось и внесем изменения */

    if(
        !empty($_REQUEST['FORM_RESULT']['DELIVERY_PROPS'])
        && (
            $_REQUEST['STEP'] == 3
            || $_REQUEST['STEP'] == 'make_order'
        )
    )
    {
        $arResult['FORM_RESULT']['DELIVERY_PROPS'] = $_REQUEST['FORM_RESULT']['DELIVERY_PROPS'];
    }

    $_SESSION['WBL_FORM_RESULT']['FORM_RESULT'] = $arResult['FORM_RESULT'];
    $_SESSION['WBL_FORM_RESULT']['STEP'] = $arResult['STEP'];
    /* / Получаем входные значения */



    // Ввод данных

    /** ШАГ 1 */
    /* Получим список типов плательщиков */
    $rsPersonTypes = CSalePersonType::GetList(Array("SORT" => "ASC"), Array("LID"=>SITE_ID));
    if( $rsPersonTypes->SelectedRowsCount() < 1 )
        throw new \Exception('Список типов плательщиков пуст');

    while ($arItem = $rsPersonTypes->Fetch()){
        $arResult['FORM_DATA']['PERSON_TYPES'][ $arItem['ID'] ] = array('ID'=>$arItem['ID'], 'NAME'=>$arItem['NAME']);
    }
    /* / Получим список типов плательщиков */

    /* Получение списка св-в заказа */
    $intLocationPropID = 0;
    if( !empty($arResult['FORM_RESULT']['PERSON_TYPE']) ){
        /* Получим группу с контактной информацией */
        $intPropGroup = 0;
        $rsPropsGroup = CSaleOrderPropsGroup::GetList(array('SORT'=>'ASC'), array('SORT'=>10, 'PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'ACTIVE'=>'Y'));
        if( $rsPropsGroup->SelectedRowsCount() < 0 )
            throw new \Exception('Нет группы "Контактная информация" у данного типа плательщика. Она должна быть помечена как сортировка 10');
        $arTmp = $rsPropsGroup->Fetch();
        $intPropGroup = $arTmp['ID'];
        $arResult['FORM_DATA']['ORDER_GROUP'] = $arTmp;
        unset($arTmp);
        /* / Получим группу с контактной информацией */

        $rsOrderProps = CSaleOrderProps::GetList(
            array('SORT'=>'ASC'),
            array('PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'PROPS_GROUP_ID'=>$intPropGroup, 'ACTIVE'=>'Y', 'UTIL'=>'N')
        );
        while ($arItem = $rsOrderProps->Fetch()){
            $arResult['FORM_DATA']['ORDER_PROPS'][ $arItem['ID'] ] = $arItem;
            if( $arItem['TYPE'] == 'LOCATION' )
                $intLocationPropID = $arItem['ID'];

            if( $arItem['IS_EMAIL'] == 'Y' )
                $arResult['FORM_DATA']['USER_PROFILE_ID']['EMAIL'] = $arItem['ID'];
        }

        if( $intLocationPropID > 0 && empty($_REQUEST['FORM_RESULT']['ORDER_PROPS'][$intLocationPropID]) )
            $arResult['STEP'] = 1;
    }
    /* / Получение списка св-в заказа */
    /** / ШАГ 1 */

    /** ШАГ 2 */
    /* Список способов доставки */
    $boolSelectedPickup = false;
    if( $arResult['FORM_RESULT']['ORDER_PROPS'][$intLocationPropID] > 0 ){

        // Настраиваемые
        $rsDeliveryList = CSaleDelivery::GetList( array('SORT'=>'ASC'), array('LID'=>SITE_ID, 'ACTIVE'=>'Y', 'LOCATION'=>$arResult['FORM_RESULT']['ORDER_PROPS'][$intLocationPropID]) );
        while ($arItem = $rsDeliveryList->Fetch()){
            $arResult['FORM_DATA']['DELIVERY'][ $arItem['ID'] ] = $arItem;

            if( $arResult['FORM_RESULT']['DELIVERY'] == $arItem['ID'] )
                $arResult['TOTAL']['DELIVERY'] = $arItem['PRICE'];

            // Проверка на самовывоз. Самовывоз помечается сортировкой 20
            if( $arResult['FORM_RESULT']['DELIVERY'] == $arItem['ID'] && $arItem['SORT'] != 20 ){

                // Не самовывоз. Подтащим соответствующие св-ва заказа
                $rsPropsGroup = CSaleOrderPropsGroup::GetList(array('SORT'=>'ASC'), array('SORT'=>20, 'PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'ACTIVE'=>'Y'));
                if( $rsPropsGroup->SelectedRowsCount() < 0 )
                    throw new \Exception('Нет группы "Данные о доставке" у данного типа плательщика, хотя выбран самовывоз. Она должна быть помечена как сортировка 20');
                $arTmp = $rsPropsGroup->Fetch();
                $arResult['FORM_DATA']['DELIVERY_GROUP'] = $arTmp;
                unset($arTmp);

                $rsOrderProps = CSaleOrderProps::GetList(
                    array('SORT'=>'ASC'),
                    array('PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'PROPS_GROUP_ID'=>$arResult['FORM_DATA']['DELIVERY_GROUP']['ID'], 'ACTIVE'=>'Y', 'UTIL'=>'N')
                );
                while ($arItem = $rsOrderProps->Fetch()){
                    $arResult['FORM_DATA']['DELIVERY_PROPS'][ $arItem['ID'] ] = $arItem;
                }
            }
        }

        // Автоматизированные
        $arPreOrder = array(
            "WEIGHT" => "0", // вес заказа в граммах
            "PRICE" => $arResult['TOTAL']['AMOUNT'], // стоимость заказа в базовой валюте магазина
            "LOCATION_FROM" => COption::GetOptionInt('sale', 'location'), // местоположение магазина
            "LOCATION_TO" => $arResult['FORM_RESULT']['ORDER_PROPS'][ $intLocationPropID ], // местоположение доставки
        );

        $rsAutoDelivery = CSaleDeliveryHandler::GetList(
            array('NAME'=>'ASC'),
            array(
                'ACTIVE' => 'Y',
                'COMPABILITY' => $arPreOrder
            )
        );
        while ($arAutoDelivery = $rsAutoDelivery->Fetch()){

            $rsHandler = CSaleDeliveryHandler::GetBySID($arAutoDelivery['SID'] );
            while ($arHandler = $rsHandler->Fetch()){
                $arProfiles = CSaleDeliveryHandler::GetHandlerCompability($arPreOrder, $arHandler);

                if( !empty($arProfiles) ){
                    foreach ($arProfiles as $keysProfile=>$arProfile){

                        $arPrice = CSaleDeliveryHandler::CalculateFull(
                            $arAutoDelivery['SID'],
                            $keysProfile,
                            $arPreOrder,
                            'RUB'
                        );

                        $arResult['FORM_DATA']['DELIVERY'][ $arHandler['SID'].':'.$keysProfile ] = array(
                            'ID' => $arHandler['SID'].':'.$keysProfile,
                            'NAME' => $arHandler['NAME'].' ('.$arProfile['TITLE'].')',
                            'DESCRIPTION' => $arProfile['DESCRIPTION'],
                            'LOGOTIP' => $arHandler['LOGOTIP'],
                            'PRICE' => $arPrice['VALUE'],
                            'TRANSIT' => $arPrice['TRANSIT']
                        );

                        if( $arResult['FORM_RESULT']['DELIVERY'] == $arHandler['SID'].':'.$keysProfile ){
                            // Не самовывоз. Подтащим соответствующие св-ва заказа
                            $arResult['TOTAL']['DELIVERY'] = $arPrice['VALUE'];

                            $rsPropsGroup = CSaleOrderPropsGroup::GetList(array('SORT'=>'ASC'), array('SORT'=>20, 'PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'ACTIVE'=>'Y'));
                            if( $rsPropsGroup->SelectedRowsCount() < 0 )
                                throw new \Exception('Нет группы "Данные о доставке" у данного типа плательщика, хотя выбран самовывоз. Она должна быть помечена как сортировка 20');
                            $arTmp = $rsPropsGroup->Fetch();
                            $arResult['FORM_DATA']['DELIVERY_GROUP'] = $arTmp;
                            unset($arTmp);

                            $rsOrderProps = CSaleOrderProps::GetList(
                                array('SORT'=>'ASC'),
                                array('PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE'], 'PROPS_GROUP_ID'=>$arResult['FORM_DATA']['DELIVERY_GROUP']['ID'], 'ACTIVE'=>'Y', 'UTIL'=>'N')
                            );
                            while ($arItem = $rsOrderProps->Fetch()){
                                $arResult['FORM_DATA']['DELIVERY_PROPS'][ $arItem['ID'] ] = $arItem;
                            }
                        }

                    }

                }
            }
        }

        if( empty($arResult['FORM_DATA']['DELIVERY']) )
            throw new \Exception('Нет ни одного способа доставки');

    }
    /* / Список способов доставки */
    /** / ШАГ 2 */

    /** ШАГ 3 */
    if( !empty($arResult['FORM_RESULT']['DELIVERY']) ){

        $rsPaySystem = CSalePaySystem::GetList(array('SORT'=>'ASC'), array('ACTIVE'=>'Y', "CURRENCY"=>"RUB", 'PERSON_TYPE_ID'=>$arResult['FORM_RESULT']['PERSON_TYPE']));
        if( $rsPaySystem->SelectedRowsCount() < 1 )
            throw new \Exception('Нет ни одной платежной системы');
        while ($arItem = $rsPaySystem->Fetch()){
            $arResult['FORM_DATA']['PAYMENT'][ $arItem['ID'] ] = $arItem;
        }

        
        if( !empty($arResult['FORM_RESULT']['YADELIVERY']) && $arResult['FORM_RESULT']['DELIVERY'] == 70 )
        {
            $arYADelivery = explode('#|#', $arResult['FORM_RESULT']['YADELIVERY']);
            $arResult['TOTAL']['DELIVERY'] = $arYADelivery[0];
        }

    }
    /** / ШАГ 3 */

    if( $arResult['STEP'] == 'make_order' ){
        // Оформление заказа
        if(
            empty($arResult['FORM_RESULT']['DELIVERY'])
            || $arResult['FORM_RESULT']['PAYMENT'] < 1
            || $arResult['FORM_RESULT']['PERSON_TYPE'] < 1
            || empty( $arResult['FORM_RESULT']['ORDER_PROPS'] )
        ){
            $arResult['STEP'] = 3;
            throw new \Exception('Во время оформления заказа не переданны все данные');
        }


        $intUserId = $GLOBALS['USER']->GetID();
        if( $GLOBALS['USER']->GetID() < 1 ){
            $user = new CUser;
            $strPass = md5(rand(100000, 999999));
            $intUserId = $user->Add(
                array(
                    'EMAIL' => $arResult['FORM_RESULT']['ORDER_PROPS'][ $arResult['FORM_DATA']['USER_PROFILE_ID']['EMAIL'] ],
                    'LOGIN' => $arResult['FORM_RESULT']['ORDER_PROPS'][ $arResult['FORM_DATA']['USER_PROFILE_ID']['EMAIL'] ],
                    'PASSWORD' => $strPass,
                    'CONFIRM_PASSWORD' => $strPass,
                )
            );
            if( $intUserId > 0 )
                $GLOBALS['USER']->Authorize($intUserId);
        }

        $http = new \Bitrix\Main\Web\HttpClient();

        $arCreateOrderFields = [
            'result' => $arResult,
            'arCoupons' => \Bitrix\Sale\DiscountCouponsManager::get(),
            'intUserID' => $GLOBALS['USER']->getID(),
            'Fuser' => \Bitrix\Sale\Fuser::getId()
        ];

        $http->post('https://'.\Bitrix\Main\Application::getInstance()->getContext()->getRequest()->getHttpHost().'/local/components/weblooter/wbl-sale-order-ajax/createOrder.php', $arCreateOrderFields);

        if( $http->getStatus() !== 200 )
            throw new \Exception('Не удалось оформить заказ. Попробуйте позже');

        $arResult = json_decode($http->getResult(), true);

        if( $arResult['ORDER_ID'] > 0 )
        {
            unset($_SESSION['WBL_FORM_RESULT']);
        }
        else
        {
            unset($arResult['YA_EC']);
            throw new \Exception($arResult['ORDER_CREATE_ERROR']);
        }


    }

}
catch (\Exception $e){
    $arResult['LAST_ERROR'] = $e->getMessage();
}

if( empty($arResult['STEP']) )
    $arResult['STEP'] = 1;

$this->IncludeComponentTemplate();
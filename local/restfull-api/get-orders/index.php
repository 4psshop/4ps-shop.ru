<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");

try
{

    if(
        empty( \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('TOKEN') )
        || empty( \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('COUPON') )
    )
        throw new \Exception('Не передан код купона или токен');

    require \Bitrix\Main\Application::getDocumentRoot().'/local/api/wbl_coupons_and_tokens.php';



    $arAgents = \WblCouponsAndTokens::getInstance()->getAgents();
    if( empty( $arAgents ) )
        throw new \Exception('Список контрагентов пуст');



    $strAgentCode = '';
    foreach ($arAgents as $agentCode => $arAgent)
    {
        if( \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('TOKEN') == $arAgent['TOKEN'] )
        {
            $strAgentCode = $agentCode;
            break;
        }
    }

    if( $strAgentCode == '' )
        throw new \Exception('Не удалось найти контрагента по данному токену');



    $arCoupons = \WblCouponsAndTokens::getInstance()->getAgentCoupons($strAgentCode);
    if( !in_array( \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('COUPON') , $arCoupons ) )
        throw new \Exception('У данного конртагента нет данного промокода');



    $arOrders = [];
    $rsOrders = \Bitrix\Sale\Internals\OrderCouponsTable::getList(array(
        'select' => array('ORDER_ID'),
        'filter' => array('=COUPON' => \Bitrix\Main\Application::getInstance()->getContext()->getRequest()->get('COUPON'))
    ));
    while ($coupon = $rsOrders->fetch())
    {
        $arOrders[ $coupon['ORDER_ID'] ] = $coupon['ORDER_ID'];
    }

    $rsOrders = \Bitrix\Sale\Order::getList([
        'order' => ['ID'=>'DESC'],
        'filter' => ['ID' => $arOrders]
    ]);
    while ($arOrder = $rsOrders->fetch())
    {
        $arReturnFields = [];
        $arReturnFields['ORDER_ID'] = $arOrder['ID'];
        $arReturnFields['DATE_CREATE'] = \Bitrix\Main\Type\DateTime::convertFormatToPhp($arOrder['DATE_INSERT']);
        $arReturnFields['CANCELED'] = $arOrder['CANCELED'];

        $rsProps = \Bitrix\Sale\PropertyValue::getList([
            'filter' => [
                'ORDER_ID'=>$arOrder['ID'],
                'CODE' => [
                    'NAME',
                    'PHONE',
                    'EMAIL',
                    'LOCATION',
                ]
            ]
        ]);
        while ($arProp = $rsProps->fetch())
        {
            if( $arProp['CODE'] == 'LOCATION' )
            {
                $arProp['VALUE'] = Bitrix\Sale\Location\Admin\LocationHelper::getLocationPathDisplay( $arProp['VALUE'] );
            }
            $arReturnFields['PROPERTIES'][ $arProp['CODE'] ] = $arProp['VALUE'];
        }

        $rsBasket = \Bitrix\Sale\Basket::getList([
            'filter' => [
                'ORDER_ID' => $arOrder['ID']
            ]
        ]);
        while ($arBasket = $rsBasket->fetch())
        {
            $arReturnFields['BASKET'][] = [
                'NAME' => $arBasket['NAME'],
                'PRICE_PER_ITEM' => round($arBasket['PRICE'], 0),
                'QUANTITY' => round($arBasket['QUANTITY'], 0)
            ];
        }

        $arOrders[ $arOrder['ID'] ] = $arReturnFields;
    }

    echo json_encode(['status'=>'success', 'orders'=>$arOrders]);


}
catch (\Exception $e)
{
    echo json_encode(['status'=>'error', 'text'=>$e->getMessage()]);
}



require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
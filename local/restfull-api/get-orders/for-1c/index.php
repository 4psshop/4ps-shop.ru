<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("main");
CModule::IncludeModule("iblock");
CModule::IncludeModule("sale");
CModule::IncludeModule("catalog");


$funGetProductIDToArticle = function ()
{
    $arProductIDToArticle = [];
    $rsIBlockProds = CIBlockElement::GetList([], ['IBLOCK_ID'=>2], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_ARTICUL']);
    while ($ar = $rsIBlockProds->Fetch())
    {
        $arProductIDToArticle[ $ar['ID'] ] = $ar['PROPERTY_ARTICUL_VALUE'];
    }

    $rsIBlockOffers = CIBlockElement::GetList([], ['IBLOCK_ID'=>15], false, false, ['ID', 'IBLOCK_ID', 'PROPERTY_CML2_LINK']);
    while ($ar = $rsIBlockOffers->Fetch())
    {
        $arProductIDToArticle[ $ar['ID'] ] = $arProductIDToArticle[ $ar['PROPERTY_CML2_LINK_VALUE'] ];
    }
    return $arProductIDToArticle;
};
$arProductIDToArticle = $funGetProductIDToArticle();






$obXML = new \SimpleXMLElement('<orders></orders>');

$rs = \Bitrix\Sale\OrderTable::getList([
    'filter' => [
        '<=DATE_INSERT' => \Bitrix\Main\Type\DateTime::createFromTimestamp( strtotime('tomorrow -1 seconds') ),
        '>=DATE_INSERT' => \Bitrix\Main\Type\DateTime::createFromTimestamp( strtotime('-31 days') ),
    ],
    'order' => ['ID' => 'DESC'],
    'select' => ['ID']
]);
while ($arOrder = $rs->fetch())
{
    $obOrder = \Bitrix\Sale\Order::load($arOrder['ID']);
    $obXMLOrder = $obXML->addChild('order');

    $obXMLOrder->addChild('id', $obOrder->getId() );
    $obXMLOrder->addAttribute('externalId', $obOrder->getId());

    $obStatusCache = \Bitrix\Main\Application::getInstance()->getCache();
    if( $obStatusCache->startDataCache(60*5, __FILE__.__LINE__) )
    {
        $arStatuses = [];
        $rsStatuses = \Bitrix\Sale\StatusLangTable::getList([
            'filter' => ['LID' => 'ru']
        ]);
        if( $rsStatuses->getSelectedRowsCount() < 1 )
            $obStatusCache->abortDataCache();
        while ($ar = $rsStatuses->fetch())
        {
            $arStatuses[ $ar['STATUS_ID'] ] = $ar;
        }
        $obStatusCache->endDataCache($arStatuses);
    }
    else
    {
        $arStatuses = $obStatusCache->getVars();
    }
    $tmp = $obXMLOrder->addChild('status', $arStatuses[ $obOrder->getField('STATUS_ID') ]['NAME'] );
    $tmp->addAttribute('externalId', $obOrder->getField('STATUS_ID') );


    /**
     * Получим и запишем тип плательщика. Кэшируем запись getList()
     */
    $obPersonCache = \Bitrix\Main\Application::getInstance()->getCache();
    if( $obPersonCache->startDataCache(60*5, __FILE__.__LINE__) )
    {
        $arPersonTypes = [];
        $rsPersonTypes = \Bitrix\Sale\PersonTypeTable::getList([]);
        if( $rsPersonTypes->getSelectedRowsCount() < 1 )
            $obPersonCache->abortDataCache();
        while ($ar = $rsPersonTypes->fetch())
        {
            $arPersonTypes[ $ar['ID'] ] = $ar;
        }
        $obPersonCache->endDataCache($arPersonTypes);
    }
    else
    {
        $arPersonTypes = $obPersonCache->getVars();
    }

    $tmp = $obXMLPerson = $obXMLOrder->addChild('personType');
    $tmp->addChild('name', htmlspecialcharsbx( $arPersonTypes[ $obOrder->getPersonTypeId() ]['NAME'] ) );
    $obXMLPerson->addAttribute('externalId', $obOrder->getPersonTypeId() );




    /**
     * Получим способ платежа. Кэшируем запись getList()
     *
     * @var \Bitrix\Sale\PaymentCollection $obPaymentColl
     * @var \Bitrix\Sale\Payment $obPayment
     */
    $obPaymentCache = \Bitrix\Main\Application::getInstance()->getCache();
    if( $obPaymentCache->startDataCache(60*5, __FILE__.__LINE__) )
    {
        $arPayments = [];
        $rsPayments = \Bitrix\Sale\PaySystem\Manager::getList([]);
        if( $rsPayments->getSelectedRowsCount() < 1 )
            $obPaymentCache->abortDataCache();
        while ($ar = $rsPayments->fetch())
        {
            $arPayments[ $ar['ID'] ] = $ar;
        }
        $obPaymentCache->endDataCache($arPayments);
    }
    else
    {
        $arPayments = $obPaymentCache->getVars();
    }

    $obPaymentColl = $obOrder->getPaymentCollection();
    foreach ($obPaymentColl as $obPayment)
    {
        $obXMLPayment = $obXMLOrder->addChild('payment');

        $obXMLPayment->addAttribute('externalId', $obPayment->getPaymentSystemId());
        $obXMLPayment->addChild('id', (string)$obPayment->getId() );
        $obXMLPayment->addChild('name', htmlspecialcharsbx( $arPayments[ $obPayment->getPaymentSystemId() ]['NAME'] ) );
        $obXMLPayment->addChild('paid', ( $obPayment->isPaid() ? 'true' : 'false' ) );
        $obXMLPayment->addChild('paidDate', \Bitrix\Main\Type\DateTime::convertFormatToPhp( $obPayment->getField('DATE_PAID') ) );
    }




    /**
     * Получим и запишем способ доставки
     *
     * @var \Bitrix\Sale\Shipment $obShipment
     * @var \Bitrix\Sale\ShipmentItemCollection $obShipmentItemColl
     * @var \Bitrix\Sale\ShipmentItem $obShipmentItem
     */
    $obShipmentColl = $obOrder->getShipmentCollection();
    $obXMLShipments = $obXMLOrder->addChild('shipments');
    foreach ($obShipmentColl as $obShipment)
    {
        if( $obShipment->isSystem() )
            continue;

        $obXMLShipment = $obXMLShipments->addChild('shipment');
        $obXMLShipment->addAttribute('externalId', $obShipment->getDeliveryId() );
        $obXMLShipment->addChild('name', $obShipment->getDeliveryName() );
        $obXMLShipment->addChild('price', $obShipment->getPrice() );

        $obShipmentItemColl = $obShipment->getShipmentItemCollection();
        $obXMLBasketItemsInShipment = $obXMLShipment->addChild('basketItemsInShipment');
        foreach ($obShipmentItemColl as $obShipmentItem)
        {
            $obBasketItem = $obShipmentItem->getBasketItem();
            $tmp = $obXMLBasketItemsInShipment->addChild('item');
            $tmp->addAttribute('externalId', $obBasketItem->getProductId());
            $tmp->addChild('name', htmlspecialcharsbx( $obBasketItem->getField('NAME') ));
            $tmp->addChild('quantity', round( $obBasketItem->getField('QUANTITY'), 2 ) );
        }
    }




    /**
     * Получим и запишем корзину
     *
     * @var \Bitrix\Sale\Basket $obBasket
     * @var \Bitrix\Sale\BasketItem $obBasketItem
     */
    $obBasket = $obOrder->getBasket();
    $obXMLBasket = $obXMLOrder->addChild('basket');
    foreach ( $obBasket->getBasketItems() as $obBasketItem )
    {
        $obXMLBasketItem = $obXMLBasket->addChild('item');
        $obXMLBasketItem->addAttribute('externalId', $obBasketItem->getField('PRODUCT_ID'));

        $obXMLBasketItem->addChild('name', htmlspecialcharsbx( $obBasketItem->getField('NAME') ) );
        $obXMLBasketItem->addChild('quantity', round( $obBasketItem->getField('QUANTITY'), 2 ) );
        $obXMLBasketItem->addChild('pricePerItem', round( $obBasketItem->getField('PRICE'), 2 ) );

        $obXMLBasketItem->addAttribute('externalArticle', $arProductIDToArticle[ $obBasketItem->getField('PRODUCT_ID') ]);
    }




    /**
     * Получим значения св-в заказа. Кэшируем запись getList()
     *
     * @var \Bitrix\Sale\PropertyValueCollection $obPropsColl
     * @var \Bitrix\Sale\PropertyValue $obProp
     */
    $obPropsColl = $obOrder->getPropertyCollection();
    $obXMLOrderProperties = $obXMLOrder->addChild('orderProperties');
    foreach ($obPropsColl as $obProp)
    {
        $tmp = $obXMLOrderProperties->addChild('property');
        $tmp->addAttribute( 'externalId', $obProp->getProperty()['ID'] );
        $tmp->addChild('name', $obProp->getField('NAME'));

        if( $obProp->getProperty()['IS_LOCATION'] == 'Y' )
        {
            $tmp->addChild('value', htmlspecialcharsbx( \Bitrix\Sale\Location\Admin\LocationHelper::getLocationPathDisplay($obProp->getValue()) ) );
        }
        else
        {
            $tmp->addChild('value', htmlspecialcharsbx( $obProp->getValue() ) );
        }

    }

}


Header('Content-type: text/xml');
echo $obXML->asXML();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Защита информации");
$APPLICATION->SetPageProperty("description", "Политика использования cookie и обработки персональных данных.");
$APPLICATION->SetTitle("Защита информации");
?><p>
 <b>ОБЩИЕ УКАЗАНИЯ ОБ ОБРАБОТКЕ ДАННЫХ<br>
 </b><b>Сбор данных при обращении к страницам интернет-магазина 4ПРО СПОРТ, расположенного в сети Интернет по адресу </b><a href="https://4ps-shop.ru"><b>https://4ps-shop.ru</b></a><b>, включая его мобильную версию (далее: сайт).&nbsp;</b>
</p>
<p>
 <br>
</p>
<p>
	 Когда Вы заходите на наш сайт, мы регистрируем данные об IP-адресе, веб-странице, с которой был произведен запрос файла, названии файла, дате и времени запроса, переданном объеме данных и сообщении об успешности обращения к данным (веб-журнал). Благодаря этому мы производим оптимизацию страниц сайта. Но мы не связываем эти данные с Вами.
</p>
<p>
 <b><br>
 </b>
</p>
<p>
 <b>Куки (cookie)<br>
 </b>Страницы нашего сайта используют куки. Вы можете выключить их сохранение в Вашем браузере. Однако тогда Вы, возможно, не сможете использовать все функции нашего сайта.
</p>
<p>
 <br>
</p>
<p>
 <b>Внешние ссылки</b><br>
	 Страницы нашего сайта содержат ссылки на страницы внешних поставщиков (третьих сторон). Наше заявление о защите данных распространяется только на наш сайт и его страницы. Обратите внимание на заявления внешних поставщиков о защите данных.
</p>
<p>
 <br>
</p>
<p>
 <b>Трансграничная передача</b><br>
	 Ваши персональные данные могут быть переданы, обработаны и храниться в любой стране, где расположены или где ведут свою деятельность наши компании-партнеры и поставщики товаров, их дочерние компании, службы доставки товара, и вы понимаете, что ваши данные могут быть переданы в страны за пределами вашей страны проживания, за пределы Европейского экономического пространства, включая США, где могут действовать правила о защите данных, отличающиеся от правил вашей страны. В определенных обстоятельствах суды, правоохранительные структуры, контролирующие органы или органы безопасности в этих других странах могут иметь право получить доступ к вашим персональным данным.
</p>
<p>
 <br>
</p>
<p>
 <b>Google Analytics</b><br>
	 Для анализа использования нашей веб-страницы мы применяем Google Analytics от Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA. Ваш IP-адрес внутри Европейского экономического пространства укорачивается перед передачей в Google, США. Полный IP-адрес передается только в исключительных случаях и укорачивается уже там.
</p>
<p>
	 Google заверяет, что IP-адрес, переданный Вашим браузером в рамках Google Analytics, не будет связан с другими данными Google, имеющими отношение к личности пользователей.
</p>
<p>
	 Google Analytics использует 'Cookies', выполняющие следующие функции:
</p>
<p>
</p>
- определение измеряемого домена;<br>
- распознавание уникальных пользователей;<br>
- сохранение информации о количестве и продолжительности предыдущих посещений;<br>
- сохранение информации об источнике трафика;<br>
- определение начала и конца сеанса;<br>
- сохранение значений пользовательских переменных на уровне посетителей.<br>
<ul>
</ul>
<p>
</p>
<p>
	 Google хранит и использует такую информацию на протяжении промежутка времени от 30 минут до 2 лет в зависимости от типа файла «cookie». Политика конфиденциальности Google доступна по адресу: <a href="https://www.google.com/policies/privacy/">https://www.google.com/policies/privacy/</a>.
</p>
<p>
	 Вы можете предотвратить обработку в Google собранных так данных. Для этого загрузите имеющееся расширение браузера и установите его.
</p>
<p>
 <br>
</p>
<p>
 <b>Yandex</b><br>
	 Наш сайт использует средства Yandex.Метрики для анализа статистики использования нашего сайта. Яндекс.Метрика генерирует статистическую и прочую информацию об использовании нашего сайта посредством файлов «cookie», хранящихся на компьютерах пользователей. Информация, сгенерированная в отношении нашего сайта, используется при создании отчетов об использовании сайта.
</p>
<p>
	 Yandex хранит и использует такую информацию на протяжении промежутка времени, установленным внутренними регламентами сервисов, а также законодательством Российской Федерации в зависимости от типа файла «cookie».
</p>
<p>
	 Политика конфиденциальности Яндекс доступна по адресу: <a href="https://yandex.ru/legal/confidential/">https://yandex.ru/legal/confidential/</a>.
</p>
<p>
 <br>
</p>
<p>
	 Использование под псевдонимом или анонимное (неопознанное) использование
</p>
<p>
	 "Помощь" Вашего браузера подскажет, как предотвратить или усложнить отслеживание ваших действий в сети третьими лицами. Ищите там понятие „tracking“ (отслеживание).
</p>
<p>
 <br>
</p>
<p>
	 В полях ввода данных нашей веб-страницы можно указать не Ваши собственные, а другие личные данные („псевдонимы“). Если Вы вводите данные других лиц, не получив от них разрешения, Вы нарушаете их право на защиту данных. Далее, при неправильно введенных данных мы не можем выполнить некоторые требуемые действия (например, претензии по гарантии, ответный е-mail).
</p>
<p>
 <br>
</p>
<p>
 <b>Социальные сети</b><br>
	 На страницах нашего сайта мы использует плагины следующих социальных сетей:
</p>
<p>
 <br>
</p>
<p>
 <i>Facebook/Instagram</i>
</p>
<p>
	 Facebook Inc., 1601 S. California Ave, Palo Alto, CA 94304, USA
</p>
<p>
 <br>
</p>
<p>
 <i>YouTube/Google</i>
</p>
<p>
	 Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA
</p>
<p>
 <br>
</p>
<p>
	 Компании, которые эксплуатируют социальные сети, сохраняют эту информацию, как правило, на своих серверах вне Европейского экономического пространства.
</p>
<p>
	 Цель и объем сбора данных и их дальнейшая обработка и использование владельцами социальных сетей, а также их права в этой сфере и возможности настроек для защиты Вашего личного пространства представлены в указаниях о защите данных соответствующих сетей.
</p>
<p>
 <br>
</p>
<p>
 <b>Защита Ваших персональных данных</b><br>
	 Мы внедрили разумные технические, административные и физические меры, направленные на защиту хранимых нами Ваших персональных данных или контроль от несанкционированного доступа, использования, изменения и раскрытия.
</p>
<p>
	 Мы разработали сайт для приема заказов исключительно из браузеров, разрешающих передачу информации через протокол SSL (протокол безопасных соединений), который зашифровывает ваши данные во время их передачи нам. В браузерах, не соответствующих этому требованию, нельзя будет переходить в разделы сайта.
</p>
<p>
 <b><span style="color: #ee1d24;">Вы должны знать, что обеспечить абсолютную безопасность при передаче или хранении данных не всегда возможно.</span></b>
</p>
<p>
 <br>
</p>
<p>
 <b>Сторонние платежные службы</b><br>
	 На нашем сайте мы используем стороннюю платежную службу для обработки платежей. Если Вы хотите совершить платеж, Ваши персональные данные будет собирать соответствующая третья сторона, а не наш сайт и наша компания, и такой сбор данных будет регулироваться политикой конфиденциальности третьей стороны, а не настоящей политикой конфиденциальности. Мы не контролируем сбор, использование и разглашение Ваших персональных данных этой третьей стороной и не несем за это никакой ответственности.
</p>
<p>
 <br>
</p>
<p>
 <b>Информационный бюллетень</b><br>
	 Вы можете зарегистрироваться через наш сайт для получения информационного бюллетеня 4ПРО СПОРТ. Если вы зарегистрировались для рассылки, вы получите от нас интересную информацию, предложения и новинки продукции наших партнеров.
</p>
<p>
 <br>
</p>
<p>
 <b>Информация для европейских пользователей согл. регламенту ЕС № 524/2013</b><br>
	 Европейская комиссия предоставляет платформу для урегулирования споров в режиме онлайн. Платформа находится по адресу: <a href="http://ec.europa.eu/consumers/odr/">http://ec.europa.eu/consumers/odr/</a>.
</p>
<p>
 <br>
</p>
<p>
 <b>Законодательство:</b>
</p>
<p>
 <i>Федеральный закон РФ от 27 июля 2006 года № 152-ФЗ «О персональных данных»<br>
 </i>Федеральный закон, регулирующий деятельность по обработке (использованию) персональных данных.
</p>
<p>
 <br>
</p>
<p>
 <i>Общий регламент защиты персональных данных, Генеральный регламент о защите персональных данных (англ. General Data Protection Regulation, GDPR; Постановление (Европейский Союз) 2016/679)</i>
</p>
<p>
	 GDPR направлен прежде всего на то, чтобы дать гражданам контроль над собственными персональными данными, и на упрощение нормативной базы для международных экономических отношений.
</p>
<p>
 <br>
</p>
<p>
 <i>Ключевые принципы GDPR:</i>
</p>
<p>
</p>
- Законность, справедливость и прозрачность - должны быть легальные основания в рамках GDPR для сбора и использования данных, ненарушение любых законов, открытость, честность от начала и до конца об использовании персональных данных;<br>
- Ограничение целью - обработка должна сводиться к тому, что было заявлено субъекту данных. Все конкретные задачи должны быть закреплены в политике приватности и должны чётко соблюдаться;<br>
- Минимизация данных - использование адекватного количества данных для выполнения поставленных целей ограниченных только необходимым количеством;<br>
- Точность - персональные данные должны быть точными и не должны вводить в заблуждение; исправление неправильных;<br>
- Ограничение хранения данных - не хранить данные дольше чем нужно, периодически проводить аудит данных и удалять неиспользуемые;<br>
- Целостность и конфиденциальность/безопасность - хранить данные в безопасном месте и уделять достаточное внимание сохранности данных;<br>
- Подотчeтность - ответственность за обработку персональных данных и выполнение всех остальных принципов GDPR включая записи о конфиденциальности; защите, использовании, проверки данных; назначении должностного лица по защите данных (англ. DPO, data protection officer).<br>
<ul>
</ul>
<p>
</p>
<p>
 <br>
</p>
<p>
 <b>Администратор политики конфиденциальности:</b>
</p>
<p>
	 ООО «Здоровье и Спорт»<br>
	 127247, Россия, г. Москва, Дмитровское ш. 100/2 БЦ Норд Хаус, оф. 2112<br>
	 Телефон: +7 499 649 49 84<br>
	 E-mail: <a href="mailto:info@4prosport.ru">info@4prosport.ru</a>
</p>
<p>
 <br>
</p>
<p>
 <b>Администратор политики конфиденциальности на территории Европейского экономического пространства:<br>
 </b>Ideal Sports Network s.r.o.<br>
	 Příkop 843/4, 602 00, Brno, Česká republika<br>
	 Телефон: +420 723 846 882<br>
	 E-mail: <a href="mailto:info@idealscout.com">info@idealscout.com</a>
</p>
 <br>
ДАТА ПОСЛЕДНЕГО ОБНОВЛЕНИЯ: 27 декабря 2019 г.<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
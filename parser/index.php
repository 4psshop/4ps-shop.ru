<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(CModule::IncludeModule("iblock"))
 if(CModule::IncludeModule("catalog")) 
?>
<?
global $USER;
if ($USER->IsAdmin()){
	# подключаем библиотеку
	require_once "$_SERVER[DOCUMENT_ROOT]/parser/PHPExcel.php";
	# Указываем путь до файла .xlsx
	$File = "$_SERVER[DOCUMENT_ROOT]/upload/xls/main.xlsx";
	$Excel = PHPExcel_IOFactory::load($File);
	# С какой строки начинаются данные
	$Start = 3;
	$Res = array();
	for ($i= $Start; $i <= 1000; $i++)
	{
	    $Row = new stdClass();
	    $Row->articul = $Excel->getActiveSheet()->getCell('A'.$i )->getValue();
	    $Row->name = $Excel->getActiveSheet()->getCell('B'.$i )->getValue(); 	
	    $Row->name_eng = $Excel->getActiveSheet()->getCell('C'.$i )->getValue();
	    $Row->brand = $Excel->getActiveSheet()->getCell('D'.$i )->getValue(); 
		$Row->descr = $Excel->getActiveSheet()->getCell('E'.$i )->getValue();
		$Row->weight = $Excel->getActiveSheet()->getCell('F'.$i )->getValue();
		$Row->napr = $Excel->getActiveSheet()->getCell('G'.$i )->getValue(); 
		$Row->height = $Excel->getActiveSheet()->getCell('H'.$i )->getValue(); 
		$Row->width = $Excel->getActiveSheet()->getCell('I'.$i )->getValue();
		$Row->length = $Excel->getActiveSheet()->getCell('J'.$i )->getValue();
		$Row->quantity = $Excel->getActiveSheet()->getCell('K'.$i )->getValue(); 
		$Row->price = $Excel->getActiveSheet()->getCell('L'.$i )->getValue();
		$Row->kom1 = $Excel->getActiveSheet()->getCell('M'.$i )->getValue();
		$Row->kom2 = $Excel->getActiveSheet()->getCell('N'.$i )->getValue();
		$Row->kom3 = $Excel->getActiveSheet()->getCell('O'.$i )->getValue();
		$Row->kom4 = $Excel->getActiveSheet()->getCell('P'.$i )->getValue();
		$Row->kom5 = $Excel->getActiveSheet()->getCell('Q'.$i )->getValue();
		$Row->kom6 = $Excel->getActiveSheet()->getCell('R'.$i )->getValue();
		$Row->kom7 = $Excel->getActiveSheet()->getCell('S'.$i )->getValue();
		$Row->kom8 = $Excel->getActiveSheet()->getCell('T'.$i )->getValue();
		$Row->photo = $Excel->getActiveSheet()->getCell('U'.$i )->getValue();
		$Row->sec = $Excel->getActiveSheet()->getCell('V'.$i )->getValue();	 	 
	    if($Row->name == null) continue;
	    $Res[] = $Row;
	}
	// convert obj to array 
	function cvf_convert_object_to_array($data) {
	    if (is_object($data)) {
	        $data = get_object_vars($data);
	    }
	    if (is_array($data)) {
	        return array_map(__FUNCTION__, $data);
	    }
	    else {
	        return $data;
	    }
	} 
	$arResult['ITEMS'] = cvf_convert_object_to_array($Res);
	
	$komp = array();
	foreach ($arResult['ITEMS'] as $key => $arItem) {
		$komp = array($arItem['kom1'],$arItem['kom2'],$arItem['kom3'],$arItem['kom4'],$arItem['kom5'],$arItem['kom6'],$arItem['kom7'],$arItem['kom8']);

		// get id section
		$arFilter = Array('IBLOCK_ID'=>2,"NAME"=>$arItem['sec']);
		$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
		while($ar_result = $db_list->GetNext())
		{
			$el = new CIBlockElement;
			$PROP = array();
			$PROP[43] = $arItem['articul'];    
			$PROP[44] = $arItem['name_eng'];
			$PROP[45] = $arItem['weight'];
			$PROP[46] = $arItem['napr'];
			$PROP[47] = $arItem['height'];
			$PROP[48] = $arItem['width'];
			$PROP[49] = $arItem['length'];
			$PROP[54] = $komp;
			$PROP[58] = $arItem['brand'];
			$arLoadProductArray = Array(
				"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => $ar_result['ID'],          // элемент лежит в корне раздела
				"IBLOCK_ID"      => 2,
				"PROPERTY_VALUES"=> $PROP,
				"CODE" => $arItem['name_eng'],
				"NAME"           => $arItem['name'],
				"ACTIVE"         => "Y",            // активен
				"PREVIEW_TEXT"   => $arItem['descr'],
				'PREVIEW_TEXT_TYPE' => 'html',
				"PRICE" => $arItem['price'],
				"PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/photo/". $arItem['photo'])
			);
			if($PRODUCT_ID = $el->Add($arLoadProductArray)){
				echo "Создан новый товар ID: ".$PRODUCT_ID.'<br>';
			}
			else{
				// echo "Error: ".$el->LAST_ERROR.'<br>';
						// update
		$arSelect = Array('ID', 'CATALOG_QUANTITY',"PROPERTY_ARTICUL");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
		$arFilter = Array("IBLOCK_ID"=>IntVal(2), "PROPERTY_ARTICUL"=>$arItem['articul']);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
		while($ob = $res->GetNext()){ 
			$arFilter = Array('IBLOCK_ID'=>2,"NAME"=>$arItem['sec']);
			$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
			while($ar_result = $db_list->GetNext())
			{
				$el = new CIBlockElement;

				$PROP = array();
				$PROP[43] = $arItem['articul'];    
				$PROP[44] = $arItem['name_eng'];
				$PROP[45] = $arItem['weight'];
				$PROP[46] = $arItem['napr'];
				$PROP[47] = $arItem['height'];
				$PROP[48] = $arItem['width'];
				$PROP[49] = $arItem['length'];
				$PROP[54] = $komp;
				$PROP[58] = $arItem['brand'];
			
				$arLoadProductArray = Array(
					"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
					"IBLOCK_SECTION_ID" => $ar_result['ID'],          // элемент лежит в корне раздела
					"IBLOCK_ID"      => 2,
					"PROPERTY_VALUES"=> $PROP,
					"CODE" => $arItem['name_eng'],
					"NAME"           => $arItem['name'],
					"ACTIVE"         => "Y",            // активен
					"PREVIEW_TEXT"   => $arItem['descr'],
					'CATALOG_QUANTITY' => $arItem['quantity'],
					'PREVIEW_TEXT_TYPE' => 'html',
					"PRICE" => $arItem['price'],
					"PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/upload/photo/". $arItem['photo'])
				);
				$res = $el->Update($ob['ID'], $arLoadProductArray);
			}
			 
			$arFields = array("PURCHASING_PRICE"=>$arItem['price'], "PURCHASING_CURRENCY"=>"RUB", 'QUANTITY' => $arItem['quantity']);// зарезервированное количество
			CCatalogProduct::Update($ob['ID'], $arFields);
			// Установим для товара  цену типа 2  
			$PRICE_TYPE_ID = 1;
			$arFields = Array(
				"PRODUCT_ID" => $ob['ID'],
				"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
				"PRICE" => $arItem['price'],
				"CURRENCY" => "RUB",
				"QUANTITY_FROM" => 0, 
	                "QUANTITY_TO" => 0 
			);
			$res = CPrice::GetList(
				array(),
				array(
					"PRODUCT_ID" => $ob['ID'],
					"CATALOG_GROUP_ID" => $PRICE_TYPE_ID
				)
			);
			if ($arr = $res->Fetch())
			{
				CPrice::Update($arr["ID"], $arFields);
			}
			else
			{
				CPrice::Add($arFields);
			} 
				echo "Обновление прошло успешно<br>"; 
		}	
			}
		}

		CCatalogProduct::add(array('ID' => $PRODUCT_ID, "PURCHASING_PRICE"=>$arItem['price'], "PURCHASING_CURRENCY"=>"RUB", 'QUANTITY' => $arItem['quantity']), false);
		// Установим для товара  цену типа 2  
		$PRICE_TYPE_ID = 1;
		$arFields = Array(
			"PRODUCT_ID" => $PRODUCT_ID,
			"CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
			"PRICE" => $arItem['price'],
			"CURRENCY" => "RUB",
			"QUANTITY_FROM" => 0, 
                "QUANTITY_TO" => 0 
		);
		$res = CPrice::GetList(
			array(),
			array(
				"PRODUCT_ID" => $PRODUCT_ID,
				"CATALOG_GROUP_ID" => $PRICE_TYPE_ID
			)
		);
		if ($arr = $res->Fetch())
		{
			CPrice::Update($arr["ID"], $arFields);
		}
		else
		{
			CPrice::Add($arFields);
		} 

 		
	}

}

?>
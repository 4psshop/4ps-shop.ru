<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление заказа");
?>
<style type="text/css">
    [data-wbl-sale-order-ajax-form] {padding: 35px 30px 35px 30px; background: #fff;}
</style>

<div data-wbl-sale-order-ajax-form>
    <?
    CModule::IncludeModule('sale');
    CModule::IncludeModule('catalog');


    $ORDER_ID = htmlspecialcharsbx($_GET['ORDER_ID']); // Id заказа
    $paymentId = htmlspecialcharsbx($_GET['PAYMENT']); // Id опталы безналом

    $order = \Bitrix\Sale\Order::load($ORDER_ID);
    if ($order)
    {
        $guestStatuses = \Bitrix\Main\Config\Option::get("sale", "allow_guest_order_view_status", "");
        $guestStatuses = (strlen($guestStatuses) > 0) ?  unserialize($guestStatuses) : array();

        /** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
        $paymentCollection = $order->getPaymentCollection();

        if ($paymentCollection)
        {
            if ($paymentId)
            {
                $data = \Bitrix\Sale\PaySystem\Manager::getIdsByPayment($paymentId);

                if ($data[1] > 0)
                    $paymentItem = $paymentCollection->getItemById($data[1]);
            }

            if ($paymentItem === null)
            {
                /** @var \Bitrix\Sale\Payment $item */
                foreach ($paymentCollection as $item)
                {
                    if (!$item->isInner() && !$item->isPaid())
                    {
                        $paymentItem = $item;
                        break;
                    }
                }
            }


            if ($paymentItem !== null)
            {
                $service = \Bitrix\Sale\PaySystem\Manager::getObjectById($paymentItem->getPaymentSystemId());

                switch ($service->getField('PAY_SYSTEM_ID'))
                {
                    case '11':
                        ?>
                        <p>
                            По данному заказу к Вас выбрана оплата наличными курьеру. Если Вы хотите оплатить онлайн - смените способ оплаты в <a href="/personal/">личном кабинете</a>.
                        </p>
                        <?
                        break;
                    case '16':
                        $APPLICATION->RestartBuffer();
						if ($service)
						{
							$context = \Bitrix\Main\Application::getInstance()->getContext();

							$result = $service->initiatePay($paymentItem, $context->getRequest());
							if (!$result->isSuccess())
							{

								echo implode('<br>', $result->getErrorMessages());
							}

							if($service->getField('ENCODING') != '')
							{
								define("BX_SALE_ENCODING", $service->getField('ENCODING'));

								AddEventHandler("main", "OnEndBufferContent", "ChangeEncoding");
								function ChangeEncoding($content)
								{
									global $APPLICATION;
									header("Content-Type: text/html; charset=".BX_SALE_ENCODING);
									$content = $APPLICATION->ConvertCharset($content, SITE_CHARSET, BX_SALE_ENCODING);
									$content = str_replace("charset=".SITE_CHARSET, "charset=".BX_SALE_ENCODING, $content);
								}
							}
						}
						die();
                        break;
                    default:
						if ($service)
						{
							$context = \Bitrix\Main\Application::getInstance()->getContext();

							$result = $service->initiatePay($paymentItem, $context->getRequest());
							if (!$result->isSuccess())
							{

								echo implode('<br>', $result->getErrorMessages());
							}

							if($service->getField('ENCODING') != '')
							{
								define("BX_SALE_ENCODING", $service->getField('ENCODING'));

								AddEventHandler("main", "OnEndBufferContent", "ChangeEncoding");
								function ChangeEncoding($content)
								{
									global $APPLICATION;
									header("Content-Type: text/html; charset=".BX_SALE_ENCODING);
									$content = $APPLICATION->ConvertCharset($content, SITE_CHARSET, BX_SALE_ENCODING);
									$content = str_replace("charset=".SITE_CHARSET, "charset=".BX_SALE_ENCODING, $content);
								}
							}
						}
                        break;
                }
            }
        }
    }

    ?>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

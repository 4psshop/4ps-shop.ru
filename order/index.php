<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек");
$APPLICATION->SetPageProperty("keywords_inner", "Blademaster, блайдмастер, блейдмастер, SSM, ССМ, точильное оборудование, клепание коньков, заточка коньков, станки для заточки, сушилки коньков, сервис, шлифовальные станции, лента для клюшек");
$APPLICATION->SetPageProperty("title", "Оформление заказа в 4 ПРО СПОРТ");
$APPLICATION->SetPageProperty("keywords", "точильное оборудование, станок, шлифовальная станция Blademaster, блайдмастер, блейдмастер, клепание коньков, заточка коньков, люверс, сушка, кронштейн для коньков, клепки для коньков, камень, пылесос, SSM, ССМ, спортивная одежда, обувь, керлинг, фигурное катание, лента для клюшек, Blue sport, Nike, Adidas, Asics, 4ps-shop, 4 про спорт");
$APPLICATION->SetPageProperty("description", "Оформление заказа в 4 ПРО СПОРТ");
$APPLICATION->SetTitle("Оформление заказа");
?><script src="https://api-maps.yandex.ru/2.1/?apikey=230d2ae9-f66c-4c16-b78f-4804174e51d8&lang=ru_RU" type="text/javascript"></script>
<?$APPLICATION->IncludeComponent('weblooter:wbl-sale-order-ajax', '', array('AJAX_MODE'=>'Y'))?>

    <script type="text/javascript">
        var wblForm = $('[data-wbl-sale-order-ajax-form]');
        function wblFormSubmit() {
            $('[data-wbl-form-send]').trigger('click');
        }
        function wblClearDeliveryInputs() {
            $('[data-delivery_props], [data-order_props], [name="FORM_RESULT[DELIVERY]"], [name="FORM_RESULT[DELIVERY_YANDEX]"]').each(function (key, val) {
                $(val).removeAttr('required');
            })
        }


        function checkFormValid(stepNum) {
            var result = true;
            $('[data-step-block='+stepNum+'] input[required], [data-step-block='+stepNum+'] textarea[required]').each(function (key, val) {
                if( $(val).val() == '' ){
                    $(val).parents('.wbl-input-row').addClass('wbl-error');
                    result = false;
                }
                else
                    $(val).parents('.wbl-input-row').removeClass('wbl-error');
            });
            if( !result )
                $('[data-step-block='+stepNum+'] .wbl-inputs-error').show();
            else
                $('[data-step-block='+stepNum+'] .wbl-inputs-error').hide();
            return result;
        }

        $(document).ready(function () {
            $(document)
                .on('change', '[name="FORM_RESULT[PERSON_TYPE]"], [name="FORM_RESULT[DELIVERY]"], [name="FORM_RESULT[PAYMENT]"]', function () {
                    wblClearDeliveryInputs();
                    wblFormSubmit();
                })
                .on('keyup', '[data-step-block=1] input[required], [data-step-block=1] textarea[required]', function () {
                    checkFormValid(1);
                })
                .on('click', '[data-step-block=1] .wbl-next', function (e) {
                    if( checkFormValid(1) )
                        return true;
                    else{
                        e.preventDefault();
                        return false;
                    }
                })
                .on('keyup', '[data-step-block=2] input[required], [data-step-block=2] textarea[required]', function () {
                    checkFormValid(2);
                })
                .on('click', '[data-step-block=2] .wbl-next', function (e) {
                    if( checkFormValid(2) )
                        return true;
                    else{
                        e.preventDefault();
                        return false;
                    }
                })
                .on('click', '[name="BACK_TO_STEP"]', function () {
                    wblClearDeliveryInputs();
                    $.each( $('[name="FORM_RESULT[DELIVERY]"], [name="FORM_RESULT[PAYMENT]"], [name^="FORM_RESULT[DELIVERY_PROPS]"]'), function (key, val) {
                        $(val).removeAttr('required');
                    } )
                })
                .on('click', '[data-step-block=3] [name="STEP"][value="make_order"]', function (e) {
                    $.each( $('[name="FORM_RESULT[DELIVERY]"], [name="FORM_RESULT[PAYMENT]"], [name^="FORM_RESULT[DELIVERY_PROPS]"]'), function (key, val) {
                        $(val).removeAttr('required');
                    } );
                    return true;
                });
        })
    </script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>